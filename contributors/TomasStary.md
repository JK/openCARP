2022-12-01

I hereby agree to the terms of the Contributor Agreement, version 1.1, with
MD5 checksum a259dbbb2f90249a5998f19b62082fcf. 

All my contributions submitted through my gitlab account @tomas.stary
are authored as part of my employment at Karlsruhe Institute of
Technology (KIT).

I furthermore declare that I am authorized by Axel Loewe (on behalf of
KIT) and able to make this agreement and sign this declaration.

Signed,

Tomáš Starý
https://git.opencarp.org/tomas.stary
