#ifndef _SF_PETSC_SOLVER_H
#define _SF_PETSC_SOLVER_H
#ifdef WITH_PETSC

#include <petscmat.h>
#include <petscksp.h>

#include "basics.h"
#include "petsc_compat.h"

#include "SF_abstract_lin_solver.h"
#include "SF_globals.h"

#include "SF_petsc_vector.h"
#include "SF_petsc_matrix.h"

//#include "mechanics.h"


namespace SF {

/**
 * @brief A class encapsulating a PETSc solver.
 *
 * @note Objects of this class should not be used or initialized directly.
 *       Instead, use virtual functions on the abstract_linera_solver base
 *       type and use the init_solver() function from numerics/SF_init.h.
 *
 * @see abstract_linear_solver
 * @see numerics/SF_init.h
 */
struct petsc_solver : public abstract_linear_solver<SF_int,SF_real>
{
  using norm_t = typename abstract_linear_solver<SF_int,SF_real>::norm_t;
  petsc_matrix* matrix           = NULL;
  bool          check_start_norm = false;

  KSP          ksp       = NULL;
  MatNullSpace nullspace = NULL;

  void operator() (abstract_vector<SF_int,SF_real> & x_, const abstract_vector<SF_int,SF_real> & b_) override
  {
    auto &x = dynamic_cast<petsc_vector &>(x_);
    auto &b = dynamic_cast<const petsc_vector &>(b_);
    assert (x.data != b.data);
    assert (ksp != NULL);

    const double solve_zero = 1e-16;
    const double NORMB = check_start_norm ? b.mag() : 1.0;

    if(NORMB > solve_zero)
    {
      KSPSolve(ksp, b.data, x.data);
      KSPGetIterationNumber(ksp, &(abstract_linear_solver<SF_int,SF_real>::niter));

      KSPConvergedReason r; KSPGetConvergedReason(ksp, &r);
      abstract_linear_solver<SF_int,SF_real>::reason = int(r);
      KSPGetResidualNorm(ksp, &(abstract_linear_solver<SF_int,SF_real>::final_residual));
    }
    else {
      abstract_linear_solver<SF_int,SF_real>::niter = 0;
      abstract_linear_solver<SF_int,SF_real>::final_residual = NORMB;
    }
  }


  void setup_solver(abstract_matrix<SF_int, SF_real>& mat, double tol, int max_it, short norm,
                    std::string name, bool has_nullspace, void* logger,
                    const char* solver_opts_file, const char* default_opts) override;

protected:
  void set_stopping_criterion(norm_t normtype, double tol, int max_it, bool verbose, void* logger) override;

  /** insert petsc solver options file
   *
   * @param default_opts_str  string with default options. used if no options file provided.
   * @param verbose  Whether to be verbose or not.
   * @param logger   The file descriptor we write to if we are verbose.
   *
   * @returns  0, if successful
   *          -1, otherwise, i.e. options file not found
   */
  int insert_solver_opts(const char* default_ops_str, bool verbose, opencarp::FILE_SPEC logger);
};

/**
 * @brief A class encapsulating a PETSc nonlinear solver.
 *
 * @note Objects of this class should not be used or initialized directly.
 *       Instead, use virtual functions on the abstract_nonlinear_solver base
 *       type and use the init_nl_solver() function from numerics/SF_init.h.
 *
 * @see abstract_nonlinear_solver
 * @see numerics/SF_init.h
 */
struct petsc_nl_solver : public abstract_nonlinear_solver<SF_int, SF_real>
{
  using norm_t = typename abstract_nonlinear_solver<SF_int, SF_real>::norm_t;
  petsc_matrix * matrix           = NULL;
  bool                check_start_norm = false;

  SNES         snes      = NULL;
  KSP          ksp       = NULL;
  PC           pc        = NULL;
  MatNullSpace nullspace = NULL;

  void operator() (abstract_vector<SF_int, SF_real> & x_, const abstract_vector<SF_int, SF_real> & b_) override
  {
    
    auto &x = dynamic_cast<petsc_vector &>(x_);
    auto &b = dynamic_cast<const petsc_vector &>(b_);
    assert (x.data != b.data);
    assert (this->ksp != NULL);
    assert (this->snes != NULL);
    
    PetscErrorCode ierr;
    
    PetscInt snesIts = 0;
    PetscInt kspIts = 0;
    
    SNESConvergedReason snesReason;
    KSPConvergedReason kspReason;
    
    ierr = SNESSolve(this->snes, NULL, x.data);
  
    SNESGetIterationNumber(this->snes, &snesIts);
    KSPGetTotalIterations(this->ksp, &kspIts);
    SNESGetConvergedReason(this->snes, &snesReason);
    KSPGetConvergedReason(this->ksp, &kspReason);
    
    bool printsnes = false;
    
    if(printsnes)
    {
      std::cout<<"\n Snes iterations: " << snesIts << "\n Ksp iterations: " << kspIts << "\n";
      std::cout<<"\n SNES Converged reason: " << snesReason << "\n";
      std::cout<<"\n KSP Converged reason: " << kspReason << "\n";
    }
  }

  void setup_solver(abstract_vector<SF_int, SF_real> &residuum, abstract_matrix<SF_int, SF_real>& mat, double tol, int max_it, short norm, const char* name, bool has_nullspace, void* logger, const char  *solver_opts_file, const char* default_opts, PetscErrorCode function(SNES, Vec, Vec, void*), PetscErrorCode jacobian(SNES, Vec, Mat, Mat, void*), void * solver_pointer) override;

protected:
  void set_stopping_criterion(norm_t normtype, double tol, int max_it, bool verbose, void* logger) override;

  /** insert petsc solver options file
   *
   * @param default_opts_str  string with default options. used if no options file provided.
   * @param verbose  Whether to be verbose or not.
   * @param logger   The file descriptor we write to if we are verbose.
   *
   * @returns  0, if successful
   *          -1, otherwise, i.e. options file not found
   */
  int insert_solver_opts(const char* default_ops_str, bool verbose, opencarp::FILE_SPEC logger);
};


void init_solver_petsc(abstract_linear_solver<SF_int,SF_real>** sol);

void init_nl_solver_petsc(abstract_nonlinear_solver<SF_int,SF_real>** sol);

}  // namespace SF

#endif // WITH_PETSC
#endif // _SF_PETSC_SOLVER_H
