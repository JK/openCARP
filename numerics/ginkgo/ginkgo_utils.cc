#include "ginkgo_utils.h"
#include "basics.h"

#include <map>

using shared_exec = std::shared_ptr<const gko::Executor>;

shared_exec read_exec()
{
  std::map<std::string, std::function<shared_exec()>>
      exec_map{
          {"ref", [] { return gko::ReferenceExecutor::create(); }},
          {"omp", [] { return gko::OmpExecutor::create(); }},
          {"cuda", [] { return gko::CudaExecutor::create(param_globals::device_id, gko::OmpExecutor::create(),
                                                  true);
              }},
          {"hip", [] { return gko::HipExecutor::create(param_globals::device_id, gko::OmpExecutor::create(),
                                                  true);
              }},
          {"dpcpp", [] { return gko::DpcppExecutor::create(param_globals::device_id,
                                                  gko::OmpExecutor::create());
              }}};
  return exec_map.at(param_globals::ginkgo_exec)();
}

shared_exec& get_global_exec()
{
  static shared_exec global_exec = read_exec();
  return global_exec;
}
