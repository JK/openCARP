#include "SF_ginkgo_vector.h"
#include "basics.h"

namespace SF {


#define OPENCARP_DECLARE_GINKGO_VECTOR(IndexType, ValueType) \
class ginkgo_vector<IndexType, ValueType>
OPENCARP_INSTANTIATE_FOR_EACH_VALUE_AND_INDEX_TYPE(OPENCARP_DECLARE_GINKGO_VECTOR);


template<class T, class S>
void init_vector_ginkgo(abstract_vector<T,S>** vec)
{
  *vec = new ginkgo_vector<T,S>();
}

#define OPENCARP_DECLARE_INIT_VECTOR(IndexType, ValueType) \
void init_vector_ginkgo<IndexType, ValueType>(abstract_vector<IndexType,ValueType>**)
OPENCARP_INSTANTIATE_FOR_EACH_VALUE_AND_INDEX_TYPE(OPENCARP_DECLARE_INIT_VECTOR);


} // namespace SF
