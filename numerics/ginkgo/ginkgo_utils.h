#ifndef OPENCARP_GINKGO_UTILS_H_
#define OPENCARP_GINKGO_UTILS_H_

#ifdef WITH_GINKGO
#include <ginkgo/ginkgo.hpp>

std::shared_ptr<const gko::Executor>& get_global_exec();
#endif // WITH_GINKGO

#endif // OPENCARP_GINKGO_UTILS_H_
