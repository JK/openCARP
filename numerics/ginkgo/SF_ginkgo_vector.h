// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#ifndef _SF_GINKGO_VECTOR_H
#define _SF_GINKGO_VECTOR_H

#include "SF_abstract_vector.h"

#ifdef WITH_GINKGO

#include <memory>

#include <ginkgo/ginkgo.hpp>
#include "ginkgo_utils.h"

#include "SF_globals.h"


namespace SF {


/**
* @brief A class encapsulating a Ginkgo vector.
*
* @note Objects of this class should not be used or initialized directly.
*       Instead, use virtual functions on the abstract_vector base type and
*       use the init_vector() functions from numerics/SF_init.h.
*
* @see abstract_vector
* @see numerics/SF_init.h
*/
template<class T, class S>
class ginkgo_vector: public abstract_vector<T,S>
{
  using typename abstract_vector<T,S>::ltype;

  public:

  using gko_vec = gko::matrix::Dense<S>;
  std::shared_ptr<gko_vec> data   = NULL;    ///< the Ginkgo vector pointer
  mutable std::shared_ptr<gko_vec> host_data = NULL; ///< a pointer guaranteed to be on CPU

  ginkgo_vector()
  {}

  ~ginkgo_vector() override
  {}

  ginkgo_vector(const meshdata<mesh_int_t,mesh_real_t> & imesh,
                int idpn, ltype inp_layout) {
    init(imesh, idpn, inp_layout);
  }

  ginkgo_vector(int igsize, int ilsize, int idpn, ltype ilayout) {
    init(igsize, ilsize, idpn, ilayout);
  }

  ginkgo_vector(const ginkgo_vector<T,S> & vec) {
    init(vec);
  }

  /**
   * Default intiialization to empty vectors
   */
  inline void init()
  {
    data = gko_vec::create(get_global_exec());
    host_data = gko_vec::create(get_global_exec()->get_master());
  }

  inline void init(const meshdata<mesh_int_t,mesh_real_t> & imesh,
                   int idpn, ltype inp_layout) override
  {
    int N = 0, n = 0;
    std::tie(N, n) = abstract_vector<T,S>::init_common(imesh, idpn, inp_layout);
    data = gko_vec::create(get_global_exec(), gko::dim<2>{N, 1});
    host_data = gko_vec::create(get_global_exec()->get_master(), gko::dim<2>{N, 1});
    set(0.0);
  }

  inline void init(int igsize, int ilsize, int idpn = 1, ltype ilayout = abstract_vector<T,S>::unset) override
  {
    data = gko_vec::create(get_global_exec(), gko::dim<2>{igsize, 1});
    host_data = gko_vec::create(get_global_exec()->get_master(), gko::dim<2>{igsize, 1});

    this->mesh = NULL;
    this->dpn = idpn;
    this->layout = ilayout;
    set(0.0);
  }

  inline void init(const abstract_vector<T,S> & vec_) override
  {
    auto &vec = dynamic_cast<const ginkgo_vector<T,S> &>(vec_);
    data = gko_vec::create_with_config_of(vec.data.get());
    host_data = gko_vec::create(get_global_exec()->get_master(), data->get_size());

    this->mesh   = vec.mesh;
    this->dpn    = vec.dpn;
    this->layout = vec.layout;
    set(0.0);
  }

  inline void set(const vector<T>& idx, const vector<S>& vals, const bool additive = false) override
  {
    host_data->copy_from(data.get());
    for (auto i = 0; i < idx.size(); i++) {
      host_data->at(idx.data()[i], 0) = additive ? host_data->at(idx.data()[i], 0) + vals.data()[i] : vals.data()[i];
    }
    data->copy_from(host_data.get());
  }

  inline void set(const vector<T>& idx, const S val) override
  {
    vector<S> vals(idx.size(), val);
    set(idx, vals);
  }

  inline void set(const S val) override
  {
    data->fill(val);
  }

  inline void set(const T idx, const S val) override
  {
    host_data->copy_from(data.get());
    host_data->at(idx, 0) = val;
    data->copy_from(host_data.get());
  }

  inline void get(const vector<T> & idx, S *out) override
  {
    host_data->copy_from(data.get());
    for (T i = 0; i < idx.size(); i++) {
      out[i] = host_data->at(idx[i], 0);
    }
  }

  S get(const T idx) override
  {
    host_data->copy_from(data.get());
    return host_data->at(idx, 0);
  }

  inline void operator *= (const S sca) override
  {
    const auto scale_op = gko::initialize<gko_vec>({sca}, get_global_exec());
    data->scale(scale_op.get());
  }

  inline void operator /= (const S sca) override
  {
    (*this) *= 1.0 / sca;
  }

  inline void operator *= (const abstract_vector<T,S> & vec_) override
  {
    auto &vec = dynamic_cast<const ginkgo_vector<T,S> &>(vec_);
    auto diag_array = gko::Array<S>::view(get_global_exec(), vec.gsize(), vec.data->get_values());
    auto diag = gko::matrix::Diagonal<S>::create(get_global_exec(), vec.gsize(), diag_array);
    diag->apply(data.get(), data.get());
  }

  void add_scaled(const abstract_vector<T,S> & vec_, S k) override
  {
    auto &vec = dynamic_cast<const ginkgo_vector<T,S> &>(vec_);
    T mysize = this->lsize();

    assert(mysize == vec.lsize());

    const auto scale_op = gko::initialize<gko_vec>({k}, get_global_exec());
    data->add_scaled(scale_op.get(), vec.data.get());
  }

  inline void operator += (const abstract_vector<T,S> & vec_) override
  {
    auto &vec = dynamic_cast<const ginkgo_vector<T,S> &>(vec_);
    this->add_scaled(vec, 1.0);
  }

  inline void operator -= (const abstract_vector<T,S> & vec_) override
  {
    auto &vec = dynamic_cast<const ginkgo_vector<T,S> &>(vec_);
    this->add_scaled(vec, -1.0);
  }

  /// scalar add, equivalent to VecShift
  inline void operator += (S c) override
  {
    auto add_op = gko_vec::create(get_global_exec(), gko::dim<2>{this->lsize(), 1});
    add_op->fill(c);
    const auto scale_op = gko::initialize<gko_vec>({1.0}, get_global_exec());
    data->add_scaled(scale_op.get(), add_op.get());
  }

  /// deep copy vector
  inline void operator= (const vector<S> & rhs) override
  {
    assert(this->lsize() == rhs.size());

    const S *r = rhs.data();

    for(size_t i=0; i<rhs.size(); i++) {
      host_data->at(i, 0) = r[i];
    }
    data->copy_from(host_data.get());
  }

  inline void operator=(const abstract_vector<T, S>& rhs) override
  {
    this->deep_copy(rhs);
  }

  inline void shallow_copy(const abstract_vector<T,S> & v_) override
  {
    auto &v = dynamic_cast<const ginkgo_vector<T,S> &>(v_);
    if (!data) init();
    this->data   = v.data;
    this->mesh   = v.mesh;
    this->dpn    = v.dpn;
    this->layout = v.layout;
  }

  inline void deep_copy(const abstract_vector<T,S> & vec_) override
  {
    auto &vec = dynamic_cast<const ginkgo_vector<T,S> &>(vec_);

    this->mesh   = vec.mesh;
    this->dpn    = vec.dpn;
    this->layout = vec.layout;
    this->data->copy_from(vec.data.get());
  }


  inline void overshadow(const abstract_vector<T,S> & sub, bool member, int offset, int sz, bool share) override
  {
    auto &vec = dynamic_cast<const ginkgo_vector<T,S> &>(sub);
    std::runtime_error("The overshadow feature is not implemented with the Ginkgo backend.");
    // TODO: not implemented yet
  }

  inline T lsize() const override
  {
    return data != NULL ? data->get_size()[0] : 0;
  }

  inline T gsize() const override
  {
    return data != NULL ? data->get_size()[0] : 0;
  }

  inline void get_ownership_range(T & start, T & stop) const override
  {
    start = 0;
    stop = data != NULL ? data->get_size()[0] : 0;
  }

  inline S* ptr() override
  {
    S *p;
    if (data != NULL) {
      host_data->copy_from(data.get());
      p = host_data->get_values();
    }
    return p;
  }

  inline const S* const_ptr() const override
  {
    const S* p;
    if (data != NULL) {
      host_data->copy_from(data.get());
      p = host_data->get_const_values();
    }
    return p;
  }

  inline void release_ptr(S*& p) override
  {
    if (data) {
      data->copy_from(host_data.get());
    }
  }

  inline void const_release_ptr(const S*& p) const override {}

  inline S mag() const override
  {
    S mag = 0.;
    if (this->lsize() > 0) {
      auto result = gko::initialize<gko_vec>({0.0}, get_global_exec());
      data->compute_norm2(result.get());
      auto host_result = gko_vec::create(get_global_exec()->get_master());
      host_result->copy_from(result.get());
      mag = host_result->at(0,0);
    }
    return mag;
  }

  inline S sum() const override
  {
    S sum = 0.;
    if (this->lsize() > 0) {
      auto one_vec = gko_vec::create(get_global_exec(), gko::dim<2>{this->lsize(), 1});
      one_vec->fill(1.0);
      auto result = gko::initialize<gko_vec>({0.0}, get_global_exec());
      data->compute_dot(one_vec.get(), result.get());
      auto host_result = gko_vec::create(get_global_exec()->get_master());
      host_result->copy_from(result.get());
      sum = host_result->at(0,0);
    }
    return sum;
  }

  inline S dot(const abstract_vector<T,S> & v_) const override
  {
    auto &v = dynamic_cast<const ginkgo_vector<T,S> &>(v_);
    auto result = gko::initialize<gko_vec>({0.0}, get_global_exec());
    data->compute_dot(v.data.get(), result.get());
    auto host_result = gko_vec::create(get_global_exec()->get_master());
    host_result->copy_from(result.get());
    return host_result->at(0,0);
  }

  inline S min() const override
  {
    host_data->copy_from(data.get());
    auto vals = host_data->get_values();
    auto min = std::min_element(vals, vals + this->lsize());
    return *min;
  }

  bool is_init() const override
  {
    auto ret = this->data != NULL;
    return ret;
  }

  inline std::string to_string() const override
  {
    std::string result = "ginkgo_vector (" + std::to_string(lsize()) + ") [";

    const S* p = const_ptr();

    for (auto i = 0; i < lsize(); i++) {
      if (i > 0) result += ", ";
      result += std::to_string(p[i]);
    }
    result += "]";

    return result;
  }

  inline bool equals(const abstract_vector<T,S> & rhs_) const override
  {
    auto &rhs = dynamic_cast<const ginkgo_vector<T,S> &>(rhs_);
    auto tmp = gko_vec::create(get_global_exec());
    tmp->copy_from(rhs.data.get());
    auto scalar = gko::initialize<gko_vec>({-1.0}, get_global_exec());
    tmp->add_scaled(scalar.get(), data.get());
    tmp->compute_norm2(scalar.get());
    auto host_result = gko_vec::create(get_global_exec()->get_master());
    host_result->copy_from(scalar.get());

    return fabs(host_result->at(0,0)) < 0.0001;
  }

  inline void finish_assembly() override
  {
    data->copy_from(host_data.get());
  }

  // TODO: This is very slow, every time we want to apply the scattering Ginkgo
  // needs to copy this->data and out.data to host, then copy back the updated
  // out.data to device
  inline void forward(abstract_vector<T, S>& out, scattering& sc, bool add = false) override
  {
    auto pin  = this->const_ptr();
    auto pout = out.ptr();
    for (int i = 0; i < sc.idx_a.size(); i++) {
      pout[sc.idx_b[i]] = add ? pout[sc.idx_b[i]] + pin[sc.idx_a[i]] : pin[sc.idx_a[i]];
    }
    out.release_ptr(pout);
    this->const_release_ptr(pin);
  }

  inline void backward(abstract_vector<T, S>& out, scattering& sc, bool add = false) override
  {
    auto pin  = this->const_ptr();
    auto pout = out.ptr();
    for (int i = 0; i < sc.idx_a.size(); i++) {
      pout[sc.idx_a[i]] = add ? pout[sc.idx_a[i]] + pin[sc.idx_b[i]] : pin[sc.idx_b[i]];
    }
    out.release_ptr(pout);
    this->const_release_ptr(pin);
  }

  inline void apply_scattering(scattering &sc, bool fwd) override {}
};


template<class T, class S>
void init_vector_ginkgo(abstract_vector<T,S>** vec);


} // namespace SF

#endif // WITH_GINKGO
#endif // _SF_GINKGO_VECTOR_H
