// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#ifndef _SF_GINKGO_MATRIX_H
#define _SF_GINKGO_MATRIX_H

#include "SF_abstract_matrix.h"

#ifdef WITH_GINKGO

#include <cassert>

#include <ginkgo/ginkgo.hpp>
#include "ginkgo_utils.h"

#include "SF_globals.h"
#include "SF_ginkgo_vector.h"


namespace SF {

/**
 * @brief A class encapsulating a Ginkgo matrix.
 *
 * @note Objects of this class should not be used or initialized directly.
 *       Instead, use virtual functions on the abstract_matrix base type and
 *       use the init_matrix() function from numerics/SF_init.h.
 *
 * @see abstract_matrix
 * @see numerics/SF_init.h
 */
template<class T, class S>
class ginkgo_matrix : public abstract_matrix<T,S>
{
  public:

  using mtx = gko::matrix::Csr<S, T>;
  using gko_vec = gko::matrix::Dense<S>;

  gko::matrix_assembly_data<S, T> raw_data;

  /// the ginkgo matrix object
  mutable std::shared_ptr<mtx> data;

  /** Default constructor */
  ginkgo_matrix() : abstract_matrix<T,S>::abstract_matrix(), data{}, raw_data{{0,0}}
  {}

  // destructor
  ~ginkgo_matrix() override
  {}

  inline void init(T iNRows, T iNCols, T ilrows, T ilcols,
                   T loc_offset, T mxent) override
  {
    // init member vars
    abstract_matrix<T, S>::init(iNRows, iNCols, ilrows, ilcols, loc_offset, mxent);

    // TODO: Ginkgo does not support distributed matrices yet.
    if (this->start != 0 || this->stop != this->NRows) {
      throw std::runtime_error("Ginkgo does not support distributed matrices yet.");
    }

    data = mtx::create(get_global_exec(), gko::dim<2>(this->NRows, this->NCols));

    raw_data = gko::matrix_assembly_data<S, T>(gko::dim<2>(this->NRows, this->NCols));
  }

  inline void zero() override
  {
    data->scale(gko::initialize<gko::matrix::Dense<S>>({gko::zero<S>()}, get_global_exec()).get());
    raw_data = gko::matrix_assembly_data<S, T>(gko::dim<2>(this->NRows, this->NCols));
  }

  inline void mult(const abstract_vector<T,S> & x_, abstract_vector<T,S> & b_) const override
  {
    auto &x = dynamic_cast<const ginkgo_vector<T,S> &>(x_);
    auto &b = dynamic_cast<ginkgo_vector<T,S> &>(b_);
    data->apply(x.data.get(), b.data.get());
  }

  inline void mult_LR(const abstract_vector<T,S> & L_, const abstract_vector<T,S> & R_) override
  {
    auto &L = dynamic_cast<const ginkgo_vector<T,S> &>(L_);
    auto &R = dynamic_cast<const ginkgo_vector<T,S> &>(R_);
    auto temp = mtx::create(get_global_exec(), data->get_size());
    if (L.is_init()) {
      if (L.gsize() == data->get_size()[0]) {
        auto L_array = gko::Array<S>::view(get_global_exec(), L.gsize(), L.data->get_values());
        auto L_diag  = gko::matrix::Diagonal<S>::create(get_global_exec(), L.gsize(), L_array);
        L_diag->apply(data.get(), temp.get());
      }
    } else {
      temp->copy_from(data.get());
    }
    if (R.is_init()) {
      if (R.gsize() == data->get_size()[1]) {
        auto R_array = gko::Array<S>::view(get_global_exec(), R.gsize(), R.data->get_values());
        auto R_diag  = gko::matrix::Diagonal<S>::create(get_global_exec(), R.gsize(), R_array);
        R_diag->rapply(temp.get(), data.get());
      }
    } else {
      data->copy_from(temp.get());
    }
  }

  inline void diag_add(const abstract_vector<T,S> & diag_) override
  {
    auto &diag = dynamic_cast<const ginkgo_vector<T,S> &>(diag_);
    auto diag_array = gko::Array<S>::view(get_global_exec(), diag.gsize(), diag.data->get_values());
    auto diag_matrix = gko::matrix::Diagonal<S>::create(get_global_exec(), diag.gsize(), diag_array);
    auto I = gko::matrix::Identity<S>::create(get_global_exec(), data->get_size()[0]);
    auto one_op = gko::initialize<gko::matrix::Dense<S>>({gko::one<S>()}, get_global_exec());
    auto diag_csr = mtx::create(get_global_exec());
    diag_matrix->move_to(diag_csr.get());

    diag_csr->apply(one_op.get(), I.get(), one_op.get(), data.get());
  }

  inline void get_diagonal(abstract_vector<T,S> & vec_) const override
  {
    auto &vec = dynamic_cast<ginkgo_vector<T,S> &>(vec_);
    auto size = data->get_size()[0];
    auto diag = data->extract_diagonal();
    auto tmp_array = gko::Array<S>::view(get_global_exec(), size, diag->get_values());
    auto tmp = gko::matrix::Dense<S>::create(get_global_exec(), gko::dim<2>{size, 1}, tmp_array, 1);

    tmp->move_to(vec.data.get());
  }

  inline void finish_assembly() override
  {
    data->read(raw_data);
  }

  inline void scale(S s) override
  {
    const auto s_op = gko::initialize<gko::matrix::Dense<S>>({s}, get_global_exec());
    data->scale(s_op.get());
  }

  inline void add_scaled_matrix(const abstract_matrix<T,S> & A_, const S s,
                                const bool same_nnz) override
  {
    auto &A = dynamic_cast<const ginkgo_matrix<T,S> &>(A_);
    auto I = gko::matrix::Identity<S>::create(get_global_exec(), data->get_size()[0]);
    auto s_op = gko::initialize<gko::matrix::Dense<S>>({s}, get_global_exec());
    auto one_op = gko::initialize<gko::matrix::Dense<S>>({gko::one<S>()}, get_global_exec());
    A.data->apply(s_op.get(), I.get(), one_op.get(), data.get());
  }

  inline void duplicate (const abstract_matrix<T,S>& M_) override
  {
    auto &M = dynamic_cast<const ginkgo_matrix<T,S> &>(M_);

    this->NRows   = M.NRows;
    this->NCols   = M.NCols;
    this->row_dpn = M.row_dpn;
    this->col_dpn = M.col_dpn;
    this->lsize   = M.lsize;
    this->start   = M.start;
    this->stop    = M.stop;
    this->mesh    = M.mesh;

    data = gko::clone(M.data);
  }

  // TODO: this is slow!
  inline void set_values(const vector<T> & row_idx, const vector<T> & col_idx,
                         const vector<S> & vals, bool add) override
  {
    assert(row_idx.size() == col_idx.size() == vals.size());

    for (auto i = 0; i < row_idx.size(); i++) {
      if (add) {
        raw_data.add_value(row_idx.data()[i], col_idx.data()[i], vals.data()[i]);
      } else {
        raw_data.set_value(row_idx.data()[i], col_idx.data()[i], vals.data()[i]);
      }
    }
  }

  // TODO: this is slow!
  inline void set_values(const vector<T>& row_idx, const vector<T>& col_idx,
                         const S* vals, bool add) override
  {
    const auto n = row_idx.size();
    const auto m = col_idx.size();
    assert(n == m);

    for (auto i = 0; i < n; i++) {
      for (auto j = 0; j < m; j++) {
        if (add) {
          raw_data.add_value(row_idx.data()[i], col_idx.data()[j], vals[i * m + j]);
        } else {
          raw_data.set_value(row_idx.data()[i], col_idx.data()[j], vals[i * m + j]);
        }
      }
    }
  }

  inline void set_value(T row_idx, T col_idx, S val, bool add) override
  {
    if (add) {
      raw_data.add_value(row_idx, col_idx, val);
    } else {
      raw_data.set_value(row_idx, col_idx, val);
    }
  }

  // TODO: this is slow!
  inline S get_value(T row_idx, T col_idx) const override
  {
    const T *rows = data->get_const_row_ptrs();
    const T row = rows[row_idx];
    const T next_row = rows[row_idx + 1];

    const T *cols = data->get_const_col_idxs();

    const S *values = data->get_const_values();

    for (int i = row; i < next_row; i++) {
        if (cols[i] == col_idx) {
            return values[i];
        }
    }
    return 0;
  }

  inline void write(const char* filename) const override
  {
    auto outfile = std::ofstream(filename);
    gko::write(outfile, lend(data), gko::layout_type::coordinate);
  }
};


template<class T, class S>
void init_matrix_ginkgo(abstract_matrix<T,S>** mat);


} // namespace SF

#endif // WITH_GINKGO
#endif // _SF_GINKGO_MATRIX_H
