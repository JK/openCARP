#ifndef _TIMERS_H
#define _TIMERS_H

#include <string>

#include "basics.h"

namespace opencarp {

//! for analysis of the \#iterations to solve CG
struct lin_solver_stats {
  int    min          = INT_MAX; //!< minimum \#interations
  int    max          = 0;     //!< maximum \#iterations
  int    tot          = 0;     //!< total \#
  int    last_tot     = 0;     //!< previous total \#
  int    iter         = 0;     //!< its previous solver step
  int    solves       = 0;     //!< \#solutions performed
  int    totsolves    = 0;     //!< total \# of solutions
  double slvtime      = 0.0;   //!< total solver time
  double lastSlvtime  = 0.0;   //!< total solver time
  FILE_SPEC  logger   = NULL;  //!< file in which to write stats

  ~lin_solver_stats()
  {
    f_close(logger);
  }

  void init_logger(const char* filename);
  void update_iter(const int curiter);
  void log_stats(double tm, bool cflg);
};

//! for analysis of the \#iterations to solve CG
struct generic_timing_stats {
  int    calls          = 0;     //!< \# calls for this interval, this is incremented externally
  int    tot_calls      = 0;     //!< total \# calls
  double tot_time       = 0.0;   //!< total time, this is incremented externally
  double last_tot_time  = 0.0;   //!< last total time

  FILE_SPEC  logger   = NULL;  //!< file in which to write stats

  ~generic_timing_stats()
  {
    f_close(logger);
  }

  void init_logger(const char* filename);
  void log_stats(double tm, bool cflg);
};


} // namespace opencarp


#endif // _TIMERS_H
