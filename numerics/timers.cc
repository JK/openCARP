#include "timers.h"

#include <string>


namespace opencarp {


void lin_solver_stats::init_logger(const char* filename)
{
  logger = f_open(filename, "w");

  const char* h1 = "          ----- ----- -------  -----  ----- | ---- ----- --------- --------- --------- |";
  const char* h2 = "          mn it mx it  avg it    its  ttits |   ss    ts        st      stps     stpit |";

  if(logger == NULL)
    log_msg(NULL, 3, 0,"%s error: Could not open file %s in %s. Turning off logging.\n",
            __func__, filename);
  else {
    log_msg(logger, 0, 0, "%s", h1);
    log_msg(logger, 0, 0, "%s", h2);
  }
}

void lin_solver_stats::log_stats(double tm, bool cflg)
{
  if(!this->logger) return;

  // make sure this->solves is > 0
  if(!this->solves) this->solves++;

  char itbuf[256];
  char stbuf[64];

  // iterations in this period
  float its = (this->tot-this->last_tot);

  // total solver time spent in this period
  float tstm    = this->slvtime-this->lastSlvtime;

  // solver time per solve
  float stm     = tstm/this->solves;

  // solver time per iteration
  float itm     = its?tstm/its:0.;

  if(!its)
    this->min = 0;
  this->totsolves  += this->solves;

  snprintf(itbuf, sizeof itbuf, "%5d %5d %7.1f %6d %6d",
          this->min, this->max, (float)(its/this->solves),
          this->tot - this->last_tot, this->tot );
  snprintf(stbuf, sizeof stbuf, "%4d %5d %9.4f %9.4f %9.4f",this->solves,this->totsolves,
                                                 (float)tstm,stm,itm);

  unsigned char flag = cflg? ECHO : 0;
  log_msg(this->logger, 0, flag|FLUSH|NONL, "%9.3f %s | %s |\n", tm, itbuf, stbuf);

  this->min         = INT_MAX;
  this->max         = 0;
  this->last_tot    = this->tot;
  this->solves      = 0;
  this->lastSlvtime = this->slvtime;
}

void lin_solver_stats::update_iter(const int curiter)
{
  if (curiter > max) max = curiter;
  if (curiter < min) min = curiter;
  tot += curiter;
  solves++;
}

void generic_timing_stats::init_logger(const char* filename)
{
  logger = f_open(filename, "w");

  const char* h1 = "          ----- ----- | --------- --------- |";
  const char* h2 = "            cls ttcls |      time  tot time |";

  if(logger == NULL)
    log_msg(NULL, 3, 0,"%s error: Could not open file %s in %s. Turning off logging.\n",
            __func__, filename);
  else {
    log_msg(logger, 0, 0, "%s", h1);
    log_msg(logger, 0, 0, "%s", h2);
  }
}

void generic_timing_stats::log_stats(double tm, bool cflg)
{
  if(!this->logger) return;

  char cbuf[256];
  char tbuf[256];

  // time spent in this period
  float ctm    = this->tot_time - this->last_tot_time;
  this->tot_calls     += this->calls;

  snprintf(cbuf, sizeof cbuf, "%5d %5d", this->calls, this->tot_calls);
  snprintf(tbuf, sizeof tbuf, "%9.4f %9.4f", ctm, this->tot_time);

  unsigned char flag = cflg? ECHO : 0;
  log_msg(this->logger, 0, flag|FLUSH|NONL, "%9.3f %s | %s |\n", tm, cbuf, tbuf);

  this->last_tot_time = this->tot_time;
  this->calls = 0;
}


} // namespace opencarp
