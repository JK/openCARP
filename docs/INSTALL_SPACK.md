## Installation with Spack

Spack is a package manager for supercomputers, Linux and MacOS. It is designed to make the installation of scientific software easier.

### Setting up Spack

To get Spack, clone its repository by entering the following commands in your terminal:
```
git clone https://github.com/spack/spack.git
cd spack
```

Then set up your environment by sourcing the Spack setup script:
```
. ./share/spack/setup-env.sh
```
If you are using another shell as bash/zsh/sh, please refer to the [Spack documentation to get the appropriate command](https://spack.readthedocs.io/en/latest/getting_started.html#shell-support)

Please also note that Spack has some [minimum system requirements](https://spack.readthedocs.io/en/latest/getting_started.html#system-prerequisites), which are assumed to be present on the machine where Spack is run.

You can now use the Spack commands.

### Install the openCARP Spack package

In order to install openCARP with carputils and meshtool, you can use the following command in the shell:
```
spack install opencarp +carputils +meshtool
```

If the compilers you wish to use are loaded from a module (typical situation on clusters), it is likely that you have to do a modification in the Spack configuration in order to be able to install packages: open the file located at `$HOME/.spack/linux/compilers.yaml` (if you are on Linux) and add the name of the module in the `modules` field of your compiler. Example of a resulting file:
```
compilers:
- compiler:
    spec: gcc@10.2.0
    paths:
        cc: /opt/bwhpc/common/compiler/gnu/10.2.0/bin/gcc
        cxx: /opt/bwhpc/common/compiler/gnu/10.2.0/bin/g++
        f77: /opt/bwhpc/common/compiler/gnu/10.2.0/bin/gfortran
        fc: /opt/bwhpc/common/compiler/gnu/10.2.0/bin/gfortran
    flags:  {}
    operating_system: rhel8
    target: x86_64
    modules: [compiler/gnu/10.2]
```

### Using openCARP

Once openCARP is installed, you can load it as it would be done for a module:
```
spack load opencarp
```
openCARP is now installed and ready to be used.

### Using external packages with Spack

By default, Spack installs all the openCARP dependencies, even if some of them are already installed on the user's system.
In order to use external packages in Spack, you can use one of two ways:
1. Rely on the `spack external` command, e.g., `spack external find cmake`.
If `cmake` is found, it will be added to the list of external packages that can be used by Spack.
2. Modify `~/.spack/packages.yaml` or `$SPACK/etc/spack/packages.yaml` as in the following example:
```
packages:
  openmpi:
    externals:
    - spec: "openmpi@1.4.3%gcc@4.4.7 arch=linux-debian7-x86_64"
      prefix: /opt/openmpi-1.4.3
    - spec: "openmpi@1.4.3%gcc@4.4.7 arch=linux-debian7-x86_64+debug"
      prefix: /opt/openmpi-1.4.3-debug
  cmake:
    externals:
    - spec: cmake@3.7.2
      modules:
      - CMake/3.7.2
    buildable: False
  ```

For more information about Spack, see the [documentation](https://spack.readthedocs.io/).

