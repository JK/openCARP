## Build openCARP with the MLIR code generator

The Multi-Level Intermediate Representation [(MLIR)](https://mlir.llvm.org/) from the [LLVM](https://llvm.org/) compiler infrastructure can be used to generate optimized CPU and GPU code for the ionic models of the LIMPET library. At present, with the MLIR code generator, one can generate vectorized (with openMP) CPU and single GPU CUDA (NVIDIA GPUs) or ROCm (AMD GPUs) code. The steps to use MLIR as a code generator are detailed below.
Compatibility between openCARP and LLVM/MLIR is done through LLVM Tags, i.e., updates to openCARP is based upon tagged versions of LLVM. For now, the latest tagged LLVM version supported by openCARP is [15.0.2](https://github.com/llvm/llvm-project/tree/llvmorg-15.0.2).

Before trying to compile this version, make sure that your environment is correctly set up by compiling the baseline openCARP, using the instructions found in [BUILD.md](https://git.opencarp.org/openCARP/openCARP/-/blob/master/docs/BUILD.md)

**Building MLIR and building openCARP both require `cmake` version 3.19 at least**. Before going any further, if your distribution does not provide a recent version (try `cmake --version`), you can get and install the latest version from [here](https://github.com/Kitware/CMake); or you can find a precompiled binary of version 3.23.2 for Linux/x86_64 [here](https://github.com/Kitware/CMake/releases/download/v3.23.2/cmake-3.23.2-linux-x86_64.tar.gz): uncompress the archive and add the `bin/` subfolder to your `PATH`.

### Build LLVM/MLIR

Build MLIR from the [LLVM](https://github.com/llvm/llvm-project) git repository. Clone LLVM tag [15.0.2](https://github.com/llvm/llvm-project/tree/llvmorg-15.0.2), then build MLIR, Clang and openMP project from the [instructions](https://mlir.llvm.org/getting_started/).

- The flag `MLIR_ENABLE_BINDINGS_PYTHON` should be enabled (to use python bindings).

For example, you can build LLVM/MLIR as below:
```
git clone https://github.com/llvm/llvm-project.git && cd llvm-project
git checkout llvmorg-15.0.2
mkdir build && cd build
cmake -G Ninja ../llvm -DLLVM_ENABLE_PROJECTS="clang;mlir;openmp" -DLLVM_BUILD_EXAMPLES=OFF -DLLVM_TARGETS_TO_BUILD="X86;NVPTX;AMDGPU" \
 -DCMAKE_BUILD_TYPE=Release -DLLVM_ENABLE_ASSERTIONS=ON -DMLIR_ENABLE_BINDINGS_PYTHON=ON -DLLVM_ENABLE_RUNTIMES="libcxx;libcxxabi;libunwind" \
 -DMLIR_ENABLE_CUDA_RUNNER=ON -DMLIR_ENABLE_ROCM_RUNNER=ON -DBUILD_SHARED_LIBS=True
ninja all
```

You can choose the path for installation using `CMAKE_INSTALL_PREFIX` and do `ninja all install`. Update the `PATH`, `LIBRARY_PATH`, and `LD_LIBRARY_PATH` to include the MLIR executable(s) and libraries. The flag `LLVM_ENABLE_RUNTIMES` is optional and can be removed if the build fails.

Now, build python bindings for MLIR with the [instructions](https://mlir.llvm.org/docs/Bindings/Python/) and add them to the `PYTHONPATH`.
To verify your python/MLIR environment, check that the command `python3 -m mlir.ir` does not fail.

### Build openCARP with MLIR

Now, you can build openCARP with MLIR.
- The CMake flag `ENABLE_MLIR_CODEGEN` should be enabled to build with MLIR code generator.
- For CPU vectorized code generation, define an environmental variable `export MLIR_NUM_ELEMENTS=2` OR `4` OR `8` (double's) based on the vector architecture you choose. `2` is for SSE vector architecture, `4` is for AVX2 vector architecture, and `8` is for AVX512 vector architecture. By default, `MLIR_NUM_ELEMENTS` is set to `8` (AVX512). You can check your processor capability on Linux by running `grep avx /proc/cpuinfo`: if you get avx2 only and no avx512, you should use `4`.
- For CPU, the flag `MLIR_VECTOR_LIBRARY` is optional for mentioning the vector math library. The MLIR generated code performs *much* better with the `SVML` library (from the Intel compiler). This requires the `libsvml.so` (and `libintlc.so.?`) to be present in your `LD_LIBRARY_PATH` environment variable.
- For GPU, the flag `MLIR_CUDA_PTX_FEATURE` is optional for mentioning the `ptx` version (for example `-DMLIR_CUDA_PTX_FEATURE=ptx72`)

For example, you can build openCARP as below for vectorized (AVX2) CPU code generation:
```
git clone https://git.opencarp.org/openCARP/openCARP.git
cd openCARP
cmake -S. -B_build -GNinja -DCMAKE_C_COMPILER=clang -DDLOPEN=OFF -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_COMPILER=clang++ \
-DCMAKE_CXX_FLAGS="-O3 -fopenmp -march=native -mprefer-vector-width=256" -DENABLE_MLIR_CODEGEN=True -DUSE_OPENMP=ON -DMLIR_VECTOR_LIBRARY=SVML
export MLIR_NUM_ELEMENTS=4
cd _build && ninja all

```

You can use `MLIR_NUM_ELEMENTS=2` and `-mprefer-vector-width=128` for SSE vector architectures and `MLIR_NUM_ELEMENTS=8`, `-march=cascadelake`, `-mprefer-vector-width=512` for AVX512 vector architectures, respectively.

For GPU code generation, the `-march` and `-mprefer-vector-width` flags are ignored. If your compiling infrastructure has CUDA or ROCm available, CMake will generate GPU code.

#### Choosing which versions to generate

The `physics/limpet/models/imp_list.txt` file is used by the build system to
know which version of each ionic model should be compiled. At the beginning of
each line, you can find the `model` model keyword followed by the model's name.
The `plugin` keyword indicate that the IMP is a a plugin.
The presence of the `generated` keyword tells the build system that this model
should be generated during the build. After the `generated` keyword, you can
specify one or more targets to build the IMP for. Possible values are `cpu`,
`mlir-cpu`, `mlir-rocm` and `mlir-cuda`. If no target is specified, the build
system will try to generated every possible target according to your environment
and build options.

### CUDA specifics

Clang/LLVM 15 and CMake 3.19+ require a specific version of the nvcc CUDA compiler. As we write this documentation, they support nvcc up to version 11.5. If nvcc 12 is installed on your machine in `/usr/local/cuda` you should rename this directory, or uninstall it. Some versions of Linux also have a `/etc/alternatives/cuda` link that should be set correctly.

Another nvcc/Clang incompatibility is the ptx version they can both handle. If you get an error message like:
```
clang-15: warning: CUDA version is newer than the latest supported version 11.5 [-Wunknown-cuda-version]
ptxas /tmp/MULTI_ION_IF-2e39ef/MULTI_ION_IF-sm_35.s, line 5; fatal   : Unsupported .version 7.5; current version is '7.2'
```
then you should add `-DCUDA_GPU_FLAGS='--cuda-feature=+ptx72'`to the CMake options.

### ROCm specifics

When using PETSc and compiling for AMD GPUs, the rocThrust library is needed as it is a dependency of PETSc. In turn,
the rocPrim library is also needed as it is a dependency of rocThrust.

### Run experiments

You can use the `bench` executable available in bin to test the experiments.
The `--target` option is used to chose which version of the ionic model to run.
Possible values are:
- `cpu`
- `mlir-cpu`
- `mlir-rocm`
- `mlir-cuda`
- `auto`
The default value is `auto`. It tries to select the "most specialized" target
generated for the given model. It goes in the following order:
`mlir-cuda > mlir-rocm > mlir-cpu > cpu`.
For example, you can use bench like this:
```
./bin/bench -I Courtemanche -n 819200 -a 10 --target mlir-cpu
```

##### Using the openCARP executable or carputils options
You can run openCARP with default options for MLIR CPU vectorized code using your newly compiled openCARP executable.
This also works when openCARP is called from carputils.

For GPU, you have to build and run openCARP with Ginkgo options enabled (see [BUILD_WITH_GINKGO.md](https://git.opencarp.org/openCARP/openCARP/-/blob/master/docs/BUILD_WITH_GINKGO.md)).
