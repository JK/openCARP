#!/bin/bash
# Cut away the header
LINENUMBER=$(grep -n "Use the IMP library" bench.txt | cut -d : -f 1)
LINENUMBER=$(($LINENUMBER-1))
sed "1,$LINENUMBER d" bench.txt > bench.tex

sed -i "s/cm^2/cm$^2$/g" bench.tex 

# add subsection headings
sed -i -E 's/^ ?([a-zA-z\:\/\(\)][a-zA-z\:\/\(\) ]+)$/\\end{tabularx}\n\\subsection{\1}\n\\begin{tabularx}{\\textwidth}{l l X}/g' bench.tex 

# remove the first one
#LINENUMBER=$(grep -n "end{tabularx}" bench.tex  | head -n 1 | cut -d : -f 1 )
#sed -i "$LINENUMBER d" bench.tex

# add the last end
echo '\end{tabularx}' >> bench.tex

# add the first begin
sed -i '3 i \ \\begin{tabularx}{\\textwidth}{l l X}' bench.tex


# join multiline descriptions
awk -v ORS= '
  NR>1 && !/                                 / {print "\n"}
  {print}
  END {if (NR) print "\n"}' <  bench.tex > bench2.tex
sed -i 's/                                 / /g' bench2.tex  

# process parameters with short command  
sed -i -E 's/^  (-[a-zA-Z0-9]+), --([]=a-zA-Z0-9[\-]+) +(.*)/\\texttt{-{}-}\2 \& \1 \& \3\\\\/g' bench2.tex 
# process parameters without short command
sed -i -E 's/^      --([]=a-zA-Z0-9[\-]+) +(.*)/\\texttt{-{}-}\1 \& \& \2\\\\/g' bench2.tex 

# remove extra lines
sed -i -E 's/^  (.*)//g' bench2.tex 

# escape underscores
sed -i -E 's/_/\\_/g' bench2.tex 
