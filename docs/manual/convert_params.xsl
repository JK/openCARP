<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="xml"/>
  <xsl:template
    match="//struct-ref[@father-name='tsav' or @father-name='tsav_ext' or @father-name='external_imp' or @father-name='trace_node' or @father-name='LAT_ID']">
  <program name="{@father-name}">
      <param name="{@father-name}">
        <xsl:copy-of select="@*" /><!-- copy all attributes -->
        <xsl:apply-templates /><!-- copy content -->
      </param>
    </program>
  </xsl:template>

  <!-- copy everything else -->
  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>
