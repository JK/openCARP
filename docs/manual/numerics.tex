\section{Numerical Schemes}
\label{sec:numerics}

\subsection{Spatial discretization using the Galerkin FEM}

For the spatial discretization of PDEs \eqref{eq:elliptic}, \eqref{eq:parabolic}
we use the classical finite element method (FEM)
(for the introduction's details see \cite{SzaboBabuska1991},
\cite{Ciarlet1978}, \cite{Trew2005}, \cite{OdenIV1983}, \cite{Bathe1995}).
An application of the FEM to the cardiac electro physiology (bidomain model) is in details described in \cite{Sundnesandothers2006}.

Briefly, the bidomain equations are multiplied with a weighting function, $\alpha$, and integrated over the entire domain, $\Omega$:
\begin{align}
\label{eq:elliptic_weak}
\int_\Omega \nabla \cdot (\boldsymbol{\sigma}_\mathrm{i}+\boldsymbol{\sigma}_\mathrm{e}) \nabla \phi_\mathrm{e} \alpha \mathrm{d}\Omega &=
            -\int_\Omega \nabla \cdot \boldsymbol{\sigma}_\mathrm{i} \nabla V_\mathrm{m} \alpha \mathrm{d}\Omega - \int_\Omega \alpha I_{e} \mathrm{d}\Omega \\
\label{eq:parabolic_weak}
\int_\Omega \nabla \cdot \boldsymbol{\sigma}_\mathrm{i} \nabla V_\mathrm{m} \alpha \mathrm{d}\Omega &=
-\int_\Omega \nabla \cdot \boldsymbol{\sigma}_\mathrm{i} \nabla \phi_\mathrm{e} \alpha \mathrm{d}\Omega + \beta \int_\Omega \alpha I_\mathrm{m} \mathrm{d}\Omega
\end{align}
Arbitrary weighting functions can be chosen for $\alpha$,
however, $\alpha$ has to fulfill any prescribed boundary conditions.
Using the identity
\begin{equation}
 \nabla \cdot (\alpha \boldsymbol{j} ) = \nabla \alpha \cdot \boldsymbol{j} + \alpha \nabla \cdot \boldsymbol{j}
\end{equation}
or
\begin{equation}
 \alpha \nabla \cdot \boldsymbol{j} =  \nabla \cdot (\alpha \boldsymbol{j} ) - \nabla \alpha \cdot \boldsymbol{j}
\end{equation}
where the term $\alpha \nabla \cdot \boldsymbol{j}$ with $\boldsymbol{j} = \boldsymbol{\sigma} \nabla \phi$
appears in the integral of Eqs.~\eqref{eq:elliptic_weak}-\eqref{eq:parabolic_weak},
allows to rewrite these integrals as
\begin{equation}
 \int_\Omega \alpha \nabla \cdot \boldsymbol{j} \mathrm{d}\Omega = \int_\Omega \nabla \cdot (\alpha \boldsymbol{j} ) \mathrm{d}\Omega
                                                    - \int_\Omega \nabla \alpha \cdot \boldsymbol{j} \mathrm{d}\Omega
\end{equation}
or
\begin{equation}
 \int_\Omega \alpha \nabla \cdot \boldsymbol{j} \mathrm{d}\Omega = \int_\Gamma \alpha \boldsymbol{j} \mathrm{d}\Gamma
                                                    - \int_\Omega \nabla \alpha \cdot \boldsymbol{j} \mathrm{d}\Omega \label{eq:_identity}
\end{equation}
where the surface integral over the domain boundary $\partial \Omega$ is zero,
unless non-zero Neumann flux boundary conditions are enforced.
Substituting Eq.~\eqref{eq:_identity} into Eqs.~\eqref{eq:elliptic_weak}-\eqref{eq:parabolic_weak} yields
\begin{align}
\label{eq:elliptic_weak_a}
-\int_\Omega \nabla \alpha \cdot (\boldsymbol{\sigma}_\mathrm{i}+\boldsymbol{\sigma}_\mathrm{e}) \nabla \phi_\mathrm{e} \mathrm{d}\Omega &=
            \int_\Omega \nabla \alpha \cdot \boldsymbol{\sigma}_\mathrm{i} \nabla V_\mathrm{m} \mathrm{d}\Omega - \int_\Omega \alpha I_\mathrm{e} \mathrm{d}\Omega \\
\label{eq:parabolic_weak_a}
-\int_\Omega \nabla \alpha \cdot \boldsymbol{\sigma}_\mathrm{i} \nabla V_\mathrm{m} \mathrm{d}\Omega &=
 \int_\Omega \nabla \alpha \cdot \boldsymbol{\sigma}_\mathrm{i} \nabla \phi_\mathrm{e} \mathrm{d}\Omega + \beta \int_\Omega \alpha I_\mathrm{m} \mathrm{d}\Omega
\end{align}

If we further assume that $\alpha(x)=1$ holds everywhere over the domain, except maybe along Dirichlet boundaries,
we may write all sought after functions in terms of products such as
\begin{align}
 V_\mathrm{m}(x) = \alpha(x) V_\mathrm{m}(x) \\
 I_\mathrm{m}(x) = \alpha(x) I_\mathrm{m}(x) \\
 I_\mathrm{ion}(x) = \alpha(x) I_\mathrm{ion}(x) \\
 \phi_\mathrm{e}(x) = \alpha(x) \phi_\mathrm{e}(x)
\end{align}
and substituting into Eqs.~\eqref{eq:elliptic_weak_a}-\eqref{eq:parabolic_weak_a} yields
the final form
\begin{align}
\label{eq:elliptic_weak_b}
-\int_\Omega \nabla \alpha \cdot (\boldsymbol{\sigma}_\mathrm{i}+\boldsymbol{\sigma}_\mathrm{e}) \nabla \phi_\mathrm{e} \mathrm{d}\Omega &=
            \int_\Omega \nabla \alpha \cdot \boldsymbol{\sigma}_\mathrm{i} \nabla V_\mathrm{m} \mathrm{d}\Omega - \int_\Omega \alpha I_\mathrm{e} \mathrm{d}\Omega \\
\label{eq:parabolic_weak_b}
-\int_\Omega \nabla \alpha \cdot \boldsymbol{\sigma}_\mathrm{i} \nabla V_\mathrm{m} \mathrm{d}\Omega &=
 \int_\Omega \nabla \alpha \cdot \boldsymbol{\sigma}_\mathrm{i} \nabla \phi_\mathrm{e} \mathrm{d}\Omega + \beta \int_\Omega \alpha I_\mathrm{m} \mathrm{d}\Omega
\end{align}


Stiffness matrices, $\boldsymbol{\sigma}$ are discretized to have positive main diagonals,
that is
\begin{equation}
 \mathbf{K_\zeta} \phi \approx -\nabla \cdot \boldsymbol{\sigma}_\zeta \nabla \phi
\end{equation}

We write the elliptic parabolic cast of the bidomain equations,
as given by Eq.~\eqref{eq:elliptic}-\eqref{eq:parabolic}, in their spatially discrete form as follows:
\begin{align}
  \mathbf{K}_\mathrm{i+e} \mathbf{\boldsymbol\phi_\mathrm{e}} &= -
          \mathbf{K}_\mathrm{i} \mathbf{v_\mathrm{m}} + \mathbf{M_\mathrm{e} I_\mathrm{e}} \\%\label{eq:ellip_x} \\  \label{eq:parab_x}
  \beta C_\mathrm{m} \mathbf{M_\mathrm{i}} \frac{\partial \mathbf{v_\mathrm{m}}}{\partial t} &= -
  \mathbf{K_\mathrm{i}}\left(\mathbf{v_\mathrm{m}}+\boldsymbol\phi_\mathrm{e} \right) - \beta \mathbf{M_\mathrm{i}} (I_\mathrm{ion}-I_\mathrm{tr})
\end{align}
By default the implementation of the bidomain equations in CARP relies on
elliptic-parabolic decoupling and operator splitting of the parabolic equations
which results in
\begin{align}
  \mathbf{K_\mathrm{i+e}} \mathbf{\boldsymbol\phi_\mathrm{e}} &= -
          \mathbf{K_\mathrm{i}} \mathbf{v_\mathrm{m}} + \mathbf{M_\mathrm{e} I_\mathrm{e}} \\%\label{eq:ellip_x_splt} \\
  \beta C_\mathrm{m} \mathbf{M_\mathrm{i}} \frac{\partial \mathbf{v_\mathrm{m}}}{\partial t} &=
  -\beta \mathbf{M_\mathrm{i}} (\mathbf{I_\mathrm{ion}}-\mathbf{I_\mathrm{tr}}) \label{eq:_ode_x_splt} \\
  \beta C_\mathrm{m} \mathbf{M_\mathrm{i}} \frac{\partial \mathbf{v_\mathrm{m}}}{\partial t} &=
  -\mathbf{K_\mathrm{i}}\left(\mathbf{v_\mathrm{m}}+\boldsymbol\phi_\mathrm{e} \right) %\label{eq:parab_x_splt}
\end{align}
where in Eq.~\eqref{eq:_ode_x_splt} $\beta \mathbf{M_\mathrm{i}}$ cancels out.
The implementation of Eq.~\eqref{eq:_ode_x_splt} is then
\begin{equation}
  \frac{\partial \mathbf{v_\mathrm{m}}}{\partial t} = -\frac{1}{C_\mathrm{m}}(\mathbf{I_\mathrm{ion}}-\mathbf{I_\mathrm{tr}}) \label{eq:_ode_x_splt_imp} \\
\end{equation}

For the spatial discretization of the mechanics solver we refer to \autoref{sec:spatial_discretization_mech}.

\subsection{Domain mapping}

In the presence of a bath intracellular and extracellular domain differ in size.
Extracellular and intracellular domain overlap over the entire domain $\Omega_\mathrm{i}$,
however, in the bath domain $\Omega_\mathrm{b}=\Omega_\mathrm{e} \setminus \Omega_\mathrm{i}$ the intracellular space does not exist.
Therefore, vectors defined on the two discrete spaces have  to be mapped as follows
\begin{align}
  \mathbf{K_\mathrm{i+e}} \mathbf{\boldsymbol\phi_\mathrm{e}} &= -
          \mathbf{K_\mathrm{i} P} \mathbf{v_\mathrm{m}} + \mathbf{M_\mathrm{e} I_\mathrm{e}}\\% \label{eq:ellip_x_splt} \\
  \beta C_\mathrm{m} \mathbf{M_\mathrm{i}} \frac{\partial \mathbf{v_\mathrm{m}}}{\partial t} &=
  -\mathbf{K_\mathrm{i}}\left(\mathbf{v_\mathrm{m}}+\mathbf{P^{-\top}} \boldsymbol\phi_\mathrm{e} \right) \\%\label{eq:parab_x_splt}
\end{align}
where $\mathbf{P}$ is the prolongation $\Omega_\mathrm{i} \mapsto \Omega_\mathrm{e}$ and
$\mathbf{P^{-\top}}$ is the restriction $\Omega_\mathrm{e} \mapsto \Omega_\mathrm{i}$.
It is worth noting that in our implementation the same spatial discretization is shared
in the overlapping domain, thus only index-based mapping is performed for transferring
data between the grids.


\subsection{Temporal discretization schemes}
\label{sec:_app_temporal}

Temporal discretization is based on the assumption of parabolic-elliptic decoupling of the bidomain equations.
That is, unlike in \cite{southern09:_coupled}, elliptic and parabolic portions are not solved as a single system.
First, the elliptic portion is solved for to obtain $\phi_\mathrm{e}(t)$ as a function
of the current distribution of the transmembrane voltage, $V_\mathrm{m}$, enforced Dirichlet potentials $\phi_\mathrm{e}^\mathrm{D}$
or extracellular stimulus currents, $I_\mathrm{e}$.
The extracellular potential field $\phi_\mathrm{e}$ is used then in the parabolic equation
to solve for $V_\mathrm{m}^{t+\mathrm{d}t}$.
There are two basic approaches implemented for this solve
where the standard method relies upon operator splitting \cite{strang68:_split}.
In theory, a Strang splitting should be used to achieve second order accuracy,
however, in practice only a first order accurate Godunov splitting scheme is used.
See \cite{sundnes05:_split} for details.
In practice, we do not expect any difference between the two schemes.
Only the first time step is different between the two schemes.
That is, if the solution during the first shift by half a time step to achieve a $\mathrm{d}t/2$ delay
between ODE and parabolic PDE solution does not change,
there cannot be any differences between a Godunov and a Strang splitting.
The main advantage of operator splitting is that it renders the parabolic PDE linear,
which can be beneficial in terms of iteration numbers when using an iterative method.
In the alternative scenario we refrain from operator splitting
and solve the parabolic PDE which is non-linear now
since the non-linear term $I_\mathrm{ion}(V_\mathrm{m},\boldsymbol{\eta},t)$ shows up on the right hand side.

Using the spatially discrete representations of $V_\mathrm{m}$, $\mathbf{v_\mathrm{m}}$, and of $\phi_\mathrm{e}$, $\boldsymbol{\phi_\mathrm{e}}$,
and discretizing in time with
\begin{equation}
  t = k \mathrm{d}t
\end{equation}
we write spatio-temporally discretized representations as
\begin{align}
 V_\mathrm{m}(x,t)    & \approx \mathbf{v_\mathrm{m}}^k \\
 \phi_\mathrm{e}(x,t) & \approx \boldsymbol{\phi_\mathrm{e}}^k
\end{align}

The basics of the two schemes are given as follows:

\paragraph{Temporal discretization with operator splitting}

\begin{align}
  \mathbf{K_\mathrm{i+e}} \boldsymbol{\phi}_\mathrm{e}^{k} &= -
          \mathbf{P K_\mathrm{i}} \mathbf{v_\mathrm{m}}^{k} + \mathbf{M_\mathrm{e} I_\mathrm{e}} \\
  \frac{\partial{\boldsymbol{\eta}}}{\partial t} &= g( \mathbf{v}_\mathrm{m}^{k},\boldsymbol{\eta}^{k}) \Longrightarrow \boldsymbol{\eta}^{k+1} \\
  \mathbf{I_\mathrm{ion}^{\chi}} & = f(\mathbf{v_\mathrm{m}}^k,\boldsymbol{\eta}^{k+1}) \\
  \mathbf{v_\mathrm{m}^{\chi}} & = \mathbf{v_\mathrm{m}}^k-\frac{\Delta t_\mathrm{o}}{C_\mathrm{m}}\left( \mathbf{I}_\mathrm{ion}^{\chi} -\mathbf{I_\mathrm{tr}} \right) \label{eq:_osplit_vm_update} \\
  \beta C_\mathrm{m} \mathbf{M_\mathrm{i}} \frac{\partial \mathbf{v_\mathrm{m}^{\chi}}}{\partial t} &= -
  \mathbf{K_\mathrm{i}}\left(\mathbf{v_\mathrm{m}^{\chi}}+\mathbf{P^{-\top}}\boldsymbol\phi_\mathrm{e}^{k}\right) \label{eq:_osplit_diffuse}
\end{align}
Here $\mathbf{M}$ is the mass matrix, $\mathbf{K}$ is the stiffness matrix with the subscript specifying which conductivity to use,
intracellular $(\mathrm{i})$, extracellular $(\mathrm{e})$ or their sum $(\mathrm{i+e})$,
$\boldsymbol\eta$ is the set of state variables of the ionic model,
$g(\cdot)$ is the ionic model dependent set of state equations and
$f(\cdot)$ is a function describing the total ionic current across the membrane as a function of the current state, $\boldsymbol{\eta}$.
Note that $\chi$ indicates the result of an intermediate compute step
where $\mathbf{I_\mathrm{ion}^\chi}$ is computed with an updated state $\boldsymbol{\eta}^{k+1}$
and the current transmembrane voltage $\mathbf{v_\mathrm{m}}^{k}$.
The symbol $\Longrightarrow$ indicates that the set of ODEs is solved to compute $\boldsymbol{\eta}^{k+1}$.
Unlike in Eq.~\eqref{eq:_osplit_vm_update} where we always use a simple forward Euler update
where we advance the solution by the ODE time step, $\Delta t_\mathrm{o}$.
Numerous options are available for updating $\boldsymbol{\eta}^{k+1}$, thus only a symbol is used to indicate
that an implementation-dependent solver step is executed.
By default our implementation relies upon an accelerated Rush-Larsen technique
which has been described in detail elsewhere \cite{plank08:_ecme_rirr}.
In the following derivations it is convenient to scale the intracellular mass matrix, $\mathbf{M_\mathrm{i}}$
with $\kappa$ such that
\begin{align}
 \kappa &=\frac{\beta C_\mathrm{m}}{\Delta t_\mathrm{p}} \\
 \mathbf{\bar{M}_\mathrm{i}} &= \kappa \mathbf{M_\mathrm{i}} \label{eq:_mass_scaled} \\
 \Delta t_\mathrm{p} &= \frac{\Delta t} {n_\mathrm{s}}
\end{align}
where $\mathbf{M_\mathrm{i}}$ is the unscaled mass matrix and $\Delta t_\mathrm{p}$ is the time step
used for advancing the parabolic solution
which can be a fraction (but not a multiple) of the global electrical time step, $\Delta t$.
That is, $n_\mathrm{s} \ge 1$ is the integer number which specifies the number of parabolic sub-timesteps
which are used to diffuse the change in $V_\mathrm{m}$ due to the ODE reaction terms.
In general, we use $\Delta t = \Delta t_\mathrm{o} = \Delta t_\mathrm{p}$ since $n_\mathrm{s}=1$ is the default value.
Using a value $n_\mathrm{s}>1$ is beneficial when using an explicit method with an unstructured grid
where the CFL condition may impose severe limits upon the choice of $\Delta t$.
In such cases, one may chose $\Delta t_\mathrm{o}=\Delta t$ and $n_\mathrm{s}$ sufficiently large.
Thus the ODE load is kept constant, the parabolic compute lead increases linearly with $n_\mathrm{s}$,
since parabolic solver steps are repeated $n_\mathrm{s}$ times.
However, significant improvements in strong scaling characteristics may allow to achieve
shorter execution times. Details are found in \cite{niederer11:_clinical}.


\paragraph{Temporal discretization without operator splitting}

\begin{align}
  \mathbf{K_\mathrm{i+e}} \boldsymbol{\phi}_\mathrm{e}^{k} &= -
          \mathbf{P K_\mathrm{i}} \mathbf{v_\mathrm{m}}^{k} + \mathbf{M_\mathrm{e} I_\mathrm{e}} \\
  \frac{\partial{\boldsymbol{\eta}}}{\partial t} &= g( \mathbf{v_\mathrm{m}}^{k},\boldsymbol{\eta}^{k}) \Longrightarrow \boldsymbol{\eta}^{k+1}  \\
  \mathbf{I_\mathrm{ion}^{\chi}} & = f(\mathbf{v_\mathrm{m}}^k,\boldsymbol{\eta}^{k+1}) \\
  \label{eq:parabdiscret}
  \beta C_\mathrm{m} \mathbf{M_\mathrm{i}} \frac{\partial \mathbf{v_\mathrm{m}^{\chi}}}{\partial t} &= -
  \mathbf{K_\mathrm{i}}\left
  (\mathbf{v_\mathrm{m}}^{k}+\mathbf{P^{-\top}} \boldsymbol\phi_\mathrm{e}^{k}\right ) - \beta \mathbf{M_\mathrm{i}} \left( \mathbf{I_\mathrm{ion}^{\chi}-I_\mathrm{tr}} \right)
\end{align}

For the time discretization we consider one of the simple, for construction and implementation,
explicit technique --- Forward Euler scheme, as well as two fully Implicit methods
--- Crank-Nicolson and Second order time stepping schemes.

\subsubsection{Forward Euler scheme (FE)}
Forward Euler scheme is well known as Explicit method and has a following form:
\begin{align}
 \mathbf{v_\mathrm{m}^{\chi}} &= \mathbf{v}_\mathrm{m}^k-\frac{\Delta t_\mathrm{o}}{C_\mathrm{m}}\left( \mathbf{I_\mathrm{ion}^{\chi}} -\mathbf{I_\mathrm{tr}} \right) \\
 \mathbf{v_\mathrm{m}}^{k+1}  &= \mathbf{v}_\mathrm{m}^k + \mathbf{\bar{M}}_\mathrm{i}^{-1} \mathbf{K_\mathrm{i}}
                          \left(\mathbf{v_\mathrm{m}^{\chi}}+\mathbf{P^{-\top}} \boldsymbol{\phi}_\mathrm{e}^{k}\right ) \label{eq:_fe_splt}
\end{align}
or, in the case without operator splitting,
\begin{equation}
\mathbf{v}_\mathrm{m}^{k+1} = \mathbf{v}_\mathrm{m}^k + \mathbf{\bar{M}}_\mathrm{i}^{-1}\mathbf{K_\mathrm{i}}\left
(\mathbf{v}_\mathrm{m}^{k}+\mathbf{P^{-\top}} \boldsymbol{\phi}_\mathrm{e}^{k} \right )
-\beta \mathbf{M_\mathrm{i}}\mathbf{\bar{M}}_\mathrm{i}^{-1}(\mathbf{I_\mathrm{ion}-I_\mathrm{tr}}) \label{eq:_fe}
\end{equation}
where
\begin{equation}
 \beta \mathbf{M_\mathrm{i}}\mathbf{\bar{M}}_\mathrm{i}^{-1} = \frac{\Delta t_\mathrm{p}}{C_\mathrm{m}}.
\end{equation}

It has advantages in sense of less requirements of memory and computational time,
but is not as stable and has some stability restrictions on the time step
which are governed by the Courant-Friedrichs-Levy (CFL) condition.
This restrictions upon $\Delta t_\mathrm{p}$ may become prohibitive when using fine spatial discretizations.
This is of particular relevance when using unstructured grids for spatial discretization
since a single short element edge in the grid may enforce a very small time step.
In this case, FE mesh quality is of importance.
One way of alleviating this problem is to use parabolic sub-timestepping.
In this scenario a global $\Delta t_\mathrm{p}$ is used for solving the system  of ODEs,
but diffusion is computed in time steps of $\Delta t_\mathrm{p}/n_s$ where $n_s$ is an integer
for subdividing the interval.
Since a single Forward Euler step is so cheap, the method can still be beneficial,
see for instance, in Niederer et al \cite{niederer11:_clinical}.
A further disadvantage is due to the requirement of mass lumping
which is necessitated for simple inversion of the mass matrix
since $\mathbf{M}_\mathrm{i}^{-1}$ is used at the right hand side of Eq.~\eqref{eq:_fe}.

\subsubsection{Crank-Nicolson scheme (CN)}
The Crank-Nicolson method gives possibility to avoids the strict stability restrictions on the time step
and to improve accuracy and stability. In the case of operator splitting we solve
\begin{align}
\mathbf{v_\mathrm{m}^{\chi}} &= \mathbf{v}_\mathrm{m}^k-\frac{\Delta t_\mathrm{o}}{C_\mathrm{m}}\left( \mathbf{I_\mathrm{ion}^{\chi}} -\mathbf{I_\mathrm{tr}} \right) \\
\left (\mathbf{\bar{M}_\mathrm{i}}+\frac{\mathbf{K_\mathrm{i}}}{2}\right)\mathbf{v_\mathrm{m}^{k+1}} &=  - \mathbf{K_\mathrm{i}}\left
  (\frac{\mathbf{v_\mathrm{m}^{\chi}}}{2}+\mathbf{P^{-\top}} \boldsymbol\phi_\mathrm{e}^{k}\right )+\mathbf{\bar{M}_\mathrm{i}}\mathbf{v_\mathrm{m}^{\chi}}
\end{align}
or, without operator splitting, we have
\begin{equation}
\left (\mathbf{\bar{M}_\mathrm{i}}+\frac{\mathbf{K_\mathrm{i}}}{2}\right)\mathbf{v}_\mathrm{m}^{k+1} =  - \mathbf{K_\mathrm{i}}\left
  (\frac{\mathbf{v_\mathrm{m}^{\chi}}}{2}+\mathbf{P^{-\top}} \boldsymbol\phi_\mathrm{e}^k\right )+\mathbf{\bar{M}_\mathrm{i}}\mathbf{v_\mathrm{m}^{\chi}}
  -\beta \mathbf{M_\mathrm{i}}(\mathbf{I_\mathrm{ion}-I_\mathrm{tr}}).
\end{equation}
Note that in the case where we refrain from operator splitting both versions of the intracellular mass matrix,
the unscaled matrix $\mathbf{M_\mathrm{i}}$ and the scaled matrix $\mathbf{\bar{M}_\mathrm{i}}$, are used.
Since only the scaled matrix $\mathbf{\bar{M}_\mathrm{i}}$ is stored we compute the right hand side contribution
of ionic current and transmembrane stimulus current differently, using
\begin{equation}
\beta \mathbf{M_\mathrm{i}}(\mathbf{I_\mathrm{ion}-I_\mathrm{tr}}) = \frac{\Delta t_\mathrm{p}}{C_\mathrm{m}}\mathbf{\bar{M}_\mathrm{i}}(\mathbf{I_\mathrm{ion}-I_\mathrm{tr}}).
\end{equation}
In our current implementation we allow the bidomain surface-to-volume ratio $\beta$ to vary, however,
the membrane capacitance $C_\mathrm{m}$ is considered to be constant, i.e.\ $C_\mathrm{m}=\SI{1}{\micro \farad \per \square \cm}$.
As opposed to the explicit method, the solution of large systems of non-linear equations is required
for each time step.
However, due to the diagonal dominance of the problem the systems is solved quite efficiently with iterative methods,
requiring only a few iterations.
A particular advantage of the operator splitting approach is
that the number of iterations tends to be even lower since the parabolic problem is linear.

\subsubsection{\texorpdfstring{$\Theta$}{t}-schemes}

Crank-Nicolson, forward as well as backward Euler can be seen
as special cases of a general $\Theta$-scheme
where the individual methods correspond to the choices $\Theta=0.5$ (CN), $\Theta=0.$ (FE) and $\Theta=1.0$ (BE).
CN is second order accurate, but may be more prone to oscillations than BE.
The general $\Theta$ scheme for the bidomain is given as
\begin{align}
\mathbf{v_\mathrm{m}^{\chi}} &= \mathbf{v}_\mathrm{m}^k-\frac{\Delta t_\mathrm{o}}{C_\mathrm{m}}\left( \mathbf{I_\mathrm{ion}^{\chi}} -\mathbf{I_\mathrm{tr}} \right) \\
\left (\mathbf{\bar{M}_\mathrm{i}}+\theta \mathbf{K_\mathrm{i}}\right)\mathbf{v}_\mathrm{m}^{k+1} &=  - \mathbf{K_\mathrm{i}}\left
  ((1-\Theta) \mathbf{v_\mathrm{m}^{\chi}}+\mathbf{P^{-\top}} \boldsymbol\phi_\mathrm{e}\mathbf{^{k}}\right )+\mathbf{\bar{M}_\mathrm{i}}\mathbf{v_\mathrm{m}^{\chi}}
\end{align}
or, without operator splitting, we have
\begin{equation}
\left (\mathbf{\bar{M}_\mathrm{i}}+\frac{\mathbf{K_\mathrm{i}}}{2}\right)\mathbf{v}_\mathrm{m}^{k+1} =  - \mathbf{K_\mathrm{i}}\left
  (\frac{\mathbf{v_\mathrm{m}^{\chi}}}{2}+\mathbf{P^{-\top}} \boldsymbol\phi_\mathrm{e}^k\right )+\mathbf{\bar{M}_\mathrm{i}}\mathbf{v_\mathrm{m}^{\chi}}
  -\beta \mathbf{M_\mathrm{i}}(\mathbf{I_\mathrm{ion}-I_\mathrm{tr}}).
\end{equation}

\subsection{Nonlinear solver}
For the equilibrium solver, there is no time stepping scheme necessary since the solution is a steady state of equilibrium between the external and internal forces. However, the underlying equations are nonlinear. We solve the problem using multiple loading steps and an iterative Newton linesearch algorithm from the PETSc nonlinear solver (\href{https://petsc.org/main/manual/snes/}{SNES}) libraries. 
%We use Newton's method and calculate the jacobian via a finite differences approach. 
The equation to solve is defined as:
\begin{equation}
	\underline{r} (\underline{u}) \coloneqq \underline{f}^\mathrm{int} (\underline{u}) - \underline{f}^\mathrm{ext} (\underline{u}) = \underline{0}
\end{equation}
%We define the residual vector
%\begin{equation}
%	\underline{r} (\underline{\hat u}) \coloneqq \underline{f}^\mathrm{int} (\underline{\hat u}) - \underline{f}^\mathrm{ext} (\underline{\hat u})
%\end{equation}
For the solution vector $\underline{u}$, the vector of residuals $\underline{r}(\underline{u})$ is $\underline{0}$. We try to approximate this solution vector so that 
\begin{equation}
	\underline{r} (\underline{\hat u}) = \underline{f}^\mathrm{int} (\underline{\hat u}) - \underline{f}^\mathrm{ext} (\underline{\hat u}) \approx \underline{0}
\end{equation}
for a given displacement vector guess $\underline{\hat u} \approx \underline{u}$. The guess is updated with each iteration until the residual vector's magnitude reaches a user-defined tolerance and the next loading step will be applied. For any iteration $(k + 1)$, the update of $\underline{\hat u}$ is done via the Newton's method
\begin{equation}
	\underline{\hat u}_{k + 1} 
	= \underline{\hat u}_{k} - \underline{\underline{J}}^{-1} 
	(\underline{\hat u}_k ) \underline{r}_k (\underline{\hat u}_k), \:\: k=0, 1, 2, ...
\end{equation}
where the jacobian $\underline{\underline{J}}$ is approximated using a finite differences approach. Instead of inverting $\underline{\underline{J}}$, each iteration is solved in a two step process:
\begin{align}
	&\text{1. (Approximately) solve}  & \underline{\underline{J}}(\underline{\hat u}_k)  \Delta \underline{\hat u}_k & = - \underline{r} (\underline{\hat u}_k) \\
	& \text{2. Update} & \underline{\hat u}_{k + 1} & \leftarrow   \underline{\hat u}_k + \Delta \underline{\hat u}_k.
\end{align}
% \subsection{Second order time stepping scheme (TSt)}
% Beside Crank-Nicolson Method we use another one fully Implicit type method --- Second order time stepping scheme
% which is of the form
% \begin{align}
% \mathbf{v_\mathrm{m}^{\chi}} &= \mathbf{v_\mathrm{m}^k}-\frac{\Delta t}{C_\mathrm{m}}\left( \mathbf{I_\mathrm{ion}^{\chi}} -\mathbf{I_\mathrm{tr}} \right) \\
%          \frac{1}{2} \Bigl (3\kappa \mathbf{M_\mathrm{i}}- \mathbf{K_\mathrm{i}} \Bigr ) \mathbf{v_\mathrm{m}^{k+1}} & =
%          \mathbf{K_\mathrm{i}} \left (\frac{\mathbf{v_\mathrm{m}^{\chi}}}{2}+\mathbf{P^{-\top}}\boldsymbol{\phi_\mathrm{e}} \right ) +
%          \kappa \mathbf{M_\mathrm{i}} \left ( 2 \mathbf{v_\mathrm{m}^{\chi}} - \frac{1}{2} \mathbf{v_\mathrm{m}^{k-1}} \right )
% \end{align}
% or, without operator splitting
% \begin{equation}
%          \frac{1}{2} \Bigl (3\kappa \mathbf{M_\mathrm{i}}- \mathbf{K_\mathrm{i}} \Bigr ) \mathbf{v_\mathrm{m}^{k+1}} =
%          \mathbf{K_\mathrm{i}} \left (\frac{\mathbf{v_\mathrm{m}^{\chi}}}{2}+\mathbf{P^{-\top}}\boldsymbol{\phi_\mathrm{e}} \right ) +
%          \kappa \mathbf{M_\mathrm{i}} \left ( 2 \mathbf{v_\mathrm{m}^{\chi}} - \frac{1}{2} \mathbf{v_\mathrm{m}^{k-1}} \right )
%          -\beta \mathbf{M_\mathrm{i}}(\mathbf{I_\mathrm{ion}-I_\mathrm{tr}}).
% \end{equation}
% \textbf{This is not correct yet and needs further checking.
% The implementation is also wrong for the non operator-splitting case.}


