## Build openCARP with the Ginkgo backend

The linear algebra library [Gingko](https://ginkgo-project.github.io/) can be used as an alternative to PETSc. Currently, Ginkgo can be run on a single GPU using CUDA (NVIDIA GPUs), HIP (AMD GPUs), DPC++ (Intel GPUs), and for CPUs OpenMP, and there is no MPI support.
This file explains how to use the Ginkgo backend in openCARP.

### Install Ginkgo

First, Ginkgo must be installed, according to the [official instructions](https://github.com/ginkgo-project/ginkgo/blob/develop/INSTALL.md). Yet, there are some specificities to use it with openCARP on GPUs depending on your backend of choice. In the following, we target NVIDIA GPUs:

- The `openCARP_backend` branch of the Ginkgo repository must be used
- In order to use the Ginkgo CUDA module, you must have `CUDA 9.2+` installed on your system
- The CMake `GINKGO_BUILD_CUDA` option must be enabled

For example, you can install Ginkgo using:
```
git clone -b openCARP_backend https://github.com/ginkgo-project/ginkgo.git
cd ginkgo && mkdir build && cd build
cmake .. -G "Unix Makefiles" -DGINKGO_BUILD_CUDA=ON -DCMAKE_INSTALL_PREFIX=$HOME/install
cmake --build . --target install
```

You can choose any path for `CMAKE_INSTALL_PREFIX`, as long as you provide it to openCARP in the next section.

In order to make the compilation of Ginkgo faster, you can choose not to build Ginkgo's benchmarks and examples by using the following cmake options: `-DGINKGO_BUILD_BENCHMARKS=OFF -DGINKGO_BUILD_EXAMPLES=OFF`.

### Build openCARP with Ginkgo

You can then build openCARP:
```
git clone -b ginkgo_integration https://git.opencarp.org/openCARP/openCARP.git
cd openCARP
cmake -S. -B_build -DENABLE_GINKGO=ON -DCMAKE_PREFIX_PATH=$HOME/install -DDLOPEN=ON
cmake --build _build
```

For now, using the Ginkgo backend is only possible in the branch `ginkgo_integration` of openCARP. If you want to use carputils to run experiments with Ginkgo, it is also advised to install the carputils version which integrates the Ginkgo options:
```
git clone -b add_ginkgo_options https://git.opencarp.org/openCARP/carputils.git
```
See the documentation on how to install carputils: https://opencarp.org/download/installation#installing-carputils 


### Run experiments

#### Using the carputils options

If you installed carputils from the branch `add_ginkgo_options`, you can now use carputils to run experiments using the Ginkgo backend:
```
python3 run.py --flavor ginkgo
```
By default, this option will use the reference executor of Ginkgo, for sequential CPU execution.

In order to run the simulation in parallel on an NVIDIA GPU, you will have to set up the following openCARP option inside `run.py`:
```
cmd += ['-ginkgo_exec', 'cuda']
```
Alternatively, the option `-ginkgo_exec` can take the following values:
- `ref`: Reference Executor for sequential CPU execution
- `omp`: OpenMP parallelized CPU execution
- `cuda`: Cuda executor for execution on NVIDIA GPUs
- `hip`: Hip executor for execution on AMD GPUs
- `dpcpp`: Dpcpp executor for execution on Intel GPUs

If several accelerators are available on your machine, you can set up the ID of the accelerator that should be used with the follwing option:
```
cmd += ['-device_id', '1']
```
By default, the accelerator with ID `0` will be used.

#### Using the openCARP executable directly

You can also set up the Ginkgo backend by using the openCARP options directly:
```
openCARP [...] -flavor ginkgo -ginkgo_exec cuda -device_id 0
```
where the `-ginkgo_exec` and `-device_id` options can take similar values as the ones introduced in the previous section.

In this case, you will also have to set up the `-parab_options_file` and `-ellip_options_file` options by providing the paths to ginkgo configuration files, available here: https://git.opencarp.org/openCARP/carputils/-/tree/add_ginkgo_options/carputils/resources/ginkgo_options .

