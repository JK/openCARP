# It searches for a particular python module.
# Sources : see http://www.cmake.org/pipermail/cmake/2011-January/041666.html
# Usage : find_python_module(<module> <optional_submodule> REQUIRED)
# Usage : find_python_module(mpi4py REQUIRED)
# Usage : find_python_module(mlir mlir.ir REQUIRED)
function(find_python_module module)
	string(TOUPPER ${module} module_upper)
	if (ARGC EQUAL 2)
		set(${module}_FIND_REQUIRED TRUE)
		set(search_module ${module})
	elseif(ARGC EQUAL 3)
		if (ARGV1 STREQUAL "REQUIRED")
	  		set(search_module ${ARGV2})
	  		set(${module}_FIND_REQUIRED TRUE)
	  	else()
	  		set(search_module ${ARGV1})
			if (ARGV2 STREQUAL "REQUIRED")
	  			set(${module}_FIND_REQUIRED TRUE)
	  		else()
	  			message(FATAL_ERROR "UNKNOWN OPTION")
	  		endif()
	  	endif()
	else()
		message(FATAL_ERROR "UNKNOWN NUMBER OF ARGUMENTS")
	endif()

	# A module's location is usually a directory, but for binary modules
	# it's a .so file.
	execute_process(COMMAND ${PYTHON_EXECUTABLE} -c
		"import re, ${search_module}; print(re.compile('/__init__.py.*').sub('',${search_module}.__file__))"
		RESULT_VARIABLE _${module}_status
		OUTPUT_VARIABLE _${module}_location
		ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE)

	if(NOT _${module}_status)
		set(python_${module_upper} ${_${module}_location} CACHE STRING
			"Location of Python module ${module}")
	endif(NOT _${module}_status)

	execute_process(COMMAND ${PYTHON_EXECUTABLE} -c
		"import ${search_module}; print(${search_module}.__version__)"
	  OUTPUT_VARIABLE _${module}_version
	  ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE)

	find_package_handle_standard_args(${module} REQUIRED_VARS _${module}_location VERSION_VAR _${module}_version)
	set(${module}_FOUND ${${module_upper}_FOUND} PARENT_SCOPE)
	set(${module}_VERSION ${_${module}_version} PARENT_SCOPE)
endfunction(find_python_module)

