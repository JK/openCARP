

#include <stdio.h>
#include <iostream>
#include <random>
#include <string>
#include <mpi.h>
#include <ginkgo/ginkgo.hpp>

#include "src/SF_base.h"

#include "test_suite.cpp"

//#include "petsc_utils.h"
//using namespace opencarp;

class TestGinkgo: TestSuite {
  private:

    int n_l = 4;
    int n_g = 4;

    SF::vector<SF_int> vec_rows;
    SF::vector<SF_int> vec_cols;
    SF::vector<double> vec_vals;
    SF::petsc_vector<int,double> *vec_g_1;
    SF::petsc_vector<int,double> *vec_g_2;
    SF::petsc_vector<int,double> *vec_p_1;
    SF::petsc_vector<int,double> *vec_p_2;

    SF::vector<SF_int> mat_rows;
    SF::vector<SF_int> mat_cols;
    SF::vector<double> mat_vals;
    SF::ginkgo_matrix<int,double> *mat_g_1;
    SF::ginkgo_matrix<int,double> *mat_g_2;
    SF::petsc_matrix<int,double> *mat_p_1;
    SF::petsc_matrix<int,double> *mat_p_2;

  protected:
    void beforeAll()
    {
      // Prepare data for vectors
      vec_rows = SF::vector<int>(n_g);
      vec_cols = SF::vector<int>(n_g);
      vec_vals = SF::vector<double>(n_g);
      for (size_t i = 0; i < n_g; i++) {
        vec_rows[i] = i;
        vec_cols[i] = i;
        vec_vals[i] = (double)i;
      }
    }
    void afterAll()
    {

    }

    void beforeEach()
    {
      // Initialize vectors
      vec_g_1 = new SF::petsc_vector<int, double>();
      vec_g_2 = new SF::petsc_vector<int, double>();
      vec_p_1 = new SF::petsc_vector<int, double>();
      vec_p_2 = new SF::petsc_vector<int, double>();

      init_vector(*vec_g_1);
      init_vector(*vec_g_2);
      init_vector(*vec_p_1);
      init_vector(*vec_p_2);

      // Initialize matrices
      mat_g_1 = new SF::ginkgo_matrix<int, double>();
      mat_g_2 = new SF::ginkgo_matrix<int, double>();
      mat_p_1 = new SF::petsc_matrix<int, double>();
      mat_p_2 = new SF::petsc_matrix<int, double>();

      init_matrix(*mat_g_1);
      init_matrix(*mat_g_2);
      init_matrix(*mat_p_1);
      init_matrix(*mat_p_2);
    }

    void afterEach()
    {

    }


  public:
    bool test_petsc = true;
    bool test_ginkgo = true;

    TestGinkgo(void)
    {
       beforeAll();
    }
    ~TestGinkgo(void)
    {
       afterAll();
    }

    typedef void(TestGinkgo::*TestFct)(
      SF::petsc_vector<int, double> & vec_1,
      SF::petsc_vector<int, double> & vec_2,
      SF::abstract_matrix<int, double> & mat_1,
      SF::abstract_matrix<int, double> & mat_2
    );

    void init_vector(SF::petsc_vector<int, double> & vec)
    {
      vec.init(n_g, n_l);
      vec.set(vec_rows, vec_vals);
    }

    void init_matrix(SF::abstract_matrix<int, double> & mat)
    {
      mat.init(n_g, n_g, n_l, n_l, 0, n_l);

      mat_rows = SF::vector<int>(n_g);
      mat_cols = SF::vector<int>(n_g);
      mat_vals = SF::vector<double>(n_g);

      for (size_t r = 0; r < n_g; r++) {
        for (size_t c = 0; c < n_g; c++) {
          mat_rows[c] = r;
          mat_cols[c] = c;
          mat_vals[c] = (double)((r*n_g + c) + 1);
          mat.set_value(r, c, (double)((r*n_g + c) + 1), false);
        }
        //mat.set_values(mat_rows, mat_cols, mat_vals, false);
      }
    }


    void fill_random(SF::petsc_vector<int, double> & vec)
    {
      SF_real* p = vec.ptr();
      for(SF_int i=0; i < vec.lsize(); i++)
      {
        p[i] = rand();
      }
      vec.release_ptr(p);
    }

    void fill_random(SF::abstract_matrix<int, double> & mat)
    {
      mat.init(n_g, n_g, n_l, n_l, 0, n_l);

      mat_rows = SF::vector<int>(n_g);
      mat_cols = SF::vector<int>(n_g);
      mat_vals = SF::vector<double>(n_g);

      for (size_t r = 0; r < n_g; r++) {
        for (size_t c = 0; c < n_g; c++) {
          mat_rows[c] = r;
          mat_cols[c] = c;
          mat_vals[c] = rand();
        }
        mat.set_values(mat_rows, mat_cols, mat_vals, false);
      }
    }

    void exec_test(TestGinkgo::TestFct pFct)
    {
      if (test_ginkgo)
      {
        std::cout << std::endl << "[[GINKGO]]" << std::endl;
        beforeEach();
        (this->*pFct)(*vec_g_1, *vec_g_2, *mat_g_1, *mat_g_2);
        afterEach();
      }

      if (test_petsc)
      {
        std::cout << std::endl << "[[PETSc]]" << std::endl;
        beforeEach();
        (this->*pFct)(*vec_p_1, *vec_p_2, *mat_p_1, *mat_p_2);
        afterEach();
      }
    }

    virtual void test_vec_set(
      SF::petsc_vector<int, double> & vec_1,
      SF::petsc_vector<int, double> & vec_2,
      SF::abstract_matrix<int, double> & mat_1,
      SF::abstract_matrix<int, double> & mat_2
    ) {
      fill_random(vec_2);
      SF_real *p = vec_2.ptr();
      SF_real a = 47.0;

      // Test void set(const SF_real val)
      vec_2.set(a);
      bool all_equals = true;
      for (size_t i = 0; i < n_g; i++) {
        all_equals &= p[i] == a;
      }

      assertTrue(all_equals, "setting with one value");

      // Test void set(const vector<SF_int> & idx, const vector<SF_real> & vals, const bool additive = false)
      a++;
      SF::vector<SF_int> idx = SF::vector<SF_int>(2);
      const SF::vector<SF_real> vals = SF::vector<SF_real>(2, a);
      idx.data()[0] = 1;
      idx.data()[1] = 2;
      vec_2.set(idx, vals);

      assertTrue(
        p[0] != a &&
        p[1] == a &&
        p[2] == a, "setting multiple indexes and values");

      // Test void set(const vector<SF_int> & idx, const SF_real val)
      a++;
      vec_2.set(idx, a);

      assertTrue(
        p[0] != a &&
        p[1] == a &&
        p[2] == a, "setting multiple indexes and one value");

      vec_2.release_ptr(p);
    }

    virtual void test_vec_sizes(
      SF::petsc_vector<int, double> & vec_1,
      SF::petsc_vector<int, double> & vec_2,
      SF::abstract_matrix<int, double> & mat_1,
      SF::abstract_matrix<int, double> & mat_2
    ) {
      assertTrue(vec_1.lsize() == n_l, "getting lsize()");
      assertTrue(vec_1.gsize() == n_g, "getting gsize()");
    }

    virtual void test_vec_mag_sum(
      SF::petsc_vector<int, double> & vec_1,
      SF::petsc_vector<int, double> & vec_2,
      SF::abstract_matrix<int, double> & mat_1,
      SF::abstract_matrix<int, double> & mat_2
    ) {
      fill_random(vec_1);
      SF_real* p = vec_1.ptr();

      // calc sum and norm
      SF_real sum = 0;
      SF_real norm = 0;
      for (size_t i = 0; i < n_g; i++) {
        auto val = p[i];
        sum += val;
        norm += val * val;
      }
      norm = sqrt(norm);

      // SF_real comparison; seems to work
      assertTrue(vec_1.mag() == norm, "calculating mag");
      assertTrue(vec_1.sum() == sum, "calculating sum");

      vec_1.release_ptr(p);
    }

    virtual void test_vec_read_write(
      SF::petsc_vector<int, double> & vec_1,
      SF::petsc_vector<int, double> & vec_2,
      SF::abstract_matrix<int, double> & mat_1,
      SF::abstract_matrix<int, double> & mat_2
    ) {
      std::string file = "test_vec";
      FILE* fd = NULL;


      fill_random(vec_1);
      vec_1.write_binary<double>(file);
      vec_2.read_binary<double>(file);
      assertTrue(vec_1.equals(vec_2), "writing and reading binary from file path");

      fill_random(vec_1);
      fd = fopen(file.c_str(), "w");
      vec_1.write_binary<double>(fd);
      fclose(fd);
      fd = fopen(file.c_str(), "r");
      vec_2.read_binary<double>(fd);
      fclose(fd);
      assertTrue(vec_1.equals(vec_2), "writing and reading binary from file descriptor");


      fill_random(vec_1);
      vec_1.write_ascii(file.c_str(), false);
      vec_2.read_ascii(file.c_str());
      assertTrue(vec_1.equals(vec_2), "writing and reading ascii from file path");


      fill_random(vec_1);
      vec_1.write_ascii(file.c_str(), false);
      fd = fopen(file.c_str(), "r");
      vec_2.read_ascii(fd);
      fclose(fd);
      assertTrue(vec_1.equals(vec_2), "writing and reading ascii from file descriptor");


      fill_random(vec_1);
      vec_1.write_ascii(file.c_str(), true);
      vec_2.read_ascii(file.c_str());
      assertTrue(vec_1.equals(vec_2), "writing and reading ascii with write_header=true");
    }

    virtual void test_vec_operations(
      SF::petsc_vector<int, double> & vec_1,
      SF::petsc_vector<int, double> & vec_2,
      SF::abstract_matrix<int, double> & mat_1,
      SF::abstract_matrix<int, double> & mat_2
    ) {
      fill_random(vec_1);
      fill_random(vec_2);
      SF_real factor = rand();

      double *v = vec_1.ptr();
      double *w = vec_2.ptr();

      auto *vec_result = new SF::petsc_vector<int, double>(); init_vector(*vec_result);

      auto *vec_mul = new SF::petsc_vector<int, double>(); init_vector(*vec_mul);
      auto *vec_add = new SF::petsc_vector<int, double>(); init_vector(*vec_add);
      auto *vec_sub = new SF::petsc_vector<int, double>(); init_vector(*vec_sub);
      auto *vec_mul_factor = new SF::petsc_vector<int, double>(); init_vector(*vec_mul_factor);
      auto *vec_add_factor = new SF::petsc_vector<int, double>(); init_vector(*vec_add_factor);
      auto *vec_div_factor = new SF::petsc_vector<int, double>(); init_vector(*vec_div_factor);
      auto *vec_add_scaled = new SF::petsc_vector<int, double>(); init_vector(*vec_add_scaled);

      SF_real *p_mul = vec_mul->ptr();
      SF_real *p_add = vec_add->ptr();
      SF_real *p_sub = vec_sub->ptr();
      SF_real *p_mul_factor = vec_mul_factor->ptr();
      SF_real *p_add_factor = vec_add_factor->ptr();
      SF_real *p_div_factor = vec_div_factor->ptr();
      SF_real *p_add_scaled = vec_add_scaled->ptr();

      for (size_t i = 0; i < n_g; i++) {
        p_mul[i] = v[i] * w[i];
        p_add[i] = v[i] + w[i];
        p_sub[i] = v[i] - w[i];
        p_mul_factor[i] = v[i] * factor;
        p_add_factor[i] = v[i] + factor;
        p_div_factor[i] = v[i] / factor;
        p_add_scaled[i] = v[i] + w[i] * factor;
      }

      *vec_result = vec_1;
      *vec_result *= vec_2;
      assertTrue(vec_mul->equals(*vec_result), "multiplying vector-wise");

      *vec_result = vec_1;
      *vec_result += vec_2;
      assertTrue(vec_add->equals(*vec_result), "adding vector-wise");

      *vec_result = vec_1;
      *vec_result -= vec_2;
      assertTrue(vec_sub->equals(*vec_result), "subtracting vector-wise");

      *vec_result = vec_1;
      *vec_result *= factor;
      assertTrue(vec_mul_factor->equals(*vec_result), "multiplying element-wise");

      *vec_result = vec_1;
      *vec_result += factor;
      assertTrue(vec_add_factor->equals(*vec_result), "adding element-wise");

      *vec_result = vec_1;
      *vec_result /= factor;
      assertTrue(vec_div_factor->equals(*vec_result), "dividing element-wise");

      *vec_result = vec_1;
      vec_result->add_scaled(vec_2, factor);
      assertTrue(vec_add_scaled->equals(*vec_result), "adding scaled");

      vec_mul->release_ptr(p_mul);
      vec_add->release_ptr(p_add);
      vec_sub->release_ptr(p_sub);
      vec_mul_factor->release_ptr(p_mul_factor);
      vec_add_factor->release_ptr(p_add_factor);
      vec_div_factor->release_ptr(p_div_factor);
      vec_add_scaled->release_ptr(p_add_scaled);

      vec_1.release_ptr(v);
      vec_2.release_ptr(w);
    }


    virtual void test_mat_setup(
      SF::petsc_vector<int, double> & vec_1,
      SF::petsc_vector<int, double> & vec_2,
      SF::abstract_matrix<int, double> & mat_1,
      SF::abstract_matrix<int, double> & mat_2
    ) {
      // after last set_values
      mat_1.finish_assembly();

      bool equal = true;
      for (size_t row = 0; row < n_g; row++)
        for (size_t col = 0; col < n_g; col++)
          if (mat_1.get_value(row, col) != (row * n_g + col + 1))
            equal = false;

      assertTrue(equal, "setting up");
      if (!equal)
        std::cout << mat_1.to_string() << std::endl;
    }

    virtual void test_mat_get_values(
      SF::petsc_vector<int, double> & vec_1,
      SF::petsc_vector<int, double> & vec_2,
      SF::abstract_matrix<int, double> & mat_1,
      SF::abstract_matrix<int, double> & mat_2
    ) {
      // after last set_values
      mat_1.finish_assembly();

      SF::vector<int> col_idx = SF::vector<int>(n_g, 0);
      SF::vector<int> row_idx = SF::vector<int>(n_g, 0);
      SF::vector<double> vals = SF::vector<double>(n_g, 0);

      bool equal = true;
      for (size_t row = 0; row < n_g; row++)
      {
        // setup row & col indices
        for (size_t col = 0; col < n_g; col++)
        {
          col_idx[col] = col;
          row_idx[col] = row;
        }
        mat_1.get_values(row_idx, col_idx, vals);

        for (size_t col = 0; col < n_g; col++)
        {
          // std::cout << row_idx[col] << " | " << col_idx[col] << " = " << vals[col] << std::endl;
          if (vals[col] != (row * n_g + col + 1))
            equal = false;
        }
      }
      assertTrue(equal, "getting multiple values row-wise");


      equal = true;
      for (size_t col = 0; col < n_g; col++)
      {
        // setup row & col indices
        for (size_t row = 0; row < n_g; row++)
        {
          col_idx[row] = col;
          row_idx[row] = row;
        }
        mat_1.get_values(row_idx, col_idx, vals);

        for (size_t row = 0; row < n_g; row++)
        {
          // std::cout << row_idx[row] << " | " << col_idx[row] << " = " << vals[row] << std::endl;
          if (vals[row] != (row * n_g + col + 1))
            equal = false;
        }
      }
      assertTrue(equal, "getting multiple values column-wise");
    }


    virtual void test_mat_set_values(
      SF::petsc_vector<int, double> & vec_1,
      SF::petsc_vector<int, double> & vec_2,
      SF::abstract_matrix<int, double> & mat_1,
      SF::abstract_matrix<int, double> & mat_2
    ) {

    }

    virtual void test_mat_write(
      SF::petsc_vector<int, double> & vec_1,
      SF::petsc_vector<int, double> & vec_2,
      SF::abstract_matrix<int, double> & mat_1,
      SF::abstract_matrix<int, double> & mat_2
    ) {
      mat_1.finish_assembly();

      char path[1024];
      strcat(path, "test_mat.bin");

      char path_abs[1024];
      getcwd(path_abs, 1024);
      strcat(path_abs, "/");
      strcat(path_abs, path);

      // mat fct
      mat_1.write(path);
      mat_1.write(path_abs);

      std::ifstream file_istream(path);
      std::string txt;
      int lines = 0;
      while (std::getline(file_istream, txt)) {
        lines ++;
      }
      file_istream.close();
      assertTrue(lines > 0, "writing a file");

      remove(path);
    }

    virtual void test_mat_duplicate(
      SF::petsc_vector<int, double> & vec_1,
      SF::petsc_vector<int, double> & vec_2,
      SF::abstract_matrix<int, double> & mat_1,
      SF::abstract_matrix<int, double> & mat_2
    ) {

      fill_random(mat_2);

      mat_1.finish_assembly();
      mat_2.finish_assembly();

      mat_1.duplicate(mat_2);

      assertTrue(mat_1.equals(mat_2), "duplicating");
    }

    virtual void test_mat_mult(
      SF::petsc_vector<int, double> & vec_1,
      SF::petsc_vector<int, double> & vec_2,
      SF::abstract_matrix<int, double> & mat_1,
      SF::abstract_matrix<int, double> & mat_2
    ) {

      mat_1.finish_assembly();
      mat_1.mult(vec_1, vec_2);

      /*
      std::cout << mat_1.to_string() << std::endl;
      std::cout << " * " << std::endl;
      std::cout << vec_1.to_string() << std::endl;
      std::cout << " = " << std::endl;
      std::cout << vec_2.to_string() << std::endl;
      */

      SF_real* p1 = vec_1.ptr();
      SF_real* p2 = vec_2.ptr();

      bool equal = true;
      for (size_t row = 0; row < n_g; row++)
      {
        double y = 0;
        for (size_t col = 0; col < n_g; col++)
        {
          y += mat_1.get_value(row, col) * p1[col];
        }
        if (fabs(y - p2[row]) > 0.0001) { equal = false; }
      }

      assertTrue(equal, "multiplying y = Ax");

      vec_1.release_ptr(p1);
      vec_2.release_ptr(p2);
    }

    virtual void test_mat_diag(
      SF::petsc_vector<int, double> & vec_1,
      SF::petsc_vector<int, double> & vec_2,
      SF::abstract_matrix<int, double> & mat_1,
      SF::abstract_matrix<int, double> & mat_2
    ) {
      mat_1.finish_assembly();

      mat_1.get_diagonal(vec_1);
      SF_real* p1 = vec_1.ptr();
      bool equal = true;
      for (size_t d = 0; d < n_g; d++)
      {
        if (fabs(mat_1.get_value(d, d) - (d * n_g + d + 1)) > 0.0001) {
          equal = false;
        }
      }
      assertTrue(equal, "getting the diagonal");
      if (!equal)
      {
        std::cout << mat_1.to_string() << std::endl;
        std::cout << vec_1.to_string() << std::endl;
      }
      vec_1.release_ptr(p1);

      equal = true;
      vec_2.set(1);
      SF_real* p2 = vec_2.ptr();
      mat_1.diag_add(vec_2);

      for (size_t d = 0; d < n_g; d++)
      {
        double expected = 1 + (d*(1+n_g)) + p2[d];
        if (fabs(mat_1.get_value(d, d) - expected) > 0.0001)
          equal = false;
      }
      assertTrue(equal, "adding to diagonal");
      if (!equal)
      {
        std::cout << mat_1.to_string() << std::endl;
      }
      vec_2.release_ptr(p2);

    }

    virtual void test_mat_scale(
      SF::petsc_vector<int, double> & vec_1,
      SF::petsc_vector<int, double> & vec_2,
      SF::abstract_matrix<int, double> & mat_1,
      SF::abstract_matrix<int, double> & mat_2
    ) {
      mat_1.finish_assembly();
      mat_2.finish_assembly();

      double fac = rand();
      mat_1.scale(fac);

      bool equal = true;
      for (size_t col = 0; col < n_g; col++)
      {
        for (size_t row = 0; row < n_g; row++)
        {
          if (fabs(mat_1.get_value(row, col) - fac * mat_2.get_value(row, col)) > 0.0001)
            equal = false;
        }
      }
      assertTrue(equal, "scaling by factor");

      mat_1.scale(1.0/fac);
      assertTrue(mat_1.equals(mat_2), "undoing scaling with inverse");


      mat_2.add_scaled_matrix(mat_2, fac, true);
      equal = true;
      for (size_t col = 0; col < n_g; col++)
      {
        for (size_t row = 0; row < n_g; row++)
        {
          double expected = (col + row*n_g + 1) * (fac + 1);

          if (fabs(mat_2.get_value(row, col) - expected) > 0.0001)
            equal = false;
        }
      }
      assertTrue(equal, "adding scaled matrix");
    }
};



int main(int argc, char** argv)
{
  // init MPI
  MPI_Init(&argc, &argv);
  MPI_Comm comm = SF_COMM;
  int rank, size;
  MPI_Comm_size(comm, &size); MPI_Comm_rank(comm, &rank);


  // init PETSc
  PetscInitialize( &argc, &argv, (char *)0, "" );

  // start testing
  auto test = TestGinkgo();

  std::cout << std::endl << "[[[ TESTING VECTORS ]]]" << std::endl;
  test.test_petsc = true;
  test.test_ginkgo = false;
  test.exec_test(&TestGinkgo::test_vec_set);
  test.exec_test(&TestGinkgo::test_vec_sizes);
  test.exec_test(&TestGinkgo::test_vec_mag_sum);
  test.exec_test(&TestGinkgo::test_vec_read_write);
  test.exec_test(&TestGinkgo::test_vec_operations);

  std::cout << std::endl << "[[[ TESTING MATRICES ]]]" << std::endl;
  test.test_petsc = true;
  test.test_ginkgo = true;
  test.exec_test(&TestGinkgo::test_mat_setup);
  test.exec_test(&TestGinkgo::test_mat_get_values);
  test.exec_test(&TestGinkgo::test_mat_set_values);
  test.exec_test(&TestGinkgo::test_mat_write);
  test.exec_test(&TestGinkgo::test_mat_duplicate);
  test.exec_test(&TestGinkgo::test_mat_mult);
  test.exec_test(&TestGinkgo::test_mat_diag);
  test.exec_test(&TestGinkgo::test_mat_scale);


  // end MPI
  MPI_Finalize();
}
