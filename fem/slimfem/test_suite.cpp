
#include <stdio.h>
#include <iostream>
#include <string>
#include <mpi.h>
#include <ginkgo/ginkgo.hpp>

#include "src/SF_base.h"

class TestSuite
{
  public:
    void assertTrue(bool cond_bool, std::string str)
    {
        if (cond_bool) {
            std::cout << "\033[32m[SUCCESS]\033[0m  " << str << std::endl;
        } else {
            std::cout << "\033[31m[FAILURE]\033[0m  " << str << std::endl;
        }
    }

  protected:
    virtual void beforeAll() = 0;
    virtual void afterAll() = 0;
    virtual void beforeEach() = 0;
    virtual void afterEach() = 0;
};
