# MLIR tests

## Run MLIR regression tests

The regression tests for the MLIR build of openCARP are ran using the
[LLVM Integrated Tester](https://llvm.org/docs/CommandGuide/lit.html) (lit).

Tests can be ran using the CMake target `check-mlir-codegen`.
First build openCARP using CMake then you can build the `check-mlir-codegen` target.
The `check-mlir-codegen` target is only generated for MLIR builds.
Assuming your build directory is `_build` you can use the following command

```bash
cmake --build _build --target check-mlir-codegen
```

## Add new tests or update existing tests

Test configuration along with reference data is generated using the
`generate_test_data.py` script located in `test/mlir`

```
Usage: generate_test_data.py [options]

Options:
  -h, --help            show this help message and exit
  -b BUILD_DIR, --build-dir=BUILD_DIR
                        build directory for retrieving results
  -m MODELS, --models=MODELS
                        List of models (Default:all, options: all, or
                        model1;model2;model3)
  -o OUT_DIR, --output-dir=OUT_DIR
                        Relative path to output (Default: Current directory)
  -p PRECISION, --precision=PRECISION
                        Number of decimal digits of precision of output values
                        (Default: 3)
```

To use the script, first navigate to the mlir test directory

```bash
cd test/mlir
```

Then, to generate the test configuration for a new model,
or to overwrite the test configuration for an existing model, use the command

```bash
python3 generate_test_data.py -b ../../_build -m <Model name>
```

You can generate configurations for all models using

```bash
python3 generate_test_data.py -b ../../_build -m all
```
