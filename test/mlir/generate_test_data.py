#!/usr/bin/env python3

from optparse import OptionParser
import numpy as np
import itertools
import re
import subprocess
import sys
import os
from os import path
import warnings


def custom_formatwarning(msg, *args, **kwargs):
    # ignore everything except the message
    return str(msg) + '\n'


# Returns a tuple (Model Name, isPlugin)
def get_models(filename, models_str):
    all_models = []
    with open(filename) as imp_list:

        lines = imp_list.readlines()
        pattern = re.compile('^model ([a-zA-Z_0-9]+).*')

        for line in lines:
            match = pattern.match(line)
            if match != None:
                if "no-mlir" in match.group(0):
                    continue
                if "plugin" in match.group(0):
                    all_models.append([match.group(1), True])
                else:
                    all_models.append([match.group(1), False])

    if models_str == "all":
        return all_models

    valid_models = []
    models_to_run = models_str.split(sep=";")

    # Append valid
    [valid_models.append(model) for model in all_models if model[0] in models_to_run]

    # Remake the list only with valid models and test it
    warnings.formatwarning = custom_formatwarning
    valid_models_to_run = [model[0] for model in valid_models]
    [warnings.warn("WARNING: Model " + model + " not found\n")
     for model in models_to_run if model not in valid_models_to_run]

    return valid_models


def execute_model(executable, model_name, is_plugin, out):
    if is_plugin:  # is a Plugin
        args = ' -P '
    else:
        args = ' -I '
    args = args + model_name + ' -n 1 -a 10'
    print("Executing model " + model_name)
    subprocess.run(executable + args, shell=True, stdout=out, stderr=None)


def write_header(file, model_name, is_plugin):
    # // RUN: %bench-test -I mMS -n 43 -a 10 | %filecheck %s
    if is_plugin:
        file.write('// RUN: %bench-test -P ' + model_name + ' -n 43 -a 10 | %filecheck %s\n\n')
    else:
        file.write('// RUN: %bench-test -I ' + model_name + ' -n 43 -a 10 | %filecheck %s\n\n')


def write_line(file, line, precision):
    file.write("// CHECK: {:.3f}\n".format(line[0]))

    def write_multiple_values(line_str):
        for _ in range(0, 8):
            string_value = "{:+.6e}\n".format(line_str)
            new_string_value = re.sub('[0-9]{%s}e' %precision, '{{[0-9]+}}e', string_value)
            file.write("// CHECK: (Cell: {{{{[0-9]+}}}}) {}".format(new_string_value))

    [write_multiple_values(line[i]) for i in range(1, len(line))]


def generate_new_test_file(input_file, output_dir, precision, model, is_plugin):
    lines = itertools.islice(input_file, 0, 14)
    data = np.loadtxt(lines, skiprows=1, max_rows=14)

    with open(output_dir + "/" + model + '.test', 'w+') as data_file:
        write_header(data_file, model, is_plugin)
        [write_line(data_file, i, precision) for i in data]


def generate_model_test(executable, model, output_dir, precision):
    with open(output_dir + "/tmp", "w+") as tmp_file:
        execute_model(executable, model[0], model[1], tmp_file.fileno())
        tmp_file.seek(0)
        file_lines = tmp_file.readlines()
        generate_new_test_file(file_lines, output_dir, precision, model[0], model[1])
    tmp_file.close()


def create_tmp_file(filename):
    if not path.exists(filename):
        file = open(filename, "w+")
        file.close()


def parse_options():

    script_path = os.path.realpath(__file__)
    script_dir = os.path.dirname(script_path)
    current_dir = os.getcwd()

    parser = OptionParser()
    parser.add_option("-b", "--build-dir", action="store", dest="build_dir",
                      help="build directory for retrieving results", type='string')

    parser.add_option("-m", "--models", action="store", dest="models", default="all",
                      help="List of models (Default:all, options: all, or model1;model2;model3)", type='string')

    parser.add_option("-o", "--output-dir", action="store", dest="out_dir", default=".",
                      help="Relative path to output (Default: Current directory)", type='string')

    parser.add_option("-p", "--precision", action="store", dest="precision", default="3",
                      help="Number of decimal digits of precision of output values (Default: 3)", type='int')

    (options, args) = parser.parse_args()
    if not options.build_dir:
        parser.error('--build-dir not specified')

    output_dir = current_dir + "/" + options.out_dir
    if not os.path.isdir(output_dir):
        parser.error('--out_dir cannot be specified')

    build_path = os.path.abspath(current_dir + "/" + options.build_dir)

    models = get_models(script_dir + '/../../physics/limpet/models/imp_list.txt', options.models)
    return build_path + "/bin/bench", models, output_dir, (6 - options.precision)


def clean_folder(models, tmp_file):
    current_dir = os.getcwd()
    [subprocess.run("rm -f " + current_dir + "/" + model[0] + "*.txt", shell=True, stdout=None, stderr=None) for model in models]

    subprocess.run("rm -f " + tmp_file, shell=True, stdout=None, stderr=None)
    subprocess.run("rm -f " + current_dir + "/Trace_0.dat", shell=True, stdout=None, stderr=None)


if __name__ == '__main__':

    bench_bin, models, output_dir, precision = parse_options()

    create_tmp_file(output_dir + "/tmp")
    [generate_model_test(bench_bin, i, output_dir, precision) for i in models]

    clean_folder(models, output_dir + "/tmp")
