// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2022 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file async_io.h
* @brief Async IO functions.
* @author Aurel Neic
* @version 
* @date 2022-06-22
*/

#ifndef _ASYNC_IO_H
#define _ASYNC_IO_H

namespace opencarp {
namespace async {

#define ASYNC_TAG 1001

#define ASYNC_CMD_EXIT 1
#define ASYNC_CMD_REGISTER_OUTPUT 2
#define ASYNC_CMD_OUTPUT 3

/// minimal information needed for communication between MPI_Comms
struct intercomm_layout {
  int loc_rank = 0;                    ///< the local rank
  int loc_size = 0;                    ///< the local communicator size
  int rem_size = 0;                    ///< the remote communicator size

  /// setup routine for the members
  inline void setup(MPI_Comm ic)
  {
    loc_rank = get_rank();
    loc_size = get_size();
    rem_size = get_remote_size(ic);
  }
};

/// queue with the data required for performing async IO writes to IGB
struct async_IO_queue {
  SF::vector<IGBheader*>             IGBs;     ///< IGBs with open filehandles on rank 0
  SF::vector<SF::vector<long int>>   layouts;  ///< data layouts
  SF::vector<SF::commgraph<size_t>>  cg;       ///< commgraphs for MPI_Exchange
  SF::vector<SF::vector<mesh_int_t>> perm_b;   ///< permutation before MPI_Exchange
  SF::vector<SF::vector<mesh_int_t>> perm_a;   ///< permutation after MPI_Exchange

  /// add one slice of IO info to the queue
  inline int add(IGBheader* igb,
                 const SF::vector<long int> & lt,
                 const SF::commgraph<size_t> & c,
                 const SF::vector<mesh_int_t> & pb,
                 const SF::vector<mesh_int_t> & pa)
  {
    int id = cg.size();

    IGBs   .push_back(igb);
    layouts.push_back(lt);
    cg     .push_back(c);
    perm_b .push_back(pb);
    perm_a .push_back(pa);

    return id;
  }
};

// functions used by IO nodes
void IO_poll_for_output(async_IO_queue & io_queue);
void IO_register_output(async_IO_queue & io_queue);
void IO_get_sender_ranks(const intercomm_layout & il, SF::vector<int> & sender);
void IO_prepare_sort(const SF::vector<mesh_int_t> & inp_idx,
                     SF::commgraph<size_t> & grph,
                     SF::vector<mesh_int_t> & perm_before_comm,
                     SF::vector<mesh_int_t> & perm_after_comm);
void IO_do_output(async_IO_queue & io_queue);


// functions used by COMPUTE nodes
int  COMPUTE_register_output(const SF::vector<mesh_int_t> & idx,
                             const int dpn,
                             const char* name,
                             const char* units);
void COMPUTE_send_exit_flag();
int  COMPUTE_get_receive_rank(const intercomm_layout & il);
void COMPUTE_do_output(SF_real* data, const int lsize, const int IO_id);
void COMPUTE_do_output(SF_real* dat, const SF::vector<mesh_int_t> & idx, const int IO_id);

}}

#endif
