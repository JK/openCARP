// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file mechanics.h
* @brief Tissue level mechanics, main Mechanics physics class.
* @author Jonathan Krauss, Tobias Gerach
* @version 
* @date 2022-05-03
*/

#ifndef _MECHANICS_H
#define _MECHANICS_H
#define WITH_MECH

#include "physics_types.h"
#include "sim_utils.h"
#include "stimulate.h"
#include "sf_interface.h"
#include "timers.h"
#include "constitutive_model_library.h"
#include "constitutive_model.h"


namespace opencarp {

/**
 @ brief Class for the definition of both Neumann and DIrichlet mechanics boundary conditions
 */
class mech_bcs
{
  public:

  enum def_t {Neumann, Dirichlet};
  enum nbc_t {pressure, force};
  enum dbc_t {all, x, y, z, xy, xz, yz};
  
  def_t definition; //!< type of boundary condition
  nbc_t nbc_type; //!< pressure given for elements, force given for nodes
  dbc_t dbc_type; //!< fixed coordinates
  SF::Point direction; //!< direction in which the bc is applied
  SF::vector<mesh_int_t> vertices; //!< list of nodes that bc is active on
  SF::vector<SF_real>  scaling; //!< scaling for Neumann bcs
  std::string  input_filename; //!< filename with list of nodes

  void setup(int idx);
};

/**
 @ brief Class for static mechanics solver
 Inspired by CBSolverEquilibrium-->CardioMechanics and elliptic_solver-->openCARP
 */
class equilibrium_solver
{
  public:
  sf_vec* position = nullptr; //!< position x
  sf_vec* reference_pos = nullptr; //!< reference position X
  sf_vec* position_nodal = nullptr; //!< position x in nodal layout
  sf_vec* displacement = nullptr; //!< absolute displacement u
  sf_vec* du = nullptr; //!< incremental displacement du
  sf_vec* initial_guess = nullptr; //!< needed for initial guess for nonlinear solver
  sf_vec* f_internal = nullptr; //!<internal forces f_int
  sf_vec* f_external = nullptr; //!<external forces f_ext
//  sf_vec* f_surf; //!<external forces applied on surfaces
  sf_vec* residuum = nullptr; //!<residuum r
  SF::vector<SF_int> dbc_idx; //!<Indices for Dirichlet boundary conditions
  SF::vector<SF_int> nbc_idx; //!<Indices for Neumann boundary conditions
  sf_vec* dirichlet_bcs_0 = nullptr; //!<Dirichlet boundary conditions, 0 = dbc exists
  sf_vec* dirichlet_bcs_1 = nullptr; //!<Dirichlet boundary conditions but with 1 = dbc exists
  sf_mat* K_eq        = nullptr; //!<stiffness matrix for static equilibrium solver K
  
  mech_bcs dmbcs[10]; //!< Dirichlet boundary condtions
  mech_bcs nmbcs[10]; //!< Neumann boundary conditions

  sf_nl_sol* nonlin_solver = nullptr; //!< the nonlinear solver
    
  // solver config
  double tol = 1e-8; //!<CG stopping tolerance
  int max_it = 300;  //!<maximum number of iterations
  
  void init();
  void rebuild_matrices(MaterialType mtype, FILE_SPEC logger);
  void rebuild_stiffness(MaterialType mtype, FILE_SPEC logger);
  void calc_internal_forces(MaterialType mtype, FILE_SPEC logger);
  void calc_internal_forces_jacobian(MaterialType mtype, FILE_SPEC logger);
  void apply_external_forces(MaterialType mtype, FILE_SPEC logger, sf_mesh surfmesh, mech_bcs nmbcs);
  void solve();
  inline void apply_bcs_forces()
  {
    double * f_int = f_internal->ptr();
    double * f_ext = f_external->ptr();
    double * dbcs = dirichlet_bcs_0->ptr();
    for(int i = 0; i<dirichlet_bcs_0->lsize(); i++)
    {
      if(!dbcs[i])
      {
        f_int[i] = 0.0;
        f_ext[i] = 0.0;
      }
    }
    f_internal->release_ptr(f_int);
    f_external->release_ptr(f_ext);
    dirichlet_bcs_0->release_ptr(dbcs);
  }
  
  inline void apply_bcs_jacobian()
  {
    K_eq->mult_LR(*dirichlet_bcs_0, *dirichlet_bcs_0);
    K_eq->diag_add(*dirichlet_bcs_1);
  }

  inline void update_node_coords()
  {
    sf_mesh & mesh = get_mesh(elasticity_msh);
    /// mappings and numberings
    const SF::vector<mesh_int_t>& alg_nod  = mesh.pl.algebraic_nodes();
    const SF::vector<mesh_int_t>& int_nod = mesh.pl.interface();
    const SF::vector<mesh_int_t>& alg_dsp  = mesh.pl.algebraic_layout();
    const SF::vector<mesh_int_t> & ref_nbr = mesh.get_numbering(SF::NBR_REF);
    const SF::vector<mesh_int_t> & petsc_nbr = mesh.get_numbering(SF::NBR_PETSC);
    
//    SF::vector<SF_int> cidx(mesh.l_numpts * position->dpn);
////    SF::vector<mesh_real_t> pos_vals(mesh.l_numpts * position->dpn);
//    double pos_vals[mesh.l_numpts * position->dpn];
////    SF::vector<SF_int> lidx(mesh.l_numpts * position->dpn);
////    
////    std::iota(lidx.begin(), lidx.end(), 0);
////    
////    /// get idxes
////    SF::canonic_indices(lidx.data(), petsc_nbr, mesh.l_numpts, position->dpn, cidx.data());
//    
//    /// fill cidx vector
//    for (mesh_int_t lvtx = 0; lvtx < mesh.l_numpts; lvtx++)
//    {
//      for(mesh_int_t k = 0; k < position->dpn; k++)
//      {
//        cidx[3 * lvtx + k] = 3 * petsc_nbr[lvtx] + k;
//      }
//    }
//    
//    const SF::vector<mesh_int_t> cidx_c = cidx;
//    
//    position->get(cidx_c, pos_vals);
    
    
    /// set position and reference position vector
//    double * pos = position->ptr();
    mesh_int_t loc_node_mesh;
    mesh_int_t dpn = position->dpn;
    mesh_int_t loc_node_vec;
    
    SF::scattering *atn = get_scattering(elasticity_msh, ALG_TO_NODAL, dpn);
    
    position->forward(*position_nodal, *atn);
    
    double * pos_nod = position_nodal->ptr();
    
    
    for(mesh_int_t lidx = 0; lidx < mesh.l_numpts * dpn; lidx++)
    {
      mesh.xyz_Euler[lidx] = pos_nod[lidx];
    }
    position_nodal->release_ptr(pos_nod);
  }
  
  inline void init_node_coords()
  {
    sf_mesh & mechanics_mesh = get_mesh(elasticity_msh);
    /// mappings and numberings
    const SF::vector<mesh_int_t>& alg_nod  = mechanics_mesh.pl.algebraic_nodes();
    const SF::vector<mesh_int_t>& int_nod = mechanics_mesh.pl.interface();
    const SF::vector<mesh_int_t>& alg_dsp  = mechanics_mesh.pl.algebraic_layout();
    const SF::vector<mesh_int_t> & nbr = mechanics_mesh.get_numbering(SF::NBR_REF);
    
    double * pos = position->ptr();
    double * ref_pos = reference_pos->ptr();
//    double *pos_nodal = position_nodal->ptr();
    /// set position and reference position vector
    mesh_int_t loc_node_mesh;
    mesh_int_t loc_node_vec;
    for(mesh_int_t i = 0; i < alg_nod.size(); i++){
      loc_node_mesh = alg_nod[i];
      loc_node_vec = i;
      for(mesh_int_t k = 0; k < position->dpn; k++){
        pos[3 * loc_node_vec + k] = mechanics_mesh.xyz_Euler[loc_node_mesh * 3 + k];
//        pos_nodal[3 * loc_node_vec + k] = mechanics_mesh.xyz_Euler[loc_node_mesh * 3 + k];
        ref_pos[3 * loc_node_vec + k] = mechanics_mesh.xyz[loc_node_mesh * 3 + k];
      }
    }
    position->release_ptr(pos);
    reference_pos->release_ptr(ref_pos);
    position->finish_assembly();
    reference_pos->finish_assembly();
  }
    private:
    void setup_nonlinear_solver(FILE_SPEC logger);
};

class Mechanics : public Basic_physic
{
  public:
  MaterialType mtype;
  equilibrium_solver eq_solver;
  sf_mesh surfmesh;
    
  /// class handling the igb output
  igb_output_manager output_manager;
    
  /**
  * @brief Most of the initialization is done with initialize()
  */
  Mechanics()
  {
      name = "Mechanics";
  }
  /**
  * @brief Initialize the Mechanics.
  *
  * This could also be a constructor. But it might be better to expicitely
  * call the initialization. This way we can instanciate electrics structs
  * without calling the full setup as well.
  *
  */
  void initialize();

  void destroy();

  // This funcs from the Basic_physic interface are currently empty
  void compute_step();
  void output_step();


  ~Mechanics();

  /// figure out current value of a signal linked to a given timer
  double timer_val(const int timer_id);

  /// figure out units of a signal linked to a given timer
  std::string timer_unit(const int timer_id);


  private:

  /// set up the nonlinear solver
  void setup_solvers();

  /// VecScatter setup
  void setup_mappings();

  /// do the setup for the output_step
  void setup_output();

  /// dump the matrices (not yet implemented)
  void dump_matrices();

  /// checkpointing not yet implemented
  void checkpointing();
    
  /// register material model
  void register_material_model();
};
}   // namespace opencarp

#endif
