group { 
  #Authors ::= Stefano Severi, Matteo Fantini, Lara A. Charawi, Dario DiFrancesco
  #Year ::= 2012
  #Title ::= An updated computational model of rabbit sinoatrial action potential to investigate the mechanisms of heart rate modulation
  #Journal ::= The Journal of Physiology, 590(18):4483-99
  #DOI ::= 10.1113/jphysiol.2012.229435
} .reference();

V; .external(Vm); .nodal();
Iion; .external(); .nodal();

#constants
Iva_3_uM = 0; .units(unitless);
Cs_5_mM = 0; .units(unitless);
ACh = 0; .units(mmol/L);
Iso_1_uM = 0; .units(unitless);
Iso_cas = 0; .units(unitless);
BAPTA_10_mM = 0; .units(unitless);
CCh_cas = 0; .units(unitless);

#cell compartments
C = 3.2e-5; .units(uF);
Vjsr_part = 0.0012; .units(unitless);
Vi_part = 0.46; .units(unitless);
Vnsr_part = 0.0116; .units(unitless);
Vmyto = 633.4; .units(um^3);
R_cell = 4; .units(um);
L_cell = 70; .units(um);
L_sub = 0.02; .units(um);

#fixed ion concentations
Cao = 1.8; .units(mmol/L);    # 2.0 BY model    # 1.8 severi model
Ko = 5.4; .units(mmol/L);
Ki = 140; .units(mmol/L);
Nao = 140; .units(mmol/L);
Mgi = 2.5; .units(mmol/L);

#sarcolemmal ion currents and conductances
Km_f = 45; .units(mmol/L);
Kif = 24.4; .units(mV);    # 24.4 BY model    # 26.26 adjustment 2-step SLSQP    # 24.4 BY max. opt. adjustment 2-step SLSQP
K05if = 17.33/600; .units(mmol/L);    # 17.57/600 BY model    # 17.8741/600 adjustment 2-step SLSQP    # BY max. opt. 17.33/600 adjustment 2-step SLSQP
nif = 9.281; .units(unitless);
Vif05 = -52.5; .units(mV);    # -64.0 BY model    # -52.5 severi model
P_CaL = 0.2; .units(nA*L/mmol);
P_CaT = 0.02; .units(nA*L/mmol);
GKr_max = 0.0021637; .units(uS);    # 0.1 nS/pF BY model    # 0.0021637 severi model
Gto_max = 0.002; .units(uS);    # 0.252 nS/pF BY model    # 0.002 severi mdoel
INaK_max = 0.063; .units(nA);    # 2.88 pA/pF BY model    # 0.063 severi model
GKACh_max = 0.00864; .units(uS);    # 0.31332 nS/pF BY model    # 0.0864 severi model
GNa_max = 0.0125; .units(uS);    # 0.00486 nS/pF BY model    # 0.0125 severi model
shift = 0; .units(mV);
delta_m = 1e-5; .units(mV);

#modulation of sarcolemmal ion currents by ions
Km_fCa = 0.00035; .units(mmol/L);
Km_Kp = 1.4; .units(mmol/L);
Km_Nap = 14; .units(mmol/L);
alpha_fCa = 0.01e-3; .units(unitless/ms);    # 0.021 BY model    # 0.01e-3 severi model

#NaCa exchanger
K_NaCa = 4; .units(nA);    # 225 pA/pF BY model    # 4 severi model
Qci = 0.1369; .units(unitless);
Qco = 0; .units(unitless);
Qn = 0.4315; .units(unitless);
K1ni = 395.3; .units(mmol/L);
K2ni = 2.289; .units(mmol/L);
K3ni = 26.44; .units(mmol/L);
K1no = 1628; .units(mmol/L);
K2no = 561.4; .units(mmol/L);
K3no = 4.663; .units(mmol/L);
Kci = 0.0207; .units(mmol/L);
Kco = 3.663; .units(mmol/L);
Kcni = 26.44; .units(mmol/L);

#Ca2+ diffusion
tau_dif_Ca = 0.04; .units(ms);
tau_tr = 40; .units(ms);

#SR Ca2+ ATPase function
K_up = 0.0006; .units(mmol/L);
P_up_basal = 0.012; .units(mmol/L/ms);    # 0.0096 BY model    # 0.012 severi model

#RyR function
RyR_min = 0.0127; .units(unitless);
RyR_max = 0.02; .units(unitless);
k05RyR = 0.682891; .units(unitless);    # 0.7 BY model    # 0.682891 adjustment SLSQP
nRyR = 9.773; .units(unitless);
# koCa = 10.0; .units(L^2/mmol^2/ms);
koCa_max = 10.0; .units(L^2/mmol^2/ms);    # koCa constant in severi model 
kiCa = 0.500; .units(L/mmol/ms);
kim = 0.005; .units(unitless/ms);
kom = 0.06; .units(unitless/ms);
EC50_SR = 0.45; .units(mmol/L);
MaxSR = 15; .units(unitless);    # 13 BY model    # 15 severi model
MinSR = 1; .units(unitless);
HSR = 2.5; .units(unitless);    # 3 BY model    # 2.5 Severi model
ks = 250e3; .units(unitless/ms);    # 400e3 BY model    # 250e3 severi model

#Ca2+ and Mg2+ buffering
kb_CM = 0.542; .units(unitless/ms);    # 0.5421 BY model    # 0.542 severi model
kb_CQ = 0.445; .units(unitless/ms);
kf_CM = 227.7; .units(L/mmol/ms);
kf_CQ = 0.534; .units(L/mmol/ms);
TC_tot = 0.031; .units(mmol/L);    # 0.042 BY model    # 0.031 severi model
CQ_tot = 10; .units(mmol/L);
CM_tot = 0.045; .units(mmol/L);
TMC_tot = 0.062; .units(mmol/L);
kf_TC = 88.8; .units(L/mmol/ms);
kf_TMM = 2.277; .units(L/mmol/ms);
kf_TMC = 227.7; .units(L/mmol/ms);
kb_TC = 0.446; .units(unitless/ms);
kb_TMC = 0.00751; .units(unitless/ms);
kb_TMM = 0.751; .units(unitless/ms);
kfBAPTA = 940; .units(L/mmol/ms);
kbBAPTA = 0.11938; .units(unitless/ms);
T_Ca_dynamics = 6.928e3; .units(ms);

#mitochondrial parameters
P_Ca = 0.256; .units(unitless/ms);
phi_m = 154.226; .units(mV);
alpha_m = 0.2; .units(unitless);
alpha_e = 0.341; .units(unitless);
K_Cam = 0.003; .units(mmol/L);
Q_mo = 0.006144; .units(mmol/L/ms);

#force parameters (not used)
SL = 1.75e-3; .units(unitless/mm);
SL0 = 0.8e-3; .units(unitless/mm);
Nc = 2e13; .units(unitless/mm^2);
Fk0 = 350; .units(unitless/mmol/L);
Fk1 = 3000; .units(unitless/mmol/L);
FN = 3.5; .units(unitless);
Fk05 = 2.5e9; .units(unitless/mm^3);
Fkl = 60; .units(L/mmol*ms);
Ff = 0.04; .units(unitless/ms);
Fg0 = 0.03; .units(unitless/ms);
Fgl = 4.4e9; .units(unitless/mm);

#AC-cAMP-PKA signalling
K_ACI = 0.016/60000; .units(unitless/ms);
K_AC = 0.0735/60000; .units(unitless/ms);
K_Ca = 0.0000913802; .units(mmol/L);    # 0.000178 BY model    # 0.0000913802 manual adjustment
K_ACCa = 0.000024; .units(mmol/L);
kPKA = 0.00025; .units(mmol/L*ms);
kPKA_cAMP = 284.5/600; .units(mmol/L);
nPKA = 5.; .units(unitless);
PKAtot = 1.; .units(mmol/L);    # adopted from BY model
PKItot = 0.3; .units(mmol/L);    # adopted from BY model

#phosphorylation parameters
kPLBp = 52.25/60000; .units(unitless/ms);
nPLB = 1.; .units(unitless);
kPKA_PLB = 1.610336; .units(unitless);    # 1.651 BY model    # 1.610336 adjustment SLSQP
PP1 = 0.89e-3; .units(mmol/L);
kPP1 = 23.575e3/60000; .units(L/mmol*ms);    # .units(unitless/umol/min);
kPP1_PLB = 0.06967; .units(unitless);

#ATP patameters    # adopted from BY model matlab code
ATPi_max = 2.533; .units(mmol/L);
kATP = 6142; .units(unitless);
kATP05 = 6724; .units(unitless);
cAMPb = 20/600; .units(mmol/L);
nATP = 3.36; .units(unitless);
KATP_min = 6034; .units(unitless);

PI = 3.141592653589793238462643383279502884; .units(unitless);

#initial values
V_init = -52; .units(mV);    # -65 BY model    # -52 severi model
Nai_init = 7.5; .units(mmol/L);    # 10 BY model (const)    # 7.5 severi model
y_init = 0.181334538702451; .units(unitless);    # 1 BY model    # 0.181334538702451 severi model
m_init = 0.440131579215766; .units(unitless);
h_init = 1.3676940140066e-5; .units(unitless);
dL_init = 0; .units(unitless);
fL_init = 0.497133507285601; .units(unitless);    # 1 BY model    # 0.497133507285601 severi model
fCa_init = 0.697998543259722; .units(unitless);    # 1 BY model    # 0.697998543259722 severi model
dT_init = 0; .units(unitless);
fT_init = 0; .units(unitless);    # 1 BY model    # 0 severi model
R_Ca_SR_release_init = 0.912317231017262; .units(unitless);    # 0.749 BY model (R)    # 0.912317231017262 severi model
O_init = 1.7340201253e-7; .units(unitless);    # 3.4e-6 BY model    # 1.7340201253e-7 severi model
I_init = 7.86181717518e-8; .units(unitless);    # 1.1e-6 BY model    # 7.86181717518e-8 severi model
RI_init = 0.211148145512825; .units(unitless);    # 0.25 BY model    # 0.211148145512825 severi model
fTMM_init = 0.501049376634; .units(unitless);
fCMi_init = 0.0373817991524254; .units(unitless);    # 0.042 BY model    # 0.0373817991524254 severi model
fCMs_init = 0.054381370046; .units(unitless);    # 0.089 BY model    # 0.054381370046 severi model
fTC_init = 0.0180519400676086; .units(unitless);
fTMC_init = 0.281244308217086; .units(unitless);
fCQ_init = 0.299624275428735; .units(unitless);    # 0.032 BY model    # 0.299624275428735 severi model
Cai_init = 1e-5; .units(mmol/L);    # 1e-4 BY model    # 1e-5 severi model
Cam_init = 5e-5; .units(mmol/L);    # mitochondrial Ca2+
Ca_nsr_init = 1.05386465080816; .units(mmol/L);    # 1.35 BY model    # 1.05386465080816
Ca_jsr_init = 0.316762674605; .units(mmol/L);    # 0.029 BY model    # 0.316762674605 severi model
Ca_sub_init = 1e-5; .units(mmol/L);    # 0.000223 BY model    # 1e-5 severi model
fBAPTA_init = 0; .units(mmol/L);
fBAPTA_sub_init = 0; .units(mmol/L);
q_init = 0.506139850982478; .units(unitless);    # 1 BY model    # 0.506139850982478 severi model
r_init = 0.0144605370597924; .units(unitless);    # 0 BY model    # 0.0144605370597924 severi model
paS_init = 0.322999177802891; .units(unitless);    # 0 BY model    # 0.322999177802891 severi model
paF_init = 0.0990510403258968; .units(unitless);    # 0 BY model    # 0.0990510403258968 severi model
piy_init = 0.705410877258545; .units(unitless);    # 1 BY mdoel (pai)    # 0.705410877258545 severi model
n_init = 0; .units(unitless);    # BY model (N)
a_init = 0; .units(unitless);
cAMP_init = 19.73/600; .units(mmol/L);
PLBp_init = 0.23; .units(unitless);    # activation level
A_init = 0.06; .units(unitless);
TT_init = 0.02; .units(unitless);
U_init = 0.06; .units(unitless);

#membrane
Itot = (If+IKr+IKs+Ito+INaK+INaCa+INa+ICaL+ICaT+IKACh); .units(nA);
Iion = -((-Itot/C)); .units(nA/uF);

#ionic values
E_Na = (RTONF*log((Nao/Nai_BAPTA))); .units(mV);
E_K = (RTONF*log((Ko/Ki))); .units(mV);
E_Ca = (0.5*RTONF*log((Cao/Ca_sub))); .units(mV);
R = 8314.472; .units(J/kmol/K);    # 8314.4 BY model    # 8314.472 severi mdoel
T = 310; .units(K);    # 310.15 BY model    # 310 severi model
F = 96485.3415; .units(A*s/mol);    # 96485 BY model    # 96485.3415 severi model
RTONF = ((R*T)/F); .units(mV);

#Nai concentration
diff_Nai = ((-1.*(INa+IfNa+IsiNa+(3.*INaK)+(3.*INaCa)))/(1.*(Vi*0.000000001+Vsub*0.000000001)*(F*1000))); .units(mmol/L/ms);
Nai_BAPTA = ((BAPTA_10_mM>0.) ? 7.5 : Nai); .units(mmol/L);

#If
Gf_Na_max = ((Iva_3_uM>=1.) ? (0.03*(1. - 0.66)) : 0.03); .units(uS);
Gf_K_max = ((Iva_3_uM>=1.) ? (0.03*(1. - 0.66)) : 0.03); .units(uS);
ICs_on_Icontrol = ((Cs_5_mM>=1.) ? ((10.6015/5.)/((10.6015/5.)+exp(((-0.71*V)/25.)))) : 1.); .units(unitless);
IfNa = ((((y*y)*Ko)/(Ko+Km_f))*Gf_Na_max*(V - E_Na)*ICs_on_Icontrol); .units(nA);
IfK = ((((y*y)*Ko)/(Ko+Km_f))*Gf_K_max*(V - E_K)*ICs_on_Icontrol); .units(nA);
If = (IfNa+IfK); .units(nA);

#If y gate
diff_y = (If_y_gate_y_infinity - y)/If_y_gate_tau_y;
ACh_shift = ((ACh>0.) ? (-1. - ((9.898*pow((1.*ACh),0.618))/(pow((1.*ACh),0.618)+0.00122423))) : 0.); .units(mV);
Iso_shift = ((Iso_1_uM>0.) ? 7.5 : 0.); .units(mV);
Iso_shift_cas_If_y_gate = ((Iso_cas>-0.1) ? (Kif * (pow(cAMP,nif) / (pow(K05if,nif) + pow(cAMP,nif))) -18.76) : 0.); .units(mV);
# Iso_shift_cas_If_y_gate = 0; .units(mV); 
If_y_gate_tau_y = (0.7166529/((0.0708*exp((-(((V+5.) - ACh_shift) - Iso_shift)/20.2791)))+(10.6*exp((((V - ACh_shift) - Iso_shift)/18.)))))*1000; .units(ms);
If_y_gate_y_infinity = (1./(1.+exp(((((V-Vif05) - ACh_shift) - Iso_shift - Iso_shift_cas_If_y_gate)/9.)))); .units(unitless);

#INaK
Iso_increase = ((Iso_1_uM>0.) ? 1.2 : 1.); .units(unitless);
Iso_inc_cas_INaK = ((Iso_cas>-0.1) ? (-0.2152 + 0.435692 * pow(PKA,10.0808) / (pow(0.719701,10.0808) + pow(PKA,10.0808))) : 0.); .units(unitless);    # assumption: similar to ICaL # 0.435692, 0.719701 calculation SLSQP
# Iso_inc_cas_INaK = 0; .units(unitless);
INaK = ((1+Iso_inc_cas_INaK)*Iso_increase*INaK_max*(1/(1.+pow((Km_Kp/Ko),1.2)))*(1/(1.+pow((Km_Nap/Nai_BAPTA),1.3)))*(1/(1.+exp((-((V - E_Na)+110.)/20.))))); .units(nA);

#INaCa
INaCa = ((K_NaCa*((x2*k21) - (x1*k12)))/(x1+x2+x3+x4)); .units(nA);
#enable/disable reverse mode of NCX
# if (((K_NaCa*((x2*k21) - (x1*k12)))/(x1+x2+x3+x4)) < 0) {
# 	INaCa = ((K_NaCa*((x2*k21) - (x1*k12)))/(x1+x2+x3+x4)); .units(nA);
# }
# else {
# 	INaCa = 0; .units(nA);
# }
x1 = ((k41*k34*(k23+k21))+(k21*k32*(k43+k41))); .units(unitless);
x2 = ((k32*k43*(k14+k12))+(k41*k12*(k34+k32))); .units(unitless);
x3 = ((k14*k43*(k23+k21))+(k12*k23*(k43+k41))); .units(unitless);
x4 = ((k23*k34*(k14+k12))+(k14*k21*(k34+k32))); .units(unitless);
k43 = (Nai_BAPTA/(K3ni+Nai_BAPTA)); .units(unitless);
k12 = (((Ca_sub/Kci)*exp(((-Qci*V)/RTONF)))/di); .units(unitless);
k14 = (((((Nai_BAPTA/K1ni)*Nai_BAPTA)/K2ni)*(1.+(Nai_BAPTA/K3ni))*exp(((Qn*V)/(2.*RTONF))))/di); .units(unitless);
k41 = exp(((-Qn*V)/(2.*RTONF))); .units(unitless);
di = (1.+((Ca_sub/Kci)*(1.+exp(((-Qci*V)/RTONF))+(Nai_BAPTA/Kcni)))+((Nai_BAPTA/K1ni)*(1.+((Nai_BAPTA/K2ni)*(1.+(Nai_BAPTA/K3ni)))))); .units(unitless);
k34 = (Nao/(K3no+Nao)); .units(unitless);
k21 = (((Cao/Kco)*exp(((Qco*V)/RTONF)))/var_do); .units(unitless);
k23 = (((((Nao/K1no)*Nao)/K2no)*(1.+(Nao/K3no))*exp(((-Qn*V)/(2.*RTONF))))/var_do); .units(unitless);
k32 = exp(((Qn*V)/(2.*RTONF))); .units(unitless);
var_do = (1.+((Cao/Kco)*(1.+exp(((Qco*V)/RTONF))))+((Nao/K1no)*(1.+((Nao/K2no)*(1.+(Nao/K3no)))))); .units(unitless);

#INa
E_mh = (RTONF*log(((Nao+(0.12*Ko))/(Nai_BAPTA+(0.12*Ki))))); .units(mV);
INa = (GNa_max*(m*m*m)*h*(V - E_mh)); .units(nA);

#INa m gate
diff_m = INa_m_gate_alpha_m*(1.00000 - m) -  INa_m_gate_beta_m*m;
E0_m = (V+41.); .units(mV);
INa_m_gate_alpha_m = ((fabs(E0_m)<delta_m) ? 2000. : ((200.*E0_m)/(1. - exp((-0.1*E0_m)))))/1000; .units(unitless/ms);
INa_m_gate_beta_m = (8000.*exp((-0.056*(V+66.))))/1000; .units(unitless/ms);

#INa h gate
diff_h = INa_h_gate_alpha_h*(1.00000 - h) -  INa_h_gate_beta_h*h;
INa_h_gate_alpha_h = (20.*exp((-0.125*(V+75.))))/1000; .units(unitless/ms);
INa_h_gate_beta_h = (2000./((320.*exp((-0.1*(V+75.))))+1.))/1000; .units(unitless/ms);

#ICaL
Iso_increase_ICaL = ((Iso_1_uM>0.) ? 1.23 : 1.); .units(unitless);
Iso_inc_cas_ICaL = ((Iso_cas>-0.1) ? (-0.2152 + 0.470657 * pow(PKA,10.0808) / (pow(0.730287,10.0808) + pow(PKA,10.0808))) : 0.); .units(unitless);    # 1.6913, 0.8836 BY model    # 0.470657, 0.730287 adjustment SLSQP
# Iso_inc_cas_ICaL = 0; .units(unitless);
ACh_block = ((0.31*ACh)/(ACh+0.00009)); .units(unitless);
IsiCa = (((2.*P_CaL*(V - 0.))/(RTONF*(1. - exp(((-1.*(V - 0.)*2.)/RTONF)))))*(Ca_sub - (Cao*exp(((-2.*(V - 0.))/RTONF))))*dL*fL*fCa); .units(nA);
IsiK = (((0.000365*P_CaL*(V - 0.))/(RTONF*(1. - exp(((-1.*(V - 0.))/RTONF)))))*(Ki - (Ko*exp(((-1.*(V - 0.))/RTONF))))*dL*fL*fCa); .units(nA);
IsiNa = (((0.0000185*P_CaL*(V - 0.))/(RTONF*(1. - exp(((-1.*(V - 0.))/RTONF)))))*(Nai_BAPTA - (Nao*exp(((-1.*(V - 0.))/RTONF))))*dL*fL*fCa); .units(nA);
ICaL = ((IsiCa+IsiK+IsiNa)*(1. + Iso_inc_cas_ICaL)*(1. - ACh_block)*1.*Iso_increase_ICaL); .units(nA);

#ICaL dL gate
Iso_shift_ICaL_dL_gate = ((Iso_1_uM>0.) ? -8. : 0.); .units(mV);
Iso_shift_cas_ICaL_dL_gate = ((Iso_cas>-0.1) ? (-(24.4 * 1.128124 * pow(PKA,9.281)/(pow(0.661450,9.281) + pow(PKA,9.281)) - 18.76)) : 0.); .units(mV);    # assumption: similar to If & cAMP # 1.128124, 0.661450 calculation SLSQP
# Iso_shift_cas_ICaL_dL_gate = 0; .units(mV);
Iso_slope = ((Iso_1_uM>0.) ? 0.69 : 1.); .units(unitless);
Iso_slope_cas_ICaL = ((Iso_cas>-0.1) ? (-1.4762 * PKA + 2.1219) : 1.); .units(unitless);    # assumption: linear
# Iso_slope_cas_ICaL = 1.0; .units(unitless);
diff_dL = ((ICaL_dL_gate_dL_infinity - dL)/ICaL_dL_gate_tau_dL); .units(unitless/ms);
ICaL_dL_gate_dL_infinity = (1./(1.+exp((-((V+20.3) - Iso_shift_ICaL_dL_gate - Iso_shift_cas_ICaL_dL_gate)/(Iso_slope*Iso_slope_cas_ICaL*4.2))))); .units(unitless);
ICaL_dL_gate_tau_dL = (0.001/(ICaL_dL_gate_alpha_dL+ICaL_dL_gate_beta_dL)); .units(ms);
ICaL_dL_gate_alpha_dL = (((-0.02839*((adVm+41.8) - Iso_shift_ICaL_dL_gate - Iso_shift_cas_ICaL_dL_gate))/(exp((-((adVm+41.8) - Iso_shift_ICaL_dL_gate - 
						Iso_shift_cas_ICaL_dL_gate)/2.5)) - 1.)) - ((0.0849*((adVm+6.8) - Iso_shift_ICaL_dL_gate - Iso_shift_cas_ICaL_dL_gate)) / 
						(exp((-((adVm+6.8) - Iso_shift_ICaL_dL_gate - Iso_shift_cas_ICaL_dL_gate)/4.8)) - 1.)))/1000; .units(unitless/ms);
adVm = ((V==-41.8) ? -41.80001 : ((V==0.) ? 0. : ((V==-6.8) ? -6.80001 : V))); .units(mV);
ICaL_dL_gate_beta_dL = ((0.01143*((bdVm+1.8) - Iso_shift_ICaL_dL_gate - Iso_shift_cas_ICaL_dL_gate))/(exp((((bdVm+1.8) - Iso_shift_ICaL_dL_gate - 
					   Iso_shift_cas_ICaL_dL_gate)/2.5)) - 1.))/1000; .units(unitless/ms);
bdVm = ((V==-1.8) ? -1.80001 : V); .units(mV);

#ICaL fL gate
diff_fL = (ICaL_fL_gate_fL_infinity - fL)/ICaL_fL_gate_tau_fL;
ICaL_fL_gate_fL_infinity = (1./(1.+exp(((V+37.4)/5.3)))); .units(unitless);
ICaL_fL_gate_tau_fL = (0.001*(44.3+(230.*exp(-(((V+36.)/10.)*((V+36.)/10.))))))*1000; .units(ms);

#ICaL fCa gate
diff_fCa = (ICaL_fCa_gate_fCa_infinity - fCa)/ICaL_fCa_gate_tau_fCa;
ICaL_fCa_gate_fCa_infinity = (Km_fCa/(Km_fCa+Ca_sub)); .units(unitless);
ICaL_fCa_gate_tau_fCa = ((0.001*ICaL_fCa_gate_fCa_infinity)/alpha_fCa); .units(ms);

#ICaT
ICaT = (((2.*P_CaT*V)/(RTONF*(1. - exp(((-1.*V*2.)/RTONF)))))*(Ca_sub - (Cao*exp(((-2.*V)/RTONF))))*dT*fT); .units(nA);

#ICaT dT gate
diff_dT = ((ICaT_dT_gate_dT_infinity - dT)/ICaT_dT_gate_tau_dT); .units(unitless/ms);
ICaT_dT_gate_dT_infinity = (1./(1.+exp((-(V+38.3)/5.5)))); .units(unitless);
ICaT_dT_gate_tau_dT = (0.001/((1.068*exp(((V+38.3)/30.)))+(1.068*exp((-(V+38.3)/30.)))))*1000; .units(ms);

#ICaT fT gate
diff_fT = (ICaT_fT_gate_fT_infinity - fT)/ICaT_fT_gate_tau_fT;
ICaT_fT_gate_fT_infinity = (1./(1.+exp(((V+58.7)/3.8)))); .units(unitless);
ICaT_fT_gate_tau_fT = (1./((16.67*exp((-(V+75.)/83.3)))+(16.67*exp(((V+75.)/15.38)))))*1000; .units(ms);

#Ca SR release
j_SRCarel = (ks*O*(Ca_jsr - Ca_sub)); .units(mmol/L/ms);
kCaSR = (MaxSR - ((MaxSR - MinSR)/(1.+pow((EC50_SR/Ca_jsr),HSR)))); .units(unitless);
koCa = ((Iso_cas>-0.1) ? (koCa_max * (RyR_min - RyR_max * pow(PKA,nRyR) / (pow(k05RyR,nRyR) + pow(PKA,nRyR))+1)) : 10.); .units(L^2/mmol^2/ms);
koSRCa = (koCa/kCaSR); .units(L^2/mmol^2/ms);
kiSRCa = (kiCa*kCaSR); .units(L/mmol/ms);
diff_R_Ca_SR_release = (((kim*RI) - (kiSRCa*Ca_sub*R_Ca_SR_release)) - ((koSRCa*(Ca_sub*Ca_sub)*R_Ca_SR_release) - (kom*O))); .units(unitless/ms);
diff_O = (((koSRCa*(Ca_sub*Ca_sub)*R_Ca_SR_release) - (kom*O)) - ((kiSRCa*Ca_sub*O) - (kim*I))); .units(unitless/ms);
diff_I = (((kiSRCa*Ca_sub*O) - (kim*I)) - ((kom*I) - (koSRCa*(Ca_sub*Ca_sub)*RI))); .units(unitless/ms);
diff_RI = (((kom*I) - (koSRCa*(Ca_sub*Ca_sub)*RI)) - ((kim*RI) - (kiSRCa*Ca_sub*R_Ca_SR_release))); .units(unitless/ms);

#Ca intracellular fluxes
b_up = ((Iso_1_uM>0.) ? -0.25 : ((ACh>0.) ? ((0.7*ACh)/(0.00009+ACh)) : 0.)); .units(unitless);
P_up = (P_up_basal*(1. - b_up)); .units(mmol/L/ms);
j_Ca_dif = ((Ca_sub - Cai)/tau_dif_Ca); .units(mmol/L/ms);
F_PLBp = ((PLBp>0.23) ? (3.3931 * pow(PLBp,4.0695) / (pow(0.2805,4.0695) + pow(PLBp,4.0695))) : (1.698 * pow(PLBp,13.5842) / 
		 (pow(0.2240,13.5842) + pow(PLBp,13.5842)))); .units(unitless);
j_up = ((Iso_cas>-0.1) ? (0.9*P_up * F_PLBp / (1. + K_up / Cai)) : (P_up / (1. + (K_up / Cai)))); .units(mmol/L/ms);
# j_up = P_up / (1. + (K_up / Cai)); .units(mmol/L/ms);
j_tr = ((Ca_nsr - Ca_jsr)/tau_tr); .units(mmol/L/ms);
# j_uni = P_Ca*2*phi_m/(26.72654982639788)*(alpha_m*Cam*exp(-2*phi_m/(26.72654982639788))-alpha_e*Cai)/(exp(-2*phi_m/(26.72654982639788))-1); .units(mmol/L/ms);
j_uni = P_Ca*2*phi_m/(RTONF)*(alpha_m*Cam*exp(-2*phi_m/(RTONF))-alpha_e*Cai)/(exp(-2*phi_m/(RTONF))-1); .units(mmol/L/ms);
j_NaCam = Q_mo*(Cam/(K_Cam+Cam)); .units(mmol/L/ms);

#Ca buffering
diff_fTC = delta_fTC; .units(unitless/ms);
delta_fTC = ((kf_TC*Cai*(1. - fTC)) - (kb_TC*fTC)); .units(unitless/ms);
# delta_fTC = Fkl * Cai * (1 - A - TT) - k_l * (A + TT); .units(unitless/ms);
diff_fTMC = delta_fTMC; .units(unitless/ms);
delta_fTMC = ((kf_TMC*Cai*(1. - (fTMC+fTMM))) - (kb_TMC*fTMC)); .units(unitless/ms);
diff_fTMM = delta_fTMM; .units(unitless/ms);
delta_fTMM = ((kf_TMM*Mgi*(1. - (fTMC+fTMM))) - (kb_TMM*fTMM)); .units(unitless/ms);
diff_fCMi = delta_fCMi; .units(unitless/ms);
delta_fCMi = ((kf_CM*Cai*(1. - fCMi)) - (kb_CM*fCMi)); .units(unitless/ms);
diff_fCMs = delta_fCMs; .units(unitless/ms);
delta_fCMs = ((kf_CM*Ca_sub*(1. - fCMs)) - (kb_CM*fCMs)); .units(unitless/ms);
diff_fCQ = delta_fCQ; .units(unitless/ms);
delta_fCQ = ((kf_CQ*Ca_jsr*(1. - fCQ)) - (kb_CQ*fCQ)); .units(unitless/ms);

#Ca dynamics
BAPTA = ((BAPTA_10_mM>0. and t>T_Ca_dynamics) ? 10. : 0.); .units(mmol/L);
diff_Cai = ((((1.*(j_Ca_dif*Vsub-j_up*Vnsr))/Vi) - ((CM_tot*delta_fCMi)+(TC_tot*delta_fTC)+(TMC_tot*delta_fTMC))) - ((kfBAPTA*Cai*(BAPTA - fBAPTA)) - 
		   (kbBAPTA*fBAPTA))); .units(mmol/L/ms);
# diff_Cai = (((j_Ca_dif*Vsub-j_up*Vnsr)/Vi) - (CM_tot*delta_fCMi+TC_tot*delta_fTC) - delta_Cam) - ((kfBAPTA*Cai*(BAPTA - fBAPTA)) - (kbBAPTA*fBAPTA)); .units(mmol/L/ms);
diff_fBAPTA = ((kfBAPTA*Cai*(BAPTA - fBAPTA)) - (kbBAPTA*fBAPTA)); .units(mmol/L/ms);
diff_Ca_sub = ((((j_SRCarel*Vjsr)/Vsub) - ((((IsiCa+ICaT) - (2.*INaCa))/(2.*(F*1000)*Vsub*0.000000001))+j_Ca_dif+(CM_tot*delta_fCMs))) - 
			  ((kfBAPTA*Ca_sub*(BAPTA - fBAPTA_sub)) - (kbBAPTA*fBAPTA_sub))); .units(mmol/L/ms);
diff_fBAPTA_sub = ((kfBAPTA*Ca_sub*(BAPTA - fBAPTA_sub)) - (kbBAPTA*fBAPTA_sub)); .units(mmol/L/ms);
diff_Ca_nsr = (j_up - ((j_tr*Vjsr)/Vnsr)); .units(mmol/L/ms);
diff_Ca_jsr = (j_tr - (j_SRCarel+(CQ_tot*delta_fCQ))); .units(mmol/L/ms);
diff_Cam = (Vmyto/Vi) * (j_uni - j_NaCam); .units(mmol/L/ms);
delta_Cam = diff_Cam; .units(mmol/L/ms);

#cell parameters
Vcell = (PI*(R_cell*R_cell)*L_cell); .units(um^3);    # 3.5185838 [pL]
Vsub = (2.*PI*L_sub*(R_cell - (L_sub/2.))*L_cell); .units(um^3);    # 0.035097874 [pL]
Vjsr = (Vjsr_part*Vcell); .units(um^3);    # 0.042 [pL]
Vi = ((Vi_part*Vcell) - Vsub); .units(um^3);    # 1.5835 [pL]
Vnsr = (Vnsr_part*Vcell); .units(um^3);    # 0.0408 [pL]

#Ito
Ito = (Gto_max*(V - E_K)*q*r); .units(nA);

#Ito q gate
diff_q = (Ito_q_gate_q_infinity - q)/Ito_q_gate_tau_q;
Ito_q_gate_q_infinity = (1./(1.+exp(((V+49.)/13.)))); .units(unitless);
Ito_q_gate_tau_q = (0.001*0.6*((65.17/((0.57*exp((-0.08*(V+44.))))+(0.065*exp((0.1*(V+45.93))))))+10.1))*1000; .units(ms);

#Ito r gate
diff_r = (Ito_r_gate_r_infinity - r)/Ito_r_gate_tau_r;
Ito_r_gate_r_infinity = (1./(1.+exp((-(V - 19.3)/15.)))); .units(unitless);
Ito_r_gate_tau_r = (0.001*0.66*1.4*((15.59/((1.037*exp((0.09*(V+30.61))))+(0.369*exp((-0.12*(V+23.84))))))+2.98))*1000; .units(ms);

#IKr
IKr = (GKr_max*(V - E_K)*((0.9*paF)+(0.1*paS))*piy); .units(nA);

#IKr pa gate
IKr_pa_gate_alpha_paF = ((1./(1.+exp((-(V+23.2)/6.6))))/(0.84655354/((37.2*exp((V/11.9)))+(0.96*exp((-V/18.5))))))/1000; .units(unitless/ms);
IKr_pa_gate_beta_paF = (4.*((((37.2*exp((V/15.9)))+(0.96*exp((-V/22.5))))/0.84655354) - ((1./(1.+exp((-(V+23.2)/10.6))))/
					   (0.84655354/((37.2*exp((V/15.9)))+(0.96*exp((-V/22.5))))))))/1000; .units(unitless/ms);
IKr_pa_gate_pa_infinity = (1./(1.+exp((-(V+14.8)/8.5)))); .units(unitless);
IKr_pa_gate_tau_paS = (0.846554/((4.2*exp((V/17.)))+(0.15*exp((-V/21.6)))))*1000; .units(ms);
IKr_pa_gate_tau_paF = (1./((30.*exp((V/10.)))+exp((-V/12.))))*1000; .units(ms);
diff_paS = ((IKr_pa_gate_pa_infinity - paS)/IKr_pa_gate_tau_paS); .units(unitless/ms);
diff_paF = ((IKr_pa_gate_pa_infinity - paF)/IKr_pa_gate_tau_paF); .units(unitless/ms);

#IKr pi gate (piy due to constant pi)
IKr_pi_gate_tau_pi = (1./((100.*exp((-V/54.645)))+(656.*exp((V/106.157)))))*1000; .units(ms);
IKr_pi_gate_pi_infinity = (1./(1.+exp(((V+28.6)/17.1)))); .units(unitless);
diff_piy = ((IKr_pi_gate_pi_infinity - piy)/IKr_pi_gate_tau_pi); .units(unitless/ms);

#IKs
GKs_max = ((Iso_1_uM>0.) ? (1.2*0.0016576) : 0.0016576); .units(uS);
Iso_inc_cas_IKs = ((Iso_cas>-0.1) ? (-0.2152 + 0.435692 * pow(PKA,10.0808) / (pow(0.719701,10.0808) + pow(PKA,10.0808))) : 0.); .units(unitless);    # assumption: similar to ICaL # 0.435692, 0.719701 calculation SLSQP
# Iso_inc_cas_IKs = 0;  .units(unitless);
E_Ks = (RTONF*log(((Ko+(0.*Nao))/(Ki+(0.*Nai_BAPTA))))); .units(mV);
IKs = ((1+Iso_inc_cas_IKs)*GKs_max*(V - E_Ks)*(n*n)); .units(nA);

#IKs n gate
Iso_shift_IKs_n_gate = ((Iso_1_uM>0.) ? -14. : 0.); .units(mV);
Iso_shift_cas_IKs_n_gate = ((Iso_cas>-0.1) ? (-(24.4 * 1.411375 * pow(PKA,9.281)/(pow(0.704217,9.281) + pow(PKA,9.281)) - 18.76)) : 0.); .units(mV);    # assumption: similar to If & cAMP # 0.704217 calculation SLSQP
# Iso_shift_cas_IKs_n_gate = 0;  .units(mV);
diff_n = (IKs_n_gate_n_infinity - n)/IKs_n_gate_tau_n;
IKs_n_gate_n_infinity = ((14./(1.+exp((-((V - 40.) - Iso_shift_IKs_n_gate - Iso_shift_cas_IKs_n_gate)/12.))))/((14./(1.+exp((-((V - 40.) - 
						Iso_shift_IKs_n_gate)/12.))))+(1.*exp((-(V - Iso_shift_IKs_n_gate)/45.))))); .units(unitless);
IKs_n_gate_tau_n = (1./(IKs_n_gate_alpha_n+IKs_n_gate_beta_n)); .units(ms);
IKs_n_gate_alpha_n = (28./(1.+exp((-((V - 40.) - Iso_shift_IKs_n_gate - Iso_shift_cas_IKs_n_gate)/3.))))/1000; .units(unitless/ms);
IKs_n_gate_beta_n = (1.*exp((-(((V - Iso_shift_IKs_n_gate - Iso_shift_cas_IKs_n_gate) - shift) - 5.)/25.)))/1000; .units(unitless/ms);

#IKACh
IKACh = ((ACh>0.) ? (GKACh_max*(V - E_K)*(1.+exp(((V+20.)/20.)))*a) : 0.); .units(nA);

#IKACh a gate
IKACh_a_gate_alpha_a = (((3.5988 - 0.025641)/(1.+(0.0000012155/pow((1.*ACh),1.6951))))+0.025641)/1000; .units(unitless/ms);
IKACh_a_gate_beta_a = (10.*exp((0.0133*(V+40.))))/1000; .units(unitless/ms);
diff_a = (IKACh_a_gate_a_infinity - a)/IKACh_a_gate_tau_a;
IKACh_a_gate_a_infinity = (IKACh_a_gate_alpha_a/(IKACh_a_gate_alpha_a+IKACh_a_gate_beta_a)); .units(unitless);
IKACh_a_gate_tau_a = (1./(IKACh_a_gate_alpha_a+IKACh_a_gate_beta_a)); .units(ms);

#AC-cAMP-PKA signalling
diff_cAMP = ((kiso - kcch) * ATPi + k1 * ATPi - k2 * cAMP - k3 * cAMP); .units(mmol/L/ms);
kiso = (0.007 + 0.1181 * (pow(Iso_cas,0.8664) / (pow(48.1212,0.8664) + pow(Iso_cas,0.8664)))) / 60000; .units(unitless/ms);
kcch = (0.0146 * (pow(CCh_cas,1.4402) / (pow(51.7331,1.4402) + pow(CCh_cas,1.4402)))) / 60000; .units(unitless/ms);
k1 = (K_ACI + (K_AC / (1 + exp((K_Ca - kb_CM * ((fCMi) / (kf_CM * (1 - (fCMi))))) / K_ACCa)))); .units(unitless/ms);
k2 = (1.1 * 237.9851 * (pow((cAMP*600),5.101) / (pow(20.1077,6.101) + pow((cAMP*600),6.101)))) / 60000; .units(unitless/ms);    # adopted from BY model matlab code
k3 = (kPKA * (pow(cAMP,(nPKA-1)) / (pow(kPKA_cAMP,nPKA) + pow(cAMP,nPKA)))); .units(unitless/ms);

#PLB activity
diff_PLBp = (k4 - k5); .units(unitless/ms);
k4 = ((kPLBp * pow(PKA,nPLB)) / (pow(kPKA_PLB,nPLB) + pow(PKA, nPLB))); .units(unitless/ms);
k5 = (kPP1 * (PP1 * PLBp/ (kPP1_PLB + PLBp))); .units(unitless/ms);

#PKA activity
PKA = ((cAMP*600)<25.87) ? (-0.9483029 * exp(-cAMP*600*0.06561479) + 0.97781646) : (-0.45260509 * exp(-cAMP*600*0.03395094) + 0.99221714); .units(unitless);    # based on cAMP (0 to 19.73 & 19.73 to 200 pmol/mg protein)

#ATP
ATPi = (ATPi_max * (((kATP * pow((cAMP*100/cAMPb),nATP)) / (kATP05 + pow((cAMP*100/cAMPb),nATP))) - KATP_min) / 100); .units(mmol/L);

#force (not used)
Nxb = (SL - SL0) / 2 * Nc * (TT + U); .units(unitless/mm);
KCa = Fk0 + Fk1 * pow(Nxb,FN) / (pow(Fk05,FN) + pow(Nxb,FN)); .units(unitless/mmol/L);
k_l = Fkl / KCa; .units(unitless/ms);
diff_A = Fkl * Cai * (1 - A - TT - U) - A * (Ff + k_l) + TT * Fg0; .units(unitless/ms);
diff_TT = Ff * A - TT * (Fg0 + k_l) + Fkl * Cai * U; .units(unitless/ms);
diff_U = k_l * TT - (Fg0 + Fkl * Cai) * U; .units(unitless/ms);

#traced variables (currents, state variabels & rates)
group {
	V;
	Iion;
	If;
	INa;
	INaK;
	INaCa;
	ICaL;
	ICaT;
	IKr;
	IKs;
	Itot;
	Ito;
	Nai;
	y;
	m;
	h;
	dL;
	fL;
	fCa;
	dT;
	fT;
	R_Ca_SR_release;
	O;
	I;
	RI;
	fTC;
	fTMC;
	fTMM;
	fCMi;
	fCMs;
	fCQ;
	Cai;
	fBAPTA;
	Ca_sub;
	fBAPTA_sub;
	Ca_nsr;
	Ca_jsr;
	q;
	r;
	paS;
	paF;
	piy;
	n;
	a;
	diff_Ca_sub;
	diff_Nai;
	diff_R_Ca_SR_release;
	diff_O;
	diff_I;
	diff_RI;
	diff_Ca_jsr;
	diff_Ca_nsr;
	diff_Cai;
	diff_fTMM;
	diff_fCMi;
	diff_fCMs;
	diff_fTC;
	diff_fTMC;
	diff_fCQ;
	diff_fBAPTA;
	diff_fBAPTA_sub;
	diff_paS;
	diff_paF;
	diff_piy;
	Iso_shift_cas_If_y_gate;
	If_y_gate_y_infinity;
	Iso_inc_cas_ICaL;
	Iso_shift_cas_ICaL_dL_gate;
	Iso_inc_cas_IKs;
	Iso_shift_cas_IKs_n_gate;
	Iso_inc_cas_INaK;
	Iso_slope_cas_ICaL;
	kCaSR;
	koCa;
	koSRCa;
	kiSRCa;
	F_PLBp;
	j_up;
	j_uni;
	j_NaCam;
	j_Ca_dif;
	j_SRCarel;
	diff_Cam;
	Cam;
	PKA;
	cAMP;
	diff_cAMP;
	ATPi;
	PLBp;
	diff_PLBp;
	kiso;
	kcch;
	k1;
	k2;
	k3;
	k4;
	k5;
	A;
	TT;
	U;
	di;
	var_do;
	k12;
	k14;
	k21;
	k23;
	k32;
	k34;
	k41;
	k43;
	x1;
	x2;
	x3;
	x4;
}.trace();

#parameters
group{
	Cao;
	Iva_3_uM;
	Cs_5_mM;
	ACh;
	Iso_1_uM;
	Iso_cas;
	CCh_cas;
}.param();