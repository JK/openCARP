group { 
  #Authors ::= Hodgkin, A. L., Huxley, A. F.
  #Year ::= 1952
  #Title ::= A quantitative description of membrane current and its application to conduction and excitation in nerve
  #Journal ::= The Journal of Physiology, 117
  #DOI ::= 10.1113/jphysiol.1952.sp004764
} .reference();

V; .external(Vm); .nodal(); .trace();
Iion; .external(); .nodal();

V_init = -65.0;.units(mV);
m_init = 0.0588;
h_init = 0.5645;
n_init = 0.3315;

Cm = 1.0;.units(uF/cm^2);

GNa = 120.0; .units(mS/cm^2); .param();
E_Na = 52;.units(mV);
alpha_m = (0.1*(V+40))/(1-exp(-(0.1)*(V+40)));
beta_m =  4*exp(-0.0556*(V+65));
dm_dt = (alpha_m*( 1.0 -m)) -(beta_m*m);
alpha_h = 0.07*exp(-0.05*(V+65));
beta_h =  1/(1+exp(-(0.1)*(V+35)));
dh_dt = (alpha_h*( 1.0 -h)) -(beta_h*h);
INa = GNa*m*m*m*h*(V-E_Na);.units(uA/cm^2);

GK = 36.0; .units(mS/cm^2); .param();
E_K = -72;.units(mV);
alpha_n = (0.01*(V+55))/(1-exp(-(0.1)*(V+55)));
beta_n = 0.125*exp(-0.0125*(V+65));
dn_dt = (alpha_n*( 1.0 -n)) -(beta_n*n);
IK = GK*n*n*n*n*(V-E_K);.units(uA/cm^2);

GL = 0.3; .units(mS/cm^2); .param();
E_L = -58;.units(mV);
IL = GL*(V-E_L);.units(uA/cm^2);

dV_dt = Iion/Cm;

Iion = (INa+IK+IL);

group{
  n;
  m;
  h;
}.trace();
