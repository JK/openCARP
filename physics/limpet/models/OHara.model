group { 
  #Authors ::= O'Hara T, Virag L, Varro A, Rudy Y
  #Year ::= 2011
  #Title ::= Simulation of the Undiseased Human Cardiac Ventricular Action Potential: Model Formulation and Experimental Validation
  #Journal ::= PLOS Computational Biology, 7(5)
  #DOI ::= 10.1371/journal.pcbi.1002061
  #Comment ::= Sodium current can be changed by flag to the tenTusscherPanfilov formulation as done in Dutta et al. doi:10.1016/j.pbiomolbio.2017.02.007. Additionally, model modifications as suggested by Michael Clerx can be modifed by flags: https://journals.plos.org/ploscompbiol/article/comment?id=10.1371/annotation/ec788479-502e-4dbb-8985-52b9af657990
} .reference();

v;    .external(Vm); .nodal();
Iion; .external();   .nodal();
v;    .lookup(-1000,1000,1e-2);
Cai;  .lookup(1e-6,1e-2,1e-6);

group {
  CaMKt;
  Cai;
  Ki;
  Nai;
  cajsr;
  cansr;
  cass;
  kss;
  nass;
  nca;
} .lb(1e-9);

group {
  a;
  ap;
  d;
  fcaf;
  fcafp;
  fcas;
  ff;
  ffp;
  fs;
  hL;
  hLp;
  hf;
  hs;
  hsp;
  iF;
  iFp;
  iS;
  iSp;
  j;
  jca;
  jp;
  m;
  mL;
  xk1;
  xrf;
  xrs;
  xs1;
  xs2;
} .lb(0); .ub(1);

group {
  GNa;
  GNaL;
  Gto;
  factorICaL;
  factorICaNa;
  factorICaK;
  GKr;
  GKs;
  GK1;
  GNaCa;
  factorINaCa;
  factorINaCass;
  factorINaK;
  GKb;
  factorIbNa;
  GpCa;
  factorIbCa;
  Ko;
  Nao;
  Cao;
  vHalfXs;
  scale_tau_m ;
  shift_m_inf;
} .param();

group {
  INa;
  INaL;
  Ito;
  ICaL;
  IKr;
  IKs;
  IK1;
  INaCa;
  INaCa_ss;
  INaK;
  IKb;
  IbNa;
  IbCa;
  IpCa;
  Jdiff;
  JdiffNa;
  JdiffK;
  Jup;
  Jleak;
  Jtr;
  Jrel;
  CaMKa; 
} .trace();

ENDO = 0;
EPI = 1;
MCELL = 2;
celltype = ENDO; .flag(ENDO,EPI,MCELL);

# Model formulation
ORIGINAL = 0;
CLERX = 1;
modelformulation = ORIGINAL; .flag(ORIGINAL,CLERX);

//extracellular ionic concentrations
Nao = 140.0;
Cao = 1.8;
Ko  = 5.4;

//physical constants
R = 8314.0;
T = 310.0;
F = 96485.0;

//cell geometry
L     = 0.01;
rad   = 0.0011;
vcell = 1000*3.14*rad*rad*L;
Ageo  = 2*3.14*rad*rad+2*3.14*rad*L;
Acap  = 2*Ageo;
vmyo  = 0.68*vcell;
vnsr  = 0.0552*vcell;
vjsr  = 0.0048*vcell;
vss   = 0.02*vcell;

/////////////////////////////////////////////////////////////////

//give names to the state vector values
v_init      = -87;
Nai_init    = 7.0;
nass_init   = Nai;
Ki_init     = 145.0;
kss_init    = Ki;
Cai_init    = 1.0e-4; .units(mM);
cass_init   = Cai;
cansr_init  = 1.2;
cajsr_init  = cansr;
m_init      = 0.0;
hf_init     = 1.0;
hs_init     = 1.0;
j_init      = 1.0;
hsp_init    = 1.0;
jp_init     = 1.0;
mL_init     = 0.0;
hL_init     = 1.0;
hLp_init    = 1.0;
a_init      = 0.0;
iF_init     = 1.0;
iS_init     = 1.0;
ap_init     = 0.0;
iFp_init    = 1.0;
iSp_init    = 1.0;
d_init      = 0.0;
ff_init     = 1.0;
fs_init     = 1.0;
fcaf_init   = 1.0;
fcas_init   = 1.0;
jca_init    = 1.0;
nca_init    = 0.0;
ffp_init    = 1.0;
fcafp_init  = 1.0;
xrf_init    = 0.0;
xrs_init    = 0.0;
xs1_init    = 0.0;
xs2_init    = 0.0;
xk1_init    = 1.0;
Jrelnp_init = 0.0;
Jrelp_init  = 0.0;
CaMKt_init  = 0.0;

///////////////////////////////////////////////////////////////////////

//CaMK constants
KmCaMK = 0.15;

aCaMK = 0.05;
bCaMK = 0.00068;
CaMKo = 0.05;
KmCaM = 0.0015;

//update CaMK
CaMKb = CaMKo*(1.0-CaMKt)/(1.0+KmCaM/cass);
CaMKa = CaMKb+CaMKt;
diff_CaMKt = aCaMK*CaMKb*(CaMKb+CaMKt)-bCaMK*CaMKt;

//////////////////////////////////////////////////////////////////////

//reversal potentials
ENa  = (R*T/F)*log(Nao/Nai);
EK   = (R*T/F)*log(Ko/Ki);
PKNa = 0.01833;
EKs  = (R*T/F)*log((Ko+PKNa*Nao)/(Ki+PKNa*Nai));

//convenient shorthand calculations
vffrt = v*F*F/(R*T);
vfrt  = v*F/(R*T);

///////////////////////////////////////////////////////////////////////

// scaling some INa properties to facilitate propagation in tissue level simulations
scale_tau_m = 0.3;
shift_m_inf = -8.0;
scale_GNa   = 0.37;

TT2INa = 0;
ORdINa = 1;
INa_Type = ORdINa; .flag(TT2INa, ORdINa);

//TT2 INa
aa_mTT2   = 1./(1.+exp((-60.-v)/5.));
bb_mTT2   = 0.1/(1.+exp((v+35.)/5.))+0.10/(1.+exp((v-50.)/200.));
tau_mTT2 = aa_mTT2 * bb_mTT2;
mTT2_inf = 1./((1.+exp((-56.86-v)/9.03))*(1.+exp((-56.86-v)/9.03)));

aa_hTT2 = (0.057*exp(-(v+80.)/6.8));
bb_hTT2 = (2.7*exp(0.079*v)+(3.1e5)*exp(0.3485*v));
if (v >= -40) {
  aa_hTT2 = 0;
  bb_hTT2 = (0.77/(0.13*(1.+exp(-(v+10.66)/11.1))));
}
tau_hTT2 = 1.0/(aa_hTT2+bb_hTT2);
hTT2_inf = 1./((1.+exp((v+71.55)/7.43))*(1.+exp((v+71.55)/7.43)));


aa_jTT2 = (((-2.5428e4)*exp(0.2444*v)-(6.948e-6)*
            exp(-0.04391*v))*(v+37.78)/
           (1.+exp(0.311*(v+79.23))));
bb_jTT2 = (0.02424*exp(-0.01052*v)/(1.+exp(-0.1378*(v+40.14))));
if (v >= -40) {
  aa_jTT2 = 0;
  bb_jTT2 = (0.6*exp((0.057)*v)/(1.+exp(-0.1*(v+32.))));
}
tau_jTT2 = 1.0/(aa_jTT2 + bb_jTT2);
jTT2_inf = hTT2_inf;


//calculate INa
mORd_inf = 1.0/(1.0+exp((-(v+39.57-shift_m_inf))/9.871));
tau_mORd = scale_tau_m*1.0/(6.765*exp((v+11.64)/34.77)+8.552*exp(-(v+77.42)/5.955));
//diff_m=(m_inf-m)/tau_m;
h_inf = 1.0/(1+exp((v+82.90)/6.086));
tau_hf = 1.0/(1.432e-5*exp(-(v+1.196)/6.285)+6.149*exp((v+0.5096)/20.27));
tau_hs = 1.0/(0.009794*exp(-(v+17.95)/28.05)+0.3343*exp((v+5.730)/56.66));
Ahf = 0.99;
Ahs = 1.0-Ahf;
hf_inf = h_inf;
//diff_hf=(h_inf-hf)/tau_hf;
hs_inf = h_inf;
//diff_hs=(h_inf-hs)/tau_hs;
h = Ahf*hf+Ahs*hs;
j_inf = h_inf;
tau_j = 2.038+1.0/(0.02136*exp(-(v+100.6)/8.281)+0.3052*exp((v+0.9941)/38.45));
//diff_j=(j_inf-j)/tau_j;
hsp_inf = 1.0/(1+exp((v+89.1)/6.086));
tau_hsp = 3.0*tau_hs;
//diff_hsp=(hsp_inf-hsp)/tau_hsp;
hp = Ahf*hf+Ahs*hsp;
tau_jp = 1.46*tau_j;
jp_inf = j_inf;
//diff_jp=(j_inf-jp)/tau_jp;
fINap=(1.0/(1.0+KmCaMK/CaMKa));

if (INa_Type == TT2INa) {
  GNa = 14.838;
  INa = GNa*mTT2*mTT2*mTT2*hTT2*jTT2*(v-ENa);
  tau_m = tau_mTT2;
} else {
  GNa = 75*scale_GNa;
  INa = GNa*(v-ENa)*mORd*mORd*mORd*((1.0-fINap)*h*j+fINap*hp*jp);
  tau_m = tau_mORd;
}


//calculate INaL
mL_inf = 1.0/(1.0+exp((-(v+42.85))/5.264));
tau_mL = tau_m;
//diff_mL=(mL_inf-mL)/tau_mL;
hL_inf = 1.0/(1.0+exp((v+87.61)/7.488));
tau_hL = 200.0;
//diff_hL=(hL_inf-hL)/tau_hL;
hLp_inf = 1.0/(1.0+exp((v+93.81)/7.488));
tau_hLp = 3.0*tau_hL;
//diff_hLp=(hLp_inf-hLp)/tau_hLp;
GNaL = 0.0075;
if (celltype == EPI) {
  GNaL *= 0.6;
}
fINaLp = (1.0/(1.0+KmCaMK/CaMKa));
INaL = GNaL*(v-ENa)*mL*((1.0-fINaLp)*hL+fINaLp*hLp);

//calculate Ito
a_inf = 1.0/(1.0+exp((-(v-14.34))/14.82));
tau_a = 1.0515/(1.0/(1.2089*(1.0+exp(-(v-18.4099)/29.3814)))+3.5/(1.0+exp((v+100.0)/29.3814)));
//diff_a=(a_inf-a)/tau_a;
i_inf = 1.0/(1.0+exp((v+43.94)/5.711));
if (celltype == EPI) {
  delta_epi = 1.0-(0.95/(1.0+exp((v+70.0)/5.0)));
} else {
  delta_epi = 1.0;
}
tau_iF = 4.562+1/(0.3933*exp((-(v+100.0))/100.0)+0.08004*exp((v+50.0)/16.59));
tau_iS = 23.62+1/(0.001416*exp((-(v+96.52))/59.05)+1.780e-8*exp((v+114.1)/8.079));
tau_iF *= delta_epi;
tau_iS *= delta_epi;
AiF = 1.0/(1.0+exp((v-213.6)/151.2));
AiS = 1.0-AiF;
iF_inf = i_inf;
//diff_iF=(i_inf-iF)/tau_iF;
iS_inf = i_inf;
//diff_iS=(i_inf-iS)/tau_iS;
i = AiF*iF+AiS*iS;
ap_inf = 1.0/(1.0+exp((-(v-24.34))/14.82));
tau_ap = tau_a;
//diff_ap=(ap_inf-ap)/tau_a;
dti_develop = 1.354+1.0e-4/(exp((v-167.4)/15.89)+exp(-(v-12.23)/0.2154));
dti_recover = 1.0-0.5/(1.0+exp((v+70.0)/20.0));
tau_iFp = dti_develop*dti_recover*tau_iF;
tau_iSp = dti_develop*dti_recover*tau_iS;
iFp_inf = i_inf;
iSp_inf = i_inf;
//diff_iFp=(i_inf-iFp)/tau_iFp;
//diff_iSp=(i_inf-iSp)/tau_iSp;
ip = AiF*iFp+AiS*iSp;
Gto = 0.02;
if (celltype == EPI) {
  Gto *= 4.0;
} elif (celltype == MCELL) {
  Gto *= 4.0;
}
fItop = (1.0/(1.0+KmCaMK/CaMKa));
Ito = Gto*(v-EK)*((1.0-fItop)*a*i+fItop*ap*ip);

//calculate ICaL, ICaNa, ICaK
d_inf = 1.0/(1.0+exp((-(v+3.940))/4.230));
tau_d = 0.6+1.0/(exp(-0.05*(v+6.0))+exp(0.09*(v+14.0)));
//diff_d=(d_inf-d)/tau_d;
f_inf = 1.0/(1.0+exp((v+19.58)/3.696));
tau_ff = 7.0+1.0/(0.0045*exp(-(v+20.0)/10.0)+0.0045*exp((v+20.0)/10.0));
tau_fs = 1000.0+1.0/(0.000035*exp(-(v+5.0)/4.0)+0.000035*exp((v+5.0)/6.0));
Aff = 0.6;
Afs = 1.0-Aff;
ff_inf = f_inf;
//diff_ff=(f_inf-ff)/tau_ff;
fs_inf = f_inf;
//diff_fs=(f_inf-fs)/tau_fs;
f = Aff*ff+Afs*fs;
fca_inf = f_inf;
tau_fcaf = 7.0+1.0/(0.04*exp(-(v-4.0)/7.0)+0.04*exp((v-4.0)/7.0));
tau_fcas = 100.0+1.0/(0.00012*exp(-v/3.0)+0.00012*exp(v/7.0));
Afcaf = 0.3+0.6/(1.0+exp((v-10.0)/10.0));
Afcas = 1.0-Afcaf;
fcaf_inf = fca_inf;
//diff_fcaf=(fca_inf-fcaf)/tau_fcaf;
fcas_inf = fca_inf;
//diff_fcas=(fca_inf-fcas)/tau_fcas;
fca = Afcaf*fcaf+Afcas*fcas;
tau_jca = 75.0;
jca_inf = fca_inf;
tau_ffp = 2.5*tau_ff;
ffp_inf = f_inf;
fp = Aff*ffp+Afs*fs;
tau_fcafp = 2.5*tau_fcaf;
fcafp_inf = fca_inf;
//diff_fcafp=(fca_inf-fcafp)/tau_fcafp;
fcap = Afcaf*fcafp+Afcas*fcas;
Kmn = 0.002;
k2n = 1000.0;
km2n = jca*1.0;
kmn_cass_4 = (1.0+Kmn/cass)*(1.0+Kmn/cass)*(1.0+Kmn/cass)*(1.0+Kmn/cass);
anca = 1.0/(k2n/km2n+kmn_cass_4);
diff_nca = anca*k2n-nca*km2n;
vffrt_expm1_2vfrt = vffrt/expm1(2.0*vfrt);
vffrt_expm1_vfrt = vffrt/expm1(vfrt);
if (v == 0) {
  vffrt_expm1_vfrt = F;
  vffrt_expm1_2vfrt = F/2;
}
exp_2vfrt = exp(2.0*vfrt);
exp_vfrt = exp(1.0*vfrt);
PhiCaL = 4.0*vffrt_expm1_2vfrt*(cass*exp_2vfrt-0.341*Cao);
PhiCaNa = 1.0*vffrt_expm1_vfrt*(0.75*nass*exp_vfrt-0.75*Nao);
PhiCaK = 1.0*vffrt_expm1_vfrt*(0.75*kss*exp_vfrt-0.75*Ko);
zca = 2.0;
PCa = 0.0001;
if (celltype == EPI) {
  PCa *= 1.2;
} elif (celltype == MCELL) {
  PCa *= 2.5;
}
PCap = 1.1*PCa;
PCaNa = 0.00125*PCa;
PCaK = 3.574e-4*PCa;
PCaNap = 0.00125*PCap;
PCaKp = 3.574e-4*PCap;
fICaLp = (1.0/(1.0+KmCaMK/CaMKa));
factorICaL = 1.0;
ICaL = factorICaL*(1.0-fICaLp)*PCa*PhiCaL*d*(f*(1.0-nca)+jca*fca*nca)+fICaLp*PCap*PhiCaL*d*(fp*(1.0-nca)+jca*fcap*nca);
factorICaNa = 1.0;
ICaNa = factorICaNa*(1.0-fICaLp)*PCaNa*PhiCaNa*d*(f*(1.0-nca)+jca*fca*nca)+fICaLp*PCaNap*PhiCaNa*d*(fp*(1.0-nca)+jca*fcap*nca);
factorICaK = 1.0;
ICaK = factorICaK*(1.0-fICaLp)*PCaK*PhiCaK*d*(f*(1.0-nca)+jca*fca*nca)+fICaLp*PCaKp*PhiCaK*d*(fp*(1.0-nca)+jca*fcap*nca);

//calculate IKr
xr_inf = 1.0/(1.0+exp((-(v+8.337))/6.789));
tau_xrf = 12.98+1.0/(0.3652*exp((v-31.66)/3.869)+4.123e-5*exp((-(v-47.78))/20.38));
tau_xrs = 1.865+1.0/(0.06629*exp((v-34.70)/7.355)+1.128e-5*exp((-(v-29.74))/25.94));
Axrf = 1.0/(1.0+exp((v+54.81)/38.21));
Axrs = 1.0-Axrf;
xrf_inf = xr_inf;
//diff_xrf=(xr_inf-xrf)/tau_xrf;
xrs_inf = xr_inf;
//diff_xrs=(xr_inf-xrs)/tau_xrs;
xr = Axrf*xrf+Axrs*xrs;
rkr = 1.0/(1.0+exp((v+55.0)/75.0))*1.0/(1.0+exp((v-10.0)/30.0));
GKr = 0.046;
if (celltype == EPI) {
  GKr *= 1.3;
} elif (celltype == MCELL) {
  GKr *= 0.8;
}
sqrt_Ko_54 = sqrt(Ko/5.4);
IKr = GKr*sqrt_Ko_54*xr*rkr*(v-EK);

//calculate IKs
vHalfXs = 11.60;
xs1_inf = 1.0/(1.0+exp((-(v+vHalfXs))/8.932));
tau_xs1 = 817.3+1.0/(2.326e-4*exp((v+48.28)/17.80)+0.001292*exp((-(v+210.0))/230.0));
//diff_xs1=(xs1_inf-xs1)/tau_xs1;
xs2_inf = xs1_inf;
tau_xs2 = 1.0/(0.01*exp((v-50.0)/20.0)+0.0193*exp((-(v+66.54))/31.0));
//diff_xs2=(xs2_inf-xs2)/tau_xs2;
KsCa = 1.0+0.6/(1.0+pow(3.8e-5/Cai,1.4));
GKs = 0.0034;
if (celltype == EPI) {
  GKs *= 1.4;
}
IKs = GKs*KsCa*xs1*xs2*(v-EKs);

xk1_inf = 1.0/(1.0+exp(-(v+2.5538*Ko+144.59)/(1.5692*Ko+3.8115)));
tau_xk1 = 122.2/(exp((-(v+127.2))/20.36)+exp((v+236.8)/69.33));
//diff_xk1=(xk1_inf-xk1)/tau_xk1;
rk1 = 1.0/(1.0+exp((v+105.8-2.6*Ko)/9.493));
GK1 = 0.1908;
if (celltype == EPI) {
  GK1 *= 1.2;
} elif (celltype == MCELL) {
  GK1 *= 1.3;
}
sqrt_Ko = sqrt(Ko);
IK1 = GK1*sqrt_Ko*rk1*xk1*(v-EK);

//calculate INaCa
kna1 = 15.0;
kna2 = 5.0;
kna3 = 88.12;
kasymm = 12.5;
wna = 6.0e4;
wca = 6.0e4;
wnaca = 5.0e3;
kCaon = 1.5e6;
kCaoff = 5.0e3;
qna = 0.5224;
qca = 0.1670;
hca = exp((qca*v*F)/(R*T));
hna = exp((qna*v*F)/(R*T));
h1_i = 1+Nai/kna3*(1+hna);
h2_i = (Nai*hna)/(kna3*h1_i);
h3_i = 1.0/h1_i;
h4_i = 1.0+Nai/kna1*(1+Nai/kna2);
h5_i = Nai*Nai/(h4_i*kna1*kna2);
h6_i = 1.0/h4_i;
h7_i = 1.0+Nao/kna3*(1.0+1.0/hna);
h8_i = Nao/(kna3*hna*h7_i);
h9_i = 1.0/h7_i;
h10_i = kasymm+1.0+Nao/kna1*(1.0+Nao/kna2);
h11_i = Nao*Nao/(h10_i*kna1*kna2);
h12_i = 1.0/h10_i;
k1_i = h12_i*Cao*kCaon;
k2_i = kCaoff;
k3p_i = h9_i*wca;
k3pp_i = h8_i*wnaca;
k3_i = k3p_i+k3pp_i;
k4p_i = h3_i*wca/hca;
k4pp_i = h2_i*wnaca;
k4_i = k4p_i+k4pp_i;
k5_i = kCaoff;
k6_i = h6_i*Cai*kCaon;
k7_i = h5_i*h2_i*wna;
k8_i = h8_i*h11_i*wna;
x1_i = k2_i*k4_i*(k7_i+k6_i)+k5_i*k7_i*(k2_i+k3_i);
x2_i = k1_i*k7_i*(k4_i+k5_i)+k4_i*k6_i*(k1_i+k8_i);
x3_i = k1_i*k3_i*(k7_i+k6_i)+k8_i*k6_i*(k2_i+k3_i);
x4_i = k2_i*k8_i*(k4_i+k5_i)+k3_i*k5_i*(k1_i+k8_i);
E1_i = x1_i/(x1_i+x2_i+x3_i+x4_i);
E2_i = x2_i/(x1_i+x2_i+x3_i+x4_i);
E3_i = x3_i/(x1_i+x2_i+x3_i+x4_i);
E4_i = x4_i/(x1_i+x2_i+x3_i+x4_i);
KmCaAct_i = 150.0e-6;
allo_i = 1.0/(1.0+(KmCaAct_i/Cai)*(KmCaAct_i/Cai));
zna = 1.0;
JncxNai = 3.0*(E4_i*k7_i-E1_i*k8_i)+E3_i*k4pp_i-E2_i*k3pp_i;
JncxCai = E2_i*k2_i-E1_i*k1_i;
GNaCa = 0.0008;
if (celltype == EPI) {
  GNaCa *= 1.1;
} elif (celltype == MCELL) {
  GNaCa *= 1.4;
}
factorINaCa = 1;
INaCa = factorINaCa*0.8*GNaCa*allo_i*(zna*JncxNai+zca*JncxCai);

//calculate INaCa_ss
h1_ss = 1+nass/kna3*(1+hna);
h2_ss=(nass*hna)/(kna3*h1_ss);
h3_ss = 1.0/h1_ss;
h4_ss = 1.0+nass/kna1*(1+nass/kna2);
h5_ss = nass*nass/(h4_ss*kna1*kna2);
h6_ss = 1.0/h4_ss;
h7_ss = 1.0+Nao/kna3*(1.0+1.0/hna);
h8_ss = Nao/(kna3*hna*h7_ss);
h9_ss = 1.0/h7_ss;
h10_ss = kasymm+1.0+Nao/kna1*(1+Nao/kna2);
h11_ss = Nao*Nao/(h10_ss*kna1*kna2);
h12_ss = 1.0/h10_ss;
k1_ss = h12_ss*Cao*kCaon;
k2_ss = kCaoff;
k3p_ss = h9_ss*wca;
k3pp_ss = h8_ss*wnaca;
k3_ss = k3p_ss+k3pp_ss;
k4p_ss = h3_ss*wca/hca;
k4pp_ss = h2_ss*wnaca;
k4_ss = k4p_ss+k4pp_ss;
k5_ss = kCaoff;
k6_ss = h6_ss*cass*kCaon;
k7_ss = h5_ss*h2_ss*wna;
k8_ss = h8_ss*h11_ss*wna;
x1_ss = k2_ss*k4_ss*(k7_ss+k6_ss)+k5_ss*k7_ss*(k2_ss+k3_ss);
x2_ss = k1_ss*k7_ss*(k4_ss+k5_ss)+k4_ss*k6_ss*(k1_ss+k8_ss);
x3_ss = k1_ss*k3_ss*(k7_ss+k6_ss)+k8_ss*k6_ss*(k2_ss+k3_ss);
x4_ss = k2_ss*k8_ss*(k4_ss+k5_ss)+k3_ss*k5_ss*(k1_ss+k8_ss);
E1_ss = x1_ss/(x1_ss+x2_ss+x3_ss+x4_ss);
E2_ss = x2_ss/(x1_ss+x2_ss+x3_ss+x4_ss);
E3_ss = x3_ss/(x1_ss+x2_ss+x3_ss+x4_ss);
E4_ss = x4_ss/(x1_ss+x2_ss+x3_ss+x4_ss);
KmCaAct_ss = 150.0e-6;
allo_ss = 1.0/(1.0+(KmCaAct_ss/cass)*(KmCaAct_ss/cass));
JncxNa_ss = 3.0*(E4_ss*k7_ss-E1_ss*k8_ss)+E3_ss*k4pp_ss-E2_ss*k3pp_ss;
JncxCa_ss = E2_ss*k2_ss-E1_ss*k1_ss;
factorINaCass = 1.0;
INaCa_ss = factorINaCass*0.2*GNaCa*allo_ss*(zna*JncxNa_ss+zca*JncxCa_ss);

//calculate INaK
k1p = 949.5;
k1m = 182.4;
k2p = 687.2;
k2m = 39.4;
k3p = 1899.0;
k3m = 79300.0;
k4p = 639.0;
k4m = 40.0;
Knai0 = 9.073;
KNao0 = 27.78;
delta = -0.1550;
Knai = Knai0*exp((delta*v*F)/(3.0*R*T));
KNao = KNao0*exp(((1.0-delta)*v*F)/(3.0*R*T));
Kki = 0.5;
KKo = 0.3582;
MgADP = 0.05;
MgATP = 9.8;
Kmgatp = 1.698e-7;

// Modifications as suggested by Michael Clerx 16.02.2024: https://journals.plos.org/ploscompbiol/article/comment?id=10.1371/annotation/ec788479-502e-4dbb-8985-52b9af657990
H = 1.0e-7;
if (modelformulation == CLERX) {
  H = 1.0e-4;
}

eP = 4.2;
Khp = 1.698e-7;
Knap = 224.0;
Kxkur = 292.0;
P = eP/(1.0+H/Khp+Nai/Knap+Ki/Kxkur);
nai_Knai_3 = (Nai/Knai)*(Nai/Knai)*(Nai/Knai);
nai_Knai_p1_3 = (1.0+Nai/Knai)*(1.0+Nai/Knai)*(1.0+Nai/Knai);
Nao_KNao_3 = (Nao/KNao)*(Nao/KNao)*(Nao/KNao);
Nao_KNao_p1_3 = (1.0+Nao/KNao)*(1.0+Nao/KNao)*(1.0+Nao/KNao);
a1=(k1p*nai_Knai_3)/(nai_Knai_p1_3+(1.0+Ki/Kki)*(1.0+Ki/Kki)-1.0);
b1 = k1m*MgADP;
a2 = k2p;
b2 = (k2m*Nao_KNao_3)/(Nao_KNao_p1_3+(1.0+Ko/KKo)*(1.0+Ko/KKo)-1.0);
a3 = (k3p*(Ko/KKo)*(Ko/KKo))/(Nao_KNao_p1_3+(1.0+Ko/KKo)*(1.0+Ko/KKo)-1.0);
b3 = (k3m*P*H)/(1.0+MgATP/Kmgatp);
a4 = (k4p*MgATP/Kmgatp)/(1.0+MgATP/Kmgatp);
b4 = (k4m*(Ki/Kki)*(Ki/Kki))/(nai_Knai_p1_3+(1.0+Ki/Kki)*(1.0+Ki/Kki)-1.0);

// Modification as suggested by Michael Clerx 16.02.2024: https://journals.plos.org/ploscompbiol/article/comment?id=10.1371/annotation/ec788479-502e-4dbb-8985-52b9af657990
x1 = a4*a1*a2+b2*b4*b3+a2*b4*b3+b3*a1*a2;
if (modelformulation == CLERX) {
  x1 = a4*a1*a2+b1*b4*b3+a2*b4*b3+b3*a1*a2;
}

x2 = b2*b1*b4+a1*a2*a3+a3*b1*b4+a2*a3*b4;
x3 = a2*a3*a4+b3*b2*b1+b2*b1*a4+a3*a4*b1;
x4 = b4*b3*b2+a3*a4*a1+b2*a4*a1+b3*b2*a1;


// Modification as suggested by Michael Clerx 16.02.2024: https://journals.plos.org/ploscompbiol/article/comment?id=10.1371/annotation/ec788479-502e-4dbb-8985-52b9af657990
E1 = (x1/(x1+x2+x3+x4)); .units(unitless);
E2 = (x2/(x1+x2+x3+x4)); .units(unitless);
E3 = (x3/(x1+x2+x3+x4)); .units(unitless);
E4 = (x4/(x1+x2+x3+x4)); .units(unitless);
JNaKNa = (3.*((E1*a3) - (E2*b3))); .units(mmol/L/ms);
JNaKK = (2.*((E4*b1) - (E3*a1))); .units(mmol/L/ms);
// Modification as suggested by Michael Clerx 16.02.2024: https://journals.plos.org/ploscompbiol/article/comment?id=10.1371/annotation/ec788479-502e-4dbb-8985-52b9af657990
r = (a1 * a2 * a3 * a4 - b1 * b2 * b3 * b4) / (x1 + x2 + x3 + x4);
if (modelformulation == CLERX) {
  JNaKNa = 3 * r; .units(mmol/L/ms);
  JNaKK = -2 * r; .units(mmol/L/ms);
}


zk = 1.0;
Pnak = 30;
if (celltype == EPI) {
  Pnak *= 0.9;
} elif (celltype == MCELL) {
  Pnak *= 0.7;
}
factorINaK = 1;
INaK = factorINaK*Pnak*(zna*JNaKNa+zk*JNaKK);

//calculate IKb
xkb = 1.0/(1.0+exp(-(v-14.48)/18.34));
GKb = 0.003;
if (celltype == EPI) {
    GKb*=0.6;
}
IKb = GKb*xkb*(v-EK);

//calculate IbNa
PNab = 3.75e-10;
factorIbNa = 1;
IbNa = factorIbNa*PNab*vffrt_expm1_vfrt*(Nai*exp_vfrt-Nao);

//calculate IbCa
PCab = 2.5e-8;
factorIbCa = 1;
IbCa = factorIbCa*PCab*4.0*vffrt_expm1_2vfrt*(Cai*exp_2vfrt-0.341*Cao);

//calculate IpCa
GpCa = 0.0005;
IpCa = GpCa*Cai/(0.0005+Cai);

///////////////////////////////////////////////////////////////////////

//update the membrane voltage

Iion = INa+INaL+Ito+ICaL+ICaNa+ICaK+IKr+IKs+IK1+INaCa+INaCa_ss+INaK+IbNa+IKb+IpCa+IbCa;


///////////////////////////////////////////////////////////////////////

//calculate diffusion fluxes
JdiffNa = (nass-Nai)/2.0;
JdiffK = (kss-Ki)/2.0;
Jdiff = (cass-Cai)/0.2;

//calculate ryanodione receptor calcium induced calcium release from the jsr
bt = 4.75;
a_rel = 0.5*bt;
//jrel_cajsr_term = pow(1.5/cajsr,8);
jrel_cajsr_term = 
  (1.5/cajsr)
  * (1.5/cajsr)
  * (1.5/cajsr) 
  * (1.5/cajsr) 
  * (1.5/cajsr) 
  * (1.5/cajsr) 
  * (1.5/cajsr) 
  * (1.5/cajsr)
;
jrel_ical_term = (-ICaL)/(1.0+jrel_cajsr_term);
Jrel_inf = a_rel*jrel_ical_term;
if (celltype == MCELL) {
    Jrel_inf *= 1.7;
}
tau_rel_canidate = bt/(1.0+0.0123/cajsr);

if (tau_rel_canidate<0.001) {
  tau_rel = 0.001;
} else {
  tau_rel = tau_rel_canidate;
}

Jrelnp_inf = Jrel_inf;
tau_Jrelnp = tau_rel;
//diff_Jrelnp=(Jrel_inf-Jrelnp)/tau_rel;
btp = 1.25*bt;
a_relp = 0.5*btp;
Jrel_infp = a_relp*jrel_ical_term;
if (celltype == MCELL) {
    Jrel_infp *= 1.7;
}
tau_relp_canidate = btp/(1.0+0.0123/cajsr);
tau_relp = tau_relp_canidate;
if (tau_relp_canidate<0.001) {
   tau_relp = 0.001; 
}

Jrelp_inf = Jrel_infp;
tau_Jrelp = tau_relp;
//diff_Jrelp=(Jrel_infp-Jrelp)/tau_relp;
fJrelp = (1.0/(1.0+KmCaMK/CaMKa));
Jrel_canidate = (1.0-fJrelp)*Jrelnp+fJrelp*Jrelp;
Jrel = Jrel_canidate;

jrel_stiff_const = 0.005; .param();
if (Jrel_canidate*jrel_stiff_const > cajsr) {
   Jrel = cajsr/jrel_stiff_const;
}

//calculate serca pump, ca uptake flux
Jupnp = 0.004375*Cai/(Cai+0.00092);
Jupp = 2.75*0.004375*Cai/(Cai+0.00092-0.00017);
if (celltype == EPI) {
  Jupnp *= 1.3;
  Jupp *= 1.3;
}
fJupp = (1.0/(1.0+KmCaMK/CaMKa));
Jleak = 0.0039375*cansr/15.0;
Jup = (1.0-fJupp)*Jupnp+fJupp*Jupp-Jleak;

//calculate tranlocation flux
Jtr = (cansr-cajsr)/100.0;

///////////////////////////////////////////////////////////////////////

//calcium buffer constants
cmdnmax = 0.05;
if (celltype == EPI) {
    cmdnmax *= 1.3;
}
kmcmdn  = 0.00238;
trpnmax = 0.07;
kmtrpn  = 0.0005;
BSRmax  = 0.047;
KmBSR   = 0.00087;
BSLmax  = 1.124;
KmBSL   = 0.0087;
csqnmax = 10.0;
kmcsqn  = 0.8;

//update intracellular concentrations, using buffers for Cai, cass, cajsr
diff_Nai = -(INa+INaL+3.0*INaCa+3.0*INaK+IbNa)*Acap/(F*vmyo)+JdiffNa*vss/vmyo;
diff_nass = -(ICaNa+3.0*INaCa_ss)*Acap/(F*vss)-JdiffNa;

diff_Ki = -(Ito+IKr+IKs+IK1+IKb-2.0*INaK)*Acap/(F*vmyo)+JdiffK*vss/vmyo;
diff_kss = -(ICaK)*Acap/(F*vss)-JdiffK;

Bcai = 1.0/(1.0+cmdnmax*kmcmdn/(kmcmdn+Cai)/(kmcmdn+Cai)+trpnmax*kmtrpn/(kmtrpn+Cai)/(kmtrpn+Cai));
diff_Cai = Bcai*(-(IpCa+IbCa-2.0*INaCa)*Acap/(2.0*F*vmyo)-Jup*vnsr/vmyo+Jdiff*vss/vmyo); .units(mM/ms);
Cai; .units(mM);

Bcass = 1.0/(1.0+BSRmax*KmBSR/(KmBSR+cass)/(KmBSR+cass)+BSLmax*KmBSL/(KmBSL+cass)/(KmBSL+cass));
diff_cass = Bcass*(-(ICaL-2.0*INaCa_ss)*Acap/(2.0*F*vss)+Jrel*vjsr/vss-Jdiff);

diff_cansr = Jup-Jtr*vjsr/vnsr;

Bcajsr = 1.0/(1.0+csqnmax*kmcsqn/(kmcsqn+cajsr)/(kmcsqn+cajsr));
diff_cajsr = Bcajsr*(Jtr-Jrel);