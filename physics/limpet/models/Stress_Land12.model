group {
  #Title ::= An analysis of deformation-dependent electromechanical coupling in the mouse heart
  #Authors ::= Sander Land, Steven A Niederer, Jan Magnus Aronsen, Emil K S Espe, Lili Zhang, William E Louch, Ivar Sjaastad, Ole M Sejersted, Nicolas P Smith
  #Year ::= 2012
  #Journal ::= Journal of Physiology 2012;590:4553-69
  #DOI ::=  10.1113/jphysiol.2012.231928
  #Comment ::= Rat ventricular active stress model (plugin)
} .reference();


Cai; .external(Cai); .nodal(); .units(uM);
length; .external(Lambda); .nodal(); .units(unitless);
delta_sl; .external(delLambda); .nodal(); .units(unitless/ms);
Tension; .external(); .nodal(); .units(kPa);

group {
  k_xb;     .units(unitless/ms);
  n_xb;     .units(unitless);
  TRPN_50;  .units(unitless);
  Ca_50ref; .units(umol/L);
  beta_1;   .units(unitless);
  k_TRPN;   .units(unitless/ms);
  n_TRPN;   .units(unitless);
  beta_0;   .units(unitless);
  T_ref;    .units(mN/mm^2);
  a;        .units(unitless);
  A_1;      .units(unitless);
  A_2;      .units(unitless);
  alpha_1;  .units(unitless/ms);
  alpha_2;  .units(unitless/ms);
} .param();

group {
  xb;       .units(unitless);
  TRPN;     .units(unitless);
  Q_1;      .units(unitless);
  Q_2;      .units(unitless);
} .method(rush_larsen);


# Constants
k_xb      =   0.1;   .units(unitless/ms);
n_xb      =   5;     .units(unitless);
TRPN_50   =   0.35;  .units(unitless);
Ca_50ref  =   0.8;   .units(umol/L);
beta_1    =  -1.5;   .units(unitless);
k_TRPN    =   0.1;   .units(unitless/ms);
n_TRPN    =   2;     .units(unitless);
beta_0    =   1.65;  .units(unitless);
T_ref     = 120;     .units(mN/mm^2);
a         =   0.35;  .units(unitless);
A_1       = -29;     .units(unitless);
A_2       = 116;     .units(unitless);
alpha_1   =   0.1;   .units(unitless/ms);
alpha_2   =   0.5;   .units(unitless/ms);

# Initial values
xb_init   = 0.00046; .units(unitless);
TRPN_init = 0.0752;  .units(unitless);
Q_1_init  = 0;       .units(unitless);
Q_2_init  = 0;       .units(unitless);

#dynamic_stiffness
Q = (Q_1+Q_2); .units(unitless);

# Tension is in kPa which is equivalent to 1000 N/m^2 = 1 mN/mm^2
Tension = ((Q<0.) ? ((T_0*((a*Q)+1.))/(1. - Q)) : ((T_0*(1.+((a+2.)*Q)))/(1.+Q))); .units(mN/mm^2);
diff_Q_1 = ((A_1*dlambdadt) - (alpha_1*Q_1)); .units(unitless/ms);
diff_Q_2 = ((A_2*dlambdadt) - (alpha_2*Q_2)); .units(unitless/ms);

#crossbridges
permtot = sqrt(pow((TRPN/TRPN_50),n_xb)); .units(unitless);
diff_xb = (k_xb*((permtot*(1. - xb)) - ((1./permtot)*xb))); .units(unitless/ms);

#troponin
# make sure Cai is in uMol/L
diff_TRPN = (k_TRPN*((pow((Cai/Ca_50),n_TRPN)*(1. - TRPN)) - TRPN)); .units(unitless/ms);
Ca_50 = (Ca_50ref*(1.+(beta_1*(lambda_m - 1.)))); .units(umol/L);

#filament_overlap
lambda_m = ((lambda>1.2) ? 1.2 : lambda); .units(unitless);
overlap = (1.+(beta_0*((lambda_m+lambda_s) - 1.87))); .units(unitless);
lambda_s = ((lambda_m>0.87) ? 0.87 : lambda_m); .units(unitless);

#Myofilaments
#dlambdadt = 0.; .units(unitless/ms);
#lambda = 1.; .units(unitless);

# default initialization external mechanics
delta_sl_init = 0.; .units(unitless/ms);
length_init   = 1.; .units(unitless);

lambda = length;
dlambdadt = delta_sl;

#environment

#isometric_tension
T_0 = (T_ref*xb*overlap); .units(mN/mm^2);


group{
 Tension;
} .trace();
