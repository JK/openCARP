group { 
  #Authors ::= DeBruin, K.A., Krassowska, W
  #Year ::= 1998
  #Title ::= Electroporation and Shock-Induced Transmembrane Potential in a Cardiac Fiber During Defibrillation Strength Shocks
  #Journal ::= Annals of Biomedical Engineering 26, 584-596
  #DOI ::= 10.1114/1.101
} .reference();

V;    .external(Vm); .nodal();
Iion; .external(); .nodal(); Iion = Iion;

PI = 3.14159;

group {
  alpha   = 200.0; .units(cm^-2* ms^-2); // electroporation parameter
  beta    = 6.25e-5; .units(mV^-2); //electroporation parameter
  q       = 2.46;  .units(unitless); // electroporation constant
  N0      = 1.5e5; .units(cm^-2); //pore density at Vm=0
  sigma   = 13.0; .units(ms/cm); // conductivity of pores
  h       = 5.0e-7; .units(cm); // membrane thickness (5nm)
  nn      = 0.15; .units(unitless); //relative entrance length of pores
  w0      = 5.25; .units(unitless); //energy barrier within pore
} .param();

k = 1.38066e-20; .units(mJ/K); // Boltzmann constant
T = 310.0; .units(K); // absolute temperature
e = 1.6021765e-19; .units(C); // charge on an electron

dvm_2 = V * V;
dN1   = alpha * exp( beta * dvm_2 );
dN2   = -dN1/N0*exp( -q * beta * dvm_2 );

nd_f = e / (k*T);
gp_f = PI * sigma * h * 0.25;

nd_vm = V * nd_f;   // nondimensional vm
nvm   = nn * nd_vm;   // n vm
nvmp  = w0 + nvm;   // w0 + n vm
nvmm  = w0 - nvm;   // w0 - n vm

gp = gp_f*( exp(nd_vm)-1 )/
  ( exp(nd_vm) * ( w0*exp(nvmm) - nvm ) / nvmm
    - ( w0*exp(nvmp) + nvm ) / nvmp
    );

n_init = N0*exp(q*beta*dvm_2);
//tau_n = -1/dN2;
//n_inf = dN1*tau_n;
diff_n = dN1+dN2*n;

Iion += gp*n*V;
