group {
  #Comment ::= GrandiPanditVoigt model (doi:10.1161/CIRCRESAHA.111.253955) coupled to the Land tension model (doi:10.1113/jphysiol.2012.231928)
  #Authors ::= Christoph M Augustin, Aurel Neic, Manfred Liebmann, Anton J Prassl, Steven A Niederer, Gundolf Haase, Gernot Plank
  #Year ::= 2016
  #Title ::= Anatomically accurate high resolution modeling of human whole heart electromechanics: A strongly scalable algebraic multigrid solver method for nonlinear deformation
  #Journal ::= J Comput Phys. 305:622-646
  #DOI ::= 10.1016/j.jcp.2015.10.045
} .reference();


V; .external(Vm); .nodal();
Iion;   .external(); .nodal();
length; .external(Lambda); .nodal(); .units(unitless);
delta_sl; .external(delLambda); .nodal(); .units(unitless/ms);
Tension; .external(); .nodal(); .units(kPa);

//VEk; .lookup(-500,500,0.0025);
VEk = V-ek;
//VEks; .lookup(-2000,2000,0.01);
VEks = V-eks;

// Cai is in mM, globally, Cai has to be in uM as per CARP definition
// TnCL is in mM, globally, TnCL has to be in uM 
Cai_mM = Cai/1000.; .units(mmol/L);

// Initial conditions
V_init=-8.09763e+1;
m_init = 1.405627e-3;
h_init= 9.867005e-1;
j_init = 9.915620e-1; 
d_init = 7.175662e-6; 
//f_init = 1.000681; 
f_init = 0.998607;
fcaBj_init = 2.421991e-2;
fcaBsl_init = 1.452605e-2;
xtos_init = 4.051574e-3;
ytos_init = 9.945511e-1; 
xtof_init = 4.051574e-3; 
ytof_init= 9.945511e-1; 
xkr_init = 8.641386e-3; 
xks_init= 5.412034e-3;
RyRr_init = 8.884332e-1;
RyRo_init = 8.156628e-7; 
RyRi_init = 1.024274e-7; 
NaBj_init = 3.539892;
NaBsl_init = 7.720854e-1; 
TnCL_init = 8.773191e-3; .units(mmol/L); 
TnCHc_init = 1.078283e-1; 
TnCHm_init = 1.524002e-2; 
CaM_init = 2.911916e-4; 
Myoc_init = 1.298754e-3; 
Myom_init = 1.381982e-1;
SRB_init = 2.143165e-3; 
SLLj_init = 9.566355e-3; 
SLLsl_init = 1.110363e-1; 
SLHj_init = 7.347888e-3; 
SLHsl_init = 7.297378e-2; 
Csqnb_init= 1.242988;
Ca_sr_init = 0.1e-1; //5.545201e-1; 
Naj_init = 9.06;//8.80329; 
Nasl_init = 9.06;//8.80733; 
Caj_init = 1.737475e-4; 
Casl_init= 1.031812e-4;  
rtos_init = 0.9946; 
ICajuncint_init = 1; 
ICaslint_init = 0;
C1_init = 0.0015;       // [] 
C2_init = 0.0244;       // [] 
C3_init = 0.1494;       // [] 
C4_init = 0.4071;       // [] 
C5_init = 0.4161;       // [] 
C7_init = 0.0001;       // [] 
C8_init = 0.0006;       // [] 
C9_init = 0.0008;       // [] 
C10_init = 0;           // [] 
C11_init = 0;           // [] 
C12_init = 0;           // [] 
C13_init = 0;           // [] 
C14_init = 0;           // [] 
C15_init = 0;           // [] 
O1_init = 0;            // [] 
O2_init = 0;            // [] 
C6_init = 1-(C1_init+C2_init+C3_init+C4_init+C5_init+C7_init+C8_init+C9_init+C10_init+C11_init+C12_init+C13_init+C14_init+C15_init+O1_init+O2_init);       // []

// Land initial values
xb_init   = 0.00046; .units(unitless);
TRPN_init = TnCL_init/Bmax_TnClow;  .units(unitless);
Q_1_init  = 0;       .units(unitless);
Q_2_init  = 0;       .units(unitless);

# default initialization external mechanics
delta_sl_init = 0.; .units(unitless/ms);
length_init   = 1.; .units(unitless);

// Numerical Methods
group {
  C1;
  C2;
  C3;
  C4;
  C5;
  C7;
  C8;
  C9;
  C10;
  C11;
  C12;
  C13;
  C14;
  C15;
  O1;
  C6;
  //RI;
  RyRr;
  RyRo;
  RyRi;
} .ub(1); .lb(0); .method(rush_larsen);


group {
  m;
  h;
  j;
  d;
  f;
  fcaBj;
  fcaBsl;
  xtof;
  ytof;
  xtos;
  ytos;
  xkr;
  //xks;
} .ub(1); .lb(0); .method(rush_larsen);

group {
  NaBj;
  NaBsl;
  //TnCL;
  TnCHc;
  TnCHm;
  CaM;
  Myoc;
  Myom;
  SRB;
  SLLj;
  SLLsl;
  SLHj;
  SLHsl;
  Csqnb;
  Ca_sr;
  Naj;
  Nasl;
  Caj;
  Casl;
  Cai;
} .lb(1e-10); .method(rush_larsen);

TnCl;.lb(1.e-10);

group {
  fcaBsl;
  fcaBj;
} .method(rush_larsen);

// Land variables
group {
  xb;       .units(unitless);
  Q_1;      .units(unitless);
  Q_2;      .units(unitless);
} .method(rush_larsen);

TRPN; .lb(0); .ub(1); .units(unitless);

group {
  Cai_init = 8.597401e-2;    // .units(umol/L);
  Nai_init = 9.06;//8.80853; 
  Ki_init = 120;
  markov_iks = 0;
  // Land stress
  k_xb      =   4.9e-3;.units(unitless/ms);
  n_xb      =   3.38;  .units(unitless);
  TRPN_50   =   0.37;  .units(unitless);
  Ca_50ref  =   0.52;  .units(umol/L);
  beta_1    =  -1.5;   .units(unitless);
  k_TRPN    =   0.14;  .units(unitless/ms);
  n_TRPN    =   1.54;  .units(unitless);
  beta_0    =   1.65;  .units(unitless);
  T_ref     = 117.1;   .units(mN/mm^2);
  a         =   0.35;  .units(unitless);
  A_1       = -29;     .units(unitless);
  A_2       = 116;     .units(unitless);
  alpha_1   =   0.1;   .units(unitless/ms);
  alpha_2   =   0.5;   .units(unitless/ms);
  lengthDep =   0;     .units(unitless);    // switch on length dependence
} .param(); 

// Gating variables      
//   1       2       3       4       5       6       7        8       9       10      11
//   m       h       j       d       f       fcaBj   fcaBsl   xtof    ytof    xkr     xks   

// RyR and Buffering variables
//   12      13      14      15      16      17      18      19      20      21      22
//   RyRr    RyRo    RyRi    NaBj    NaBsl   TnCL    TnCHc   TnCHm   CaM     Myoc    Myom  

// More buffering variables
//   23      24      25       26      27      28
//   SRB     SLLj    SLLsl    SLHj    SLHsl   Csqnb

// Intracellular concentrations/ Membrane voltage
//   29      30      31      32      33      34      35     36     37   
//   Ca_sr   Naj     Nasl    Nai     Ki      Caj     Casl   Cai   Vm    

// more
//   38  39  40  41  42  43  44  45  46  47   48   49   50   51   52   53
//   C1  C2  C3  C4  C5  C6  C7  C8  C9  C10  C11  C12  C13  C14  C15  O1

// even more
//   54   55    56  57
//   rkur skur  ml  hl 

// changes from ventricular model:
//   removed xtos
//   added rkur skur ml hl
//   others?


V; .external(Vm); .nodal();


//// Model Parameters
//// EPI or ENDO?
EPI = 0;
MCELL = 1;
ENDO = 2;

cell_type = EPI; .flag(EPI,MCELL,ENDO);

// Constants
pi = 3.14159;
R = 8314;       // [J/kmol*K]  
Frdy = 96485;   // [C/mol]  
Temp = 310;     // [K]
FoRT = Frdy/R/Temp;
Cm = 1.3810e-10;   // [F] membrane capacitance
Qpow = (Temp-310)/10;

// Cell geometry
cellLength = 100;     // cell length [um]
cellRadius = 10.25;   // cell radius [um]
junctionLength = 160e-3;  // junc length [um]
junctionRadius = 15e-3;   // junc radius [um]
distSLcyto = 0.45;    // dist. SL to cytosol [um]
distJuncSL = 0.5;  // dist. junc to SL [um]
DcaJuncSL = 1.64e-6;  // Dca junc to SL [cm^2/sec]
DcaSLcyto = 1.22e-6; // Dca SL to cyto [cm^2/sec]
DnaJuncSL = 1.09e-5;  // Dna junc to SL [cm^2/sec]
DnaSLcyto = 1.79e-5;  // Dna SL to cyto [cm^2/sec] 
Vcell = pi*cellRadius*cellRadius*cellLength*1e-15;    // [L]
Vmyo = 0.65*Vcell; Vsr = 0.035*Vcell; Vsl = 0.02*Vcell; Vjunc = 0.0539*.01*Vcell; 
SAjunc = 20150*pi*2*junctionLength*junctionRadius;  // [um^2]
SAsl = pi*2*cellRadius*cellLength;          // [um^2]
J_ca_juncsl =1/1.2134e12; // [L/msec] = 8.2413e-13
J_ca_slmyo = 1/2.68510e11; // [L/msec] = 3.2743e-12
J_na_juncsl = 1/(1.6382e12/3*100); // [L/msec] = 6.1043e-13
J_na_slmyo = 1/(1.8308e10/3*100);  // [L/msec] = 5.4621e-11

// Fractional currents in compartments
Fjunc = 0.11;   Fsl = 1-Fjunc;
Fjunc_CaL = 0.9; Fsl_CaL = 1-Fjunc_CaL;

// Fixed ion concentrations     
group {
  Cli = 15;   // Intracellular Cl  [mM]
  Clo = 150;  // Extracellular Cl  [mM]
  Ko = 5.4;   // Extracellular K   [mM]
  Nao = 140;  // Extracellular Na  [mM]
  Cao = 1.8;  // Extracellular Ca  [mM]1.8
  Mgi = 1;    // Intracellular Mg  [mM]
} .param();

// Nernst Potentials
ena_junc = (1/FoRT)*log(Nao/Naj);     // [mV]
ena_sl = (1/FoRT)*log(Nao/Nasl);       // [mV]
ek = (1/FoRT)*log(Ko/Ki);	        // [mV]
eca_junc = (1/FoRT/2)*log(Cao/Caj);   // [mV]
eca_sl = (1/FoRT/2)*log(Cao/Casl);     // [mV]
ecl = (1/FoRT)*log(Cli/Clo);            // [mV]

// Na transport parameters
GNa = 23;        // [mS/uF]
GNaB = 0.597e-3;    // [mS/uF] 0.897e-3
IbarNaK = 1.0*1.8;//1.90719;     // [uA/uF]
KmNaip = 11;         // [mM]11
KmKo =1.5;         // [mM]1.5
Q10NaK = 1.63;  
Q10KmNai = 1.39;

//// K current parameters
pNaK = 0.01833;      
gkp = 2*0.001;

// Cl current parameters
GClCa =0.5* 0.109625;   // [mS/uF]
GClB = 1*9e-3;        // [mS/uF]
KdClCa = 100e-3;    // [mM]

// ICa parameters
pNa = 0.50*1.5e-8;       // [cm/sec]
pCa = 0.50*5.4e-4;       // [cm/sec]
pK = 0.50*2.7e-7;        // [cm/sec]
Q10CaL = 1.8;       

//// Ca transport parameters
IbarNCX = 1.0*4.5;      // [uA/uF]5.5 before - 9 in rabbit
KmCai = 3.59e-3;    // [mM]
KmCao = 1.3;        // [mM]
KmNai = 12.29;      // [mM]
KmNao = 87.5;       // [mM]
ksat = 0.32;        // [none]  
nu = 0.27;          // [none]
Kdact = 0.150e-3;   // [mM] 
Q10NCX = 1.57;      // [none]
IbarSLCaP = 0.0673; // IbarSLCaP FEI changed [uA/uF](2.2 umol/L cytosol/sec) jeff 0.093 [uA/uF]
KmPCa = 0.5e-3;     // [mM] 
GCaB = 5.513e-4;    // [uA/uF] 3
Q10SLCaP = 2.35;    // [none]

// SR flux parameters
Q10SRCaP = 2.6;          // [none]
Vmax_SRCaP = 1.0*5.3114e-3;  // [mM/msec] (286 umol/L cytosol/sec)
Kmf = 0.246e-3;          // [mM] default
//Kmf = 0.175e-3;          // [mM]
Kmr = 1.7;               // [mM]L cytosol
hillSRCaP = 1.787;       // [mM]
ks = 25;                 // [1/ms]      
koCa = 10;               // [mM^-2 1/ms]   //default 10   modified 20
kom = 0.06;              // [1/ms]     
kiCa = 0.5;              // [1/mM/ms]
kim = 0.005;             // [1/ms]
ec50SR = 0.45;           // [mM]

// Buffering parameters
// koff: [1/s] = 1e-3*[1/ms];  kon: [1/uM/s] = [1/mM/ms]
Bmax_Naj = 7.561;       // [mM] // Bmax_Naj = 3.7; (c-code difference?)  // Na buffering
Bmax_Nasl = 1.65;       // [mM]
koff_na = 1e-3;         // [1/ms]
kon_na = 0.1e-3;        // [1/mM/ms]
Bmax_TnClow = 70e-3;    // [mM/L]                    // TnC low affinity
koff_tncl = 19.6e-3;    // [1/ms] 
kon_tncl = 32.7;        // [1/mM/ms]
Bmax_TnChigh = 140e-3;  // [mM]                      // TnC high affinity 
koff_tnchca = 0.032e-3; // [1/ms] 
kon_tnchca = 2.37;      // [1/mM/ms]
koff_tnchmg = 3.33e-3;  // [1/ms] 
kon_tnchmg = 3e-3;      // [1/mM/ms]
Bmax_CaM = 24e-3;       // [mM] **? about setting to 0 in c-code**   // CaM buffering
koff_cam = 238e-3;      // [1/ms] 
kon_cam = 34;           // [1/mM/ms]
Bmax_myosin = 140e-3;   // [mM]                      // Myosin buffering
koff_myoca = 0.46e-3;   // [1/ms]
kon_myoca = 13.8;       // [1/mM/ms]
koff_myomg = 0.057e-3;  // [1/ms]
kon_myomg = 0.0157;     // [1/mM/ms]
Bmax_SR = 19*.9e-3;     // [mM] (Bers text says 47e-3) 19e-3
koff_sr = 60e-3;        // [1/ms]
kon_sr = 100;           // [1/mM/ms]
Bmax_SLlowsl = 37.4e-3*Vmyo/Vsl;        // [mM]    // SL buffering
Bmax_SLlowj = 4.6e-3*Vmyo/Vjunc*0.1;    // [mM]    //Fei *0.1!!! junction reduction factor
koff_sll = 1300e-3;     // [1/ms]
kon_sll = 100;          // [1/mM/ms]
Bmax_SLhighsl = 13.4e-3*Vmyo/Vsl;       // [mM] 
Bmax_SLhighj = 1.65e-3*Vmyo/Vjunc*0.1;  // [mM] //Fei *0.1!!! junction reduction factor
koff_slh = 30e-3;       // [1/ms]
kon_slh = 100;          // [1/mM/ms]
Bmax_Csqn = 140e-3*Vmyo/Vsr;            // [mM] // Bmax_Csqn = 2.6;      // Csqn buffering
koff_csqn = 65;         // [1/ms] 
kon_csqn = 100;         // [1/mM/ms] 


//// Membrane Currents
// INa: Fast Na Current
mss_factor = (1 + exp( -(56.86 + V) / 9.03 ));
m_infinity = 1 / (mss_factor*mss_factor);
taum_factor1 = ((V+45.79)/15.54);
taum_factor2 = ((V-4.823)/51.12);
tau_m = 0.1292 * exp(-taum_factor1*taum_factor1) + 0.06487 * exp(-taum_factor2*taum_factor2);                 
 
ah = ((V >= -40)
      ? 0 
      : (0.057 * exp( -(V + 80) / 6.8 ))
      );
bh = ((V >= -40)
      ? (0.77 / (0.13*(1 + exp( -(V + 10.66) / 11.1 ))))
      : ((2.7 * exp( 0.079 * V) + 3.1e5 * exp(0.3485 * V)))
      );
tau_h = 1 / (ah + bh); 
hss_factor = (1 + exp( (V + 71.55)/7.43 ));
h_infinity = 1 / (hss_factor*hss_factor);
 
aj = ((V >= -40)
      ? (0)
      : (((-2.5428 * 10e4*exp(0.2444*V) - 6.948e-6 * exp(-0.04391*V)) * (V + 37.78)) / 
         (1 + exp( 0.311 * (V + 79.23) )))
      );
bj = ((V >= -40)
      ? ((0.6 * exp( 0.057 * V)) / (1 + exp( -0.1 * (V + 32) )))
      : ((0.02424 * exp( -0.01052 * V )) / (1 + exp( -0.1378 * (V + 40.14) )))
      );
tau_j = 1 / (aj + bj);
jss_factor = (1 + exp( (V + 71.55)/7.43 ));
j_infinity = 1 / (jss_factor*jss_factor);         
 
    
INa_junc = Fjunc*GNa*m*m*m*h*j*(V-ena_junc);
INa_sl = Fsl*GNa*m*m*m*h*j*(V-ena_sl);
INa = INa_junc+INa_sl;

// INabk: Na Background Current
INabk_junc = Fjunc*GNaB*(V-ena_junc);
INabk_sl = Fsl*GNaB*(V-ena_sl);
INabk = INabk_junc+INabk_sl;

// INaK: Na/K Pump Current
pow_KmNaip_Naj_4 = pow(KmNaip/Naj,4);
pow_KmNaip_Nasl_4 = pow(KmNaip/Nasl,4);
sigma = (exp(Nao/67.3)-1)/7;
fnak = 1/(1+0.1245*exp(-0.1*V*FoRT)+0.0365*sigma*exp(-V*FoRT));
INaK_junc = 1*Fjunc*IbarNaK*fnak*Ko /(1+pow_KmNaip_Naj_4) /(Ko+KmKo);
INaK_sl = 1*Fsl*IbarNaK*fnak*Ko /(1+pow_KmNaip_Nasl_4) /(Ko+KmKo);
INaK = INaK_junc+INaK_sl;

//// IKr: Rapidly Activating K Current
gkr =1.0*0.035*sqrt(Ko/5.4);
xkr_infinity = 1/(1+exp(-(V+10)/5));
tau_xkr = 550/(1+exp((-22-V)/9))*6/(1+exp((V-(-11))/9))+230/(1+exp((V-(-40))/20));
rkr = 1/(1+exp((V+74)/24));


IKr = gkr*xkr*rkr*(VEk);

//// IKs: Slowly Activating K Current
eks = (1/FoRT)*log((Ko+pNaK*Nao)/(Ki+pNaK*Nai));

xks_infinity = 1 / (1+exp(-(V + 3.8)/14.25)); // fitting Fra
tau_xks = 990.1/(1+exp(-(V+2.436)/14.12));

diff_C1=-4*alpha*C1+beta*C2;
diff_C2 = 4*alpha*C1-(beta+gamma+3*alpha)*C2+2*beta*C3;
diff_C3 = 3*alpha*C2-(2*beta+2*gamma+2*alpha)*C3+3*beta*C4;
diff_C4 = 2*alpha*C3-(3*beta+3*gamma+alpha)*C4+4*beta*C5;
diff_C5 = 1*alpha*C3-(4*beta+4*gamma)*C5+delta*C9;    
diff_C6 = gamma*C2-(delta+3*alpha)*C6+beta*C7;   
diff_C7 = 2*gamma*C3+3*alpha*C6-(delta+beta+2*alpha+gamma)*C7+2*beta*C8+2*delta*C10;
diff_C8 = 3*gamma*C4+2*alpha*C7-(delta+2*beta+1*alpha+2*gamma)*C8+3*beta*C9+2*delta*C11;
diff_C9 = 4*gamma*C5+1*alpha*C8-(delta+3*beta+0*alpha+3*gamma)*C9+2*delta*C12;
diff_C10 = 1*gamma*C7-(2*delta+2*alpha)*C10+beta*C11;  
diff_C11 = 2*gamma*C8+2*alpha*C10-(2*delta+beta+1*alpha+gamma)*C11+2*beta*C12+3*delta*C13;
diff_C12 = 3*gamma*C9+1*alpha*C11-(2*delta+2*beta+2*gamma)*C12+3*delta*C14;
diff_C13 = 1*gamma*C11-(3*delta+1*alpha)*C13+beta*C14;  
diff_C14 = 2*gamma*C12+1*alpha*C13-(3*delta+1*beta+1*gamma)*C14+4*delta*C15;
diff_C15 = 1*gamma*C14-(4*delta+teta)*C15+eta*O1;
O2 = 1-(C1+C2+C3+C4+C5+C6+C7+C8+C9+C10+C11+C12+C13+C14+C15+O1);
diff_O1 = 1*teta*C15-(eta+psi)*O1+omega*O2;

alpha = 3.98e-4*exp(3.61e-1*V*FoRT);
beta = 5.74e-5*exp(-9.23e-2*V*FoRT);
gamma = 3.41e-3*exp(8.68e-1*V*FoRT);
delta = 1.2e-3*exp(-3.3e-1*V*FoRT);
teta = 6.47e-3;
eta = 1.25e-2*exp(-4.81e-1*V*FoRT);
psi = 6.33e-3*exp(1.27*V*FoRT);
omega = 4.91e-3*exp(-6.79e-1*V*FoRT);

if (markov_iks==0) {
  gks_junc = 1*0.0035;
  gks_sl = 1*0.0035; //FRA
  IKs_junc = Fjunc*gks_junc*xks*xks*(VEks);
  IKs_sl = Fsl*gks_sl*xks*xks*(VEks); 
}
else {
  gks_junc = 0.0065;
  gks_sl = 0.0065; //FRA
  IKs_junc = Fjunc*gks_junc*(O1+O2)*(VEks);
  IKs_sl = Fsl*gks_sl*(O1+O2)*(VEks);
}

IKs = IKs_junc+IKs_sl;

//IKp: Plateau K current
kp_kp = 1/(1+exp(7.488-V/5.98));
IKp_junc = Fjunc*gkp*kp_kp*(VEk);
IKp_sl = Fsl*gkp*kp_kp*(VEk);
IKp = IKp_junc+IKp_sl;

//// Ito: Transient Outward K Current (slow and fast components)
// modified for human myocytes
if (cell_type == EPI) {
  GtoSlow = 1.0*0.13*0.12;
  GtoFast = 1.0*0.13*0.88;
} else {
  GtoSlow = 0.13*0.3*0.964;
  GtoFast = 0.13*0.3*0.036;
}
xtos_infinity = 1/(1+exp(-(V-19.0)/13));
ytos_infinity = 1/(1+exp((V+19.5)/5));
tau_xtos = 9/(1+exp((V+3.0)/15))+0.5;
tau_ytos = 800/(1+exp((V+60.0)/10))+30;
Itos = GtoSlow*xtos*ytos*(VEk);    // [uA/uF]

tau_xtof = 8.5*exp(-pow((V+45)/50,2))+0.5;
tau_ytof = 85*exp((-pow(V+40,2)/220))+7;
xtof_infinity = xtos_infinity;
ytof_infinity = ytos_infinity;
Itof = GtoFast*xtof*ytof*(VEk);
Ito = Itos + Itof;

//// IKi: Time-Independent K Current
aki = 1.02/(1+exp(0.2385*(VEk-59.215)));
bki =(0.49124*exp(0.08032*(VEk+5.476)) + exp(0.06175*(VEk-594.31))) /(1 + exp(-0.5143*(VEk+4.753)));
kiss = aki/(aki+bki);
IKi =1* 0.35*sqrt(Ko/5.4)*kiss*(VEk);

// I_ClCa: Ca-activated Cl Current, I_Clbk: background Cl Current
I_ClCa_junc = Fjunc*GClCa/(1+KdClCa/Caj)*(V-ecl);
I_ClCa_sl = Fsl*GClCa/(1+KdClCa/Casl)*(V-ecl);
I_ClCa = I_ClCa_junc+I_ClCa_sl;
I_Clbk = GClB*(V-ecl);

//// ICa: L-type Calcium Current
f_infinity = 1/(1+exp((V+35)/9))+0.6/(1+exp((50-V)/20));
d_infinity = 1/(1+exp(-(V+5)/6.0));
d_remove_tol = 1e-4;
if (-5-d_remove_tol <= V and V <= -5+d_remove_tol) {
  tau_d = d_infinity/6.0/0.035;
} else {
  tau_d = d_infinity*(1-exp(-(V+5)/6.0))/(0.035*(V+5));
}
tau_f = 1/(0.0197*exp( -pow(0.0337*(V+14.5),2) )+0.02);

diff_fcaBj = 1.7*Caj*(1-fcaBj)-11.9e-3*fcaBj; // fCa_junc   koff!!!!!!!!
diff_fcaBsl = 1.7*Casl*(1-fcaBsl)-11.9e-3*fcaBsl; // fCa_sl
fcaCaMSL = 0;
fcaCaj= 0;
pow_Q10Cal_Qpow = pow(Q10CaL,Qpow);
expm1_2VFoRT = expm1(2*V*FoRT);
expm1_VFoRT = expm1(V*FoRT);
exp_VFoRT = exp(V*FoRT);
exp_2VFoRT = exp(2*V*FoRT);
if (V == 0) {
  ibarca_j = pCa*4*(Frdy*FoRT) * (0.341*Caj) /(2*FoRT);
  ibarca_sl = pCa*4*(Frdy*FoRT) * (0.341*Casl) /(2*FoRT);
  ibark = pK*(Frdy*FoRT)*(0.75*Ki) /(FoRT);
  ibarna_j = pNa*(Frdy*FoRT) *(0.75*Naj)  /(FoRT);
  ibarna_sl = pNa*(Frdy*FoRT) *(0.75*Nasl)  /(FoRT);
} else {
  ibarca_j = pCa*4*(V*Frdy*FoRT) * (0.341*Caj*exp_2VFoRT-0.341*Cao) /(expm1_2VFoRT);
  ibarca_sl = pCa*4*(V*Frdy*FoRT) * (0.341*Casl*exp_2VFoRT-0.341*Cao) /(expm1_2VFoRT);
  ibark = pK*(V*Frdy*FoRT)*(0.75*Ki*exp_VFoRT-0.75*Ko) /(expm1_VFoRT);
  ibarna_j = pNa*(V*Frdy*FoRT) *(0.75*Naj*exp_VFoRT-0.75*Nao)  /(expm1_VFoRT);
  ibarna_sl = pNa*(V*Frdy*FoRT) *(0.75*Nasl*exp_VFoRT-0.75*Nao)  /(expm1_VFoRT);
}
ICa_junc = (Fjunc_CaL*ibarca_j*d*f*((1-fcaBj)+fcaCaj)*pow_Q10Cal_Qpow)*0.45*1;
ICa_sl = (Fsl_CaL*ibarca_sl*d*f*((1-fcaBsl)+fcaCaMSL)*pow_Q10Cal_Qpow)*0.45*1;
ICa = ICa_junc+ICa_sl;
ICaK = (ibark*d*f*(Fjunc_CaL*(fcaCaj+(1-fcaBj))+Fsl_CaL*(fcaCaMSL+(1-fcaBsl)))*pow_Q10Cal_Qpow)*0.45*1;
ICaNa_junc = (Fjunc_CaL*ibarna_j*d*f*((1-fcaBj)+fcaCaj)*pow_Q10Cal_Qpow)*0.45*1;
ICaNa_sl = (Fsl_CaL*ibarna_sl*d*f*((1-fcaBsl)+fcaCaMSL)*pow_Q10Cal_Qpow)*.45*1;
ICaNa = ICaNa_junc+ICaNa_sl;
ICatot = ICa+ICaK+ICaNa;

// INCX: Na/Ca Exchanger flux
exp_nu_V_FoRT = exp(nu*V*FoRT);
expm1_nu_V_FoRT = exp((nu-1)*V*FoRT);
pow_KmNao_3 = pow(KmNao,3);
Ka_junc = 1/(1+pow(Kdact/Caj,2));
Ka_sl = 1/(1+pow(Kdact/Casl,2));
s1_junc = exp_nu_V_FoRT*Naj*Naj*Naj*Cao;
s1_sl = exp_nu_V_FoRT*Nasl*Nasl*Nasl*Cao;
s2_junc = expm1_nu_V_FoRT*Nao*Nao*Nao*Caj;
s3_junc = KmCai*Nao*Nao*Nao*(1+pow(Naj/KmNai,3)) + pow_KmNao_3*Caj*(1+Caj/KmCai)+KmCao*Naj*Naj*Naj+Naj*Naj*Naj*Cao+Nao*Nao*Nao*Caj;
s2_sl = expm1_nu_V_FoRT*Nao*Nao*Nao*Casl;
s3_sl = KmCai*Nao*Nao*Nao*(1+pow(Nasl/KmNai,3)) + pow_KmNao_3*Casl*(1+Casl/KmCai)+KmCao*Nasl*Nasl*Nasl+Nasl*Nasl*Nasl*Cao+Nao*Nao*Nao*Casl;

pow_Q10NCX_Qpow = pow(Q10NCX,Qpow);
INCX_junc = Fjunc*IbarNCX*pow_Q10NCX_Qpow*Ka_junc*(s1_junc-s2_junc)/s3_junc/(1+ksat*expm1_nu_V_FoRT);
INCX_sl = Fsl*IbarNCX*pow_Q10NCX_Qpow*Ka_sl*(s1_sl-s2_sl)/s3_sl/(1+ksat*expm1_nu_V_FoRT);
INCX = INCX_junc+INCX_sl;

// I_pca: Sarcolemmal Ca Pump Current
pow_Q10SLCaP_Qpow = pow(Q10SLCaP,Qpow);
pow_Caj_1_6 = pow(Caj,1.6);
pow_KmPCa_1_6 = pow(KmPCa,1.6);
pow_Casl_1_6 = pow(Casl,1.6);
I_pca_junc = Fjunc*pow_Q10SLCaP_Qpow*IbarSLCaP*pow_Caj_1_6/(pow_KmPCa_1_6+pow_Caj_1_6);
I_pca_sl = Fsl*pow_Q10SLCaP_Qpow*IbarSLCaP*pow_Casl_1_6/(pow_KmPCa_1_6+pow_Casl_1_6);
I_pca = I_pca_junc+I_pca_sl;

// ICabk: Ca Background Current
ICabk_junc = Fjunc*GCaB*(V-eca_junc);
ICabk_sl = Fsl*GCaB*(V-eca_sl);
ICabk = ICabk_junc+ICabk_sl;

//// SR fluxes: Calcium Release, SR Ca pump, SR Ca leak
MaxSR = 15; MinSR = 1;
kCaSR = MaxSR - (MaxSR-MinSR)/(1+pow(ec50SR/Ca_sr,2.5));
koSRCa = koCa/kCaSR;
kiSRCa = kiCa*kCaSR;
RI = 1-RyRr-RyRo-RyRi;
diff_RyRr = (kim*RI-kiSRCa*Caj*RyRr)-(koSRCa*Caj*Caj*RyRr-kom*RyRo);   // R
diff_RyRo = (koSRCa*Caj*Caj*RyRr-kom*RyRo)-(kiSRCa*Caj*RyRo-kim*RyRi);// O
diff_RyRi = (kiSRCa*Caj*RyRo-kim*RyRi)-(kom*RyRi-koSRCa*Caj*Caj*RI);   // I
J_SRCarel = ks*RyRo*(Ca_sr-Caj);          // [mM/ms]
J_serca = 1*pow(Q10SRCaP,Qpow)*Vmax_SRCaP*(pow(Cai_mM/Kmf,hillSRCaP)-pow(Ca_sr/Kmr,hillSRCaP))
    /(1+pow(Cai_mM/Kmf,hillSRCaP)+pow(Ca_sr/Kmr,hillSRCaP));
J_SRleak = 5.348e-6*(Ca_sr-Caj);           //   [mM/ms]


//// Sodium and Calcium Buffering
diff_NaBj = kon_na*Naj*(Bmax_Naj-NaBj)-koff_na*NaBj;        // NaBj      [mM/ms]
diff_NaBsl = kon_na*Nasl*(Bmax_Nasl-NaBsl)-koff_na*NaBsl;       // NaBsl     [mM/ms]


// compute Land stress 

lambda = length; .units(unitless);
dlambdadt = delta_sl; .units(unitless/ms);

#filament_overlap
lambda_m = ((lambda>1.2) ? 1.2 : lambda); .units(unitless);
overlap  = (1.+(beta_0*((lambda_m+lambda_s) - 1.87))); .units(unitless);
lambda_s = ((lambda_m<0.87) ? 0.87 : lambda_m); .units(unitless);

#troponin
# compute fractional occupancy of buffer
#fTnCb = TnCL/Bmax_TnClow;
Ca_50 = (Ca_50ref*(1.+(beta_1*(lambda_m - 1.)))); .units(umol/L);
# TPRN simply tracks TnCL, just as fractional buffer occupancy
diff_TRPN   = k_TRPN*pow(Cai/Ca_50,      n_TRPN)*(1.         - TnCL/Bmax_TnClow) -k_TRPN*TnCL/Bmax_TnClow; .units(unitless/ms);

#crossbridges
permtot = sqrt(pow(((TnCL/Bmax_TnClow)/TRPN_50),n_xb)); .units(unitless);
diff_xb = (k_xb*((permtot*(1. - xb)) - ((1./permtot)*xb))); .units(unitless/ms);

#dynamic_stiffness
Q = (Q_1+Q_2); .units(unitless);

# Tension is in kPa which is equivalent to 1000 N/m^2 = 1 mN/mm^2
Tension = ((Q<0.) ? ((T_0*((a*Q)+1.))/(1. - Q)) : ((T_0*(1.+((a+2.)*Q)))/(1.+Q))); .units(mN/mm^2);
diff_Q_1 = ((A_1*dlambdadt) - (alpha_1*Q_1)); .units(unitless/ms);
diff_Q_2 = ((A_2*dlambdadt) - (alpha_2*Q_2)); .units(unitless/ms);

#isometric_tension
T_0 = (T_ref*xb*overlap); .units(mN/mm^2);


// Cytosolic Ca Buffers

// Land Tnc with/without length dependence
// without length dependence, we keep Ca_50 constant indepdent of lambda
Ca_50ref_mM = Ca_50ref*1e-3;
Ca_50_mM = ( lengthDep>0
            ? Ca_50ref_mM*(1.+(beta_1*(lambda_m - 1.)))
            : Ca_50ref_mM
           ); .units(mmol/L);

// compute TnCL according to Land
diff_TnCL  = k_TRPN*pow(Cai_mM/Ca_50_mM,n_TRPN)*(Bmax_TnClow-TnCL)-k_TRPN*TnCL;
diff_TnCHc = kon_tnchca*Cai_mM*(Bmax_TnChigh-TnCHc-TnCHm)-koff_tnchca*TnCHc; // TnCHc     [mM/ms]
diff_TnCHm = kon_tnchmg*Mgi*(Bmax_TnChigh-TnCHc-TnCHm)-koff_tnchmg*TnCHm;   // TnCHm     [mM/ms]
diff_CaM = kon_cam*Cai_mM*(Bmax_CaM-CaM)-koff_cam*CaM;                 // CaM       [mM/ms]
diff_Myoc = kon_myoca*Cai_mM*(Bmax_myosin-Myoc-Myom)-koff_myoca*Myoc;    // Myosin_ca [mM/ms]
diff_Myom = kon_myomg*Mgi*(Bmax_myosin-Myoc-Myom)-koff_myomg*Myom;      // Myosin_mg [mM/ms]
diff_SRB = kon_sr*Cai_mM*(Bmax_SR-SRB)-koff_sr*SRB;                    // SRB       [mM/ms]
J_CaB_cytosol = diff_TnCL +diff_TnCHc +diff_TnCHm +diff_CaM +diff_Myoc +diff_Myom +diff_SRB;

// Junctional and SL Ca Buffers
diff_SLLj = kon_sll*Caj*(Bmax_SLlowj-SLLj)-koff_sll*SLLj;       // SLLj      [mM/ms]
diff_SLLsl = kon_sll*Casl*(Bmax_SLlowsl-SLLsl)-koff_sll*SLLsl;      // SLLsl     [mM/ms]
diff_SLHj = kon_slh*Caj*(Bmax_SLhighj-SLHj)-koff_slh*SLHj;      // SLHj      [mM/ms]
diff_SLHsl = kon_slh*Casl*(Bmax_SLhighsl-SLHsl)-koff_slh*SLHsl;     // SLHsl     [mM/ms]
J_CaB_junction = diff_SLLj+diff_SLHj;
J_CaB_sl = diff_SLLsl+diff_SLHsl;

//// Ion concentrations
// SR Ca Concentrations
diff_Csqnb = kon_csqn*Ca_sr*(Bmax_Csqn-Csqnb)-koff_csqn*Csqnb;       // Csqn      [mM/ms]
diff_Ca_sr = J_serca-(J_SRleak*Vmyo/Vsr+J_SRCarel)-diff_Csqnb;         // Ca_sr     [mM/ms] //Ratio 3 leak current

// Sodium Concentrations
INa_tot_junc = INa_junc+INabk_junc+3*INCX_junc+3*INaK_junc+ICaNa_junc;   // [uA/uF]
INa_tot_sl = INa_sl+INabk_sl+3*INCX_sl+3*INaK_sl+ICaNa_sl;   // [uA/uF]
INa_tot_sl2 = 3*INCX_sl+3*INaK_sl+ICaNa_sl;   // [uA/uF]
INa_tot_junc2 = 3*INCX_junc+3*INaK_junc+ICaNa_junc;   // [uA/uF]

diff_Naj = -INa_tot_junc*Cm/(Vjunc*Frdy)+J_na_juncsl/Vjunc*(Nasl-Naj)-diff_NaBj;
diff_Nasl = -INa_tot_sl*Cm/(Vsl*Frdy)+J_na_juncsl/Vsl*(Naj-Nasl)
   +J_na_slmyo/Vsl*(Nai-Nasl)-diff_NaBsl;
diff_Nai = J_na_slmyo/Vmyo*(Nasl-Nai);             // [mM/msec] 

// Potassium Concentration
IK_tot = Ito+IKr+IKs+IKi-2*INaK+ICaK+IKp;     // [uA/uF]
diff_Ki = -IK_tot*Cm/(Vmyo*Frdy);                  // [mM/msec]

// Calcium Concentrations
ICa_tot_junc = ICa_junc+ICabk_junc+I_pca_junc-2*INCX_junc;                   // [uA/uF]
ICa_tot_sl = ICa_sl+ICabk_sl+I_pca_sl-2*INCX_sl;            // [uA/uF]
diff_Caj = -ICa_tot_junc*Cm/(Vjunc*2*Frdy)+J_ca_juncsl/Vjunc*(Casl-Caj)
    -J_CaB_junction+(J_SRCarel)*Vsr/Vjunc+J_SRleak*Vmyo/Vjunc;  // Ca_j
diff_Casl = -ICa_tot_sl*Cm/(Vsl*2*Frdy)+J_ca_juncsl/Vsl*(Caj-Casl)
    + J_ca_slmyo/Vsl*(Cai_mM-Casl)-J_CaB_sl;   // Ca_sl
diff_Cai = (-J_serca*Vsr/Vmyo-J_CaB_cytosol +J_ca_slmyo/Vmyo*(Casl-Cai_mM))*1.e3; // [uM/msec]

//// Membrane Potential
////
INa_tot = INa_tot_junc + INa_tot_sl;          // [uA/uF]
I_Cl_tot = I_ClCa+I_Clbk;                        // [uA/uF]
ICa_tot = ICa_tot_junc+ICa_tot_sl;
Iion = INa_tot+I_Cl_tot+ICa_tot+IK_tot;
// ----- END EC COUPLING MODEL ---------------
// adjust output depending on the function call


