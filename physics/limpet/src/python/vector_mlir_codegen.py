# ----------------------------------------------------------------------------
# openCARP is an open cardiac electrophysiology simulator.
#
# Copyright (C) 2022 openCARP project
#
# This program is licensed under the openCARP Academic Public License (APL)
# v1.0: You can use and redistribute it and/or modify it in non-commercial
# academic environments under the terms of APL as published by the openCARP
# project v1.0, or (at your option) any later version. Commercial use requires
# a commercial license (info@opencarp.org).
#
# This program is distributed without any warranty; see the openCARP APL for
# more details.
#
# You should have received a copy of the openCARP APL along with this program
# and can find it online: http://www.opencarp.org/license
# ----------------------------------------------------------------------------

from mlir_codegen import MlirCodegen
import mlir.ir as ir
import mlir.dialects as dialects
import mlir.dialects.arith
import mlir.dialects.func
import mlir.dialects.vector

from im import order


def value_has_changed(struct_params, store_value):
    # If this is not a CallOp, value has changed
    if not isinstance(store_value, dialects.func.CallOp):
        return True

    # If length is different, value has changed
    call_params = store_value.operands
    if len(struct_params) != len(call_params):
        return True

    # If the first operand of each CallOp is different, value has changed
    if struct_params[0] != call_params[0]:
        return True

    for i in range(1, len(call_params)):
        struct_param = struct_params[i]
        call_param = call_params[i]
        if isinstance(struct_param, ir.Value):
            struct_param = struct_param.owner
        if isinstance(call_param, ir.Value):
            call_param = call_param.owner
        if struct_param.result.result_number != call_param.result.result_number:
            return True
    return False


def save_data_structure(ds):
    temp_ds = ds.copy()
    ds.clear()
    return temp_ds


class VectorCodegen(MlirCodegen):

    def __init__(self, ionic_model, num_elements, filename, function_name, symbol_table, params_dict, region_dict,
                 ast_equations, regional_constants, state_dict, enums_dicts, tables, function_pointer_info,
                 lookup_variables, external_variables, external_private_variables, external_units,
                 needs_update_variables, all_lookup, external_update_variables, good_vars, update_inputs,
                 fe_targets, rush_larsen_targets, rk2_targets, rk4_targets, sundnes_half_targets, sundnes,
                 markov_be_targets, markov_be_solve, rosenbrock, rosenbrock_nodal_req, rosenbrock_partial_targets,
                 rosenbrock_diff_targets, rosenbrock_dict, global_constants, lhs_format_func, enable_store_opt,
                 enable_data_layout_opt, lookup_variables_order, is_openmp, model_name):
        self.num_elements = num_elements
        self.is_openmp = is_openmp
        super().__init__(ionic_model, model_name, filename, function_name, symbol_table, params_dict, region_dict,
                         ast_equations, regional_constants, state_dict, enums_dicts, tables, function_pointer_info,
                         lookup_variables, external_variables, external_private_variables, external_units,
                         needs_update_variables, all_lookup, external_update_variables, good_vars, update_inputs,
                         fe_targets, rush_larsen_targets, rk2_targets, rk4_targets, sundnes_half_targets, sundnes,
                         markov_be_targets, markov_be_solve, rosenbrock, rosenbrock_nodal_req,
                         rosenbrock_partial_targets, rosenbrock_diff_targets, rosenbrock_dict, global_constants,
                         lhs_format_func, enable_store_opt,
                         enable_data_layout_opt, lookup_variables_order)

    def get_computation_size(self):
        return self.num_elements

    def get_type(self, base_type):
        return ir.VectorType.get([self.num_elements], base_type)

    def get_adjusted_value(self, type, value):
        if type is not self.get_type(type):
            return dialects.vector.BroadcastOp(self.get_type(type), value)
        else:
            return value

    @staticmethod
    def create_omp_get_max_threads_func(module):
        with ir.InsertionPoint(module.body):
            i32 = ir.IntegerType.get_signless(32)
            return dialects.func.FuncOp("omp_get_thread_num", ([], [i32]), visibility="private")

    def get_thread_id(self):
        if not self.is_openmp:
            index_type = ir.IndexType.get()
            return dialects.arith.ConstantOp(index_type, 0)
        else:
            omp_get_thread_num_str = "omp_get_thread_num"
            if not omp_get_thread_num_str in self.function_dict:
                self.function_dict["omp_get_thread_num"] = self.create_omp_get_max_threads_func(self.module)

            index_type = ir.IndexType.get()
            omp_max_threads = dialects.func.CallOp(self.function_dict[omp_get_thread_num_str], [])
            return dialects.arith.IndexCastOp(index_type, omp_max_threads.result)

    def create_pointer_cast_operation(self, value, offset_index):
        vec_1xi8_ptr = ir.ShapedType(ir.Type.parse("memref<1xi8>", self.ctx))

        # Calculate new offset result, source, byte_shift, sizes
        return dialects.memref.ViewOp(vec_1xi8_ptr, value, offset_index, [])

    def get_maxi_value(self, value):
        i32 = ir.IntegerType.get_signless(32)
        maxi_pred = ir.Attribute.parse("#vector.kind<maxsi>")
        return dialects.vector.ReductionOp(i32, maxi_pred, value)

    def get_maxf_value(self, value):
        maxf_pred = ir.Attribute.parse("#vector.kind<maximumf>")
        return dialects.vector.ReductionOp(ir.F64Type.get(), maxf_pred, value)

    def create_integer_constant(self, element_type, value):
        vec_type = self.get_type(element_type)
        splat = ir.DenseElementsAttr.get_splat(vec_type, ir.IntegerAttr.get(element_type, int(value)))
        return dialects.arith.ConstantOp(vec_type, splat)

    def create_floating_point_constant(self, element_type, value):
        vec_type = self.get_type(element_type)
        splat = ir.DenseElementsAttr.get_splat(vec_type, ir.FloatAttr.get(element_type, float(value)))
        return dialects.arith.ConstantOp(vec_type, splat)

    def generate_rosenbrock_f_vars(self, rosenbrock):
        vars = []
        for var in order(rosenbrock):
            vars.append(var.info("diff"))
        return vars

    def generate_rosenbrock_jacobian_vars(self, rosenbrock):
        vars = []
        for row in order(rosenbrock):
            for column in order(rosenbrock):
                vars.append(self.ionic_model.partial(row.info("diff"), column))
        return vars

    def register_rosenbrock_variable(self, func, function_dict, name, pos):
        self.cache_value(function_dict, name, func.arguments[pos])
        self.params.append(name)

    def register_luts(self, func, function_dict):
        # LUT addresses
        current_pos = len(self.params)
        for i in self.tables:
            name = "table_" + i
            self.cache_value(function_dict, name, func.arguments[current_pos])
            self.params.append(name)
            current_pos += 1

    def register_rosenbrock_arguments(self, func, function_dict, has_dddd):
        if has_dddd:
            self.register_rosenbrock_variable(func, function_dict, "Rosenbrock_jacobian", 0)
        else:
            self.register_rosenbrock_variable(func, function_dict, "Rosenbrock_DX", 0)

        self.register_rosenbrock_variable(func, function_dict, "Rosenbrock_X", 1)
        self.register_regional_constants(func, function_dict)
        self.register_nodal_request(func, function_dict)
        self.register_ion_if_values(func, function_dict)
        if has_dddd:
            self.register_dddd_variable(func, function_dict)
        self.register_luts(func, function_dict)

    # Regional constants are always the third argument of the function call
    # %(name)s_rosenbrock_f_mlir(Rosenbrock_jacobian, Rosenbrock_X, rc, nr, region, p, sv, dddd);
    def register_regional_constants(self, func, function_dict):
        self.register_rosenbrock_variable(func, function_dict, "rc", 2)

    # Nodal requests are always the forth argument of the function call
    # %(name)s_rosenbrock_f_mlir(Rosenbrock_jacobian, Rosenbrock_X, rc, nr, region, p, sv, dddd);
    def register_nodal_request(self, func, function_dict):
        self.register_rosenbrock_variable(func, function_dict, "nr", 3)

    # Region is always the fifth argument of the function call
    # %(name)s_rosenbrock_f_mlir(Rosenbrock_jacobian, Rosenbrock_X, rc, nr, region, p, sv, dddd);
    def register_region_variable(self, func, function_dict):
        self.register_rosenbrock_variable(func, function_dict, "region", 4)

    # Parameters are always the sixth argument of the function call
    # %(name)s_rosenbrock_f_mlir(Rosenbrock_jacobian, Rosenbrock_X, rc, nr, region, p, sv, dddd);
    def register_parameter_variable(self, func, function_dict):
        self.register_rosenbrock_variable(func, function_dict, "p", 5)

    # Parameters are always the seventh argument of the function call
    # %(name)s_rosenbrock_f_mlir(Rosenbrock_jacobian, Rosenbrock_X, rc, nr, region, p, sv, dddd);
    def register_state_variable(self, func, function_dict):
        self.register_rosenbrock_variable(func, function_dict, "sv", 6)

    # Parameters are always the eigth argument of the function call
    # %(name)s_rosenbrock_f_mlir(Rosenbrock_jacobian, Rosenbrock_X, rc, nr, region, p, sv, dddd);
    def register_dddd_variable(self, func, function_dict):
        self.register_rosenbrock_variable(func, function_dict, "dddd", 7)

    # This registers to the following values from #ModelName_rosenbrock_f or #ModelName_rosenbrock_jacobian
    # Using the pointers variables that come from the function call
    # For example: %(name)s_rosenbrock_f_mlir(Rosenbrock_jacobian, Rosenbrock_X, rc, nr, region, p, sv, dddd);
    # This are the values registered:
    #   ION_IF* IF = ion_private->IF;
    #   cell_geom *region = &IF->cgeom;
    #   Shannon_Params *p = (Shannon_Params *)IF->params;
    #   Shannon_state *sv_base = (Shannon_state *)IF->sv_tab.y;
    #   int __i = ((Shannon_Private*)user_data)->node_number;
    #   Shannon_state *sv = sv_base+__i;
    def register_ion_if_values(self, func, function_dict):
        self.register_region_variable(func, function_dict)
        self.register_parameter_variable(func, function_dict)
        self.register_state_variable(func, function_dict)

    # Just initialize cell number with 0, since its value was already calculated
    def get_rosenbrock_cell_numbers(self):
        index_type = ir.IndexType.get()
        i32_type = ir.IntegerType.get_signless(32)
        return dialects.arith.ConstantOp(i32_type, 0), dialects.arith.ConstantOp(index_type, 0)

    # Save data structures from the main function since values cannot be accessed in the newly-created function
    # Function restore_main_function will restore values back to their originals
    # Inside the new function, data structures will be repopulated with values
    def save_main_function(self):
        return self.cell_number, self.cell_number_index, save_data_structure(self.mlir_values_dict), \
               save_data_structure(self.mlir_constant_dict), save_data_structure(self.func_args), \
               save_data_structure(self.params), self.loop

    # Restore values back from the main function. Function create_rosenbrock_function save values of the function
    def restore_main_function(self, cell_number, cell_number_idx, temp_dict, const_dict, func_args, params, loop):
        self.cell_number = cell_number
        self.cell_number_index = cell_number_idx
        self.cell_number_index_external = self.cell_number_index
        self.mlir_values_dict = temp_dict
        self.mlir_constant_dict = const_dict
        self.func_args = func_args
        self.params = params
        self.loop = loop

    def create_rosenbrock_compute_function(self, function_name, rosenbrock_targets,
                                           has_dddd_var, rosenbrock_vars, size, args):

        func_symbol, func = self.create_rosenbrock_function(function_name, args)

        # Create entry basic block with param_types
        entry_block = ir.Block.create_at_start(func.operation.regions[0], func.type.inputs)

        with ir.InsertionPoint(entry_block):

            cell_number, cell_number_idx, saved_dict, constant_dict, args, params, loop = self.save_main_function()

            self.cell_number, self.cell_number_index = self.get_rosenbrock_cell_numbers()
            self.cell_number_index_external = self.cell_number_index
            # Reset loop to this entry bb
            self.loop = entry_block
            self.register_rosenbrock_arguments(func, self.mlir_values_dict, has_dddd_var)
            self.handle_argument_pointers(func)
            self.create_luts()

            self.codegen_equations(rosenbrock_targets, self.good_vars | self.update_inputs, self.mlir_values_dict)

            # Pointer to the rosenbrock data structure
            rosenbrock_ptr = self.func_args[0]
            # Store values back to be accessed by the main function.
            # They are stored in a similar fashion to state variables, but using single precision.
            next_offset = lambda offset: offset + self.get_rosenbrock_precision()
            self.prepare_rosenbrock_values(rosenbrock_ptr, rosenbrock_vars, self.mlir_values_dict,
                                           next_offset, size, False)
            self.restore_main_function(cell_number, cell_number_idx, saved_dict, constant_dict, args, params, loop)

            # Return void
            dialects.func.ReturnOp([])


        return func_symbol, func

    def get_lut_parameters(self, params):
        [params.append("memref<8xi8>") for i in self.tables]

    def get_rosenbrock_functions(self):

        f_func_name = "%s_rosenbrock_f_mlir" % self.model_name
        f_symbol, _ = self.create_rosenbrock_function(f_func_name, ["memref<8xi8>", "memref<8xi8>", "memref<8xi8>"])

        # Get variables names to properly store them back at the end and generate the function
        rosenbrock_vars = self.generate_rosenbrock_f_vars(self.rosenbrock)
        size = len(rosenbrock_vars) * self.get_rosenbrock_precision()
        params = ["memref<8xi8>", "memref<8xi8>", "memref<8xi8>", "memref<8xi8>",
                  "memref<8xi8>", "memref<8xi8>", "memref<8xi8>"]

        # Add LUT parameters
        self.get_lut_parameters(params)
        self.create_rosenbrock_compute_function(f_func_name + "_vector", self.rosenbrock_diff_targets, False,
                                                order(rosenbrock_vars), size, params)

        # Get variables names to properly store them back at the end and generate the function
        jacobian_name = "%s_rosenbrock_jacobian" % self.model_name
        jacobian_symbol, _ = self.create_rosenbrock_function(jacobian_name + "_1d_mlir_vector", ["memref<8xi8>",
                                                             "memref<8xi8>", "memref<8xi8>", "i32"])
        rosenbrock_vars = self.generate_rosenbrock_jacobian_vars(self.rosenbrock)
        size = len(rosenbrock_vars) * self.get_rosenbrock_precision()
        params = ["memref<8xi8>", "memref<8xi8>", "memref<8xi8>", "memref<8xi8>",
                  "memref<8xi8>", "memref<8xi8>", "memref<8xi8>", "i32"]
        # Add LUT parameters
        self.get_lut_parameters(params)
        self.create_rosenbrock_compute_function(jacobian_name + "_mlir_vector", self.rosenbrock_partial_targets, True,
                                                rosenbrock_vars, size, params)
        return "rbStepX_%s" % (self.num_elements), f_symbol, jacobian_symbol

    def create_rosenbrock_stepx_call(self, rb_function, data_ptr, f_func, jacobian_func, ion_private_ptr):
        params = [data_ptr, f_func, jacobian_func, ion_private_ptr,
                  dialects.arith.TruncFOp(ir.F32Type.get(), self.get_function_argument("dt")),
                  dialects.arith.ConstantOp(ir.IntegerType.get_signless(32), len(self.rosenbrock))]
        return dialects.func.CallOp(rb_function, params)

    def create_rosenbrock_stepx(self, data_ptr, ion_private_ptr, stepx_name, f_func, jacobian_func):
        _, stepx = self.create_rosenbrock_function(stepx_name, ["memref<1xi8>", f_func.type, jacobian_func.type,
                                                                "memref<1xi8>", "f32", "i32"])
        self.create_rosenbrock_stepx_call(stepx, data_ptr, f_func, jacobian_func, ion_private_ptr)

        return "rbStepX_%s" % (self.num_elements), f_func, jacobian_func

    def get_lut_function_name(self):
        return "LUT_interpRow_n_elements_vector_" + str(self.num_elements) + "xf64"

    def get_function_info(self, op_type, is_contiguous, is_store, is_splat):

        ptr = ir.ShapedType(ir.Type.parse("memref<1xi8>", self.ctx))
        params_type = [ptr]

        f64 = ir.F64Type.get()
        if op_type == "f64":
            type_name = str(self.num_elements) + "xf64"
            vec_type = ir.VectorType.get([self.num_elements], f64)
        else:
            # Change type_name to match the function definition
            # TODO: For now, only f64 vectors are used. Enable the use of f32 vectors.
            type_name = "f64_from_type_" + str(self.num_elements) + "xf32"
            vec_type = ir.VectorType.get([self.num_elements], f64)

        if not is_store:
            operation = "load_%s_to_vector_" + type_name
            return_type = [vec_type]
        else:
            operation = "store_%s_from_vector_" + type_name
            params_type.append(vec_type)
            return_type = []

        i32 = ir.IntegerType.get_signless(32)
        if is_contiguous:
            function_name = (operation % "consecutive_elements")
            params_type.append(i32)
        elif is_splat:
            function_name = (operation % "single_value_struct")
            params_type.append(i32)
        else:
            function_name = (operation % "struct")
            params_type.append(i32)
            params_type.append(i32)

        return function_name, params_type, return_type

    # Create helper function to be called by the MLIR module
    def create_helper_functions(self):

        def create_lut_interpRow_function():
            i8_ptr = ir.ShapedType(ir.Type.parse("memref<1xi8>", self.ctx))
            f64 = ir.F64Type.get()
            vec_f64 = ir.VectorType.get([self.num_elements], f64)
            i32 = ir.IntegerType.get_signless(32)

            # Signature: void LUT_interpRow_n_elements(char*, vec<?xf64>, int, int, int, double*, int)
            function_name = "LUT_interpRow_n_elements_vector_" + str(self.num_elements) + "xf64"
            param_types = [i8_ptr, vec_f64, i32, i8_ptr, i32]
            self.function_dict[function_name] = dialects.func.FuncOp(function_name, (param_types, []),
                                                                     visibility="private")

        def create_conditional_load_store_function(is_store_call):

            vec_1xi8_ptr = ir.ShapedType(ir.Type.parse("memref<1xi8>", self.ctx))
            i32 = ir.IntegerType.get_signless(32)
            i1 = ir.IntegerType.get_signless(1)
            vec_i1 = ir.VectorType.get([self.num_elements], i1)
            f64 = ir.F64Type.get()
            vec_f64 = ir.VectorType.get([self.num_elements], f64)

            # Signature: load/store_conditional_...(char*, i32, i32, i32, i32, vector<<num_elems>xi1>,
            #                                       vector<<num_elems>xf64>, (optional) vector<<num_elems>xf64>)
            # Order: Base address, offset, 'state variable' struct size, vector mask, pass thru value or store value
            param_types = [vec_1xi8_ptr, i32, i32, vec_i1, vec_f64]

            # Second parameter changes depending on where it comes from
            if not is_store_call:
                if self.enable_data_layout_opt:
                    function_name = "load_consecutive_conditional_struct_to_vector_" + str(self.num_elements) + "xf64"
                else:
                    function_name = "load_conditional_struct_to_vector_" + str(self.num_elements) + "xf64"
                ret_type = [vec_f64]
            else:
                if self.enable_data_layout_opt:
                    function_name = "store_consecutive_conditional_vector_to_struct_" + str(self.num_elements) + "xf64"
                else:
                    function_name = "store_conditional_vector_to_struct_" + str(self.num_elements) + "xf64"
                ret_type = []

            func = dialects.func.FuncOp(function_name, (param_types, ret_type), visibility="private")
            self.function_dict[function_name] = func

        types = ["f32", "f64"]
        truefalse = [True, False]

        import itertools

        # Create a list that combines the four
        lists = [types, truefalse, truefalse, truefalse]
        lists = list(itertools.product(*lists))

        # Create helper functions for accessing struct elements, either consecutively or in strides
        for (type, is_consecutive, is_store, is_splat) in lists:
            function_name, params, ret = self.get_function_info(type, is_consecutive, is_store, is_splat)

            if function_name in self.function_dict:
                continue

            # Create a new function and cache it for later
            func = dialects.func.FuncOp(function_name, (params, ret), visibility="private")
            self.function_dict[function_name] = func

        # Create LUT_interpRow_n_elements_vector_<num elements>xf64
        create_lut_interpRow_function()

        # Create speculative load and store
        create_conditional_load_store_function(False)
        create_conditional_load_store_function(True)

    def allocate_lut(self, size):

        size = size * self.num_elements

        # Create new types that will be used for LUTs
        memref_lut_type = ir.ShapedType(ir.Type.parse("memref<%sxi8>" % (size), self.ctx))
        memref_unshaped = ir.ShapedType(ir.Type.parse("memref<?xi8>", self.ctx))
        memref_1xi8 = ir.ShapedType(ir.Type.parse("memref<1xi8>", self.ctx))
        index_type = ir.IndexType.get()

        # Allocate memory so that the LUT can store data for self.num_elements cells
        # And cast it to the right type (memref<1xi8>)
        lut_alloca = dialects.memref.AllocaOp(memref_lut_type, [], [])
        lut_unshaped = dialects.memref.CastOp(memref_unshaped, lut_alloca)
        offset_index = dialects.arith.ConstantOp(index_type, 0)
        lut_1xi8 = dialects.memref.ViewOp(memref_1xi8, lut_unshaped, offset_index, [])

        return lut_1xi8.result

    def create_lut_call(self, var, table_name, is_state_variable, enum_dict, lut_size, var_name):

        def get_argument(name):
            if name not in self.params:
                assert name + " is not a valid parameter"
            index = self.params.index(name)
            return self.func_args[index]

        params = []
        # Get table argument
        arg = get_argument("table_" + table_name)
        params.append(arg)

        i32 = ir.IntegerType.get_signless(32)
        if is_state_variable:
            lut_base_address = "sv"
        else:
            lut_base_address = var_name

        # If lut_base_address is a parameter, this is a pointer type,
        # Otherwise, this is a value
        if lut_base_address in self.mlir_values_dict:
            arg = self.mlir_values_dict[lut_base_address]
            params.append(arg.result)
        else:
            _, value = self.get_mlir_value(var)
            params.append(value.result)

        # Number of elements
        params.append(self.cell_number)

        # Get the pointer to allocated struct.
        # It is sent as a parameter of the function with its name having the prefix "lut_alloc_"
        # It is disabled for OpenMP and Rosenbrock
        if not self.is_openmp and not self.enable_rosenbrock:
            lut_alloc_index = self.params.index("lut_alloc_" + table_name)
            lut_ptr = self.func_args[lut_alloc_index]
        else:
            lut_ptr = self.allocate_lut(lut_size)
        params.append(lut_ptr)

        # Number of lut elements per cell. This values comes from the dictionary (length - 1 to ignore the 'Size' key)
        params.append(dialects.arith.ConstantOp(i32, len(enum_dict) - 1).result)

        dialects.func.CallOp(self.function_dict[self.get_lut_function_name()], params)
        return lut_ptr

    @staticmethod
    def get_adjusted_size(operand_type, size):
        if operand_type == "f32":
            return size >> 2
        else:
            return size >> 3

    @staticmethod
    def get_memory_function_name(operand_type, num_elements, prefix_str, type_conversion_prefix_str):
        if operand_type == "f32":
            type_name = type_conversion_prefix_str + str(num_elements) + "xf32"
        else:
            type_name = str(num_elements) + "xf64"
        return prefix_str + type_name

    def create_load_call(self, function_name, params):
        assert (function_name in self.function_dict)
        callee = self.function_dict[function_name]
        call_op = dialects.func.CallOp(callee, params)
        return call_op

    def create_store_call(self, function_name, store_value, params):
        # Don't bother generating store operation if this value is a parameter.
        # There are no effective changes to its value
        # Notice that we are passing a list w/o the second element of the list, which is the pointer value to be stored
        if not value_has_changed([params[i] for i in range(len(params)) if i != 1], store_value):
            return

        assert (function_name in self.function_dict)
        callee = self.function_dict[function_name]
        call_op = dialects.func.CallOp(callee, params)
        return call_op

    def create_contiguous_load(self, base_address, operand_type, offset, is_sv=False):
        function_name = self.get_memory_function_name(operand_type, self.num_elements,
                                                      ("load_%s_to_vector_" % "consecutive_elements"),
                                                      "f64_from_type_")

        i32 = ir.IntegerType.get_signless(32)
        return self.create_load_call(function_name, [base_address, dialects.arith.ConstantOp(i32, offset).result])

    def create_non_contiguous_load(self, base_address, operand_type, offset, size, is_sv=False):

        size = self.get_adjusted_size(operand_type, size)
        function_name = self.get_memory_function_name(operand_type, self.num_elements,
                                                      ("load_%s_to_vector_" % "struct"),
                                                      "f64_from_type_")

        i32 = ir.IntegerType.get_signless(32)
        return self.create_load_call(function_name, [base_address, dialects.arith.ConstantOp(i32, offset).result,
                                                     dialects.arith.ConstantOp(i32, size).result])

    def create_splat_load(self, base_address, operand_type, offset, is_sv=False):
        function_name = self.get_memory_function_name(operand_type, self.num_elements,
                                                      ("load_%s_to_vector_" % "single_value_struct"),
                                                      "f64_from_type_")

        i32 = ir.IntegerType.get_signless(32)
        return self.create_load_call(function_name, [base_address, dialects.arith.ConstantOp(i32, offset).result])

    def create_contiguous_store(self, base_address, store_value, operand_type, offset, is_sv=False):
        function_name = self.get_memory_function_name(operand_type, self.num_elements,
                                                      ("store_%s_from_vector_" % "consecutive_elements"),
                                                      "f64_from_type_")

        i32 = ir.IntegerType.get_signless(32)
        return self.create_store_call(function_name, store_value, [base_address, store_value.result,
                                                                   dialects.arith.ConstantOp(i32, offset).result])

    def create_non_contiguous_store(self, base_address, store_value, operand_type, offset, size, is_sv=False):
        size = self.get_adjusted_size(operand_type, size)
        function_name = self.get_memory_function_name(operand_type, self.num_elements,
                                                      ("store_%s_from_vector_" % "struct"),
                                                      "f64_from_type_")

        i32 = ir.IntegerType.get_signless(32)
        return self.create_store_call(function_name, store_value, [base_address, store_value.result,
                                                                   dialects.arith.ConstantOp(i32, offset).result,
                                                                   dialects.arith.ConstantOp(i32, size).result])

    def create_conditional_load_store(self, operand_type, base_address, offset, size, mask, pass_thru, is_store, offset_int=None, is_sv=False):

        if not is_store:
            if self.enable_data_layout_opt:
                function_name = "load_consecutive_conditional_struct_to_vector_" + str(self.num_elements)
            else:
                function_name = "load_conditional_struct_to_vector_" + str(self.num_elements)
        else:
            if self.enable_data_layout_opt:
                function_name = "store_consecutive_conditional_vector_to_struct_" + str(self.num_elements)
            else:
                function_name = "store_conditional_vector_to_struct_" + str(self.num_elements)

        function_name += "x" + operand_type

        callee = self.function_dict[function_name]
        call_op = dialects.func.CallOp(callee, [base_address, offset, size, mask, pass_thru])
        return call_op
