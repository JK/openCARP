package "bench"
version "1.1"
purpose "Use the IMP library for single cell experiments. All times in ms; all voltages in mV;  currents in uA/cm^2"

defmode "regstim" modedesc="regular periodic stimuli"
modeoption "numstim"     - "number of stimuli" int    default="1"  no mode="regstim"
modeoption "stim-start"  i "start of stimulation [ms]" double default="1." no mode="regstim"
modeoption "bcl" b "basic cycle length [ms]" double default="1000.0" no mode="regstim"

defmode "neqstim" modedesc="irregularly timed stimuli"
modeoption "stim-times"    - "comma separated list of stim times [ms]" string default="" no mode="neqstim" 
modeoption "DIA"          - "interpret stim times as distolic intervals" flag off     mode="neqstim" details="
        for example, bench --stim-times=0,300,200,100 --DIA
        would stimulate the cell at 0, 300, 500, and 600 ms\n"


defmode "restitute" modedesc="restitution curves"
modeoption "restitute" - "restitution experiment"  string values="S1S2","dyn","S1S2f"      mode="restitute"
modeoption "res-file"  - "definition file for restitution parameters" string default="" no mode="restitute" details="
       File format for S1S2 pacing protocols [default value]:
       1
       #prepacing beats [20] 
       S1 BCL  [1000]
       longest S2 [500]
       shortest S2 [200]
       #S1 beats between S2's [12]
       decrement of S2's [10]\n 
       for dynamic protocols:
       0
       #prepacing beats [20]
       starting BCL [400]
       ending BCL [200]
       beats per BCL [10]
       BCL decrement [10]\n"


modeoption "res-trace" - "output ionic model trace" flag off                               mode="restitute" 
modeoption "res-state-vector" - "save state vector" flag off                               mode="restitute" details="
      Save state vectors at different stages of the restitution protocol:
      \n
      S1S1   : Save state at the instant of S2 trigger
      DYNAMIC: Save state at the end of each S2 train 
               right before decrementing"

defmode "info" modedesc="output IMP information"
modeoption "list-imps"    - "list all available IMPs"    flag    off mode="info"
modeoption "plugin-outputs" - "list the outputs of available plugins" flag    off mode="info"
modeoption "imp-info"     - "print tunable parameters and state variables for particular IMPs" flag off mode="info"
modeoption "buildinfo"    - "(deprecated now printed by default) print build information about this executable" flag off mode="info"

defmode "vclamp" modedesc="Constant Voltage clamp"
modeoption "clamp-ini"  - "outside of clamp pulse, clamp Vm to this value" double default="-80.0" mode="vclamp" 
modeoption "clamp-file" - "clamp voltage to a signal read from a file"     string default="" mode="vclamp"

defgroup "stimtype" groupdesc="stimulus type"
groupoption "stim-curr" c "stimulation current [pA/pF]=[uA/cm^2]" default="60.0" float group="stimtype"
groupoption "stim-volt" - "stimulation voltage" float group="stimtype"
groupoption "stim-file" - "use signal from a .trc file for current stimulus" string group="stimtype"

section "Simulation control"
option "duration"      a "duration of simulation [ms]"  double optional 
details="      by default, duration is past-stim ms after the last stimulus delivered"
option "past-stim"     - "duration after last stim [ms]" double default="1000." no
option "dt"            D "time step [ms]"                double default=".01" no
option "num"           n "number of cells"          int    default="1" no
option "ext-vm-update" - "update Vm externally"     flag   off
option "rseed"         - "random number seed"       int    default="1" no
option "target"         - "target to run the simulation on"       string    default="auto" no

section "Stimuli"
option "stim-dur"           T "duration of stimulus pulse [ms]"  double default="1." no
option "stim-assign"        A "stimulus current assignment" flag   off
option "stim-species"       - "concentrations that should be affected by stimuli, e.g. 'Ki:Cli'" string default="Ki" no
option "stim-ratios"        - "proportions of stimlus current carried by each species, e.g. '0.7:0.3'" string default="1.0" no
option "resistance"         - "coupling resistance [kOhm]"  double default="100.0" no
option "surface-to-volume"  - "cell surface to cell volume ratio to be used to convert stimulus current to concentration change"  double default="0.14" no

section "Illumination (light stimulus ON/OFF)"
option "light-irrad"   - "unattenuated irradiance of illumination pulse (e.g. 0.24)"   double default="0.0"  no
option "light-dur"     - "duration of illumination pulses"                             double default="10."   no
option "light-numstim" - "number of illumination pulses (0: no limit)"                 int    default="1"     no
option "light-bcl"     - "basic cycle length of illumination"                          double default="1000." no
option "light-start"   - "start time for illumination pulse"                           double default="10."   no
option "light-times"   - "comma-separated list of stim times (overrides --light- vars for timing)" string default=""      no
option "light-file"    - "overrides ALL --light- vars with a signal from a file"       string default=""      no

section "Ionic Models"
option "imp"           I "IMP to use"                  string default="DrouhardRoberge" no
option "imp-par"       p "params to modify IMP"        string default="" no
option "plug-in"       P "plugins to use, separate with ':'"              string default="" no
option "plug-par"      m "params to modify plug-ins, separate params with ',' and plugin params with ':'"    string default="" no
option "load-module"   - "load a module for use with bench (implies --imp)" string default="" no multiple

section "Voltage clamp pulse"
option "clamp"           l "clamp Vm to this value [mV]"                         double default="0.0"   no
option "clamp-dur"       L "duration of Vm clamp pulse [ms]"                     double default="0.0"   no
option "clamp-start"     - "start time of Vm clamp pulse [ms]"                   double default="10.0"  no

section "Clamps"
option "clamp-SVs"      - "colon separated list of state variable to clamp" string default="" no
option "SV-clamp-files" - ": separated list of files from which to read state variables" string default="" no
option "SV-I-trigger"   - "apply SV clamps at each current stim"            flag   on
option "AP-clamp-file"  - "action potential trace applied at each stimulus" string default="" no

section "Strain"
option "strain"        N "amount of strain to apply"   double default="0" no
option "strain-time"   t "time to strain [ms]"              double default="200" no
option "strain-dur"    y "duration of strain (default=tonic) [ms]" float no
option "strain-rate"   - "time to apply/remove strain [ms]" double default="2" no

section "Output"
option "dt-out"        o "temporal output granularity [ms]" double default="1.0" no
option "start-out"     - "start time of output [ms]"        double default="0.0" no
option "fout"          O "output to files"             string default="BENCH_REG" no argoptional
option "no-trace"      - "do not output trace"         flag   off  
option "trace-no"      - "number for trace file name"  int    default="0"   no 
option "bin"           B "write binary files"          flag   off
option "imp-sv-dump"   u "sv dump list"                string default=""    no
option "plug-sv-dump"  g ": separated sv dump list for plug-ins, lists separated by semicolon"   string default="" no
option "dump-lut"      d "dump lookup tables"          flag   off
option "APstatistics"  - "compute AP statistics"       flag   off
option "validate"      v "output all SVs"              flag   off

section "State saving/restoring"
option "save-time"     s "time at which to save binary state [ms]"                  double default="0" no
option "save-file"     f "file in which to save binary state"                  string optional default="a.sv" 
option "restore"       r "restore saved binary state file"                     string default="a.sv" no
option "save-ini-file" F "text file in which to save state of a single cell"   string default="singlecell.sv" no
option "save-ini-time" S "time at which to save single cell state [ms]"             double default="0.0" no
option "read-ini-file" R "text file from which to read state of a single cell" string default="singlecell.sv" no
option "SV-init"       - "colon separated list of comma separated SV=initial_values" string no details="
        The string is of the form\n
        globals:IM:plugin1:plugin2:...\n
        where globals is one of Vm,Lambda,Ca_e, etc.
        To set Vm and Lambda-> Vm=-50,Lambda=1.1
        To set Ca_i in the ionic model-> :Ca_i=2.6
        To set ATP in the 3rd plugin-> ::::ATP=0.6
        all together-> Vm=-50,Lambda=1.1:Ca_i=2.6:::ATP=0.6"

section "Doppel"
option "doppel-on"     - "time to switch to dopple [ms]"    float optional                hidden
option "doppel-dur"    - "duration of dopple [ms]"          float default="100." optional hidden

