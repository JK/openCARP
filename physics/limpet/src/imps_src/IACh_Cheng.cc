// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Cheng D; Tung L; Sobie E.
*  Year: 1999
*  Title: Nonuniform responses of transmembrane potential during electric field stimulation of single cardiac cells
*  Journal: Am J Physiol Heart Circ Physiol 277:351-362
*  DOI: 10.1152/ajpheart.1999.277.1.H351
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "IACh_Cheng.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

IACh_ChengIonType::IACh_ChengIonType(bool plugin) : IonType(std::move(std::string("IACh_Cheng")), plugin) {}

size_t IACh_ChengIonType::params_size() const {
  return sizeof(struct IACh_Cheng_Params);
}

size_t IACh_ChengIonType::dlo_vector_size() const {

  return 1;
}

uint32_t IACh_ChengIonType::reqdat() const {
  return IACh_Cheng_REQDAT;
}

uint32_t IACh_ChengIonType::moddat() const {
  return IACh_Cheng_MODDAT;
}

void IACh_ChengIonType::destroy(IonIfBase& imp_base) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  imp.destroy_luts();
  // rarely need to do anything else
}

Target IACh_ChengIonType::select_target(Target target) const {
  switch (target) {
    case Target::AUTO:
#   ifdef IACH_CHENG_MLIR_CUDA_GENERATED
      return Target::MLIR_CUDA;
#   elif defined(IACH_CHENG_MLIR_ROCM_GENERATED)
      return Target::MLIR_ROCM;
#   elif defined(IACH_CHENG_MLIR_CPU_GENERATED)
      return Target::MLIR_CPU;
#   elif defined(IACH_CHENG_CPU_GENERATED)
      return Target::CPU;
#   else
      return Target::UNKNOWN;
#   endif
#   ifdef IACH_CHENG_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      return Target::MLIR_CUDA;
#   endif
#   ifdef IACH_CHENG_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      return Target::MLIR_ROCM;
#   endif
#   ifdef IACH_CHENG_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      return Target::MLIR_CPU;
#   endif
#   ifdef IACH_CHENG_CPU_GENERATED
    case Target::CPU:
      return Target::CPU;
#   endif
    default:
      return Target::UNKNOWN;
  }
}

void IACh_ChengIonType::compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  switch(target) {
    case Target::AUTO:
#   ifdef IACH_CHENG_MLIR_CUDA_GENERATED
      compute_IACh_Cheng_mlir_gpu_cuda(start, end, imp, data);
#   elif defined(IACH_CHENG_MLIR_ROCM_GENERATED)
      compute_IACh_Cheng_mlir_gpu_rocm(start, end, imp, data);
#   elif defined(IACH_CHENG_MLIR_CPU_GENERATED)
      compute_IACh_Cheng_mlir_cpu(start, end, imp, data);
#   elif defined(IACH_CHENG_CPU_GENERATED)
      compute_IACh_Cheng_cpu(start, end, imp, data);
#   else
#     error "Could not generate method IACh_ChengIonType::compute."
#   endif
      break;
#   ifdef IACH_CHENG_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      compute_IACh_Cheng_mlir_gpu_cuda(start, end, imp, data);
      break;
#   endif
#   ifdef IACH_CHENG_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      compute_IACh_Cheng_mlir_gpu_rocm(start, end, imp, data);
      break;
#   endif
#   ifdef IACH_CHENG_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      compute_IACh_Cheng_mlir_cpu(start, end, imp, data);
      break;
#   endif
#   ifdef IACH_CHENG_CPU_GENERATED
    case Target::CPU:
      compute_IACh_Cheng_cpu(start, end, imp, data);
      break;
#   endif
    default:
      throw std::runtime_error(std::string("Could not compute with the given target ") + get_string_from_target(target) + ".");
      break;
  }
}

// Define all constants
#define EC50 (GlobalData_t)(9.13652)
#define F (GlobalData_t)(96.4867)
#define Ki (GlobalData_t)(138.4)
#define Ko (GlobalData_t)(5.4)
#define R (GlobalData_t)(8.3143)
#define T (GlobalData_t)(310.)
#define a0 (GlobalData_t)(0.0517)
#define a1 (GlobalData_t)(0.4516)
#define a2 (GlobalData_t)(59.53)
#define a3 (GlobalData_t)(17.18)
#define n_ACh (GlobalData_t)(0.477811)
#define Ek (GlobalData_t)((((R*T)/F)*(log((Ko/Ki)))))



void IACh_ChengIonType::initialize_params(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  cell_geom* region = &imp.cgeom();
  IACh_Cheng_Params *p = imp.params();

  // Compute the regional constants
  {
    p->ACh = 0.;
    p->E_max = 10.;
  }
  // Compute the regional initialization
  {
  }

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.

    enum Rosenbrock {
    

      N_ROSEN
    };


void IACh_ChengIonType::construct_tables(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom* region = &imp.cgeom();
  IACh_Cheng_Params *p = imp.params();

  imp.tables().resize(N_TABS);

  // Define the constants that depend on the parameters.

}



void IACh_ChengIonType::initialize_sv(IonIfBase& imp_base, GlobalData_t **impdata ) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom *region = &imp.cgeom();
  IACh_Cheng_Params *p = imp.params();

  IACh_Cheng_state *sv_base = (IACh_Cheng_state *)imp.sv_tab().data();
  GlobalData_t t = 0;

  IonIfDerived* IF = &imp;
  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i < imp.get_num_node(); __i+=1 ){
    IACh_Cheng_state *sv = sv_base+__i / 1;
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;
  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
#ifdef IACH_CHENG_CPU_GENERATED
extern "C" {
void compute_IACh_Cheng_cpu(int start, int end, IonIfBase& imp_base, GlobalData_t **impdata )
{
  IACh_ChengIonType::IonIfDerived& imp = static_cast<IACh_ChengIonType::IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt()*1e0;
  cell_geom *region = &imp.cgeom();
  IACh_Cheng_Params *p  = imp.params();
  IACh_Cheng_state *sv_base = (IACh_Cheng_state *)imp.sv_tab().data();

  GlobalData_t t = imp.get_tstp().cnt*dt;

  IACh_ChengIonType::IonIfDerived* IF = &imp;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=(start / 1) * 1; __i<end; __i+=1) {
    IACh_Cheng_state *sv = sv_base+__i / 1;
                    
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    Iion = (Iion+(((p->E_max/(1.+(EC50/(pow(p->ACh,n_ACh)))))*(a0+(a1/(1.+(exp(((V+a2)/a3)))))))*(V-(Ek))));
    
    
    //Complete Forward Euler Update
    
    
    //Complete Rush Larsen Update
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    Iion = Iion;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

            }
}
#endif // IACH_CHENG_CPU_GENERATED

bool IACh_ChengIonType::has_trace() const {
    return false;
}

void IACh_ChengIonType::trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const {}
IonIfBase* IACh_ChengIonType::make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const {
        // Place the allocated IonIf in managed memory if a GPU target exists for this model
        // otherwise, place it in main RAM
    IonIfDerived* ptr;
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_ROCM, 1, true);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CUDA, 1, true);
    }
    else {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CPU, 1, true);
    }
    // Using placement new to place the object in the correct memory
    return new(ptr) IonIfDerived(*this, this->select_target(target),
    num_node, plugins);
}

void IACh_ChengIonType::destroy_ion_if(IonIfBase *imp) const {
    // Call destructor and deallocate manually because the object might
    // be located on GPU (delete won't work in this case)
    imp->~IonIfBase();
    IonIfDerived* ptr = static_cast<IonIfDerived *>(imp);
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_ROCM, ptr);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CUDA, ptr);
    }
    else {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CPU, ptr);
    }
}

}  // namespace limpet
        