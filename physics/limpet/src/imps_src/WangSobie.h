// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Linda J. Wang and Eric A. Sobie
*  Year: 2008
*  Title: Mathematical model of the neonatal mouse ventricular action potential
*  Journal: American Journal of Physiology-Heart and Circulatory Physiology, 294(6), 2565-2575
*  DOI: 10.1152/ajpheart.01376.2007
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __WANGSOBIE_H__
#define __WANGSOBIE_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"

#if !(defined(WANGSOBIE_CPU_GENERATED)    || defined(WANGSOBIE_MLIR_CPU_GENERATED)    || defined(WANGSOBIE_MLIR_ROCM_GENERATED)    || defined(WANGSOBIE_MLIR_CUDA_GENERATED))
#ifdef MLIR_CPU_GENERATED
#define WANGSOBIE_MLIR_CPU_GENERATED
#endif

#ifdef MLIR_ROCM_GENERATED
#define WANGSOBIE_MLIR_ROCM_GENERATED
#endif

#ifdef MLIR_CUDA_GENERATED
#define WANGSOBIE_MLIR_CUDA_GENERATED
#endif
#endif

#ifdef CPU_GENERATED
#define WANGSOBIE_CPU_GENERATED
#endif

namespace limpet {

#define WangSobie_REQDAT Vm_DATA_FLAG
#define WangSobie_MODDAT Iion_DATA_FLAG

struct WangSobie_Params {
    GlobalData_t Cai_init;
    GlobalData_t ICaL_slowdown;
    GlobalData_t Ki_init;
    GlobalData_t Nai_init;

};

struct WangSobie_state {
    GlobalData_t C2;
    GlobalData_t C3;
    GlobalData_t C4;
    GlobalData_t C_K1;
    GlobalData_t C_K2;
    GlobalData_t CaJSR;
    GlobalData_t CaNSR;
    GlobalData_t Cai;
    GlobalData_t Cass;
    GlobalData_t HTRPN_Ca;
    GlobalData_t I1;
    GlobalData_t I2;
    GlobalData_t I3;
    GlobalData_t ICaL_slowdown;
    GlobalData_t I_K;
    GlobalData_t Ki;
    GlobalData_t LTRPN_Ca;
    GlobalData_t Nai;
    GlobalData_t O;
    GlobalData_t O_K;
    GlobalData_t P_C2;
    GlobalData_t P_O1;
    GlobalData_t P_O2;
    GlobalData_t P_RyR;
    Gatetype ato_f;
    Gatetype aur;
    Gatetype b;
    Gatetype g;
    Gatetype h;
    Gatetype ito_f;
    Gatetype iur;
    Gatetype j;
    Gatetype m;
    Gatetype nKs;

};

class WangSobieIonType : public IonType {
public:
    using IonIfDerived = IonIf<WangSobieIonType>;
    using params_type = WangSobie_Params;
    using state_type = WangSobie_state;

    WangSobieIonType(bool plugin);

    size_t params_size() const override;

    size_t dlo_vector_size() const override;

    uint32_t reqdat() const override;

    uint32_t moddat() const override;

    void initialize_params(IonIfBase& imp_base) const override;

    void construct_tables(IonIfBase& imp_base) const override;

    void destroy(IonIfBase& imp_base) const override;

    void initialize_sv(IonIfBase& imp_base, GlobalData_t** data) const override;

    Target select_target(Target target) const override;

    void compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const override;

    bool has_trace() const override;

    void trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const override;

    void tune(IonIfBase& imp_base, const char* im_par) const override;

    int read_svs(IonIfBase& imp_base, FILE* file) const override;

    int write_svs(IonIfBase& imp_base, FILE* file, int node) const override;

    SVgetfcn get_sv_offset(const char* svname, int* off, int* sz) const override;

    int get_sv_list(char*** list) const override;

    int get_sv_type(const char* svname, int* type, char** type_name) const override;

    void print_params() const override;

    void print_metadata() const override;

    IonIfBase* make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const override;

    void destroy_ion_if(IonIfBase *imp) const override;
};

// This needs to be extern C in order to be linked correctly with the MLIR code
extern "C" {

//void compute_WangSobie(int, int, IonIfBase&, GlobalData_t**);
#ifdef WANGSOBIE_CPU_GENERATED
void compute_WangSobie_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef WANGSOBIE_MLIR_CPU_GENERATED
void compute_WangSobie_mlir_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef WANGSOBIE_MLIR_ROCM_GENERATED
void compute_WangSobie_mlir_gpu_rocm(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef WANGSOBIE_MLIR_CUDA_GENERATED
void compute_WangSobie_mlir_gpu_cuda(int, int, IonIfBase&, GlobalData_t**);
#endif

}
}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
