// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Aslanidi OV, Sleiman RN, Boyett MR, Hancox JC, Zhang H
*  Year: 2010
*  Title: Ionic mechanisms for electrical heterogeneity between rabbit Purkinje fiber and Ventricular cells
*  Journal: Biophys J, 98(11),2420-31
*  DOI: 10.1016/j.bpj.2010.02.033
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __ASLANIDISLEIMAN_H__
#define __ASLANIDISLEIMAN_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"

#if !(defined(ASLANIDISLEIMAN_CPU_GENERATED)    || defined(ASLANIDISLEIMAN_MLIR_CPU_GENERATED)    || defined(ASLANIDISLEIMAN_MLIR_ROCM_GENERATED)    || defined(ASLANIDISLEIMAN_MLIR_CUDA_GENERATED))
#ifdef MLIR_CPU_GENERATED
#define ASLANIDISLEIMAN_MLIR_CPU_GENERATED
#endif

#ifdef MLIR_ROCM_GENERATED
#define ASLANIDISLEIMAN_MLIR_ROCM_GENERATED
#endif

#ifdef MLIR_CUDA_GENERATED
#define ASLANIDISLEIMAN_MLIR_CUDA_GENERATED
#endif
#endif

#ifdef CPU_GENERATED
#define ASLANIDISLEIMAN_CPU_GENERATED
#endif

namespace limpet {

#define AslanidiSleiman_REQDAT Ke_DATA_FLAG|Vm_DATA_FLAG
#define AslanidiSleiman_MODDAT Iion_DATA_FLAG

struct AslanidiSleiman_Params {
    GlobalData_t Ca_handling;
    GlobalData_t Cae;
    GlobalData_t Cle;
    GlobalData_t Cli;
    GlobalData_t GCaB;
    GlobalData_t GCaL;
    GlobalData_t GCaT;
    GlobalData_t GCl;
    GlobalData_t GClB;
    GlobalData_t GK1;
    GlobalData_t GKB;
    GlobalData_t GKr;
    GlobalData_t GKs;
    GlobalData_t GNa;
    GlobalData_t GNaB;
    GlobalData_t GNaL;
    GlobalData_t Gto;
    GlobalData_t ICaPHill;
    GlobalData_t ICaPKmf;
    GlobalData_t ICaPVmf;
    GlobalData_t INaCamax;
    GlobalData_t Ke_init;
    GlobalData_t Ki;
    GlobalData_t Mgi;
    GlobalData_t Nae;
    GlobalData_t Nai;
    GlobalData_t PNaK;
    GlobalData_t T;
    GlobalData_t kmcaact;
    GlobalData_t kmcai;
    GlobalData_t kmcao;
    GlobalData_t kmnai1;
    GlobalData_t kmnao;
    GlobalData_t ksat;
    GlobalData_t nu;

};

struct AslanidiSleiman_state {
    GlobalData_t Cai;
    GlobalData_t Y0;
    GlobalData_t Y1;
    GlobalData_t Y11;
    GlobalData_t Y2;
    GlobalData_t Y23;
    GlobalData_t Y24;
    GlobalData_t Y25;
    GlobalData_t Y3;
    GlobalData_t Y32;
    GlobalData_t Y33;
    GlobalData_t Y34;
    GlobalData_t Y35;
    GlobalData_t Y36;
    GlobalData_t Y37;
    GlobalData_t Y38;
    GlobalData_t Y4;
    GlobalData_t Y5;
    GlobalData_t Y6;
    GlobalData_t Y7;
    Gatetype b;
    Gatetype d;
    Gatetype f;
    Gatetype g;
    Gatetype h;
    Gatetype hL;
    Gatetype i;
    Gatetype j;
    Gatetype m;
    Gatetype mL;
    Gatetype x;
    Gatetype xr;
    Gatetype xs;
    Gatetype y1;
    Gatetype y2;

};

    struct AslanidiSleiman_Regional_Constants_cpu {
                    
    GlobalData_t E_Cl;
    GlobalData_t E_Na;
    GlobalData_t Nae3;
    GlobalData_t Nai3;
    GlobalData_t RTonF;
    GlobalData_t kmnai13;
    GlobalData_t kmnao3;
    GlobalData_t partial_denomterm1_del_Cai;
    GlobalData_t partial_denomterm2_del_Cai;
    GlobalData_t partial_diff_Cai_del_Y1;
    GlobalData_t sigma;

    };

    struct AslanidiSleiman_Nodal_Req_cpu {
    
    GlobalData_t Ke;
    GlobalData_t V;

    };

    struct AslanidiSleiman_Private_cpu {
      using nodal_req_type = AslanidiSleiman_Nodal_Req_cpu;
      using regional_constants_type = AslanidiSleiman_Regional_Constants_cpu;
      IonIfBase *IF;
      int   node_number;
      void* cvode_mem;
      void* sunctx_ptr;
      bool  trace_init;
      regional_constants_type rc;
      nodal_req_type nr;
    };
    

//Printing out the Rosenbrock declarations
extern "C" {

void AslanidiSleiman_rosenbrock_f_cpu(float*, float*, void*);

void AslanidiSleiman_rosenbrock_jacobian_cpu(float**, float*, void*, int);

void rbStepX ( float *X,
                void (*calcDX) (float*,  float*, void*),
                void (*calcJ)  (float**, float*, void*, int ),
                void *params, float h, int N );
}

class AslanidiSleimanIonType : public IonType {
public:
    using IonIfDerived = IonIf<AslanidiSleimanIonType>;
    using params_type = AslanidiSleiman_Params;
    using state_type = AslanidiSleiman_state;

    using private_type = AslanidiSleiman_Private_cpu;
    #ifdef ASLANIDISLEIMAN_MLIR_CPU_GENERATED
    using private_type_vector = AslanidiSleiman_Private_mlir_cpu;
    #endif

    AslanidiSleimanIonType(bool plugin);

    size_t params_size() const override;

    size_t dlo_vector_size() const override;

    uint32_t reqdat() const override;

    uint32_t moddat() const override;

    void initialize_params(IonIfBase& imp_base) const override;

    void construct_tables(IonIfBase& imp_base) const override;

    void destroy(IonIfBase& imp_base) const override;

    void initialize_sv(IonIfBase& imp_base, GlobalData_t** data) const override;

    Target select_target(Target target) const override;

    void compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const override;

    bool has_trace() const override;

    void trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const override;

    void tune(IonIfBase& imp_base, const char* im_par) const override;

    int read_svs(IonIfBase& imp_base, FILE* file) const override;

    int write_svs(IonIfBase& imp_base, FILE* file, int node) const override;

    SVgetfcn get_sv_offset(const char* svname, int* off, int* sz) const override;

    int get_sv_list(char*** list) const override;

    int get_sv_type(const char* svname, int* type, char** type_name) const override;

    void print_params() const override;

    void print_metadata() const override;

    IonIfBase* make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const override;

    void destroy_ion_if(IonIfBase *imp) const override;
};

// This needs to be extern C in order to be linked correctly with the MLIR code
extern "C" {

//void compute_AslanidiSleiman(int, int, IonIfBase&, GlobalData_t**);
#ifdef ASLANIDISLEIMAN_CPU_GENERATED
void compute_AslanidiSleiman_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef ASLANIDISLEIMAN_MLIR_CPU_GENERATED
void compute_AslanidiSleiman_mlir_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef ASLANIDISLEIMAN_MLIR_ROCM_GENERATED
void compute_AslanidiSleiman_mlir_gpu_rocm(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef ASLANIDISLEIMAN_MLIR_CUDA_GENERATED
void compute_AslanidiSleiman_mlir_gpu_cuda(int, int, IonIfBase&, GlobalData_t**);
#endif

}
}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
