// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Hodgkin, A. L., Huxley, A. F.
*  Year: 1952
*  Title: A quantitative description of membrane current and its application to conduction and excitation in nerve
*  Journal: The Journal of Physiology, 117
*  DOI: 10.1113/jphysiol.1952.sp004764
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "HodgkinHuxley.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

HodgkinHuxleyIonType::HodgkinHuxleyIonType(bool plugin) : IonType(std::move(std::string("HodgkinHuxley")), plugin) {}

size_t HodgkinHuxleyIonType::params_size() const {
  return sizeof(struct HodgkinHuxley_Params);
}

size_t HodgkinHuxleyIonType::dlo_vector_size() const {

  return 1;
}

uint32_t HodgkinHuxleyIonType::reqdat() const {
  return HodgkinHuxley_REQDAT;
}

uint32_t HodgkinHuxleyIonType::moddat() const {
  return HodgkinHuxley_MODDAT;
}

void HodgkinHuxleyIonType::destroy(IonIfBase& imp_base) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  imp.destroy_luts();
  // rarely need to do anything else
}

Target HodgkinHuxleyIonType::select_target(Target target) const {
  switch (target) {
    case Target::AUTO:
#   ifdef HODGKINHUXLEY_MLIR_CUDA_GENERATED
      return Target::MLIR_CUDA;
#   elif defined(HODGKINHUXLEY_MLIR_ROCM_GENERATED)
      return Target::MLIR_ROCM;
#   elif defined(HODGKINHUXLEY_MLIR_CPU_GENERATED)
      return Target::MLIR_CPU;
#   elif defined(HODGKINHUXLEY_CPU_GENERATED)
      return Target::CPU;
#   else
      return Target::UNKNOWN;
#   endif
#   ifdef HODGKINHUXLEY_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      return Target::MLIR_CUDA;
#   endif
#   ifdef HODGKINHUXLEY_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      return Target::MLIR_ROCM;
#   endif
#   ifdef HODGKINHUXLEY_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      return Target::MLIR_CPU;
#   endif
#   ifdef HODGKINHUXLEY_CPU_GENERATED
    case Target::CPU:
      return Target::CPU;
#   endif
    default:
      return Target::UNKNOWN;
  }
}

void HodgkinHuxleyIonType::compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  switch(target) {
    case Target::AUTO:
#   ifdef HODGKINHUXLEY_MLIR_CUDA_GENERATED
      compute_HodgkinHuxley_mlir_gpu_cuda(start, end, imp, data);
#   elif defined(HODGKINHUXLEY_MLIR_ROCM_GENERATED)
      compute_HodgkinHuxley_mlir_gpu_rocm(start, end, imp, data);
#   elif defined(HODGKINHUXLEY_MLIR_CPU_GENERATED)
      compute_HodgkinHuxley_mlir_cpu(start, end, imp, data);
#   elif defined(HODGKINHUXLEY_CPU_GENERATED)
      compute_HodgkinHuxley_cpu(start, end, imp, data);
#   else
#     error "Could not generate method HodgkinHuxleyIonType::compute."
#   endif
      break;
#   ifdef HODGKINHUXLEY_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      compute_HodgkinHuxley_mlir_gpu_cuda(start, end, imp, data);
      break;
#   endif
#   ifdef HODGKINHUXLEY_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      compute_HodgkinHuxley_mlir_gpu_rocm(start, end, imp, data);
      break;
#   endif
#   ifdef HODGKINHUXLEY_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      compute_HodgkinHuxley_mlir_cpu(start, end, imp, data);
      break;
#   endif
#   ifdef HODGKINHUXLEY_CPU_GENERATED
    case Target::CPU:
      compute_HodgkinHuxley_cpu(start, end, imp, data);
      break;
#   endif
    default:
      throw std::runtime_error(std::string("Could not compute with the given target ") + get_string_from_target(target) + ".");
      break;
  }
}

// Define all constants
#define Cm (GlobalData_t)(1.0)
#define E_K (GlobalData_t)(-72.)
#define E_L (GlobalData_t)(-58.)
#define E_Na (GlobalData_t)(52.)
#define V_init (GlobalData_t)(-65.0)
#define h_init (GlobalData_t)(0.5645)
#define m_init (GlobalData_t)(0.0588)
#define n_init (GlobalData_t)(0.3315)



void HodgkinHuxleyIonType::initialize_params(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  cell_geom* region = &imp.cgeom();
  HodgkinHuxley_Params *p = imp.params();

  // Compute the regional constants
  {
    p->GK = 36.0;
    p->GL = 0.3;
    p->GNa = 120.0;
  }
  // Compute the regional initialization
  {
  }

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.

    enum Rosenbrock {
    

      N_ROSEN
    };


void HodgkinHuxleyIonType::construct_tables(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom* region = &imp.cgeom();
  HodgkinHuxley_Params *p = imp.params();

  imp.tables().resize(N_TABS);

  // Define the constants that depend on the parameters.

}



void HodgkinHuxleyIonType::initialize_sv(IonIfBase& imp_base, GlobalData_t **impdata ) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom *region = &imp.cgeom();
  HodgkinHuxley_Params *p = imp.params();

  HodgkinHuxley_state *sv_base = (HodgkinHuxley_state *)imp.sv_tab().data();
  GlobalData_t t = 0;

  IonIfDerived* IF = &imp;
  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i < imp.get_num_node(); __i+=1 ){
    HodgkinHuxley_state *sv = sv_base+__i / 1;
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    V = V_init;
    sv->h = h_init;
    sv->m = m_init;
    sv->n = n_init;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;
  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
#ifdef HODGKINHUXLEY_CPU_GENERATED
extern "C" {
void compute_HodgkinHuxley_cpu(int start, int end, IonIfBase& imp_base, GlobalData_t **impdata )
{
  HodgkinHuxleyIonType::IonIfDerived& imp = static_cast<HodgkinHuxleyIonType::IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt()*1e0;
  cell_geom *region = &imp.cgeom();
  HodgkinHuxley_Params *p  = imp.params();
  HodgkinHuxley_state *sv_base = (HodgkinHuxley_state *)imp.sv_tab().data();

  GlobalData_t t = imp.get_tstp().cnt*dt;

  HodgkinHuxleyIonType::IonIfDerived* IF = &imp;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=(start / 1) * 1; __i<end; __i+=1) {
    HodgkinHuxley_state *sv = sv_base+__i / 1;
                    
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    GlobalData_t IK = (((((p->GK*sv->n)*sv->n)*sv->n)*sv->n)*(V-(E_K)));
    GlobalData_t IL = (p->GL*(V-(E_L)));
    GlobalData_t INa = (((((p->GNa*sv->m)*sv->m)*sv->m)*sv->h)*(V-(E_Na)));
    Iion = ((INa+IK)+IL);
    
    
    //Complete Forward Euler Update
    
    
    //Complete Rush Larsen Update
    GlobalData_t alpha_h = (0.07*(exp((-0.05*(V+65.)))));
    GlobalData_t alpha_m = ((0.1*(V+40.))/(1.-((exp((-0.1*(V+40.)))))));
    GlobalData_t alpha_n = ((0.01*(V+55.))/(1.-((exp((-0.1*(V+55.)))))));
    GlobalData_t beta_h = (1./(1.+(exp((-0.1*(V+35.))))));
    GlobalData_t beta_m = (4.*(exp((-0.0556*(V+65.)))));
    GlobalData_t beta_n = (0.125*(exp((-0.0125*(V+65.)))));
    GlobalData_t h_rush_larsen_A = (((-alpha_h)/(alpha_h+beta_h))*(expm1(((-dt)*(alpha_h+beta_h)))));
    GlobalData_t h_rush_larsen_B = (exp(((-dt)*(alpha_h+beta_h))));
    GlobalData_t m_rush_larsen_A = (((-alpha_m)/(alpha_m+beta_m))*(expm1(((-dt)*(alpha_m+beta_m)))));
    GlobalData_t m_rush_larsen_B = (exp(((-dt)*(alpha_m+beta_m))));
    GlobalData_t n_rush_larsen_A = (((-alpha_n)/(alpha_n+beta_n))*(expm1(((-dt)*(alpha_n+beta_n)))));
    GlobalData_t n_rush_larsen_B = (exp(((-dt)*(alpha_n+beta_n))));
    GlobalData_t h_new = h_rush_larsen_A+h_rush_larsen_B*sv->h;
    GlobalData_t m_new = m_rush_larsen_A+m_rush_larsen_B*sv->m;
    GlobalData_t n_new = n_rush_larsen_A+n_rush_larsen_B*sv->n;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    Iion = Iion;
    sv->h = h_new;
    sv->m = m_new;
    sv->n = n_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

            }
}
#endif // HODGKINHUXLEY_CPU_GENERATED

bool HodgkinHuxleyIonType::has_trace() const {
    return true;
}

void HodgkinHuxleyIonType::trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** impdata) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("HodgkinHuxley_trace_header.txt","wt");
    fprintf(theader->fd,
        "V\n"
        "sv->h\n"
        "sv->m\n"
        "sv->n\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom *region = &imp.cgeom();
  HodgkinHuxley_Params *p  = imp.params();

  HodgkinHuxley_state *sv_base = (HodgkinHuxley_state *)imp.sv_tab().data();

  HodgkinHuxley_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = imp.get_tstp().cnt * dt;
  IonIfBase* IF = &imp;
  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t V = V_ext[__i];
  //Change the units of external variables as appropriate.
  
  
  //Output the desired variables
  fprintf(file, "%4.12f\t", V);
  fprintf(file, "%4.12f\t", sv->h);
  fprintf(file, "%4.12f\t", sv->m);
  fprintf(file, "%4.12f\t", sv->n);
  //Change the units of external variables as appropriate.
  
  

}
IonIfBase* HodgkinHuxleyIonType::make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const {
        // Place the allocated IonIf in managed memory if a GPU target exists for this model
        // otherwise, place it in main RAM
    IonIfDerived* ptr;
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_ROCM, 1, true);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CUDA, 1, true);
    }
    else {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CPU, 1, true);
    }
    // Using placement new to place the object in the correct memory
    return new(ptr) IonIfDerived(*this, this->select_target(target),
    num_node, plugins);
}

void HodgkinHuxleyIonType::destroy_ion_if(IonIfBase *imp) const {
    // Call destructor and deallocate manually because the object might
    // be located on GPU (delete won't work in this case)
    imp->~IonIfBase();
    IonIfDerived* ptr = static_cast<IonIfDerived *>(imp);
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_ROCM, ptr);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CUDA, ptr);
    }
    else {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CPU, ptr);
    }
}

}  // namespace limpet
        