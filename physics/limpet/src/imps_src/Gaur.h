// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Namit Gaur, Xiao-Yan Qi, David Benoist, Olivier Bernus, Ruben Coronel, Stanley Nattel, Edward J. Vigmond
*  Year: 2021
*  Title: A computational model of pig ventricular cardiomyocyte electrophysiology and calcium handling: Translation from pig to human electrophysiology
*  Journal: PLoS Computational Biology, 17(6):e1009137
*  DOI: 10.1371/journal.pcbi.1009137
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __GAUR_H__
#define __GAUR_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"

#if !(defined(GAUR_CPU_GENERATED)    || defined(GAUR_MLIR_CPU_GENERATED)    || defined(GAUR_MLIR_ROCM_GENERATED)    || defined(GAUR_MLIR_CUDA_GENERATED))
#ifdef MLIR_CPU_GENERATED
#define GAUR_MLIR_CPU_GENERATED
#endif

#ifdef MLIR_ROCM_GENERATED
#define GAUR_MLIR_ROCM_GENERATED
#endif

#ifdef MLIR_CUDA_GENERATED
#define GAUR_MLIR_CUDA_GENERATED
#endif
#endif

#ifdef CPU_GENERATED
#define GAUR_CPU_GENERATED
#endif

namespace limpet {

#define Gaur_REQDAT Vm_DATA_FLAG
#define Gaur_MODDAT Iion_DATA_FLAG

struct Gaur_Params {
    GlobalData_t GK1;
    GlobalData_t GKb;
    GlobalData_t GKr;
    GlobalData_t GKs;
    GlobalData_t GNa;
    GlobalData_t GNaL;
    GlobalData_t Gncx;
    GlobalData_t Gto;
    GlobalData_t PCa;
    GlobalData_t PNab;
    GlobalData_t SOICR;
    GlobalData_t tau_gap;
    GlobalData_t vhalf_d;
    GlobalData_t vmyo1frac;

};

struct Gaur_state {
    GlobalData_t CaMKt;
    GlobalData_t GKr;
    GlobalData_t GKs;
    Gatetype Jrel1;
    Gatetype Jrel2;
    GlobalData_t Na_i;
    GlobalData_t PCa;
    Gatetype aa;
    GlobalData_t cacsr;
    GlobalData_t cai;
    GlobalData_t cai2;
    GlobalData_t cajsr;
    GlobalData_t cansr;
    GlobalData_t cass;
    Gatetype d;
    Gatetype fca;
    Gatetype ff;
    Gatetype fs;
    Gatetype h;
    Gatetype hl;
    Gatetype j;
    GlobalData_t ki;
    GlobalData_t kss;
    Gatetype m;
    Gatetype ml;
    GlobalData_t nass;
    GlobalData_t tjsrol;
    Gatetype xr;
    Gatetype xs1;
    Gatetype xs2;

};

class GaurIonType : public IonType {
public:
    using IonIfDerived = IonIf<GaurIonType>;
    using params_type = Gaur_Params;
    using state_type = Gaur_state;

    GaurIonType(bool plugin);

    size_t params_size() const override;

    size_t dlo_vector_size() const override;

    uint32_t reqdat() const override;

    uint32_t moddat() const override;

    void initialize_params(IonIfBase& imp_base) const override;

    void construct_tables(IonIfBase& imp_base) const override;

    void destroy(IonIfBase& imp_base) const override;

    void initialize_sv(IonIfBase& imp_base, GlobalData_t** data) const override;

    Target select_target(Target target) const override;

    void compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const override;

    bool has_trace() const override;

    void trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const override;

    void tune(IonIfBase& imp_base, const char* im_par) const override;

    int read_svs(IonIfBase& imp_base, FILE* file) const override;

    int write_svs(IonIfBase& imp_base, FILE* file, int node) const override;

    SVgetfcn get_sv_offset(const char* svname, int* off, int* sz) const override;

    int get_sv_list(char*** list) const override;

    int get_sv_type(const char* svname, int* type, char** type_name) const override;

    void print_params() const override;

    void print_metadata() const override;

    IonIfBase* make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const override;

    void destroy_ion_if(IonIfBase *imp) const override;
};

// This needs to be extern C in order to be linked correctly with the MLIR code
extern "C" {

//void compute_Gaur(int, int, IonIfBase&, GlobalData_t**);
#ifdef GAUR_CPU_GENERATED
void compute_Gaur_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef GAUR_MLIR_CPU_GENERATED
void compute_Gaur_mlir_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef GAUR_MLIR_ROCM_GENERATED
void compute_Gaur_mlir_gpu_rocm(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef GAUR_MLIR_CUDA_GENERATED
void compute_Gaur_mlir_gpu_cuda(int, int, IonIfBase&, GlobalData_t**);
#endif

}
}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
