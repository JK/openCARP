// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Hu Y, Gurev V, Constantino J, Bayer JD, Trayanova NA
*  Year: 2013
*  Title: Effects of Mechano-Electric Feedback on Scroll Wave Stability in Human Ventricular Fibrillation
*  Journal: PLOS ONE, 8(4), e60287
*  DOI: 10.1371/journal.pone.0060287
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __ISAC_HU_H__
#define __ISAC_HU_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"

#if !(defined(ISAC_HU_CPU_GENERATED)    || defined(ISAC_HU_MLIR_CPU_GENERATED)    || defined(ISAC_HU_MLIR_ROCM_GENERATED)    || defined(ISAC_HU_MLIR_CUDA_GENERATED))
#ifdef MLIR_CPU_GENERATED
#define ISAC_HU_MLIR_CPU_GENERATED
#endif

#ifdef MLIR_ROCM_GENERATED
#define ISAC_HU_MLIR_ROCM_GENERATED
#endif

#ifdef MLIR_CUDA_GENERATED
#define ISAC_HU_MLIR_CUDA_GENERATED
#endif
#endif

#ifdef CPU_GENERATED
#define ISAC_HU_CPU_GENERATED
#endif

namespace limpet {

#define ISAC_Hu_REQDAT Iion_DATA_FLAG|Vm_DATA_FLAG|Lambda_DATA_FLAG
#define ISAC_Hu_MODDAT Iion_DATA_FLAG

struct ISAC_Hu_Params {
    GlobalData_t Gst;
    GlobalData_t RP;

};

struct ISAC_Hu_state {
    char dummy; //PGI doesn't allow a structure of 0 bytes.

};

class ISAC_HuIonType : public IonType {
public:
    using IonIfDerived = IonIf<ISAC_HuIonType>;
    using params_type = ISAC_Hu_Params;
    using state_type = ISAC_Hu_state;

    ISAC_HuIonType(bool plugin);

    size_t params_size() const override;

    size_t dlo_vector_size() const override;

    uint32_t reqdat() const override;

    uint32_t moddat() const override;

    void initialize_params(IonIfBase& imp_base) const override;

    void construct_tables(IonIfBase& imp_base) const override;

    void destroy(IonIfBase& imp_base) const override;

    void initialize_sv(IonIfBase& imp_base, GlobalData_t** data) const override;

    Target select_target(Target target) const override;

    void compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const override;

    bool has_trace() const override;

    void trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const override;

    void tune(IonIfBase& imp_base, const char* im_par) const override;

    int read_svs(IonIfBase& imp_base, FILE* file) const override;

    int write_svs(IonIfBase& imp_base, FILE* file, int node) const override;

    SVgetfcn get_sv_offset(const char* svname, int* off, int* sz) const override;

    int get_sv_list(char*** list) const override;

    int get_sv_type(const char* svname, int* type, char** type_name) const override;

    void print_params() const override;

    void print_metadata() const override;

    IonIfBase* make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const override;

    void destroy_ion_if(IonIfBase *imp) const override;
};

// This needs to be extern C in order to be linked correctly with the MLIR code
extern "C" {

//void compute_ISAC_Hu(int, int, IonIfBase&, GlobalData_t**);
#ifdef ISAC_HU_CPU_GENERATED
void compute_ISAC_Hu_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef ISAC_HU_MLIR_CPU_GENERATED
void compute_ISAC_Hu_mlir_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef ISAC_HU_MLIR_ROCM_GENERATED
void compute_ISAC_Hu_mlir_gpu_rocm(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef ISAC_HU_MLIR_CUDA_GENERATED
void compute_ISAC_Hu_mlir_gpu_cuda(int, int, IonIfBase&, GlobalData_t**);
#endif

}
}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
