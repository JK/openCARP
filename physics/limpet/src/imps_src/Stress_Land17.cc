// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Title: A model of cardiac contraction based on novel measurements of tension development in human cardiomyocytes
*  Authors: Sander Land, So-Jin Park-Holohan, Nicolas P. Smith, Cristobal G. dos Remedios, Jonathan C. Kentish, Steven A. Niederer
*  Year: 2017
*  Journal: Journal of Molecular and Cellular Cardiology 2017;106:68-83
*  DOI: 10.1016/j.yjmcc.2017.03.008
*  Comment: The model implements a human contraction model (plugin) at 37 degrees
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "Stress_Land17.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

Stress_Land17IonType::Stress_Land17IonType(bool plugin) : IonType(std::move(std::string("Stress_Land17")), plugin) {}

size_t Stress_Land17IonType::params_size() const {
  return sizeof(struct Stress_Land17_Params);
}

size_t Stress_Land17IonType::dlo_vector_size() const {

  return 1;
}

uint32_t Stress_Land17IonType::reqdat() const {
  return Stress_Land17_REQDAT;
}

uint32_t Stress_Land17IonType::moddat() const {
  return Stress_Land17_MODDAT;
}

void Stress_Land17IonType::destroy(IonIfBase& imp_base) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  imp.destroy_luts();
  // rarely need to do anything else
}

Target Stress_Land17IonType::select_target(Target target) const {
  switch (target) {
    case Target::AUTO:
#   ifdef STRESS_LAND17_MLIR_CUDA_GENERATED
      return Target::MLIR_CUDA;
#   elif defined(STRESS_LAND17_MLIR_ROCM_GENERATED)
      return Target::MLIR_ROCM;
#   elif defined(STRESS_LAND17_MLIR_CPU_GENERATED)
      return Target::MLIR_CPU;
#   elif defined(STRESS_LAND17_CPU_GENERATED)
      return Target::CPU;
#   else
      return Target::UNKNOWN;
#   endif
#   ifdef STRESS_LAND17_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      return Target::MLIR_CUDA;
#   endif
#   ifdef STRESS_LAND17_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      return Target::MLIR_ROCM;
#   endif
#   ifdef STRESS_LAND17_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      return Target::MLIR_CPU;
#   endif
#   ifdef STRESS_LAND17_CPU_GENERATED
    case Target::CPU:
      return Target::CPU;
#   endif
    default:
      return Target::UNKNOWN;
  }
}

void Stress_Land17IonType::compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  switch(target) {
    case Target::AUTO:
#   ifdef STRESS_LAND17_MLIR_CUDA_GENERATED
      compute_Stress_Land17_mlir_gpu_cuda(start, end, imp, data);
#   elif defined(STRESS_LAND17_MLIR_ROCM_GENERATED)
      compute_Stress_Land17_mlir_gpu_rocm(start, end, imp, data);
#   elif defined(STRESS_LAND17_MLIR_CPU_GENERATED)
      compute_Stress_Land17_mlir_cpu(start, end, imp, data);
#   elif defined(STRESS_LAND17_CPU_GENERATED)
      compute_Stress_Land17_cpu(start, end, imp, data);
#   else
#     error "Could not generate method Stress_Land17IonType::compute."
#   endif
      break;
#   ifdef STRESS_LAND17_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      compute_Stress_Land17_mlir_gpu_cuda(start, end, imp, data);
      break;
#   endif
#   ifdef STRESS_LAND17_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      compute_Stress_Land17_mlir_gpu_rocm(start, end, imp, data);
      break;
#   endif
#   ifdef STRESS_LAND17_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      compute_Stress_Land17_mlir_cpu(start, end, imp, data);
      break;
#   endif
#   ifdef STRESS_LAND17_CPU_GENERATED
    case Target::CPU:
      compute_Stress_Land17_cpu(start, end, imp, data);
      break;
#   endif
    default:
      throw std::runtime_error(std::string("Could not compute with the given target ") + get_string_from_target(target) + ".");
      break;
  }
}

// Define all constants
#define TRPN_init (GlobalData_t)(0.)
#define TmBlocked_init (GlobalData_t)(0.)
#define XS_init (GlobalData_t)(0.)
#define XW_init (GlobalData_t)(0.)
#define ZETAS_init (GlobalData_t)(0.)
#define ZETAW_init (GlobalData_t)(0.)
#define delta_sl_init (GlobalData_t)(0.)
#define length_init (GlobalData_t)(1.)
#define partial_A_del_ZETAS (GlobalData_t)(0.)
#define partial_A_del_ZETAW (GlobalData_t)(0.)
#define partial_TOT_A_del_ZETAS (GlobalData_t)(0.)
#define partial_TOT_A_del_ZETAW (GlobalData_t)(0.)
#define partial_TRPN_n_del_TRPN (GlobalData_t)(0.)
#define partial_XSSS_del_TmBlocked (GlobalData_t)(0.)
#define partial_XU_del_TmBlocked (GlobalData_t)(-1.)
#define partial_XU_del_XW (GlobalData_t)(-1.)
#define partial_XWSS_del_TmBlocked (GlobalData_t)(0.)
#define partial_beta_1_del_TRPN (GlobalData_t)(0.)
#define partial_ca50__del_TRPN (GlobalData_t)(0.)
#define partial_ca50_del_TRPN (GlobalData_t)(0.)
#define partial_cds_del_ZETAS (GlobalData_t)(0.)
#define partial_cdw_del_ZETAW (GlobalData_t)(0.)
#define partial_dlambdadt_del_ZETAS (GlobalData_t)(0.)
#define partial_dlambdadt_del_ZETAW (GlobalData_t)(0.)
#define partial_dr_del_TmBlocked (GlobalData_t)(0.)
#define partial_dr_del_XS (GlobalData_t)(0.)
#define partial_dr_del_ZETAS (GlobalData_t)(0.)
#define partial_dr_del_ZETAW (GlobalData_t)(0.)
#define partial_gamma_del_XS (GlobalData_t)(0.)
#define partial_gamma_rate_del_XS (GlobalData_t)(0.)
#define partial_gamma_rate_w_del_XW (GlobalData_t)(0.)
#define partial_gamma_wu_del_XW (GlobalData_t)(0.)
#define partial_gr_w__del_XW (GlobalData_t)(0.)
#define partial_k_su_del_XS (GlobalData_t)(0.)
#define partial_k_uw_del_XW (GlobalData_t)(0.)
#define partial_k_uw_del_ZETAW (GlobalData_t)(0.)
#define partial_k_ws_del_XS (GlobalData_t)(0.)
#define partial_k_ws_del_XW (GlobalData_t)(0.)
#define partial_k_ws_del_ZETAS (GlobalData_t)(0.)
#define partial_k_wu_del_XW (GlobalData_t)(0.)
#define partial_koff_del_TRPN (GlobalData_t)(0.)
#define partial_ktm_block_del_TmBlocked (GlobalData_t)(0.)
#define partial_ktm_unblock_del_TmBlocked (GlobalData_t)(0.)
#define partial_lambda_del_TRPN (GlobalData_t)(0.)
#define partial_lambda_m_del_TRPN (GlobalData_t)(0.)
#define partial_mu_del_XS (GlobalData_t)(0.)
#define partial_mu_del_XW (GlobalData_t)(0.)
#define partial_mu_del_ZETAS (GlobalData_t)(0.)
#define partial_nperm_del_TmBlocked (GlobalData_t)(0.)
#define partial_nu_del_XW (GlobalData_t)(0.)
#define partial_nu_del_ZETAW (GlobalData_t)(0.)
#define partial_perm50_del_TmBlocked (GlobalData_t)(0.)
#define partial_phi_del_ZETAS (GlobalData_t)(0.)
#define partial_phi_del_ZETAW (GlobalData_t)(0.)
#define partial_trpn_np__del_TmBlocked (GlobalData_t)(0.)
#define partial_trpn_np_del_TmBlocked (GlobalData_t)(0.)
#define partial_wfrac_del_TmBlocked (GlobalData_t)(0.)
#define partial_wfrac_del_XS (GlobalData_t)(0.)
#define partial_wfrac_del_XW (GlobalData_t)(0.)
#define partial_wfrac_del_ZETAS (GlobalData_t)(0.)
#define partial_wfrac_del_ZETAW (GlobalData_t)(0.)
#define partial_xb_ws_del_XS (GlobalData_t)(0.)
#define partial_zs__del_XS (GlobalData_t)(0.)
#define partial_zs_neg_del_XS (GlobalData_t)(0.)
#define partial_zs_pos_del_XS (GlobalData_t)(0.)
#define set_TRPN_tozero_in_TRPN_n (GlobalData_t)(2.)
#define set_TRPN_tozero_in_beta_1 (GlobalData_t)(-2.4)
#define set_TRPN_tozero_in_ca50 (GlobalData_t)(0.805)
#define set_TRPN_tozero_in_koff (GlobalData_t)(0.1)
#define set_TmBlocked_tozero_in_XSSS (GlobalData_t)((0.25*0.5))
#define set_TmBlocked_tozero_in_XWSS (GlobalData_t)((((1.-(0.25))*0.5)*0.5))
#define set_TmBlocked_tozero_in_dr (GlobalData_t)(0.25)
#define set_TmBlocked_tozero_in_ktm_block (GlobalData_t)((((pow(0.35,5.))*0.5)/((0.5-((0.25*0.5)))-((((1.-(0.25))*0.5)*0.5)))))
#define set_TmBlocked_tozero_in_ktm_unblock (GlobalData_t)(1.)
#define set_TmBlocked_tozero_in_nperm (GlobalData_t)(5.)
#define set_TmBlocked_tozero_in_perm50 (GlobalData_t)(0.35)
#define set_TmBlocked_tozero_in_wfrac (GlobalData_t)(0.5)
#define set_XS_tozero_in_dr (GlobalData_t)(0.25)
#define set_XS_tozero_in_gamma (GlobalData_t)(0.0085)
#define set_XS_tozero_in_k_su (GlobalData_t)((((0.004*3.)*((1./0.25)-(1.)))*0.5))
#define set_XS_tozero_in_k_ws (GlobalData_t)((0.004*3.))
#define set_XS_tozero_in_mu (GlobalData_t)(3.)
#define set_XS_tozero_in_wfrac (GlobalData_t)(0.5)
#define set_XS_tozero_in_xb_su (GlobalData_t)(0.)
#define set_XS_tozero_in_xb_su_gamma (GlobalData_t)(0.)
#define set_XW_tozero_in_gamma_wu (GlobalData_t)(0.615)
#define set_XW_tozero_in_k_uw (GlobalData_t)((0.026*7.))
#define set_XW_tozero_in_k_ws (GlobalData_t)((0.004*3.))
#define set_XW_tozero_in_k_wu (GlobalData_t)((((0.026*7.)*((1./0.5)-(1.)))-((0.004*3.))))
#define set_XW_tozero_in_mu (GlobalData_t)(3.)
#define set_XW_tozero_in_nu (GlobalData_t)(7.)
#define set_XW_tozero_in_wfrac (GlobalData_t)(0.5)
#define set_XW_tozero_in_xb_ws (GlobalData_t)(0.)
#define set_XW_tozero_in_xb_wu (GlobalData_t)(0.)
#define set_XW_tozero_in_xb_wu_gamma (GlobalData_t)(0.)
#define set_ZETAS_tozero_in_A (GlobalData_t)((((0.25*25.)/(((1.-(0.25))*0.5)+0.25))*(0.25/0.25)))
#define set_ZETAS_tozero_in_TOT_A (GlobalData_t)(25.)
#define set_ZETAS_tozero_in_cds (GlobalData_t)(((((2.23*(0.004*3.))*(1.-(0.25)))*0.5)/0.25))
#define set_ZETAS_tozero_in_dr (GlobalData_t)(0.25)
#define set_ZETAS_tozero_in_k_ws (GlobalData_t)((0.004*3.))
#define set_ZETAS_tozero_in_mu (GlobalData_t)(3.)
#define set_ZETAS_tozero_in_phi (GlobalData_t)(2.23)
#define set_ZETAS_tozero_in_wfrac (GlobalData_t)(0.5)
#define set_ZETAW_tozero_in_A (GlobalData_t)((((0.25*25.)/(((1.-(0.25))*0.5)+0.25))*(0.25/0.25)))
#define set_ZETAW_tozero_in_TOT_A (GlobalData_t)(25.)
#define set_ZETAW_tozero_in_cdw (GlobalData_t)(((((2.23*(0.026*7.))*(1.-(0.25)))*(1.-(0.5)))/((1.-(0.25))*0.5)))
#define set_ZETAW_tozero_in_dr (GlobalData_t)(0.25)
#define set_ZETAW_tozero_in_k_uw (GlobalData_t)((0.026*7.))
#define set_ZETAW_tozero_in_nu (GlobalData_t)(7.)
#define set_ZETAW_tozero_in_phi (GlobalData_t)(2.23)
#define set_ZETAW_tozero_in_wfrac (GlobalData_t)(0.5)



void Stress_Land17IonType::initialize_params(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  cell_geom* region = &imp.cgeom();
  Stress_Land17_Params *p = imp.params();

  // Compute the regional constants
  {
    p->TOT_A = 25.;
    p->TRPN_n = 2.;
    p->Tref = 120.;
    p->beta_0 = 2.3;
    p->beta_1 = -2.4;
    p->ca50 = 0.805;
    p->dr = 0.25;
    p->gamma = 0.0085;
    p->gamma_wu = 0.615;
    p->koff = 0.1;
    p->ktm_unblock = 1.;
    p->mu = 3.;
    p->nperm = 5.;
    p->nu = 7.;
    p->perm50 = 0.35;
    p->phi = 2.23;
    p->wfrac = 0.5;
  }
  // Compute the regional initialization
  {
  }

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.

    enum Rosenbrock {
    

      N_ROSEN
    };


void Stress_Land17IonType::construct_tables(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom* region = &imp.cgeom();
  Stress_Land17_Params *p = imp.params();

  imp.tables().resize(N_TABS);

  // Define the constants that depend on the parameters.
  double A = (((0.25*p->TOT_A)/(((1.-(p->dr))*p->wfrac)+p->dr))*(p->dr/0.25));
  double XSSS = (p->dr*0.5);
  double XWSS = (((1.-(p->dr))*p->wfrac)*0.5);
  double k_uw = (0.026*p->nu);
  double k_ws = (0.004*p->mu);
  double cds = ((((p->phi*k_ws)*(1.-(p->dr)))*p->wfrac)/p->dr);
  double cdw = ((((p->phi*k_uw)*(1.-(p->dr)))*(1.-(p->wfrac)))/((1.-(p->dr))*p->wfrac));
  double k_su = ((k_ws*((1./p->dr)-(1.)))*p->wfrac);
  double k_wu = ((k_uw*((1./p->wfrac)-(1.)))-(k_ws));
  double ktm_block = (((p->ktm_unblock*(pow(p->perm50,p->nperm)))*0.5)/((0.5-(XSSS))-(XWSS)));
  double partial_xb_uw_del_XW = (k_uw*-1.);
  double partial_xb_ws_del_XW = k_ws;
  double partial_diff_ZETAS_del_ZETAS = (-cds);
  double partial_diff_ZETAW_del_ZETAW = (-cdw);
  double partial_xb_su_del_XS = k_su;
  double partial_xb_wu_del_XW = k_wu;
  double ZETAS_rush_larsen_B = (exp((dt*partial_diff_ZETAS_del_ZETAS)));
  double ZETAW_rush_larsen_B = (exp((dt*partial_diff_ZETAW_del_ZETAW)));

}



void Stress_Land17IonType::initialize_sv(IonIfBase& imp_base, GlobalData_t **impdata ) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom *region = &imp.cgeom();
  Stress_Land17_Params *p = imp.params();

  Stress_Land17_state *sv_base = (Stress_Land17_state *)imp.sv_tab().data();
  GlobalData_t t = 0;

  IonIfDerived* IF = &imp;
  // Define the constants that depend on the parameters.
  double A = (((0.25*p->TOT_A)/(((1.-(p->dr))*p->wfrac)+p->dr))*(p->dr/0.25));
  double XSSS = (p->dr*0.5);
  double XWSS = (((1.-(p->dr))*p->wfrac)*0.5);
  double k_uw = (0.026*p->nu);
  double k_ws = (0.004*p->mu);
  double cds = ((((p->phi*k_ws)*(1.-(p->dr)))*p->wfrac)/p->dr);
  double cdw = ((((p->phi*k_uw)*(1.-(p->dr)))*(1.-(p->wfrac)))/((1.-(p->dr))*p->wfrac));
  double k_su = ((k_ws*((1./p->dr)-(1.)))*p->wfrac);
  double k_wu = ((k_uw*((1./p->wfrac)-(1.)))-(k_ws));
  double ktm_block = (((p->ktm_unblock*(pow(p->perm50,p->nperm)))*0.5)/((0.5-(XSSS))-(XWSS)));
  double partial_xb_uw_del_XW = (k_uw*-1.);
  double partial_xb_ws_del_XW = k_ws;
  double partial_diff_ZETAS_del_ZETAS = (-cds);
  double partial_diff_ZETAW_del_ZETAW = (-cdw);
  double partial_xb_su_del_XS = k_su;
  double partial_xb_wu_del_XW = k_wu;
  double ZETAS_rush_larsen_B = (exp((dt*partial_diff_ZETAS_del_ZETAS)));
  double ZETAW_rush_larsen_B = (exp((dt*partial_diff_ZETAW_del_ZETAW)));
  //Prepare all the public arrays.
  GlobalData_t *Tension_ext = impdata[Tension];
  GlobalData_t *delta_sl_ext = impdata[delLambda];
  GlobalData_t *length_ext = impdata[Lambda];
  //Prepare all the private functions.
  
  int __Cai_sizeof;
  int __Cai_offset;
  SVgetfcn __Cai_SVgetfcn = imp.parent()->get_type().get_sv_offset( "Cai", &__Cai_offset, &__Cai_sizeof );
  SVputfcn __Cai_SVputfcn = __Cai_SVgetfcn ? getPutSV(__Cai_SVgetfcn) : NULL;

  //set the initial values
  for(int __i=0; __i < imp.get_num_node(); __i+=1 ){
    Stress_Land17_state *sv = sv_base+__i / 1;
    //Initialize the external vars to their current values
    GlobalData_t Tension = Tension_ext[__i];
    GlobalData_t delta_sl = delta_sl_ext[__i];
    GlobalData_t length = length_ext[__i];
    GlobalData_t Cai = __Cai_SVgetfcn ? __Cai_SVgetfcn(*imp.parent(), __i, __Cai_offset) :sv->__Cai_local;
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    sv->TRPN = TRPN_init;
    sv->TmBlocked = TmBlocked_init;
    sv->XS = XS_init;
    sv->XW = XW_init;
    sv->ZETAS = ZETAS_init;
    sv->ZETAW = ZETAW_init;
    delta_sl = delta_sl_init;
    length = length_init;
    double lambda = length;
    double lambda_m = ((lambda>1.2) ? 1.2 : lambda);
    double lambda_s = ((lambda_m<0.87) ? 0.87 : lambda_m);
    double Ta = ((lambda_s*(p->Tref/p->dr))*(((sv->ZETAS+1.)*sv->XS)+(sv->ZETAW*sv->XW)));
    Tension = Ta;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Tension_ext[__i] = Tension;
    delta_sl_ext[__i] = delta_sl;
    length_ext[__i] = length;
    if( __Cai_SVputfcn ) 
    	__Cai_SVputfcn(*imp.parent(), __i, __Cai_offset, Cai);
    else
    	sv->__Cai_local=Cai;
  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
#ifdef STRESS_LAND17_CPU_GENERATED
extern "C" {
void compute_Stress_Land17_cpu(int start, int end, IonIfBase& imp_base, GlobalData_t **impdata )
{
  Stress_Land17IonType::IonIfDerived& imp = static_cast<Stress_Land17IonType::IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt()*1e0;
  cell_geom *region = &imp.cgeom();
  Stress_Land17_Params *p  = imp.params();
  Stress_Land17_state *sv_base = (Stress_Land17_state *)imp.sv_tab().data();

  GlobalData_t t = imp.get_tstp().cnt*dt;

  Stress_Land17IonType::IonIfDerived* IF = &imp;

  // Define the constants that depend on the parameters.
  GlobalData_t A = (((0.25*p->TOT_A)/(((1.-(p->dr))*p->wfrac)+p->dr))*(p->dr/0.25));
  GlobalData_t XSSS = (p->dr*0.5);
  GlobalData_t XWSS = (((1.-(p->dr))*p->wfrac)*0.5);
  GlobalData_t k_uw = (0.026*p->nu);
  GlobalData_t k_ws = (0.004*p->mu);
  GlobalData_t cds = ((((p->phi*k_ws)*(1.-(p->dr)))*p->wfrac)/p->dr);
  GlobalData_t cdw = ((((p->phi*k_uw)*(1.-(p->dr)))*(1.-(p->wfrac)))/((1.-(p->dr))*p->wfrac));
  GlobalData_t k_su = ((k_ws*((1./p->dr)-(1.)))*p->wfrac);
  GlobalData_t k_wu = ((k_uw*((1./p->wfrac)-(1.)))-(k_ws));
  GlobalData_t ktm_block = (((p->ktm_unblock*(pow(p->perm50,p->nperm)))*0.5)/((0.5-(XSSS))-(XWSS)));
  GlobalData_t partial_xb_uw_del_XW = (k_uw*-1.);
  GlobalData_t partial_xb_ws_del_XW = k_ws;
  GlobalData_t partial_diff_ZETAS_del_ZETAS = (-cds);
  GlobalData_t partial_diff_ZETAW_del_ZETAW = (-cdw);
  GlobalData_t partial_xb_su_del_XS = k_su;
  GlobalData_t partial_xb_wu_del_XW = k_wu;
  GlobalData_t ZETAS_rush_larsen_B = (exp((dt*partial_diff_ZETAS_del_ZETAS)));
  GlobalData_t ZETAW_rush_larsen_B = (exp((dt*partial_diff_ZETAW_del_ZETAW)));
  //Prepare all the public arrays.
  GlobalData_t *Tension_ext = impdata[Tension];
  GlobalData_t *delta_sl_ext = impdata[delLambda];
  GlobalData_t *length_ext = impdata[Lambda];
  //Prepare all the private functions.
  
  int __Cai_sizeof;
  int __Cai_offset;
  SVgetfcn __Cai_SVgetfcn = imp.parent()->get_type().get_sv_offset( "Cai", &__Cai_offset, &__Cai_sizeof );
  SVputfcn __Cai_SVputfcn = __Cai_SVgetfcn ? getPutSV(__Cai_SVgetfcn) : NULL;
  
  // Calculates addresses for the MLIR code generator
  int __Cai_getfcn_exists = __Cai_SVgetfcn ? 1 : 0;
  int __Cai_putfcn_exists = __Cai_SVputfcn ? 1 : 0;
  
          char* __parent_table_address = (char*)imp.parent()->get_sv_address();
          int __parent_table_size = imp.parent()->get_sv_size() / 1;
      

#pragma omp parallel for schedule(static)
  for (int __i=(start / 1) * 1; __i<end; __i+=1) {
    Stress_Land17_state *sv = sv_base+__i / 1;
                    
    //Initialize the external vars to their current values
    GlobalData_t Tension = Tension_ext[__i];
    GlobalData_t delta_sl = delta_sl_ext[__i];
    GlobalData_t length = length_ext[__i];
    GlobalData_t Cai = __Cai_SVgetfcn ? __Cai_SVgetfcn(*imp.parent(), __i, __Cai_offset) :sv->__Cai_local;
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    GlobalData_t lambda = length;
    GlobalData_t lambda_m = ((lambda>1.2) ? 1.2 : lambda);
    GlobalData_t lambda_s = ((lambda_m<0.87) ? 0.87 : lambda_m);
    GlobalData_t Ta = ((lambda_s*(p->Tref/p->dr))*(((sv->ZETAS+1.)*sv->XS)+(sv->ZETAW*sv->XW)));
    Tension = Ta;
    
    
    //Complete Forward Euler Update
    
    
    //Complete Rush Larsen Update
    GlobalData_t ca50_ = (p->ca50+(p->beta_1*(lambda_m-(1.))));
    GlobalData_t gr_w_ = ((sv->ZETAW<0.) ? (-sv->ZETAW) : sv->ZETAW);
    GlobalData_t set_TRPN_tozero_in_diff_TRPN = (0.1*(pow((Cai/(0.805+(-2.4*(((length>1.2) ? 1.2 : length)-(1.))))),2.)));
    GlobalData_t set_TmBlocked_tozero_in_diff_TmBlocked = (((((pow(0.35,5.))*0.5)/((0.5-((0.25*0.5)))-((((1.-(0.25))*0.5)*0.5))))*(((pow(sv->TRPN,(-5./2.)))>100.) ? 100. : (pow(sv->TRPN,(-5./2.)))))*((1.-(sv->XW))-(sv->XS)));
    GlobalData_t set_XS_tozero_in_diff_XS = ((0.004*3.)*sv->XW);
    GlobalData_t set_XW_tozero_in_diff_XW = ((0.026*7.)*((1.-(sv->TmBlocked))-(sv->XS)));
    GlobalData_t set_ZETAS_tozero_in_diff_ZETAS = ((((0.25*25.)/(((1.-(0.25))*0.5)+0.25))*(0.25/0.25))*delta_sl);
    GlobalData_t set_ZETAW_tozero_in_diff_ZETAW = ((((0.25*25.)/(((1.-(0.25))*0.5)+0.25))*(0.25/0.25))*delta_sl);
    GlobalData_t trpn_np_ = (pow(sv->TRPN,((-p->nperm)/2.)));
    GlobalData_t zs_neg = ((sv->ZETAS<-1.) ? ((-sv->ZETAS)-(1.)) : 0.);
    GlobalData_t zs_pos = ((sv->ZETAS>0.) ? sv->ZETAS : 0.);
    GlobalData_t ZETAS_rush_larsen_A = ((set_ZETAS_tozero_in_diff_ZETAS/partial_diff_ZETAS_del_ZETAS)*(expm1((dt*partial_diff_ZETAS_del_ZETAS))));
    GlobalData_t ZETAW_rush_larsen_A = ((set_ZETAW_tozero_in_diff_ZETAW/partial_diff_ZETAW_del_ZETAW)*(expm1((dt*partial_diff_ZETAW_del_ZETAW))));
    GlobalData_t gamma_rate_w = (p->gamma_wu*gr_w_);
    GlobalData_t partial_diff_TRPN_del_TRPN = (p->koff*(((pow((Cai/ca50_),p->TRPN_n))*-1.)-(1.)));
    GlobalData_t trpn_np = ((trpn_np_>100.) ? 100. : trpn_np_);
    GlobalData_t zs_ = ((zs_pos>zs_neg) ? zs_pos : zs_neg);
    GlobalData_t TRPN_rush_larsen_A = ((set_TRPN_tozero_in_diff_TRPN/partial_diff_TRPN_del_TRPN)*(expm1((dt*partial_diff_TRPN_del_TRPN))));
    GlobalData_t TRPN_rush_larsen_B = (exp((dt*partial_diff_TRPN_del_TRPN)));
    GlobalData_t gamma_rate = (p->gamma*zs_);
    GlobalData_t partial_diff_TmBlocked_del_TmBlocked = (((ktm_block*trpn_np)*-1.)-((p->ktm_unblock*(pow(sv->TRPN,(p->nperm/2.))))));
    GlobalData_t partial_diff_XW_del_XW = ((((k_uw*-1.)-(k_wu))-(k_ws))-(gamma_rate_w));
    GlobalData_t TmBlocked_rush_larsen_A = ((set_TmBlocked_tozero_in_diff_TmBlocked/partial_diff_TmBlocked_del_TmBlocked)*(expm1((dt*partial_diff_TmBlocked_del_TmBlocked))));
    GlobalData_t TmBlocked_rush_larsen_B = (exp((dt*partial_diff_TmBlocked_del_TmBlocked)));
    GlobalData_t XW_rush_larsen_A = ((set_XW_tozero_in_diff_XW/partial_diff_XW_del_XW)*(expm1((dt*partial_diff_XW_del_XW))));
    GlobalData_t XW_rush_larsen_B = (exp((dt*partial_diff_XW_del_XW)));
    GlobalData_t partial_diff_XS_del_XS = ((-k_su)-(gamma_rate));
    GlobalData_t XS_rush_larsen_A = ((set_XS_tozero_in_diff_XS/partial_diff_XS_del_XS)*(expm1((dt*partial_diff_XS_del_XS))));
    GlobalData_t XS_rush_larsen_B = (exp((dt*partial_diff_XS_del_XS)));
    GlobalData_t TRPN_new = TRPN_rush_larsen_A+TRPN_rush_larsen_B*sv->TRPN;
    GlobalData_t TmBlocked_new = TmBlocked_rush_larsen_A+TmBlocked_rush_larsen_B*sv->TmBlocked;
    GlobalData_t XS_new = XS_rush_larsen_A+XS_rush_larsen_B*sv->XS;
    GlobalData_t XW_new = XW_rush_larsen_A+XW_rush_larsen_B*sv->XW;
    GlobalData_t ZETAS_new = ZETAS_rush_larsen_A+ZETAS_rush_larsen_B*sv->ZETAS;
    GlobalData_t ZETAW_new = ZETAW_rush_larsen_A+ZETAW_rush_larsen_B*sv->ZETAW;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->TRPN = TRPN_new;
    Tension = Tension;
    sv->TmBlocked = TmBlocked_new;
    sv->XS = XS_new;
    sv->XW = XW_new;
    sv->ZETAS = ZETAS_new;
    sv->ZETAW = ZETAW_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Tension_ext[__i] = Tension;
    delta_sl_ext[__i] = delta_sl;
    length_ext[__i] = length;
    if( __Cai_SVputfcn ) 
    	__Cai_SVputfcn(*imp.parent(), __i, __Cai_offset, Cai);
    else
    	sv->__Cai_local=Cai;

  }

            }
}
#endif // STRESS_LAND17_CPU_GENERATED

bool Stress_Land17IonType::has_trace() const {
    return true;
}

void Stress_Land17IonType::trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** impdata) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("Stress_Land17_trace_header.txt","wt");
    fprintf(theader->fd,
        "sv->TRPN\n"
        "Tension\n"
        "sv->TmBlocked\n"
        "sv->XS\n"
        "sv->XW\n"
        "sv->ZETAS\n"
        "sv->ZETAW\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom *region = &imp.cgeom();
  Stress_Land17_Params *p  = imp.params();

  Stress_Land17_state *sv_base = (Stress_Land17_state *)imp.sv_tab().data();

  Stress_Land17_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = imp.get_tstp().cnt * dt;
  IonIfBase* IF = &imp;
  // Define the constants that depend on the parameters.
  GlobalData_t A = (((0.25*p->TOT_A)/(((1.-(p->dr))*p->wfrac)+p->dr))*(p->dr/0.25));
  GlobalData_t XSSS = (p->dr*0.5);
  GlobalData_t XWSS = (((1.-(p->dr))*p->wfrac)*0.5);
  GlobalData_t k_uw = (0.026*p->nu);
  GlobalData_t k_ws = (0.004*p->mu);
  GlobalData_t cds = ((((p->phi*k_ws)*(1.-(p->dr)))*p->wfrac)/p->dr);
  GlobalData_t cdw = ((((p->phi*k_uw)*(1.-(p->dr)))*(1.-(p->wfrac)))/((1.-(p->dr))*p->wfrac));
  GlobalData_t k_su = ((k_ws*((1./p->dr)-(1.)))*p->wfrac);
  GlobalData_t k_wu = ((k_uw*((1./p->wfrac)-(1.)))-(k_ws));
  GlobalData_t ktm_block = (((p->ktm_unblock*(pow(p->perm50,p->nperm)))*0.5)/((0.5-(XSSS))-(XWSS)));
  GlobalData_t partial_xb_uw_del_XW = (k_uw*-1.);
  GlobalData_t partial_xb_ws_del_XW = k_ws;
  GlobalData_t partial_diff_ZETAS_del_ZETAS = (-cds);
  GlobalData_t partial_diff_ZETAW_del_ZETAW = (-cdw);
  GlobalData_t partial_xb_su_del_XS = k_su;
  GlobalData_t partial_xb_wu_del_XW = k_wu;
  GlobalData_t ZETAS_rush_larsen_B = (exp((dt*partial_diff_ZETAS_del_ZETAS)));
  GlobalData_t ZETAW_rush_larsen_B = (exp((dt*partial_diff_ZETAW_del_ZETAW)));
  //Prepare all the public arrays.
  GlobalData_t *Tension_ext = impdata[Tension];
  GlobalData_t *delta_sl_ext = impdata[delLambda];
  GlobalData_t *length_ext = impdata[Lambda];
  //Prepare all the private functions.
  
  int __Cai_sizeof;
  int __Cai_offset;
  SVgetfcn __Cai_SVgetfcn = imp.parent()->get_type().get_sv_offset( "Cai", &__Cai_offset, &__Cai_sizeof );
  SVputfcn __Cai_SVputfcn = __Cai_SVgetfcn ? getPutSV(__Cai_SVgetfcn) : NULL;

  //Initialize the external vars to their current values
  GlobalData_t Tension = Tension_ext[__i];
  GlobalData_t delta_sl = delta_sl_ext[__i];
  GlobalData_t length = length_ext[__i];
  GlobalData_t Cai = __Cai_SVgetfcn ? __Cai_SVgetfcn(*imp.parent(), __i, __Cai_offset) :sv->__Cai_local;
  //Change the units of external variables as appropriate.
  
  
  GlobalData_t lambda = length;
  GlobalData_t lambda_m = ((lambda>1.2) ? 1.2 : lambda);
  GlobalData_t lambda_s = ((lambda_m<0.87) ? 0.87 : lambda_m);
  GlobalData_t Ta = ((lambda_s*(p->Tref/p->dr))*(((sv->ZETAS+1.)*sv->XS)+(sv->ZETAW*sv->XW)));
  Tension = Ta;
  //Output the desired variables
  fprintf(file, "%4.12f\t", sv->TRPN);
  fprintf(file, "%4.12f\t", Tension);
  fprintf(file, "%4.12f\t", sv->TmBlocked);
  fprintf(file, "%4.12f\t", sv->XS);
  fprintf(file, "%4.12f\t", sv->XW);
  fprintf(file, "%4.12f\t", sv->ZETAS);
  fprintf(file, "%4.12f\t", sv->ZETAW);
  //Change the units of external variables as appropriate.
  
  

}
IonIfBase* Stress_Land17IonType::make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const {
        // Place the allocated IonIf in managed memory if a GPU target exists for this model
        // otherwise, place it in main RAM
    IonIfDerived* ptr;
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_ROCM, 1, true);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CUDA, 1, true);
    }
    else {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CPU, 1, true);
    }
    // Using placement new to place the object in the correct memory
    return new(ptr) IonIfDerived(*this, this->select_target(target),
    num_node, plugins);
}

void Stress_Land17IonType::destroy_ion_if(IonIfBase *imp) const {
    // Call destructor and deallocate manually because the object might
    // be located on GPU (delete won't work in this case)
    imp->~IonIfBase();
    IonIfDerived* ptr = static_cast<IonIfDerived *>(imp);
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_ROCM, ptr);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CUDA, ptr);
    }
    else {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CPU, ptr);
    }
}

}  // namespace limpet
        
