// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Ashihara T, Trayanova NA
*  Year: 2004
*  Title: Asymmetry in membrane responses to electric shocks: insights from bidomain simulations
*  Journal: Biophys J., 87(4):2271-82
*  DOI: 10.1529/biophysj.104.043091
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "Defib_AshiharaTrayanova.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

Defib_AshiharaTrayanovaIonType::Defib_AshiharaTrayanovaIonType(bool plugin) : IonType(std::move(std::string("Defib_AshiharaTrayanova")), plugin) {}

size_t Defib_AshiharaTrayanovaIonType::params_size() const {
  return sizeof(struct Defib_AshiharaTrayanova_Params);
}

size_t Defib_AshiharaTrayanovaIonType::dlo_vector_size() const {

  return 1;
}

uint32_t Defib_AshiharaTrayanovaIonType::reqdat() const {
  return Defib_AshiharaTrayanova_REQDAT;
}

uint32_t Defib_AshiharaTrayanovaIonType::moddat() const {
  return Defib_AshiharaTrayanova_MODDAT;
}

void Defib_AshiharaTrayanovaIonType::destroy(IonIfBase& imp_base) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  imp.destroy_luts();
  // rarely need to do anything else
}

Target Defib_AshiharaTrayanovaIonType::select_target(Target target) const {
  switch (target) {
    case Target::AUTO:
#   ifdef DEFIB_ASHIHARATRAYANOVA_MLIR_CUDA_GENERATED
      return Target::MLIR_CUDA;
#   elif defined(DEFIB_ASHIHARATRAYANOVA_MLIR_ROCM_GENERATED)
      return Target::MLIR_ROCM;
#   elif defined(DEFIB_ASHIHARATRAYANOVA_MLIR_CPU_GENERATED)
      return Target::MLIR_CPU;
#   elif defined(DEFIB_ASHIHARATRAYANOVA_CPU_GENERATED)
      return Target::CPU;
#   else
      return Target::UNKNOWN;
#   endif
#   ifdef DEFIB_ASHIHARATRAYANOVA_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      return Target::MLIR_CUDA;
#   endif
#   ifdef DEFIB_ASHIHARATRAYANOVA_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      return Target::MLIR_ROCM;
#   endif
#   ifdef DEFIB_ASHIHARATRAYANOVA_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      return Target::MLIR_CPU;
#   endif
#   ifdef DEFIB_ASHIHARATRAYANOVA_CPU_GENERATED
    case Target::CPU:
      return Target::CPU;
#   endif
    default:
      return Target::UNKNOWN;
  }
}

void Defib_AshiharaTrayanovaIonType::compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  switch(target) {
    case Target::AUTO:
#   ifdef DEFIB_ASHIHARATRAYANOVA_MLIR_CUDA_GENERATED
      compute_Defib_AshiharaTrayanova_mlir_gpu_cuda(start, end, imp, data);
#   elif defined(DEFIB_ASHIHARATRAYANOVA_MLIR_ROCM_GENERATED)
      compute_Defib_AshiharaTrayanova_mlir_gpu_rocm(start, end, imp, data);
#   elif defined(DEFIB_ASHIHARATRAYANOVA_MLIR_CPU_GENERATED)
      compute_Defib_AshiharaTrayanova_mlir_cpu(start, end, imp, data);
#   elif defined(DEFIB_ASHIHARATRAYANOVA_CPU_GENERATED)
      compute_Defib_AshiharaTrayanova_cpu(start, end, imp, data);
#   else
#     error "Could not generate method Defib_AshiharaTrayanovaIonType::compute."
#   endif
      break;
#   ifdef DEFIB_ASHIHARATRAYANOVA_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      compute_Defib_AshiharaTrayanova_mlir_gpu_cuda(start, end, imp, data);
      break;
#   endif
#   ifdef DEFIB_ASHIHARATRAYANOVA_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      compute_Defib_AshiharaTrayanova_mlir_gpu_rocm(start, end, imp, data);
      break;
#   endif
#   ifdef DEFIB_ASHIHARATRAYANOVA_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      compute_Defib_AshiharaTrayanova_mlir_cpu(start, end, imp, data);
      break;
#   endif
#   ifdef DEFIB_ASHIHARATRAYANOVA_CPU_GENERATED
    case Target::CPU:
      compute_Defib_AshiharaTrayanova_cpu(start, end, imp, data);
      break;
#   endif
    default:
      throw std::runtime_error(std::string("Could not compute with the given target ") + get_string_from_target(target) + ".");
      break;
  }
}

// Define all constants
#define Ki_init (GlobalData_t)(5.4)



void Defib_AshiharaTrayanovaIonType::initialize_params(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  cell_geom* region = &imp.cgeom();
  Defib_AshiharaTrayanova_Params *p = imp.params();

  // Compute the regional constants
  {
    p->VtakeOff = 160.;
    p->slopeFac = 1.;
  }
  // Compute the regional initialization
  {
  }

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.

    enum Rosenbrock {
    

      N_ROSEN
    };


void Defib_AshiharaTrayanovaIonType::construct_tables(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom* region = &imp.cgeom();
  Defib_AshiharaTrayanova_Params *p = imp.params();

  imp.tables().resize(N_TABS);

  // Define the constants that depend on the parameters.

}



void Defib_AshiharaTrayanovaIonType::initialize_sv(IonIfBase& imp_base, GlobalData_t **impdata ) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom *region = &imp.cgeom();
  Defib_AshiharaTrayanova_Params *p = imp.params();

  Defib_AshiharaTrayanova_state *sv_base = (Defib_AshiharaTrayanova_state *)imp.sv_tab().data();
  GlobalData_t t = 0;

  IonIfDerived* IF = &imp;
  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i < imp.get_num_node(); __i+=1 ){
    Defib_AshiharaTrayanova_state *sv = sv_base+__i / 1;
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    sv->Ki = Ki_init;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;
  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
#ifdef DEFIB_ASHIHARATRAYANOVA_CPU_GENERATED
extern "C" {
void compute_Defib_AshiharaTrayanova_cpu(int start, int end, IonIfBase& imp_base, GlobalData_t **impdata )
{
  Defib_AshiharaTrayanovaIonType::IonIfDerived& imp = static_cast<Defib_AshiharaTrayanovaIonType::IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt()*1e0;
  cell_geom *region = &imp.cgeom();
  Defib_AshiharaTrayanova_Params *p  = imp.params();
  Defib_AshiharaTrayanova_state *sv_base = (Defib_AshiharaTrayanova_state *)imp.sv_tab().data();

  GlobalData_t t = imp.get_tstp().cnt*dt;

  Defib_AshiharaTrayanovaIonType::IonIfDerived* IF = &imp;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=(start / 1) * 1; __i<end; __i+=1) {
    Defib_AshiharaTrayanova_state *sv = sv_base+__i / 1;
                    
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    GlobalData_t Ia = ((V>p->VtakeOff) ? ((exp((.09*(p->VtakeOff-(100.)))))*(((0.09*p->slopeFac)*(V-(p->VtakeOff)))+1.)) : (exp((.09*(V-(p->VtakeOff))))));
    Iion = (Iion+Ia);
    
    
    //Complete Forward Euler Update
    GlobalData_t diff_Ki = (region->sl_i2c*Ia);
    GlobalData_t Ki_new = sv->Ki+diff_Ki*dt;
    
    
    //Complete Rush Larsen Update
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    Iion = Iion;
    sv->Ki = Ki_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

            }
}
#endif // DEFIB_ASHIHARATRAYANOVA_CPU_GENERATED

bool Defib_AshiharaTrayanovaIonType::has_trace() const {
    return false;
}

void Defib_AshiharaTrayanovaIonType::trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const {}
IonIfBase* Defib_AshiharaTrayanovaIonType::make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const {
        // Place the allocated IonIf in managed memory if a GPU target exists for this model
        // otherwise, place it in main RAM
    IonIfDerived* ptr;
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_ROCM, 1, true);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CUDA, 1, true);
    }
    else {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CPU, 1, true);
    }
    // Using placement new to place the object in the correct memory
    return new(ptr) IonIfDerived(*this, this->select_target(target),
    num_node, plugins);
}

void Defib_AshiharaTrayanovaIonType::destroy_ion_if(IonIfBase *imp) const {
    // Call destructor and deallocate manually because the object might
    // be located on GPU (delete won't work in this case)
    imp->~IonIfBase();
    IonIfDerived* ptr = static_cast<IonIfDerived *>(imp);
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_ROCM, ptr);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CUDA, ptr);
    }
    else {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CPU, ptr);
    }
}

}  // namespace limpet
        