// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Robert Plonsey
*  Year: 1974
*  Title: The Formulation of Bioelectric Source-field Relationships in Terms of Surface Discontinuities
*  Journal: Journal of the Franklin Institute, 297(5),317-324
*  DOI: 10.1016/0016-0032(74)90036-2.
*  Comment: Passive model
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "Plonsey.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

PlonseyIonType::PlonseyIonType(bool plugin) : IonType(std::move(std::string("Plonsey")), plugin) {}

size_t PlonseyIonType::params_size() const {
  return sizeof(struct Plonsey_Params);
}

size_t PlonseyIonType::dlo_vector_size() const {

  return 1;
}

uint32_t PlonseyIonType::reqdat() const {
  return Plonsey_REQDAT;
}

uint32_t PlonseyIonType::moddat() const {
  return Plonsey_MODDAT;
}

void PlonseyIonType::destroy(IonIfBase& imp_base) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  imp.destroy_luts();
  // rarely need to do anything else
}

Target PlonseyIonType::select_target(Target target) const {
  switch (target) {
    case Target::AUTO:
#   ifdef PLONSEY_MLIR_CUDA_GENERATED
      return Target::MLIR_CUDA;
#   elif defined(PLONSEY_MLIR_ROCM_GENERATED)
      return Target::MLIR_ROCM;
#   elif defined(PLONSEY_MLIR_CPU_GENERATED)
      return Target::MLIR_CPU;
#   elif defined(PLONSEY_CPU_GENERATED)
      return Target::CPU;
#   else
      return Target::UNKNOWN;
#   endif
#   ifdef PLONSEY_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      return Target::MLIR_CUDA;
#   endif
#   ifdef PLONSEY_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      return Target::MLIR_ROCM;
#   endif
#   ifdef PLONSEY_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      return Target::MLIR_CPU;
#   endif
#   ifdef PLONSEY_CPU_GENERATED
    case Target::CPU:
      return Target::CPU;
#   endif
    default:
      return Target::UNKNOWN;
  }
}

void PlonseyIonType::compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  switch(target) {
    case Target::AUTO:
#   ifdef PLONSEY_MLIR_CUDA_GENERATED
      compute_Plonsey_mlir_gpu_cuda(start, end, imp, data);
#   elif defined(PLONSEY_MLIR_ROCM_GENERATED)
      compute_Plonsey_mlir_gpu_rocm(start, end, imp, data);
#   elif defined(PLONSEY_MLIR_CPU_GENERATED)
      compute_Plonsey_mlir_cpu(start, end, imp, data);
#   elif defined(PLONSEY_CPU_GENERATED)
      compute_Plonsey_cpu(start, end, imp, data);
#   else
#     error "Could not generate method PlonseyIonType::compute."
#   endif
      break;
#   ifdef PLONSEY_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      compute_Plonsey_mlir_gpu_cuda(start, end, imp, data);
      break;
#   endif
#   ifdef PLONSEY_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      compute_Plonsey_mlir_gpu_rocm(start, end, imp, data);
      break;
#   endif
#   ifdef PLONSEY_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      compute_Plonsey_mlir_cpu(start, end, imp, data);
      break;
#   endif
#   ifdef PLONSEY_CPU_GENERATED
    case Target::CPU:
      compute_Plonsey_cpu(start, end, imp, data);
      break;
#   endif
    default:
      throw std::runtime_error(std::string("Could not compute with the given target ") + get_string_from_target(target) + ".");
      break;
  }
}

// Define all constants



void PlonseyIonType::initialize_params(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  cell_geom* region = &imp.cgeom();
  Plonsey_Params *p = imp.params();

  // Compute the regional constants
  {
    p->Rm = 10.0;
    p->Vrest = 0.;
  }
  // Compute the regional initialization
  {
  }

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.

    enum Rosenbrock {
    

      N_ROSEN
    };


void PlonseyIonType::construct_tables(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom* region = &imp.cgeom();
  Plonsey_Params *p = imp.params();

  imp.tables().resize(N_TABS);

  // Define the constants that depend on the parameters.
  double Gm = (1./p->Rm);
  double V_init = p->Vrest;

}



void PlonseyIonType::initialize_sv(IonIfBase& imp_base, GlobalData_t **impdata ) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom *region = &imp.cgeom();
  Plonsey_Params *p = imp.params();

  Plonsey_state *sv_base = (Plonsey_state *)imp.sv_tab().data();
  GlobalData_t t = 0;

  IonIfDerived* IF = &imp;
  // Define the constants that depend on the parameters.
  double Gm = (1./p->Rm);
  double V_init = p->Vrest;
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i < imp.get_num_node(); __i+=1 ){
    Plonsey_state *sv = sv_base+__i / 1;
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    V = V_init;
    Iion = (Gm*(V-(p->Vrest)));
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;
  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
#ifdef PLONSEY_CPU_GENERATED
extern "C" {
void compute_Plonsey_cpu(int start, int end, IonIfBase& imp_base, GlobalData_t **impdata )
{
  PlonseyIonType::IonIfDerived& imp = static_cast<PlonseyIonType::IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt()*1e0;
  cell_geom *region = &imp.cgeom();
  Plonsey_Params *p  = imp.params();
  Plonsey_state *sv_base = (Plonsey_state *)imp.sv_tab().data();

  GlobalData_t t = imp.get_tstp().cnt*dt;

  PlonseyIonType::IonIfDerived* IF = &imp;

  // Define the constants that depend on the parameters.
  GlobalData_t Gm = (1./p->Rm);
  GlobalData_t V_init = p->Vrest;
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=(start / 1) * 1; __i<end; __i+=1) {
    Plonsey_state *sv = sv_base+__i / 1;
                    
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    Iion = (Gm*(V-(p->Vrest)));
    
    
    //Complete Forward Euler Update
    
    
    //Complete Rush Larsen Update
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    Iion = Iion;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

            }
}
#endif // PLONSEY_CPU_GENERATED

bool PlonseyIonType::has_trace() const {
    return false;
}

void PlonseyIonType::trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const {}
IonIfBase* PlonseyIonType::make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const {
        // Place the allocated IonIf in managed memory if a GPU target exists for this model
        // otherwise, place it in main RAM
    IonIfDerived* ptr;
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_ROCM, 1, true);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CUDA, 1, true);
    }
    else {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CPU, 1, true);
    }
    // Using placement new to place the object in the correct memory
    return new(ptr) IonIfDerived(*this, this->select_target(target),
    num_node, plugins);
}

void PlonseyIonType::destroy_ion_if(IonIfBase *imp) const {
    // Call destructor and deallocate manually because the object might
    // be located on GPU (delete won't work in this case)
    imp->~IonIfBase();
    IonIfDerived* ptr = static_cast<IonIfDerived *>(imp);
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_ROCM, ptr);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CUDA, ptr);
    }
    else {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CPU, ptr);
    }
}

}  // namespace limpet
        