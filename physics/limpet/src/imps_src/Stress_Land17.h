// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Title: A model of cardiac contraction based on novel measurements of tension development in human cardiomyocytes
*  Authors: Sander Land, So-Jin Park-Holohan, Nicolas P. Smith, Cristobal G. dos Remedios, Jonathan C. Kentish, Steven A. Niederer
*  Year: 2017
*  Journal: Journal of Molecular and Cellular Cardiology 2017;106:68-83
*  DOI: 10.1016/j.yjmcc.2017.03.008
*  Comment: The model implements a human contraction model (plugin) at 37 degrees
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __STRESS_LAND17_H__
#define __STRESS_LAND17_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"

#if !(defined(STRESS_LAND17_CPU_GENERATED)    || defined(STRESS_LAND17_MLIR_CPU_GENERATED)    || defined(STRESS_LAND17_MLIR_ROCM_GENERATED)    || defined(STRESS_LAND17_MLIR_CUDA_GENERATED))
#ifdef MLIR_CPU_GENERATED
#define STRESS_LAND17_MLIR_CPU_GENERATED
#endif

#ifdef MLIR_ROCM_GENERATED
#define STRESS_LAND17_MLIR_ROCM_GENERATED
#endif

#ifdef MLIR_CUDA_GENERATED
#define STRESS_LAND17_MLIR_CUDA_GENERATED
#endif
#endif

#ifdef CPU_GENERATED
#define STRESS_LAND17_CPU_GENERATED
#endif

namespace limpet {

#define Stress_Land17_REQDAT delLambda_DATA_FLAG|Lambda_DATA_FLAG
#define Stress_Land17_MODDAT Tension_DATA_FLAG

struct Stress_Land17_Params {
    GlobalData_t TOT_A;
    GlobalData_t TRPN_n;
    GlobalData_t Tref;
    GlobalData_t beta_0;
    GlobalData_t beta_1;
    GlobalData_t ca50;
    GlobalData_t dr;
    GlobalData_t gamma;
    GlobalData_t gamma_wu;
    GlobalData_t koff;
    GlobalData_t ktm_unblock;
    GlobalData_t mu;
    GlobalData_t nperm;
    GlobalData_t nu;
    GlobalData_t perm50;
    GlobalData_t phi;
    GlobalData_t wfrac;

};

struct Stress_Land17_state {
    GlobalData_t TRPN;
    GlobalData_t TmBlocked;
    GlobalData_t XS;
    GlobalData_t XW;
    GlobalData_t ZETAS;
    GlobalData_t ZETAW;
    GlobalData_t __Cai_local;

};

class Stress_Land17IonType : public IonType {
public:
    using IonIfDerived = IonIf<Stress_Land17IonType>;
    using params_type = Stress_Land17_Params;
    using state_type = Stress_Land17_state;

    Stress_Land17IonType(bool plugin);

    size_t params_size() const override;

    size_t dlo_vector_size() const override;

    uint32_t reqdat() const override;

    uint32_t moddat() const override;

    void initialize_params(IonIfBase& imp_base) const override;

    void construct_tables(IonIfBase& imp_base) const override;

    void destroy(IonIfBase& imp_base) const override;

    void initialize_sv(IonIfBase& imp_base, GlobalData_t** data) const override;

    Target select_target(Target target) const override;

    void compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const override;

    bool has_trace() const override;

    void trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const override;

    void tune(IonIfBase& imp_base, const char* im_par) const override;

    int read_svs(IonIfBase& imp_base, FILE* file) const override;

    int write_svs(IonIfBase& imp_base, FILE* file, int node) const override;

    SVgetfcn get_sv_offset(const char* svname, int* off, int* sz) const override;

    int get_sv_list(char*** list) const override;

    int get_sv_type(const char* svname, int* type, char** type_name) const override;

    void print_params() const override;

    void print_metadata() const override;

    IonIfBase* make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const override;

    void destroy_ion_if(IonIfBase *imp) const override;
};

// This needs to be extern C in order to be linked correctly with the MLIR code
extern "C" {

//void compute_Stress_Land17(int, int, IonIfBase&, GlobalData_t**);
#ifdef STRESS_LAND17_CPU_GENERATED
void compute_Stress_Land17_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef STRESS_LAND17_MLIR_CPU_GENERATED
void compute_Stress_Land17_mlir_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef STRESS_LAND17_MLIR_ROCM_GENERATED
void compute_Stress_Land17_mlir_gpu_rocm(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef STRESS_LAND17_MLIR_CUDA_GENERATED
void compute_Stress_Land17_mlir_gpu_cuda(int, int, IonIfBase&, GlobalData_t**);
#endif

}
}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
