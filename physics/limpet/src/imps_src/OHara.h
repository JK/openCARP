// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: O'Hara T, Virag L, Varro A, Rudy Y
*  Year: 2011
*  Title: Simulation of the Undiseased Human Cardiac Ventricular Action Potential: Model Formulation and Experimental Validation
*  Journal: PLOS Computational Biology, 7(5)
*  DOI: 10.1371/journal.pcbi.1002061
*  Comment: Sodium current can be changed by flag to the tenTusscherPanfilov formulation as done in Dutta et al. doi:10.1016/j.pbiomolbio.2017.02.007. Additionally, model modifications as suggested by Michael Clerx can be modifed by flags: https://journals.plos.org/ploscompbiol/article/comment?id=10.1371/annotation/ec788479-502e-4dbb-8985-52b9af657990
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __OHARA_H__
#define __OHARA_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"

#if !(defined(OHARA_CPU_GENERATED)    || defined(OHARA_MLIR_CPU_GENERATED)    || defined(OHARA_MLIR_ROCM_GENERATED)    || defined(OHARA_MLIR_CUDA_GENERATED))
#ifdef MLIR_CPU_GENERATED
#define OHARA_MLIR_CPU_GENERATED
#endif

#ifdef MLIR_ROCM_GENERATED
#define OHARA_MLIR_ROCM_GENERATED
#endif

#ifdef MLIR_CUDA_GENERATED
#define OHARA_MLIR_CUDA_GENERATED
#endif
#endif

#ifdef CPU_GENERATED
#define OHARA_CPU_GENERATED
#endif

namespace limpet {

#define OHara_REQDAT Vm_DATA_FLAG
#define OHara_MODDAT Iion_DATA_FLAG

struct OHara_Params {
    GlobalData_t Cao;
    GlobalData_t GK1;
    GlobalData_t GKb;
    GlobalData_t GKr;
    GlobalData_t GKs;
    GlobalData_t GNa;
    GlobalData_t GNaCa;
    GlobalData_t GNaL;
    GlobalData_t GpCa;
    GlobalData_t Gto;
    GlobalData_t INa_Type;
    GlobalData_t Ko;
    GlobalData_t Nao;
    GlobalData_t celltype;
    GlobalData_t factorICaK;
    GlobalData_t factorICaL;
    GlobalData_t factorICaNa;
    GlobalData_t factorINaCa;
    GlobalData_t factorINaCass;
    GlobalData_t factorINaK;
    GlobalData_t factorIbCa;
    GlobalData_t factorIbNa;
    GlobalData_t jrel_stiff_const;
    GlobalData_t modelformulation;
    GlobalData_t scale_tau_m;
    GlobalData_t shift_m_inf;
    GlobalData_t vHalfXs;
    char* flags;

};
static const char* OHara_flags = "TT2INa|ORdINa|ENDO|EPI|MCELL|ORIGINAL|CLERX";


struct OHara_state {
    GlobalData_t CaMKt;
    GlobalData_t Cai;
    Gatetype Jrelnp;
    Gatetype Jrelp;
    GlobalData_t Ki;
    GlobalData_t Nai;
    Gatetype a;
    Gatetype ap;
    GlobalData_t cajsr;
    GlobalData_t cansr;
    GlobalData_t cass;
    Gatetype d;
    Gatetype fcaf;
    Gatetype fcafp;
    Gatetype fcas;
    Gatetype ff;
    Gatetype ffp;
    Gatetype fs;
    Gatetype hL;
    Gatetype hLp;
    Gatetype hTT2;
    Gatetype hf;
    Gatetype hs;
    Gatetype hsp;
    Gatetype iF;
    Gatetype iFp;
    Gatetype iS;
    Gatetype iSp;
    Gatetype j;
    Gatetype jTT2;
    Gatetype jca;
    Gatetype jp;
    GlobalData_t kss;
    Gatetype mL;
    Gatetype mORd;
    Gatetype mTT2;
    GlobalData_t nass;
    GlobalData_t nca;
    Gatetype xk1;
    Gatetype xrf;
    Gatetype xrs;
    Gatetype xs1;
    Gatetype xs2;

};

class OHaraIonType : public IonType {
public:
    using IonIfDerived = IonIf<OHaraIonType>;
    using params_type = OHara_Params;
    using state_type = OHara_state;

    OHaraIonType(bool plugin);

    size_t params_size() const override;

    size_t dlo_vector_size() const override;

    uint32_t reqdat() const override;

    uint32_t moddat() const override;

    void initialize_params(IonIfBase& imp_base) const override;

    void construct_tables(IonIfBase& imp_base) const override;

    void destroy(IonIfBase& imp_base) const override;

    void initialize_sv(IonIfBase& imp_base, GlobalData_t** data) const override;

    Target select_target(Target target) const override;

    void compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const override;

    bool has_trace() const override;

    void trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const override;

    void tune(IonIfBase& imp_base, const char* im_par) const override;

    int read_svs(IonIfBase& imp_base, FILE* file) const override;

    int write_svs(IonIfBase& imp_base, FILE* file, int node) const override;

    SVgetfcn get_sv_offset(const char* svname, int* off, int* sz) const override;

    int get_sv_list(char*** list) const override;

    int get_sv_type(const char* svname, int* type, char** type_name) const override;

    void print_params() const override;

    void print_metadata() const override;

    IonIfBase* make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const override;

    void destroy_ion_if(IonIfBase *imp) const override;
};

// This needs to be extern C in order to be linked correctly with the MLIR code
extern "C" {

//void compute_OHara(int, int, IonIfBase&, GlobalData_t**);
#ifdef OHARA_CPU_GENERATED
void compute_OHara_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef OHARA_MLIR_CPU_GENERATED
void compute_OHara_mlir_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef OHARA_MLIR_ROCM_GENERATED
void compute_OHara_mlir_gpu_rocm(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef OHARA_MLIR_CUDA_GENERATED
void compute_OHara_mlir_gpu_cuda(int, int, IonIfBase&, GlobalData_t**);
#endif

}
}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
