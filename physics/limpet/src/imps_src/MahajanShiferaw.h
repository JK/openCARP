// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Aman Mahajan, Yohannes Shiferaw, Daisuke Sato, Ali Baher, Riccardo Olcese, Lai-Hua Xie, Ming-Jim Yang, Peng-Sheng Chen, Juan G. Restrepo, Alain Karma, Alan Garfinkel, Zhilin Qu, James N. Weiss
*  Year: 2008
*  Title: A Rabbit Ventricular Action Potential Model Replicating Cardiac Dynamics at Rapid Heart Rates
*  Journal: Biophysical Journal, 94(2), 392-410
*  DOI: 10.1529/biophysj.106.98160
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __MAHAJANSHIFERAW_H__
#define __MAHAJANSHIFERAW_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"

#if !(defined(MAHAJANSHIFERAW_CPU_GENERATED)    || defined(MAHAJANSHIFERAW_MLIR_CPU_GENERATED)    || defined(MAHAJANSHIFERAW_MLIR_ROCM_GENERATED)    || defined(MAHAJANSHIFERAW_MLIR_CUDA_GENERATED))
#ifdef MLIR_CPU_GENERATED
#define MAHAJANSHIFERAW_MLIR_CPU_GENERATED
#endif

#ifdef MLIR_ROCM_GENERATED
#define MAHAJANSHIFERAW_MLIR_ROCM_GENERATED
#endif

#ifdef MLIR_CUDA_GENERATED
#define MAHAJANSHIFERAW_MLIR_CUDA_GENERATED
#endif
#endif

#ifdef CPU_GENERATED
#define MAHAJANSHIFERAW_CPU_GENERATED
#endif

namespace limpet {

#define MahajanShiferaw_REQDAT Vm_DATA_FLAG
#define MahajanShiferaw_MODDAT Iion_DATA_FLAG

struct MahajanShiferaw_Params {
    GlobalData_t BASE_Ki;
    GlobalData_t Cao;
    GlobalData_t GCaL;
    GlobalData_t GK1;
    GlobalData_t GKr;
    GlobalData_t GKs;
    GlobalData_t GNa;
    GlobalData_t GNaCa;
    GlobalData_t Gtof;
    GlobalData_t Gtos;
    GlobalData_t Ko;
    GlobalData_t Nai;
    GlobalData_t Nao;
    GlobalData_t T;
    GlobalData_t constant_Ki;
    GlobalData_t factorGKs;
    GlobalData_t gss;

};

struct MahajanShiferaw_state {
    GlobalData_t Cai;
    GlobalData_t GKr;
    GlobalData_t Ki;
    GlobalData_t Nai;
    GlobalData_t c1;
    GlobalData_t c2;
    GlobalData_t cj;
    GlobalData_t cjp;
    GlobalData_t cp;
    GlobalData_t cs;
    GlobalData_t factorGKs;
    Gatetype h;
    Gatetype j;
    Gatetype m;
    GlobalData_t tropi;
    GlobalData_t trops;
    GlobalData_t xi1ba;
    GlobalData_t xi1ca;
    GlobalData_t xi2ba;
    GlobalData_t xi2ca;
    GlobalData_t xir;
    Gatetype xr;
    Gatetype xs1;
    Gatetype xs2;
    Gatetype xtof;
    Gatetype xtos;
    Gatetype ytof;
    Gatetype ytos;
    GlobalData_t __acap_local;
    GlobalData_t __fr_myo_local;
    GlobalData_t __sl_i2c_local;
    GlobalData_t __vcell_local;

};

class MahajanShiferawIonType : public IonType {
public:
    using IonIfDerived = IonIf<MahajanShiferawIonType>;
    using params_type = MahajanShiferaw_Params;
    using state_type = MahajanShiferaw_state;

    MahajanShiferawIonType(bool plugin);

    size_t params_size() const override;

    size_t dlo_vector_size() const override;

    uint32_t reqdat() const override;

    uint32_t moddat() const override;

    void initialize_params(IonIfBase& imp_base) const override;

    void construct_tables(IonIfBase& imp_base) const override;

    void destroy(IonIfBase& imp_base) const override;

    void initialize_sv(IonIfBase& imp_base, GlobalData_t** data) const override;

    Target select_target(Target target) const override;

    void compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const override;

    bool has_trace() const override;

    void trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const override;

    void tune(IonIfBase& imp_base, const char* im_par) const override;

    int read_svs(IonIfBase& imp_base, FILE* file) const override;

    int write_svs(IonIfBase& imp_base, FILE* file, int node) const override;

    SVgetfcn get_sv_offset(const char* svname, int* off, int* sz) const override;

    int get_sv_list(char*** list) const override;

    int get_sv_type(const char* svname, int* type, char** type_name) const override;

    void print_params() const override;

    void print_metadata() const override;

    IonIfBase* make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const override;

    void destroy_ion_if(IonIfBase *imp) const override;
};

// This needs to be extern C in order to be linked correctly with the MLIR code
extern "C" {

//void compute_MahajanShiferaw(int, int, IonIfBase&, GlobalData_t**);
#ifdef MAHAJANSHIFERAW_CPU_GENERATED
void compute_MahajanShiferaw_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef MAHAJANSHIFERAW_MLIR_CPU_GENERATED
void compute_MahajanShiferaw_mlir_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef MAHAJANSHIFERAW_MLIR_ROCM_GENERATED
void compute_MahajanShiferaw_mlir_gpu_rocm(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef MAHAJANSHIFERAW_MLIR_CUDA_GENERATED
void compute_MahajanShiferaw_mlir_gpu_cuda(int, int, IonIfBase&, GlobalData_t**);
#endif

}
}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
