// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Vladimir E. Bondarenko, Gyula P. Szigeti, Glenna C. L. Bett, Song-Jung Kim, and Randall L. Rasmusson
*  Year: 2004
*  Title: Computer model of action potential of mouse ventricular myocytes
*  Journal: American Journal of Physiology-Heart and Circulatory Physiology, 287(3), 1378-1403
*  DOI: 10.1152/ajpheart.00185.2003
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __BONDARENKO_H__
#define __BONDARENKO_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"

#if !(defined(BONDARENKO_CPU_GENERATED)    || defined(BONDARENKO_MLIR_CPU_GENERATED)    || defined(BONDARENKO_MLIR_ROCM_GENERATED)    || defined(BONDARENKO_MLIR_CUDA_GENERATED))
#ifdef MLIR_CPU_GENERATED
#define BONDARENKO_MLIR_CPU_GENERATED
#endif

#ifdef MLIR_ROCM_GENERATED
#define BONDARENKO_MLIR_ROCM_GENERATED
#endif

#ifdef MLIR_CUDA_GENERATED
#define BONDARENKO_MLIR_CUDA_GENERATED
#endif
#endif

#ifdef CPU_GENERATED
#define BONDARENKO_CPU_GENERATED
#endif

namespace limpet {

#define Bondarenko_REQDAT Vm_DATA_FLAG
#define Bondarenko_MODDAT Iion_DATA_FLAG

struct Bondarenko_Params {
    GlobalData_t Acap;
    GlobalData_t Beta_Na4;
    GlobalData_t Beta_Na5;
    GlobalData_t Bmax;
    GlobalData_t CSQN_tot;
    GlobalData_t C_K1_init;
    GlobalData_t C_K2_init;
    GlobalData_t C_Na1_init;
    GlobalData_t C_Na2_init;
    GlobalData_t CaJSR_init;
    GlobalData_t CaMKt_init;
    GlobalData_t CaNSR_init;
    GlobalData_t Cai_init;
    GlobalData_t Cao;
    GlobalData_t Cass_init;
    GlobalData_t Cm;
    GlobalData_t E_Cl;
    GlobalData_t F;
    GlobalData_t GClCa;
    GlobalData_t GK1;
    GlobalData_t GKr;
    GlobalData_t GKs;
    GlobalData_t GKss;
    GlobalData_t GKto_f;
    GlobalData_t GKto_s;
    GlobalData_t GKur;
    GlobalData_t GNa;
    GlobalData_t GNab;
    GlobalData_t GbCa;
    GlobalData_t I1_Na_init;
    GlobalData_t I2_Na_init;
    GlobalData_t IC_Na2_init;
    GlobalData_t IC_Na3_init;
    GlobalData_t ICaLmax;
    GlobalData_t IF_Na_init;
    GlobalData_t I_K_init;
    GlobalData_t I_init;
    GlobalData_t IpCa_max;
    GlobalData_t K_L;
    GlobalData_t K_mAllo;
    GlobalData_t K_mCai;
    GlobalData_t K_mCao;
    GlobalData_t K_mNai;
    GlobalData_t K_mNao;
    GlobalData_t Kd;
    GlobalData_t Ki_init;
    GlobalData_t Km_CSQN;
    GlobalData_t Km_Cl;
    GlobalData_t Km_Ko;
    GlobalData_t Km_Nai;
    GlobalData_t Km_pCa;
    GlobalData_t Km_up;
    GlobalData_t Ko;
    GlobalData_t Nai_init;
    GlobalData_t Nao;
    GlobalData_t O_K_init;
    GlobalData_t O_Na_init;
    GlobalData_t O_init;
    GlobalData_t P_C2_init;
    GlobalData_t P_CaL;
    GlobalData_t P_O1_init;
    GlobalData_t P_O2_init;
    GlobalData_t P_RyR_init;
    GlobalData_t P_ryr_const1;
    GlobalData_t P_ryr_const2;
    GlobalData_t R;
    GlobalData_t T;
    GlobalData_t VJSR;
    GlobalData_t VNSR;
    GlobalData_t V_L;
    GlobalData_t V_init;
    GlobalData_t V_max_NCX;
    GlobalData_t Vmyo;
    GlobalData_t Vss;
    GlobalData_t a;
    GlobalData_t aKss_init;
    GlobalData_t ato_f_init;
    GlobalData_t ato_s_init;
    GlobalData_t aur_init;
    GlobalData_t b;
    GlobalData_t const5;
    GlobalData_t delta_V_L;
    GlobalData_t eta;
    GlobalData_t ito_f_init;
    GlobalData_t ito_s_init;
    GlobalData_t iur_init;
    GlobalData_t k_minus_a;
    GlobalData_t k_minus_b;
    GlobalData_t k_minus_c;
    GlobalData_t k_plus_a;
    GlobalData_t k_plus_b;
    GlobalData_t k_plus_c;
    GlobalData_t k_sat;
    GlobalData_t kb;
    GlobalData_t kf;
    GlobalData_t m;
    GlobalData_t maxINaK;
    GlobalData_t n;
    GlobalData_t nKs_init;
    GlobalData_t off_rate;
    GlobalData_t on_rate;
    GlobalData_t phi_L;
    GlobalData_t t_L;
    GlobalData_t tau_L;
    GlobalData_t tau_i_const;
    GlobalData_t tau_tr;
    GlobalData_t tau_xfer;
    GlobalData_t v1;
    GlobalData_t v1_caff;
    GlobalData_t v2;
    GlobalData_t v2_caff;
    GlobalData_t vmup_init;
    GlobalData_t y_gate_init;
    GlobalData_t y_gate_tau_const1;
    GlobalData_t y_gate_tau_const2;

};

struct Bondarenko_state {
    GlobalData_t C_K1;
    GlobalData_t C_K2;
    GlobalData_t C_Na1;
    GlobalData_t C_Na2;
    GlobalData_t CaJSR;
    GlobalData_t CaMKt;
    GlobalData_t CaNSR;
    GlobalData_t Cai;
    GlobalData_t Cass;
    GlobalData_t I;
    GlobalData_t I1_Na;
    GlobalData_t I2_Na;
    GlobalData_t IC_Na2;
    GlobalData_t IC_Na3;
    GlobalData_t IF_Na;
    GlobalData_t I_K;
    GlobalData_t Ki;
    GlobalData_t Nai;
    GlobalData_t O;
    GlobalData_t O_K;
    GlobalData_t O_Na;
    GlobalData_t P_C2;
    GlobalData_t P_O1;
    GlobalData_t P_O2;
    GlobalData_t P_RyR;
    Gatetype aKss;
    Gatetype ato_f;
    Gatetype ato_s;
    Gatetype aur;
    Gatetype ito_f;
    Gatetype ito_s;
    Gatetype iur;
    Gatetype nKs;
    Gatetype y_gate;

};

    struct Bondarenko_Regional_Constants_cpu {
                    
    GlobalData_t Alpha_m;
    GlobalData_t partial_diff_P_C2_del_P_C2;
    GlobalData_t partial_diff_P_C2_del_P_O1;
    GlobalData_t partial_diff_P_O2_del_P_O2;
    GlobalData_t sigma;

    };

    struct Bondarenko_Nodal_Req_cpu {
    
    GlobalData_t V;

    };

    struct Bondarenko_Private_cpu {
      using nodal_req_type = Bondarenko_Nodal_Req_cpu;
      using regional_constants_type = Bondarenko_Regional_Constants_cpu;
      IonIfBase *IF;
      int   node_number;
      void* cvode_mem;
      void* sunctx_ptr;
      bool  trace_init;
      regional_constants_type rc;
      nodal_req_type nr;
    };
    

//Printing out the Rosenbrock declarations
extern "C" {

void Bondarenko_rosenbrock_f_cpu(float*, float*, void*);

void Bondarenko_rosenbrock_jacobian_cpu(float**, float*, void*, int);

void rbStepX ( float *X,
                void (*calcDX) (float*,  float*, void*),
                void (*calcJ)  (float**, float*, void*, int ),
                void *params, float h, int N );
}

class BondarenkoIonType : public IonType {
public:
    using IonIfDerived = IonIf<BondarenkoIonType>;
    using params_type = Bondarenko_Params;
    using state_type = Bondarenko_state;

    using private_type = Bondarenko_Private_cpu;
    #ifdef BONDARENKO_MLIR_CPU_GENERATED
    using private_type_vector = Bondarenko_Private_mlir_cpu;
    #endif

    BondarenkoIonType(bool plugin);

    size_t params_size() const override;

    size_t dlo_vector_size() const override;

    uint32_t reqdat() const override;

    uint32_t moddat() const override;

    void initialize_params(IonIfBase& imp_base) const override;

    void construct_tables(IonIfBase& imp_base) const override;

    void destroy(IonIfBase& imp_base) const override;

    void initialize_sv(IonIfBase& imp_base, GlobalData_t** data) const override;

    Target select_target(Target target) const override;

    void compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const override;

    bool has_trace() const override;

    void trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const override;

    void tune(IonIfBase& imp_base, const char* im_par) const override;

    int read_svs(IonIfBase& imp_base, FILE* file) const override;

    int write_svs(IonIfBase& imp_base, FILE* file, int node) const override;

    SVgetfcn get_sv_offset(const char* svname, int* off, int* sz) const override;

    int get_sv_list(char*** list) const override;

    int get_sv_type(const char* svname, int* type, char** type_name) const override;

    void print_params() const override;

    void print_metadata() const override;

    IonIfBase* make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const override;

    void destroy_ion_if(IonIfBase *imp) const override;
};

// This needs to be extern C in order to be linked correctly with the MLIR code
extern "C" {

//void compute_Bondarenko(int, int, IonIfBase&, GlobalData_t**);
#ifdef BONDARENKO_CPU_GENERATED
void compute_Bondarenko_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef BONDARENKO_MLIR_CPU_GENERATED
void compute_Bondarenko_mlir_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef BONDARENKO_MLIR_ROCM_GENERATED
void compute_Bondarenko_mlir_gpu_rocm(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef BONDARENKO_MLIR_CUDA_GENERATED
void compute_Bondarenko_mlir_gpu_cuda(int, int, IonIfBase&, GlobalData_t**);
#endif

}
}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
