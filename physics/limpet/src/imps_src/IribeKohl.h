// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Gentaro Iribe, Peter Kohl
*  Year: 2008
*  Title: Axial stretch enhances sarcoplasmic reticulum Ca2+ leak and cellular Ca2+ reuptake in guinea pig ventricular myocytes: Experiments and models
*  Journal: Progress in Biophysics and Molecular Biology,97(2-3),298-311
*  DOI: 10.1016/j.pbiomolbio.2008.02.012
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __IRIBEKOHL_H__
#define __IRIBEKOHL_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"

#if !(defined(IRIBEKOHL_CPU_GENERATED)    || defined(IRIBEKOHL_MLIR_CPU_GENERATED)    || defined(IRIBEKOHL_MLIR_ROCM_GENERATED)    || defined(IRIBEKOHL_MLIR_CUDA_GENERATED))
#ifdef MLIR_CPU_GENERATED
#define IRIBEKOHL_MLIR_CPU_GENERATED
#endif

#ifdef MLIR_ROCM_GENERATED
#define IRIBEKOHL_MLIR_ROCM_GENERATED
#endif

#ifdef MLIR_CUDA_GENERATED
#define IRIBEKOHL_MLIR_CUDA_GENERATED
#endif
#endif

#ifdef CPU_GENERATED
#define IRIBEKOHL_CPU_GENERATED
#endif

namespace limpet {

#define IribeKohl_REQDAT Vm_DATA_FLAG
#define IribeKohl_MODDAT Iion_DATA_FLAG

struct IribeKohl_Params {
    GlobalData_t Ki_init;

};

struct IribeKohl_state {
    GlobalData_t CaSR;
    GlobalData_t Cai;
    GlobalData_t Cmdn_Ca;
    GlobalData_t F_1;
    GlobalData_t F_2;
    Gatetype F_CaMK;
    GlobalData_t F_SRCa_RyR;
    GlobalData_t Ki;
    GlobalData_t N_0;
    GlobalData_t Nai;
    GlobalData_t P_0;
    GlobalData_t P_1;
    GlobalData_t P_2;
    GlobalData_t P_3;
    GlobalData_t Trpn_Ca;
    Gatetype d;
    Gatetype f;
    Gatetype h;
    Gatetype m;
    GlobalData_t r;
    Gatetype s;
    Gatetype x;

};

class IribeKohlIonType : public IonType {
public:
    using IonIfDerived = IonIf<IribeKohlIonType>;
    using params_type = IribeKohl_Params;
    using state_type = IribeKohl_state;

    IribeKohlIonType(bool plugin);

    size_t params_size() const override;

    size_t dlo_vector_size() const override;

    uint32_t reqdat() const override;

    uint32_t moddat() const override;

    void initialize_params(IonIfBase& imp_base) const override;

    void construct_tables(IonIfBase& imp_base) const override;

    void destroy(IonIfBase& imp_base) const override;

    void initialize_sv(IonIfBase& imp_base, GlobalData_t** data) const override;

    Target select_target(Target target) const override;

    void compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const override;

    bool has_trace() const override;

    void trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const override;

    void tune(IonIfBase& imp_base, const char* im_par) const override;

    int read_svs(IonIfBase& imp_base, FILE* file) const override;

    int write_svs(IonIfBase& imp_base, FILE* file, int node) const override;

    SVgetfcn get_sv_offset(const char* svname, int* off, int* sz) const override;

    int get_sv_list(char*** list) const override;

    int get_sv_type(const char* svname, int* type, char** type_name) const override;

    void print_params() const override;

    void print_metadata() const override;

    IonIfBase* make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const override;

    void destroy_ion_if(IonIfBase *imp) const override;
};

// This needs to be extern C in order to be linked correctly with the MLIR code
extern "C" {

//void compute_IribeKohl(int, int, IonIfBase&, GlobalData_t**);
#ifdef IRIBEKOHL_CPU_GENERATED
void compute_IribeKohl_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef IRIBEKOHL_MLIR_CPU_GENERATED
void compute_IribeKohl_mlir_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef IRIBEKOHL_MLIR_ROCM_GENERATED
void compute_IribeKohl_mlir_gpu_rocm(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef IRIBEKOHL_MLIR_CUDA_GENERATED
void compute_IribeKohl_mlir_gpu_cuda(int, int, IonIfBase&, GlobalData_t**);
#endif

}
}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
