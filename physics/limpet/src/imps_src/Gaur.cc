// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Namit Gaur, Xiao-Yan Qi, David Benoist, Olivier Bernus, Ruben Coronel, Stanley Nattel, Edward J. Vigmond
*  Year: 2021
*  Title: A computational model of pig ventricular cardiomyocyte electrophysiology and calcium handling: Translation from pig to human electrophysiology
*  Journal: PLoS Computational Biology, 17(6):e1009137
*  DOI: 10.1371/journal.pcbi.1009137
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "Gaur.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

GaurIonType::GaurIonType(bool plugin) : IonType(std::move(std::string("Gaur")), plugin) {}

size_t GaurIonType::params_size() const {
  return sizeof(struct Gaur_Params);
}

size_t GaurIonType::dlo_vector_size() const {

  return 1;
}

uint32_t GaurIonType::reqdat() const {
  return Gaur_REQDAT;
}

uint32_t GaurIonType::moddat() const {
  return Gaur_MODDAT;
}

void GaurIonType::destroy(IonIfBase& imp_base) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  imp.destroy_luts();
  // rarely need to do anything else
}

Target GaurIonType::select_target(Target target) const {
  switch (target) {
    case Target::AUTO:
#   ifdef GAUR_MLIR_CUDA_GENERATED
      return Target::MLIR_CUDA;
#   elif defined(GAUR_MLIR_ROCM_GENERATED)
      return Target::MLIR_ROCM;
#   elif defined(GAUR_MLIR_CPU_GENERATED)
      return Target::MLIR_CPU;
#   elif defined(GAUR_CPU_GENERATED)
      return Target::CPU;
#   else
      return Target::UNKNOWN;
#   endif
#   ifdef GAUR_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      return Target::MLIR_CUDA;
#   endif
#   ifdef GAUR_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      return Target::MLIR_ROCM;
#   endif
#   ifdef GAUR_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      return Target::MLIR_CPU;
#   endif
#   ifdef GAUR_CPU_GENERATED
    case Target::CPU:
      return Target::CPU;
#   endif
    default:
      return Target::UNKNOWN;
  }
}

void GaurIonType::compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  switch(target) {
    case Target::AUTO:
#   ifdef GAUR_MLIR_CUDA_GENERATED
      compute_Gaur_mlir_gpu_cuda(start, end, imp, data);
#   elif defined(GAUR_MLIR_ROCM_GENERATED)
      compute_Gaur_mlir_gpu_rocm(start, end, imp, data);
#   elif defined(GAUR_MLIR_CPU_GENERATED)
      compute_Gaur_mlir_cpu(start, end, imp, data);
#   elif defined(GAUR_CPU_GENERATED)
      compute_Gaur_cpu(start, end, imp, data);
#   else
#     error "Could not generate method GaurIonType::compute."
#   endif
      break;
#   ifdef GAUR_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      compute_Gaur_mlir_gpu_cuda(start, end, imp, data);
      break;
#   endif
#   ifdef GAUR_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      compute_Gaur_mlir_gpu_rocm(start, end, imp, data);
      break;
#   endif
#   ifdef GAUR_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      compute_Gaur_mlir_cpu(start, end, imp, data);
      break;
#   endif
#   ifdef GAUR_CPU_GENERATED
    case Target::CPU:
      compute_Gaur_cpu(start, end, imp, data);
      break;
#   endif
    default:
      throw std::runtime_error(std::string("Could not compute with the given target ") + get_string_from_target(target) + ".");
      break;
  }
}

// Define all constants
#define BSLmax (GlobalData_t)(1.124)
#define BSRmax (GlobalData_t)(0.047)
#define CaMKo (GlobalData_t)(0.05)
#define CaMKt_init (GlobalData_t)(0.00823911)
#define F (GlobalData_t)(96485.)
#define GpCa (GlobalData_t)(0.0575)
#define H (GlobalData_t)(1.0e-7)
#define Jrel1_init (GlobalData_t)(9.71e-22)
#define Jrel2_init (GlobalData_t)(8.59e-10)
#define Khp (GlobalData_t)(1.698e-7)
#define Kki (GlobalData_t)(0.5)
#define Kko (GlobalData_t)(0.3582)
#define KmBSL (GlobalData_t)(0.0087)
#define KmBSR (GlobalData_t)(0.00087)
#define KmCaAct (GlobalData_t)(150.0e-6)
#define KmCaM (GlobalData_t)(0.0015)
#define KmCaMK (GlobalData_t)(0.065)
#define Kmgatp (GlobalData_t)(1.698e-7)
#define Knai0 (GlobalData_t)(9.073)
#define Knao0 (GlobalData_t)(27.78)
#define Knap (GlobalData_t)(224.0)
#define Kxkur (GlobalData_t)(292.0)
#define L (GlobalData_t)(0.017)
#define MgADP (GlobalData_t)(0.05)
#define MgATP (GlobalData_t)(9.8)
#define Na_i_init (GlobalData_t)(6.43)
#define PCab (GlobalData_t)(2.5e-8)
#define PKNa (GlobalData_t)(0.01833)
#define Pnak (GlobalData_t)(30.)
#define R (GlobalData_t)(8314.)
#define T (GlobalData_t)(310.0)
#define aCaMK (GlobalData_t)(0.05)
#define aa_init (GlobalData_t)(0.995)
#define bCaMK (GlobalData_t)(0.00068)
#define cacsr_init (GlobalData_t)(1.30)
#define cai2_init (GlobalData_t)(6.81e-5)
#define cai_init (GlobalData_t)(6.75e-5)
#define cajsr_init (GlobalData_t)(1.31)
#define cansr_init (GlobalData_t)(1.37)
#define cao (GlobalData_t)(1.8)
#define cli (GlobalData_t)(19.53)
#define clo (GlobalData_t)(100.)
#define cmdnmax (GlobalData_t)(0.05)
#define csqnmax (GlobalData_t)(10.0)
#define d_init (GlobalData_t)(4.42e-7)
#define delta (GlobalData_t)(-0.1550)
#define eP (GlobalData_t)(4.2)
#define fca_init (GlobalData_t)(0.988)
#define grelbarjsrol (GlobalData_t)(2.)
#define h_init (GlobalData_t)(0.631)
#define hl_init (GlobalData_t)(0.301)
#define j_init (GlobalData_t)(0.631)
#define k1m (GlobalData_t)(182.4)
#define k1p (GlobalData_t)(949.5)
#define k2m (GlobalData_t)(39.4)
#define k2p (GlobalData_t)(687.2)
#define k3m (GlobalData_t)(79300.0)
#define k3p2 (GlobalData_t)(1899.0)
#define k4m (GlobalData_t)(40.0)
#define k4p2 (GlobalData_t)(639.0)
#define kasymm (GlobalData_t)(12.5)
#define kcaoff (GlobalData_t)(5.0e3)
#define kcaon (GlobalData_t)(1.5e6)
#define ki_init (GlobalData_t)(140.76)
#define kmcmdn (GlobalData_t)(0.00238)
#define kmcsqn (GlobalData_t)(0.8)
#define kmtrpn (GlobalData_t)(0.0005)
#define kna1 (GlobalData_t)(15.0)
#define kna2 (GlobalData_t)(5.0)
#define kna3 (GlobalData_t)(88.12)
#define ko (GlobalData_t)(5.4)
#define m_init (GlobalData_t)(0.0022)
#define ml_init (GlobalData_t)(0.0010)
#define nao (GlobalData_t)(140.)
#define pi (GlobalData_t)(3.14)
#define qca (GlobalData_t)(0.1670)
#define qna (GlobalData_t)(0.5224)
#define rad (GlobalData_t)(0.0011)
#define tau_hl (GlobalData_t)(600.)
#define tauoff (GlobalData_t)(5.)
#define tauon (GlobalData_t)(0.5)
#define tjsrol_init (GlobalData_t)(100.)
#define trpnmax (GlobalData_t)(0.07)
#define v_init (GlobalData_t)(-87.3)
#define vhalff (GlobalData_t)(-22.9)
#define wca (GlobalData_t)(6.0e4)
#define wna (GlobalData_t)(6.0e4)
#define wnaca (GlobalData_t)(5.0e3)
#define xr_init (GlobalData_t)(0.153)
#define xs1_init (GlobalData_t)(0.054)
#define xs2_init (GlobalData_t)(0.046)
#define zca (GlobalData_t)(2.0)
#define zk (GlobalData_t)(1.0)
#define zna (GlobalData_t)(1.0)
#define Ageo (GlobalData_t)(((((2.*pi)*rad)*rad)+(((2.*pi)*rad)*L)))
#define a2 (GlobalData_t)(k2p)
#define a4 (GlobalData_t)((((k4p2*MgATP)/Kmgatp)/(1.0+(MgATP/Kmgatp))))
#define b1 (GlobalData_t)((k1m*MgADP))
#define h10 (GlobalData_t)(((kasymm+1.0)+((nao/kna1)*(1.0+(nao/kna2)))))
#define h101 (GlobalData_t)(((kasymm+1.0)+((nao/kna1)*(1.+(nao/kna2)))))
#define k2 (GlobalData_t)(kcaoff)
#define k21 (GlobalData_t)(kcaoff)
#define k5 (GlobalData_t)(kcaoff)
#define k51 (GlobalData_t)(kcaoff)
#define rtf (GlobalData_t)(((R*T)/F))
#define vcell (GlobalData_t)(((((1000.*pi)*rad)*rad)*L))
#define Acap (GlobalData_t)((2.*Ageo))
#define ECl (GlobalData_t)((rtf*(log((cli/clo)))))
#define h11 (GlobalData_t)(((nao*nao)/((h10*kna1)*kna2)))
#define h1111 (GlobalData_t)(((nao*nao)/((h101*kna1)*kna2)))
#define h12 (GlobalData_t)((1.0/h10))
#define h121 (GlobalData_t)((1.0/h101))
#define vjsr (GlobalData_t)(((0.0048*vcell)/2.))
#define vmyo (GlobalData_t)((0.68*vcell))
#define vnsr (GlobalData_t)((0.0552*vcell))
#define vss (GlobalData_t)(((0.02*vcell)/2.))
#define k1 (GlobalData_t)(((h12*cao)*kcaon))
#define k11 (GlobalData_t)(((h121*cao)*kcaon))
#define vcsr (GlobalData_t)(vjsr)
#define vnsr1 (GlobalData_t)((vnsr/2.))
#define vnsr2 (GlobalData_t)((vnsr-(vnsr1)))



void GaurIonType::initialize_params(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  cell_geom* region = &imp.cgeom();
  Gaur_Params *p = imp.params();

  // Compute the regional constants
  {
    p->GK1 = (0.75*0.2);
    p->GKb = 0.003;
    p->GNa = 14.838;
    p->GNaL = 0.0075;
    p->Gncx = 0.0008;
    p->Gto = (0.4*0.5);
    p->PNab = 3.75e-10;
    p->SOICR = 10.;
    p->tau_gap = 3.;
    p->vhalf_d = 3.4;
    p->vmyo1frac = 0.4;
  }
  // Compute the regional initialization
  {
    p->GKr = (0.5*0.015);
    p->GKs = 0.021;
    p->PCa = 2e-4;
  }

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.

    enum Rosenbrock {
    

      N_ROSEN
    };


void GaurIonType::construct_tables(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom* region = &imp.cgeom();
  Gaur_Params *p = imp.params();

  imp.tables().resize(N_TABS);

  // Define the constants that depend on the parameters.
  double hl_rush_larsen_B = (exp(((-dt)/tau_hl)));
  double hl_rush_larsen_C = (expm1(((-dt)/tau_hl)));
  double vmyo1 = (vmyo*p->vmyo1frac);
  double vmyo2 = (vmyo-(vmyo1));

}



void GaurIonType::initialize_sv(IonIfBase& imp_base, GlobalData_t **impdata ) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom *region = &imp.cgeom();
  Gaur_Params *p = imp.params();

  Gaur_state *sv_base = (Gaur_state *)imp.sv_tab().data();
  GlobalData_t t = 0;

  IonIfDerived* IF = &imp;
  // Define the constants that depend on the parameters.
  double hl_rush_larsen_B = (exp(((-dt)/tau_hl)));
  double hl_rush_larsen_C = (expm1(((-dt)/tau_hl)));
  double vmyo1 = (vmyo*p->vmyo1frac);
  double vmyo2 = (vmyo-(vmyo1));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *v_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i < imp.get_num_node(); __i+=1 ){
    Gaur_state *sv = sv_base+__i / 1;
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t v = v_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    sv->GKr = p->GKr;
    sv->GKs = p->GKs;
    sv->PCa = p->PCa;
    // Initialize the rest of the nodal variables
    sv->CaMKt = CaMKt_init;
    sv->Jrel1 = Jrel1_init;
    sv->Jrel2 = Jrel2_init;
    sv->Na_i = Na_i_init;
    double PCaK = (3.574e-4*sv->PCa);
    double PCaNa = (0.00125*sv->PCa);
    sv->aa = aa_init;
    sv->cacsr = cacsr_init;
    sv->cai = cai_init;
    sv->cai2 = cai2_init;
    sv->cajsr = cajsr_init;
    sv->cansr = cansr_init;
    sv->d = d_init;
    sv->fca = fca_init;
    sv->h = h_init;
    sv->hl = hl_init;
    sv->j = j_init;
    sv->ki = ki_init;
    sv->m = m_init;
    sv->ml = ml_init;
    sv->tjsrol = tjsrol_init;
    v = v_init;
    sv->xr = xr_init;
    sv->xs1 = xs1_init;
    sv->xs2 = xs2_init;
    double EK = (rtf*(log((ko/sv->ki))));
    double EKs = (rtf*(log(((ko+(PKNa*nao))/(sv->ki+(PKNa*sv->Na_i))))));
    double ENa = (rtf*(log((nao/sv->Na_i))));
    double IpCa = ((GpCa*sv->cai)/(0.0005+sv->cai));
    double KsCa = (1.0+(0.6/(1.0+(pow((3.8e-5/sv->cai),1.4)))));
    double P = (eP/(((1.0+(H/Khp))+(sv->Na_i/Knap))+(sv->ki/Kxkur)));
    double allo = (1.0/(1.0+(square((KmCaAct/sv->cai)))));
    double cass_init = sv->cai;
    double f_inf = ((1.0/(1.0+(exp(((v-(vhalff))/4.9)))))+(0.35/(1.+(exp(((45.-(v))/20.))))));
    double greljsrol = ((grelbarjsrol*(1.-((exp(((-sv->tjsrol)/tauon))))))*(exp(((-sv->tjsrol)/tauoff))));
    double h4 = (1.0+((sv->Na_i/kna1)*(1.+(sv->Na_i/kna2))));
    double kss_init = sv->ki;
    double nass_init = sv->Na_i;
    double rito2 = (1./(1.+(exp(((-(v+10.))/5.)))));
    double rk1 = (1.0/(1.0+(exp((((v+79.3)-((2.6*ko)))/19.6)))));
    double rkr = (1.0/(1.0+(exp(((v+22.0)/15.0)))));
    double vfrt = ((v*F)/(R*T));
    double xkb = (1.0/(1.0+(exp(((-(v-(14.48)))/18.34)))));
    double ICab = ((((PCab*2.)*F)*((sv->cai*(exp((2.0*vfrt))))-((0.341*cao))))*((v==0.) ? 1. : ((2.*vfrt)/(expm1((2.0*vfrt))))));
    double IK1 = (((p->GK1*(sqrt((ko/5.4))))*rk1)*(v-(EK)));
    double IKb = ((p->GKb*xkb)*(v-(EK)));
    double IKr = ((((sv->GKr*(sqrt((ko/5.4))))*sv->xr)*rkr)*(v-(EK)));
    double IKs = ((((sv->GKs*KsCa)*sv->xs1)*sv->xs2)*(v-(EKs)));
    double INa = ((((((p->GNa*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(v-(ENa)));
    double INaL = (((((p->GNaL*sv->ml)*sv->ml)*sv->ml)*sv->hl)*(v-(ENa)));
    double INab = (((p->PNab*F)*((sv->Na_i*(exp(vfrt)))-(nao)))*((v==0.) ? 1. : (vfrt/(expm1(vfrt)))));
    double Knai = (Knai0*(exp(((delta*vfrt)/3.0))));
    double Knao = (Knao0*(exp((((1.0-(delta))*vfrt)/3.0))));
    double b3 = (((k3m*P)*H)/(1.0+(MgATP/Kmgatp)));
    sv->cass = cass_init;
    double ff_inf = f_inf;
    double fs_inf = f_inf;
    double h5 = ((sv->Na_i*sv->Na_i)/((h4*kna1)*kna2));
    double h6 = (1.0/h4);
    double hca = (exp((qca*vfrt)));
    double hna = (exp((qna*vfrt)));
    sv->kss = kss_init;
    sv->nass = nass_init;
    double Jrelol = (greljsrol*(sv->cajsr-(sv->cass)));
    double PhiCaK = ((F*(((0.75*sv->kss)*(exp(vfrt)))-((0.75*ko))))*((v==0.) ? 1. : (vfrt/(expm1(vfrt)))));
    double PhiCaL = (((2.0*F)*((sv->cass*(exp((2.0*vfrt))))-((0.341*cao))))*((v==0.) ? 1. : ((2.*vfrt)/(expm1((2.0*vfrt))))));
    double PhiCaNa = ((F*(((0.75*sv->nass)*(exp(vfrt)))-((0.75*nao))))*((v==0.) ? 1. : (vfrt/(expm1(vfrt)))));
    double a1 = ((k1p*(cube((sv->Na_i/Knai))))/(((cube((1.0+(sv->Na_i/Knai))))+(square((1.0+(sv->ki/Kki)))))-(1.0)));
    double a3 = ((k3p2*(square((ko/Kko))))/(((cube((1.0+(nao/Knao))))+(square((1.0+(ko/Kko)))))-(1.0)));
    double allo1 = (1.0/(1.0+(square((KmCaAct/sv->cass)))));
    double b2 = ((k2m*(cube((nao/Knao))))/(((cube((1.0+(nao/Knao))))+(square((1.0+(ko/Kko)))))-(1.0)));
    double b4 = ((k4m*(square((sv->ki/Kki))))/(((cube((1.0+(sv->Na_i/Knai))))+(square((1.0+(sv->ki/Kki)))))-(1.0)));
    double ff_init = ff_inf;
    double fs_init = fs_inf;
    double h1 = (1.+((sv->Na_i/kna3)*(1.+hna)));
    double h111 = (1.+((sv->nass/kna3)*(1.+hna)));
    double h41 = (1.0+((sv->nass/kna1)*(1.+(sv->nass/kna2))));
    double h7 = (1.0+((nao/kna3)*(1.0+(1.0/hna))));
    double h71 = (1.0+((nao/kna3)*(1.0+(1.0/hna))));
    double k6 = ((h6*sv->cai)*kcaon);
    double Jrel = (sv->Jrel1+Jrelol);
    sv->ff = ff_init;
    sv->fs = fs_init;
    double h2 = ((sv->Na_i*hna)/(kna3*h1));
    double h21 = ((sv->nass*hna)/(kna3*h111));
    double h3 = (1.0/h1);
    double h31 = (1.0/h111);
    double h51 = ((sv->nass*sv->nass)/((h41*kna1)*kna2));
    double h61 = (1.0/h41);
    double h8 = (nao/((kna3*hna)*h7));
    double h81 = (nao/((kna3*hna)*h71));
    double h9 = (1.0/h7);
    double h91 = (1.0/h71);
    double x12 = (((((a4*a1)*a2)+((b2*b4)*b3))+((a2*b4)*b3))+((b3*a1)*a2));
    double x22 = (((((b2*b1)*b4)+((a1*a2)*a3))+((a3*b1)*b4))+((a2*a3)*b4));
    double x32 = (((((a2*a3)*a4)+((b3*b2)*b1))+((b2*b1)*a4))+((a3*a4)*b1));
    double x42 = (((((b4*b3)*b2)+((a3*a4)*a1))+((b2*a4)*a1))+((b3*b2)*a1));
    double E12 = (x12/(((x12+x22)+x32)+x42));
    double E22 = (x22/(((x12+x22)+x32)+x42));
    double E32 = (x32/(((x12+x22)+x32)+x42));
    double E42 = (x42/(((x12+x22)+x32)+x42));
    double f = (sv->ff*sv->fs);
    double k3p = (h9*wca);
    double k3p1 = (h91*wca);
    double k3pp = (h8*wnaca);
    double k3pp1 = (h81*wnaca);
    double k4p = ((h3*wca)/hca);
    double k4p1 = ((h31*wca)/hca);
    double k4pp = (h2*wnaca);
    double k4pp1 = (h21*wnaca);
    double k61 = ((h61*sv->cass)*kcaon);
    double k7 = ((h5*h2)*wna);
    double k71 = ((h51*h21)*wna);
    double k8 = ((h8*h11)*wna);
    double k81 = ((h81*h1111)*wna);
    double kito2 = (1.-((1./(1.+(square((Jrel/0.4)))))));
    double ICaK = ((((PCaK*PhiCaK)*sv->d)*f)*sv->fca);
    double ICaL = ((((sv->PCa*PhiCaL)*sv->d)*f)*sv->fca);
    double ICaNa = ((((PCaNa*PhiCaNa)*sv->d)*f)*sv->fca);
    double ITo = ((((p->Gto*sv->aa)*rito2)*kito2)*(v-(ECl)));
    double JnakK = (2.0*((E42*b1)-((E32*a1))));
    double JnakNa = (3.0*((E12*a3)-((E22*b3))));
    double k3 = (k3p+k3pp);
    double k31 = (k3p1+k3pp1);
    double k4 = (k4p+k4pp);
    double k41 = (k4p1+k4pp1);
    double INaK = (Pnak*((zna*JnakNa)+(zk*JnakK)));
    double x1 = (((k2*k4)*(k7+k6))+((k5*k7)*(k2+k3)));
    double x11 = (((k21*k41)*(k71+k61))+((k51*k71)*(k21+k31)));
    double x2 = (((k1*k7)*(k4+k5))+((k4*k6)*(k1+k8)));
    double x21 = (((k11*k71)*(k41+k51))+((k41*k61)*(k11+k81)));
    double x3 = (((k1*k3)*(k7+k6))+((k8*k6)*(k2+k3)));
    double x31 = (((k11*k31)*(k71+k61))+((k81*k61)*(k21+k31)));
    double x4 = (((k2*k8)*(k4+k5))+((k3*k5)*(k1+k8)));
    double x41 = (((k21*k81)*(k41+k51))+((k31*k51)*(k11+k81)));
    double E1 = (x1/(((x1+x2)+x3)+x4));
    double E11 = (x11/(((x11+x21)+x31)+x41));
    double E2 = (x2/(((x1+x2)+x3)+x4));
    double E21 = (x21/(((x11+x21)+x31)+x41));
    double E3 = (x3/(((x1+x2)+x3)+x4));
    double E31 = (x31/(((x11+x21)+x31)+x41));
    double E4 = (x4/(((x1+x2)+x3)+x4));
    double E41 = (x41/(((x11+x21)+x31)+x41));
    double JncxCa = ((E2*k2)-((E1*k1)));
    double JncxCa1 = ((E21*k21)-((E11*k11)));
    double JncxNa = (((3.0*((E4*k7)-((E1*k8))))+(E3*k4pp))-((E2*k3pp)));
    double JncxNa1 = (((3.0*((E41*k71)-((E11*k81))))+(E31*k4pp1))-((E21*k3pp1)));
    double INaCa_i = (((0.8*p->Gncx)*allo)*((zna*JncxNa)+(zca*JncxCa)));
    double INaCa_ss = (((0.2*p->Gncx)*allo1)*((zna*JncxNa1)+(zca*JncxCa1)));
    Iion = (((((((((((((((INa+INaL)+ICaL)+ICaNa)+ICaK)+IKr)+IKs)+IK1)+ITo)+INaCa_i)+INaCa_ss)+INaK)+INab)+IKb)+IpCa)+ICab);
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    v_ext[__i] = v;
  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
#ifdef GAUR_CPU_GENERATED
extern "C" {
void compute_Gaur_cpu(int start, int end, IonIfBase& imp_base, GlobalData_t **impdata )
{
  GaurIonType::IonIfDerived& imp = static_cast<GaurIonType::IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt()*1e0;
  cell_geom *region = &imp.cgeom();
  Gaur_Params *p  = imp.params();
  Gaur_state *sv_base = (Gaur_state *)imp.sv_tab().data();

  GlobalData_t t = imp.get_tstp().cnt*dt;

  GaurIonType::IonIfDerived* IF = &imp;

  // Define the constants that depend on the parameters.
  GlobalData_t hl_rush_larsen_B = (exp(((-dt)/tau_hl)));
  GlobalData_t hl_rush_larsen_C = (expm1(((-dt)/tau_hl)));
  GlobalData_t vmyo1 = (vmyo*p->vmyo1frac);
  GlobalData_t vmyo2 = (vmyo-(vmyo1));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *v_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=(start / 1) * 1; __i<end; __i+=1) {
    Gaur_state *sv = sv_base+__i / 1;
                    
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t v = v_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    GlobalData_t EK = (rtf*(log((ko/sv->ki))));
    GlobalData_t EKs = (rtf*(log(((ko+(PKNa*nao))/(sv->ki+(PKNa*sv->Na_i))))));
    GlobalData_t ENa = (rtf*(log((nao/sv->Na_i))));
    GlobalData_t IpCa = ((GpCa*sv->cai)/(0.0005+sv->cai));
    GlobalData_t KsCa = (1.0+(0.6/(1.0+(pow((3.8e-5/sv->cai),1.4)))));
    GlobalData_t P = (eP/(((1.0+(H/Khp))+(sv->Na_i/Knap))+(sv->ki/Kxkur)));
    GlobalData_t PCaK = (3.574e-4*sv->PCa);
    GlobalData_t PCaNa = (0.00125*sv->PCa);
    GlobalData_t allo = (1.0/(1.0+(square((KmCaAct/sv->cai)))));
    GlobalData_t allo1 = (1.0/(1.0+(square((KmCaAct/sv->cass)))));
    GlobalData_t dtjsrol = (((sv->cajsr>p->SOICR)&&(sv->tjsrol>50.)) ? (-sv->tjsrol) : dt);
    GlobalData_t f = (sv->ff*sv->fs);
    GlobalData_t greljsrol = ((grelbarjsrol*(1.-((exp(((-sv->tjsrol)/tauon))))))*(exp(((-sv->tjsrol)/tauoff))));
    GlobalData_t h4 = (1.0+((sv->Na_i/kna1)*(1.+(sv->Na_i/kna2))));
    GlobalData_t h41 = (1.0+((sv->nass/kna1)*(1.+(sv->nass/kna2))));
    GlobalData_t rito2 = (1./(1.+(exp(((-(v+10.))/5.)))));
    GlobalData_t rk1 = (1.0/(1.0+(exp((((v+79.3)-((2.6*ko)))/19.6)))));
    GlobalData_t rkr = (1.0/(1.0+(exp(((v+22.0)/15.0)))));
    GlobalData_t vfrt = ((v*F)/(R*T));
    GlobalData_t xkb = (1.0/(1.0+(exp(((-(v-(14.48)))/18.34)))));
    GlobalData_t ICab = ((((PCab*2.)*F)*((sv->cai*(exp((2.0*vfrt))))-((0.341*cao))))*((v==0.) ? 1. : ((2.*vfrt)/(expm1((2.0*vfrt))))));
    GlobalData_t IK1 = (((p->GK1*(sqrt((ko/5.4))))*rk1)*(v-(EK)));
    GlobalData_t IKb = ((p->GKb*xkb)*(v-(EK)));
    GlobalData_t IKr = ((((sv->GKr*(sqrt((ko/5.4))))*sv->xr)*rkr)*(v-(EK)));
    GlobalData_t IKs = ((((sv->GKs*KsCa)*sv->xs1)*sv->xs2)*(v-(EKs)));
    GlobalData_t INa = ((((((p->GNa*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(v-(ENa)));
    GlobalData_t INaL = (((((p->GNaL*sv->ml)*sv->ml)*sv->ml)*sv->hl)*(v-(ENa)));
    GlobalData_t INab = (((p->PNab*F)*((sv->Na_i*(exp(vfrt)))-(nao)))*((v==0.) ? 1. : (vfrt/(expm1(vfrt)))));
    GlobalData_t Jrelol = (greljsrol*(sv->cajsr-(sv->cass)));
    GlobalData_t Knai = (Knai0*(exp(((delta*vfrt)/3.0))));
    GlobalData_t Knao = (Knao0*(exp((((1.0-(delta))*vfrt)/3.0))));
    GlobalData_t PhiCaK = ((F*(((0.75*sv->kss)*(exp(vfrt)))-((0.75*ko))))*((v==0.) ? 1. : (vfrt/(expm1(vfrt)))));
    GlobalData_t PhiCaL = (((2.0*F)*((sv->cass*(exp((2.0*vfrt))))-((0.341*cao))))*((v==0.) ? 1. : ((2.*vfrt)/(expm1((2.0*vfrt))))));
    GlobalData_t PhiCaNa = ((F*(((0.75*sv->nass)*(exp(vfrt)))-((0.75*nao))))*((v==0.) ? 1. : (vfrt/(expm1(vfrt)))));
    GlobalData_t b3 = (((k3m*P)*H)/(1.0+(MgATP/Kmgatp)));
    GlobalData_t h5 = ((sv->Na_i*sv->Na_i)/((h4*kna1)*kna2));
    GlobalData_t h51 = ((sv->nass*sv->nass)/((h41*kna1)*kna2));
    GlobalData_t h6 = (1.0/h4);
    GlobalData_t h61 = (1.0/h41);
    GlobalData_t hca = (exp((qca*vfrt)));
    GlobalData_t hna = (exp((qna*vfrt)));
    GlobalData_t tjsrol_new = (sv->tjsrol+dtjsrol);
    GlobalData_t ICaK = ((((PCaK*PhiCaK)*sv->d)*f)*sv->fca);
    GlobalData_t ICaL = ((((sv->PCa*PhiCaL)*sv->d)*f)*sv->fca);
    GlobalData_t ICaNa = ((((PCaNa*PhiCaNa)*sv->d)*f)*sv->fca);
    GlobalData_t Jrel = (sv->Jrel1+Jrelol);
    GlobalData_t a1 = ((k1p*(cube((sv->Na_i/Knai))))/(((cube((1.0+(sv->Na_i/Knai))))+(square((1.0+(sv->ki/Kki)))))-(1.0)));
    GlobalData_t a3 = ((k3p2*(square((ko/Kko))))/(((cube((1.0+(nao/Knao))))+(square((1.0+(ko/Kko)))))-(1.0)));
    GlobalData_t b2 = ((k2m*(cube((nao/Knao))))/(((cube((1.0+(nao/Knao))))+(square((1.0+(ko/Kko)))))-(1.0)));
    GlobalData_t b4 = ((k4m*(square((sv->ki/Kki))))/(((cube((1.0+(sv->Na_i/Knai))))+(square((1.0+(sv->ki/Kki)))))-(1.0)));
    GlobalData_t h1 = (1.+((sv->Na_i/kna3)*(1.+hna)));
    GlobalData_t h111 = (1.+((sv->nass/kna3)*(1.+hna)));
    GlobalData_t h7 = (1.0+((nao/kna3)*(1.0+(1.0/hna))));
    GlobalData_t h71 = (1.0+((nao/kna3)*(1.0+(1.0/hna))));
    GlobalData_t k6 = ((h6*sv->cai)*kcaon);
    GlobalData_t k61 = ((h61*sv->cass)*kcaon);
    GlobalData_t h2 = ((sv->Na_i*hna)/(kna3*h1));
    GlobalData_t h21 = ((sv->nass*hna)/(kna3*h111));
    GlobalData_t h3 = (1.0/h1);
    GlobalData_t h31 = (1.0/h111);
    GlobalData_t h8 = (nao/((kna3*hna)*h7));
    GlobalData_t h81 = (nao/((kna3*hna)*h71));
    GlobalData_t h9 = (1.0/h7);
    GlobalData_t h91 = (1.0/h71);
    GlobalData_t kito2 = (1.-((1./(1.+(square((Jrel/0.4)))))));
    GlobalData_t x12 = (((((a4*a1)*a2)+((b2*b4)*b3))+((a2*b4)*b3))+((b3*a1)*a2));
    GlobalData_t x22 = (((((b2*b1)*b4)+((a1*a2)*a3))+((a3*b1)*b4))+((a2*a3)*b4));
    GlobalData_t x32 = (((((a2*a3)*a4)+((b3*b2)*b1))+((b2*b1)*a4))+((a3*a4)*b1));
    GlobalData_t x42 = (((((b4*b3)*b2)+((a3*a4)*a1))+((b2*a4)*a1))+((b3*b2)*a1));
    GlobalData_t E12 = (x12/(((x12+x22)+x32)+x42));
    GlobalData_t E22 = (x22/(((x12+x22)+x32)+x42));
    GlobalData_t E32 = (x32/(((x12+x22)+x32)+x42));
    GlobalData_t E42 = (x42/(((x12+x22)+x32)+x42));
    GlobalData_t ITo = ((((p->Gto*sv->aa)*rito2)*kito2)*(v-(ECl)));
    GlobalData_t k3p = (h9*wca);
    GlobalData_t k3p1 = (h91*wca);
    GlobalData_t k3pp = (h8*wnaca);
    GlobalData_t k3pp1 = (h81*wnaca);
    GlobalData_t k4p = ((h3*wca)/hca);
    GlobalData_t k4p1 = ((h31*wca)/hca);
    GlobalData_t k4pp = (h2*wnaca);
    GlobalData_t k4pp1 = (h21*wnaca);
    GlobalData_t k7 = ((h5*h2)*wna);
    GlobalData_t k71 = ((h51*h21)*wna);
    GlobalData_t k8 = ((h8*h11)*wna);
    GlobalData_t k81 = ((h81*h1111)*wna);
    GlobalData_t JnakK = (2.0*((E42*b1)-((E32*a1))));
    GlobalData_t JnakNa = (3.0*((E12*a3)-((E22*b3))));
    GlobalData_t k3 = (k3p+k3pp);
    GlobalData_t k31 = (k3p1+k3pp1);
    GlobalData_t k4 = (k4p+k4pp);
    GlobalData_t k41 = (k4p1+k4pp1);
    GlobalData_t INaK = (Pnak*((zna*JnakNa)+(zk*JnakK)));
    GlobalData_t x1 = (((k2*k4)*(k7+k6))+((k5*k7)*(k2+k3)));
    GlobalData_t x11 = (((k21*k41)*(k71+k61))+((k51*k71)*(k21+k31)));
    GlobalData_t x2 = (((k1*k7)*(k4+k5))+((k4*k6)*(k1+k8)));
    GlobalData_t x21 = (((k11*k71)*(k41+k51))+((k41*k61)*(k11+k81)));
    GlobalData_t x3 = (((k1*k3)*(k7+k6))+((k8*k6)*(k2+k3)));
    GlobalData_t x31 = (((k11*k31)*(k71+k61))+((k81*k61)*(k21+k31)));
    GlobalData_t x4 = (((k2*k8)*(k4+k5))+((k3*k5)*(k1+k8)));
    GlobalData_t x41 = (((k21*k81)*(k41+k51))+((k31*k51)*(k11+k81)));
    GlobalData_t E1 = (x1/(((x1+x2)+x3)+x4));
    GlobalData_t E11 = (x11/(((x11+x21)+x31)+x41));
    GlobalData_t E2 = (x2/(((x1+x2)+x3)+x4));
    GlobalData_t E21 = (x21/(((x11+x21)+x31)+x41));
    GlobalData_t E3 = (x3/(((x1+x2)+x3)+x4));
    GlobalData_t E31 = (x31/(((x11+x21)+x31)+x41));
    GlobalData_t E4 = (x4/(((x1+x2)+x3)+x4));
    GlobalData_t E41 = (x41/(((x11+x21)+x31)+x41));
    GlobalData_t JncxCa = ((E2*k2)-((E1*k1)));
    GlobalData_t JncxCa1 = ((E21*k21)-((E11*k11)));
    GlobalData_t JncxNa = (((3.0*((E4*k7)-((E1*k8))))+(E3*k4pp))-((E2*k3pp)));
    GlobalData_t JncxNa1 = (((3.0*((E41*k71)-((E11*k81))))+(E31*k4pp1))-((E21*k3pp1)));
    GlobalData_t INaCa_i = (((0.8*p->Gncx)*allo)*((zna*JncxNa)+(zca*JncxCa)));
    GlobalData_t INaCa_ss = (((0.2*p->Gncx)*allo1)*((zna*JncxNa1)+(zca*JncxCa1)));
    Iion = (((((((((((((((INa+INaL)+ICaL)+ICaNa)+ICaK)+IKr)+IKs)+IK1)+ITo)+INaCa_i)+INaCa_ss)+INaK)+INab)+IKb)+IpCa)+ICab);
    
    
    //Complete Forward Euler Update
    GlobalData_t Bcacsr = (1.0/(1.0+((csqnmax*kmcsqn)/(square((kmcsqn+sv->cacsr))))));
    GlobalData_t Bcai = (1.0/((1.0+((cmdnmax*kmcmdn)/(square((kmcmdn+sv->cai)))))+((trpnmax*kmtrpn)/(square((kmtrpn+sv->cai))))));
    GlobalData_t Bcai2 = (1.0/((1.0+((cmdnmax*kmcmdn)/(square((kmcmdn+sv->cai2)))))+((trpnmax*kmtrpn)/(square((kmtrpn+sv->cai2))))));
    GlobalData_t Bcajsr = (1.0/(1.0+((csqnmax*kmcsqn)/(square((kmcsqn+sv->cajsr))))));
    GlobalData_t Bcass = (1.0/((1.0+((BSRmax*KmBSR)/(square((KmBSR+sv->cass)))))+((BSLmax*KmBSL)/(square((KmBSL+sv->cass))))));
    GlobalData_t CaMKb = ((CaMKo*(1.-(sv->CaMKt)))/(1.+(KmCaM/sv->cass)));
    GlobalData_t Jdiff = ((sv->cass-(sv->cai))/0.2);
    GlobalData_t JdiffK = ((sv->kss-(sv->ki))/2.0);
    GlobalData_t JdiffNa = ((sv->nass-(sv->Na_i))/2.0);
    GlobalData_t Jgap = ((sv->cai-(sv->cai2))/p->tau_gap);
    GlobalData_t Jleak = ((0.005*sv->cansr)/15.0);
    GlobalData_t Jtr = ((sv->cansr-(sv->cajsr))/100.0);
    GlobalData_t Jtr2 = ((sv->cansr-(sv->cacsr))/100.0);
    GlobalData_t Jupnp = ((0.005*sv->cai)/(sv->cai+0.001));
    GlobalData_t Jupnp2 = ((0.005*sv->cai2)/(sv->cai2+0.001));
    GlobalData_t Jupp = (((3.*0.005)*sv->cai)/((sv->cai+0.001)-(0.0002)));
    GlobalData_t Jupp2 = (((3.*0.005)*sv->cai2)/((sv->cai2+0.001)-(0.0002)));
    GlobalData_t CaMKa = (CaMKb+sv->CaMKt);
    GlobalData_t diff_CaMKt = (((aCaMK*CaMKb)*(CaMKb+sv->CaMKt))-((bCaMK*sv->CaMKt)));
    GlobalData_t diff_Na_i = ((((-((((INa+INaL)+(3.0*INaCa_i))+(3.0*INaK))+INab))*Acap)/((2.*F)*vmyo))+((JdiffNa*vss)/vmyo));
    GlobalData_t diff_cacsr = (Bcacsr*(Jtr2-(sv->Jrel2)));
    GlobalData_t diff_cajsr = (Bcajsr*(Jtr-(Jrel)));
    GlobalData_t diff_cass = (Bcass*(((((-(ICaL-((2.0*INaCa_ss))))*Acap)/(((2.*2.0)*F)*vss))+((Jrel*vjsr)/vss))-(Jdiff)));
    GlobalData_t diff_ki = ((((-((((IKr+IKs)+IK1)+IKb)-((2.0*INaK))))*Acap)/((2.*F)*vmyo))+((JdiffK*vss)/vmyo));
    GlobalData_t diff_kss = ((((-ICaK)*Acap)/((2.*F)*vss))-(JdiffK));
    GlobalData_t diff_nass = ((((-(ICaNa+(3.0*INaCa_ss)))*Acap)/((2.*F)*vss))-(JdiffNa));
    GlobalData_t fJupp = (1.0/(1.0+(KmCaMK/CaMKa)));
    GlobalData_t Jup = ((((1.0-(fJupp))*Jupnp)+(fJupp*Jupp))-(Jleak));
    GlobalData_t Jup2 = ((((1.0-(fJupp))*Jupnp2)+(fJupp*Jupp2))-(Jleak));
    GlobalData_t diff_cai = (Bcai*((((((-((IpCa+ICab)-((2.0*INaCa_i))))*Acap)/(((2.*2.0)*F)*vmyo1))-(((Jup*vnsr1)/vmyo1)))+((Jdiff*vss)/vmyo1))-(Jgap)));
    GlobalData_t diff_cai2 = (Bcai2*(((sv->Jrel2*(vcsr/vmyo2))+((Jgap*vmyo2)/vmyo1))-(((Jup2*vnsr2)/vmyo2))));
    GlobalData_t diff_cansr = ((((Jup*(vnsr1/vnsr))+(Jup2*(vnsr2/vnsr)))-(((Jtr*vjsr)/vnsr)))-(((Jtr2*vcsr)/vnsr)));
    GlobalData_t CaMKt_new = sv->CaMKt+diff_CaMKt*dt;
    GlobalData_t Na_i_new = sv->Na_i+diff_Na_i*dt;
    GlobalData_t cacsr_new = sv->cacsr+diff_cacsr*dt;
    GlobalData_t cai_new = sv->cai+diff_cai*dt;
    GlobalData_t cai2_new = sv->cai2+diff_cai2*dt;
    GlobalData_t cajsr_new = sv->cajsr+diff_cajsr*dt;
    GlobalData_t cansr_new = sv->cansr+diff_cansr*dt;
    GlobalData_t cass_new = sv->cass+diff_cass*dt;
    GlobalData_t ki_new = sv->ki+diff_ki*dt;
    GlobalData_t kss_new = sv->kss+diff_kss*dt;
    GlobalData_t nass_new = sv->nass+diff_nass*dt;
    
    
    //Complete Rush Larsen Update
    GlobalData_t CaMK_f = (1./(1.+(KmCaMK/CaMKa)));
    GlobalData_t Rel1 = (((((-ICaL)+(2.*INaCa_ss))*(Acap/((2.*vss)*F)))+(Jrel*(vjsr/vss)))-(Jdiff));
    GlobalData_t Rel2 = ((Jgap+(sv->Jrel2*(vcsr/vmyo2)))-((Jup2*(vnsr2/vmyo2))));
    GlobalData_t aa_h = ((v>=-40.) ? 0. : (0.057*(exp(((-(v+80.))/6.8)))));
    GlobalData_t aa_j = ((v>=-40.) ? 0. : ((((-2.5428e4*(exp((0.2444*v))))-((6.948e-6*(exp((-0.04391*v))))))*(v+37.78))/(1.+(exp((0.311*(v+79.23)))))));
    GlobalData_t aa_m = (1./(1.+(exp(((-60.-(v))/5.)))));
    GlobalData_t alpha_aa = (0.025/(1.+(exp(((v+58.)/5.)))));
    GlobalData_t aml = (0.32*((v==-47.13) ? 10. : ((-(v+47.13))/(expm1((-0.1*(v+47.13)))))));
    GlobalData_t bb_h = ((v>=-40.) ? (0.77/(0.13*(1.+(exp(((-(v+10.66))/11.1)))))) : ((2.7*(exp((0.079*v))))+(3.1e5*(exp((0.3485*v))))));
    GlobalData_t bb_j = ((v>=-40.) ? ((0.6*(exp((0.057*v))))/(1.+(exp((-0.1*(v+32.)))))) : ((0.02424*(exp((-0.01052*v))))/(1.+(exp((-0.1378*(v+40.14)))))));
    GlobalData_t bb_m = ((0.1/(1.+(exp(((v+35.)/5.)))))+(0.10/(1.+(exp(((v-(50.))/200.))))));
    GlobalData_t beta_aa = (1./(5.*(1.+(exp(((v+19.)/-9.))))));
    GlobalData_t bml = (0.08*(exp(((-v)/11.0))));
    GlobalData_t d_inf = (1.0/(1.0+(exp(((-(v-(p->vhalf_d)))/6.2)))));
    GlobalData_t f_inf = ((1.0/(1.0+(exp(((v-(vhalff))/4.9)))))+(0.35/(1.+(exp(((45.-(v))/20.))))));
    GlobalData_t fca_inf = (((0.3/(1.-(((ICaL>0.) ? 0. : (ICaL/0.05)))))+(0.55/(1.+(sv->cass/0.003))))+0.15);
    GlobalData_t h_inf = (1./((1.+(exp(((v+71.55)/7.43))))*(1.+(exp(((v+71.55)/7.43))))));
    GlobalData_t hl_inf = (1./(1.+(exp(((v+91.)/6.1)))));
    GlobalData_t m_inf = (1./((1.+(exp(((-56.86-(v))/9.03))))*(1.+(exp(((-56.86-(v))/9.03))))));
    GlobalData_t tau_d = (0.6+(1.0/((exp((-0.05*(v+6.0))))+(exp((0.09*(v+14.0)))))));
    GlobalData_t tau_ff = (7.0+(1.0/((0.0045*(exp(((-(v+20.0))/10.0))))+(0.0045*(exp(((v+20.0)/10.0)))))));
    GlobalData_t tau_fs = (70.0+(1.0/((0.000035*(exp(((-(v+5.0))/4.0))))+(0.000035*(exp(((v+5.0)/6.0)))))));
    GlobalData_t tau_xr = (12.98+(1.0/((0.3652*(exp(((v-(31.66))/3.869))))+(4.123e-5*(exp(((-(v-(47.78)))/20.38)))))));
    GlobalData_t tau_xs1 = (300.+(1.0/((1e-6*(exp(((v+50.)/20.))))+(0.04*(exp(((-(v+50.0))/20.0)))))));
    GlobalData_t tau_xs2 = (1.0/((0.01*(exp(((v-(50.0))/20.0))))+(0.0193*(exp(((-(v+66.54))/31.0))))));
    GlobalData_t trel1factor = ((20.*(1.+(1./(1.+(pow((KmCaMK/CaMKa),8.))))))/(1.+(pow((0.5/sv->cajsr),8.))));
    GlobalData_t trel2factor = ((50.*(1.+(1./(1.+(pow((KmCaMK/CaMKa),8.))))))/(1.+(pow((0.5/sv->cacsr),8.))));
    GlobalData_t xr_inf = (1.0/(1.0+(exp(((-(v+56.8))/17.8)))));
    GlobalData_t xs1_inf = (1.0/(1.0+(exp(((-(v-(25.1)))/37.1)))));
    GlobalData_t Jrel1_inf = ((Rel1>0.) ? (((60.*Rel1)*(1.+(1./(1.+(pow((KmCaMK/CaMKa),8.))))))/(1.+(pow((0.75/sv->cajsr),8.)))) : 0.);
    GlobalData_t Jrel2_inf = ((Rel2>0.) ? (((250.*Rel2)*(1.+(1./(1.+(pow((KmCaMK/CaMKa),8.))))))/(1.+(pow((0.75/sv->cacsr),8.)))) : 0.);
    GlobalData_t aa_rush_larsen_A = (((-alpha_aa)/(alpha_aa+beta_aa))*(expm1(((-dt)*(alpha_aa+beta_aa)))));
    GlobalData_t aa_rush_larsen_B = (exp(((-dt)*(alpha_aa+beta_aa))));
    GlobalData_t d_rush_larsen_B = (exp(((-dt)/tau_d)));
    GlobalData_t d_rush_larsen_C = (expm1(((-dt)/tau_d)));
    GlobalData_t ff_inf = f_inf;
    GlobalData_t ff_rush_larsen_B = (exp(((-dt)/tau_ff)));
    GlobalData_t ff_rush_larsen_C = (expm1(((-dt)/tau_ff)));
    GlobalData_t fs_inf = f_inf;
    GlobalData_t fs_rush_larsen_B = (exp(((-dt)/tau_fs)));
    GlobalData_t fs_rush_larsen_C = (expm1(((-dt)/tau_fs)));
    GlobalData_t hl_rush_larsen_A = ((-hl_inf)*hl_rush_larsen_C);
    GlobalData_t j_inf = h_inf;
    GlobalData_t ml_inf = (aml/(aml+bml));
    GlobalData_t tau_Jrel1 = (max(0.001,trel1factor));
    GlobalData_t tau_Jrel2 = (max(0.001,trel2factor));
    GlobalData_t tau_fca = (((10.*CaMK_f)+0.5)+(1./(1.+(sv->cass/0.003))));
    GlobalData_t tau_h = (1.0/(aa_h+bb_h));
    GlobalData_t tau_j = (1.0/(aa_j+bb_j));
    GlobalData_t tau_m = (aa_m*bb_m);
    GlobalData_t tau_ml = (1./(aml+bml));
    GlobalData_t xr_rush_larsen_B = (exp(((-dt)/tau_xr)));
    GlobalData_t xr_rush_larsen_C = (expm1(((-dt)/tau_xr)));
    GlobalData_t xs1_rush_larsen_B = (exp(((-dt)/tau_xs1)));
    GlobalData_t xs1_rush_larsen_C = (expm1(((-dt)/tau_xs1)));
    GlobalData_t xs2_inf = xs1_inf;
    GlobalData_t xs2_rush_larsen_B = (exp(((-dt)/tau_xs2)));
    GlobalData_t xs2_rush_larsen_C = (expm1(((-dt)/tau_xs2)));
    GlobalData_t Jrel1_rush_larsen_B = (exp(((-dt)/tau_Jrel1)));
    GlobalData_t Jrel1_rush_larsen_C = (expm1(((-dt)/tau_Jrel1)));
    GlobalData_t Jrel2_rush_larsen_B = (exp(((-dt)/tau_Jrel2)));
    GlobalData_t Jrel2_rush_larsen_C = (expm1(((-dt)/tau_Jrel2)));
    GlobalData_t d_rush_larsen_A = ((-d_inf)*d_rush_larsen_C);
    GlobalData_t fca_rush_larsen_B = (exp(((-dt)/tau_fca)));
    GlobalData_t fca_rush_larsen_C = (expm1(((-dt)/tau_fca)));
    GlobalData_t ff_rush_larsen_A = ((-ff_inf)*ff_rush_larsen_C);
    GlobalData_t fs_rush_larsen_A = ((-fs_inf)*fs_rush_larsen_C);
    GlobalData_t h_rush_larsen_B = (exp(((-dt)/tau_h)));
    GlobalData_t h_rush_larsen_C = (expm1(((-dt)/tau_h)));
    GlobalData_t j_rush_larsen_B = (exp(((-dt)/tau_j)));
    GlobalData_t j_rush_larsen_C = (expm1(((-dt)/tau_j)));
    GlobalData_t m_rush_larsen_B = (exp(((-dt)/tau_m)));
    GlobalData_t m_rush_larsen_C = (expm1(((-dt)/tau_m)));
    GlobalData_t ml_rush_larsen_B = (exp(((-dt)/tau_ml)));
    GlobalData_t ml_rush_larsen_C = (expm1(((-dt)/tau_ml)));
    GlobalData_t xr_rush_larsen_A = ((-xr_inf)*xr_rush_larsen_C);
    GlobalData_t xs1_rush_larsen_A = ((-xs1_inf)*xs1_rush_larsen_C);
    GlobalData_t xs2_rush_larsen_A = ((-xs2_inf)*xs2_rush_larsen_C);
    GlobalData_t Jrel1_rush_larsen_A = ((-Jrel1_inf)*Jrel1_rush_larsen_C);
    GlobalData_t Jrel2_rush_larsen_A = ((-Jrel2_inf)*Jrel2_rush_larsen_C);
    GlobalData_t fca_rush_larsen_A = ((-fca_inf)*fca_rush_larsen_C);
    GlobalData_t h_rush_larsen_A = ((-h_inf)*h_rush_larsen_C);
    GlobalData_t j_rush_larsen_A = ((-j_inf)*j_rush_larsen_C);
    GlobalData_t m_rush_larsen_A = ((-m_inf)*m_rush_larsen_C);
    GlobalData_t ml_rush_larsen_A = ((-ml_inf)*ml_rush_larsen_C);
    GlobalData_t Jrel1_new = Jrel1_rush_larsen_A+Jrel1_rush_larsen_B*sv->Jrel1;
    GlobalData_t Jrel2_new = Jrel2_rush_larsen_A+Jrel2_rush_larsen_B*sv->Jrel2;
    GlobalData_t aa_new = aa_rush_larsen_A+aa_rush_larsen_B*sv->aa;
    GlobalData_t d_new = d_rush_larsen_A+d_rush_larsen_B*sv->d;
    GlobalData_t fca_new = fca_rush_larsen_A+fca_rush_larsen_B*sv->fca;
    GlobalData_t ff_new = ff_rush_larsen_A+ff_rush_larsen_B*sv->ff;
    GlobalData_t fs_new = fs_rush_larsen_A+fs_rush_larsen_B*sv->fs;
    GlobalData_t h_new = h_rush_larsen_A+h_rush_larsen_B*sv->h;
    GlobalData_t hl_new = hl_rush_larsen_A+hl_rush_larsen_B*sv->hl;
    GlobalData_t j_new = j_rush_larsen_A+j_rush_larsen_B*sv->j;
    GlobalData_t m_new = m_rush_larsen_A+m_rush_larsen_B*sv->m;
    GlobalData_t ml_new = ml_rush_larsen_A+ml_rush_larsen_B*sv->ml;
    GlobalData_t xr_new = xr_rush_larsen_A+xr_rush_larsen_B*sv->xr;
    GlobalData_t xs1_new = xs1_rush_larsen_A+xs1_rush_larsen_B*sv->xs1;
    GlobalData_t xs2_new = xs2_rush_larsen_A+xs2_rush_larsen_B*sv->xs2;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->CaMKt = CaMKt_new;
    Iion = Iion;
    sv->Jrel1 = Jrel1_new;
    sv->Jrel2 = Jrel2_new;
    sv->Na_i = Na_i_new;
    sv->aa = aa_new;
    sv->cacsr = cacsr_new;
    sv->cai = cai_new;
    sv->cai2 = cai2_new;
    sv->cajsr = cajsr_new;
    sv->cansr = cansr_new;
    sv->cass = cass_new;
    sv->d = d_new;
    sv->fca = fca_new;
    sv->ff = ff_new;
    sv->fs = fs_new;
    sv->h = h_new;
    sv->hl = hl_new;
    sv->j = j_new;
    sv->ki = ki_new;
    sv->kss = kss_new;
    sv->m = m_new;
    sv->ml = ml_new;
    sv->nass = nass_new;
    sv->tjsrol = tjsrol_new;
    sv->xr = xr_new;
    sv->xs1 = xs1_new;
    sv->xs2 = xs2_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    v_ext[__i] = v;

  }

            }
}
#endif // GAUR_CPU_GENERATED

bool GaurIonType::has_trace() const {
    return true;
}

void GaurIonType::trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** impdata) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("Gaur_trace_header.txt","wt");
    fprintf(theader->fd,
        "CaMKa\n"
        "ICaK\n"
        "ICaL\n"
        "ICaNa\n"
        "ICab\n"
        "IK1\n"
        "IKb\n"
        "IKr\n"
        "IKs\n"
        "INa\n"
        "INaCa\n"
        "INaCa_i\n"
        "INaCa_ss\n"
        "INaK\n"
        "INaL\n"
        "INab\n"
        "ITo\n"
        "IpCa\n"
        "Jrel\n"
        "sv->Jrel2\n"
        "Jup\n"
        "Jup2\n"
        "sv->Na_i\n"
        "sv->cacsr\n"
        "sv->cai\n"
        "sv->cai2\n"
        "cai_avg\n"
        "sv->cajsr\n"
        "sv->cansr\n"
        "sv->cass\n"
        "sv->ki\n"
        "sv->kss\n"
        "sv->nass\n"
        "v\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom *region = &imp.cgeom();
  Gaur_Params *p  = imp.params();

  Gaur_state *sv_base = (Gaur_state *)imp.sv_tab().data();

  Gaur_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = imp.get_tstp().cnt * dt;
  IonIfBase* IF = &imp;
  // Define the constants that depend on the parameters.
  GlobalData_t hl_rush_larsen_B = (exp(((-dt)/tau_hl)));
  GlobalData_t hl_rush_larsen_C = (expm1(((-dt)/tau_hl)));
  GlobalData_t vmyo1 = (vmyo*p->vmyo1frac);
  GlobalData_t vmyo2 = (vmyo-(vmyo1));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *v_ext = impdata[Vm];
  //Prepare all the private functions.
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t v = v_ext[__i];
  //Change the units of external variables as appropriate.
  
  
  GlobalData_t CaMKb = ((CaMKo*(1.-(sv->CaMKt)))/(1.+(KmCaM/sv->cass)));
  GlobalData_t EK = (rtf*(log((ko/sv->ki))));
  GlobalData_t EKs = (rtf*(log(((ko+(PKNa*nao))/(sv->ki+(PKNa*sv->Na_i))))));
  GlobalData_t ENa = (rtf*(log((nao/sv->Na_i))));
  GlobalData_t IpCa = ((GpCa*sv->cai)/(0.0005+sv->cai));
  GlobalData_t Jleak = ((0.005*sv->cansr)/15.0);
  GlobalData_t Jupnp = ((0.005*sv->cai)/(sv->cai+0.001));
  GlobalData_t Jupnp2 = ((0.005*sv->cai2)/(sv->cai2+0.001));
  GlobalData_t Jupp = (((3.*0.005)*sv->cai)/((sv->cai+0.001)-(0.0002)));
  GlobalData_t Jupp2 = (((3.*0.005)*sv->cai2)/((sv->cai2+0.001)-(0.0002)));
  GlobalData_t KsCa = (1.0+(0.6/(1.0+(pow((3.8e-5/sv->cai),1.4)))));
  GlobalData_t P = (eP/(((1.0+(H/Khp))+(sv->Na_i/Knap))+(sv->ki/Kxkur)));
  GlobalData_t PCaK = (3.574e-4*sv->PCa);
  GlobalData_t PCaNa = (0.00125*sv->PCa);
  GlobalData_t allo = (1.0/(1.0+(square((KmCaAct/sv->cai)))));
  GlobalData_t allo1 = (1.0/(1.0+(square((KmCaAct/sv->cass)))));
  GlobalData_t cai_avg = ((p->vmyo1frac*sv->cai)+((1.-(p->vmyo1frac))*sv->cai2));
  GlobalData_t f = (sv->ff*sv->fs);
  GlobalData_t greljsrol = ((grelbarjsrol*(1.-((exp(((-sv->tjsrol)/tauon))))))*(exp(((-sv->tjsrol)/tauoff))));
  GlobalData_t h4 = (1.0+((sv->Na_i/kna1)*(1.+(sv->Na_i/kna2))));
  GlobalData_t h41 = (1.0+((sv->nass/kna1)*(1.+(sv->nass/kna2))));
  GlobalData_t rito2 = (1./(1.+(exp(((-(v+10.))/5.)))));
  GlobalData_t rk1 = (1.0/(1.0+(exp((((v+79.3)-((2.6*ko)))/19.6)))));
  GlobalData_t rkr = (1.0/(1.0+(exp(((v+22.0)/15.0)))));
  GlobalData_t vfrt = ((v*F)/(R*T));
  GlobalData_t xkb = (1.0/(1.0+(exp(((-(v-(14.48)))/18.34)))));
  GlobalData_t CaMKa = (CaMKb+sv->CaMKt);
  GlobalData_t ICab = ((((PCab*2.)*F)*((sv->cai*(exp((2.0*vfrt))))-((0.341*cao))))*((v==0.) ? 1. : ((2.*vfrt)/(expm1((2.0*vfrt))))));
  GlobalData_t IK1 = (((p->GK1*(sqrt((ko/5.4))))*rk1)*(v-(EK)));
  GlobalData_t IKb = ((p->GKb*xkb)*(v-(EK)));
  GlobalData_t IKr = ((((sv->GKr*(sqrt((ko/5.4))))*sv->xr)*rkr)*(v-(EK)));
  GlobalData_t IKs = ((((sv->GKs*KsCa)*sv->xs1)*sv->xs2)*(v-(EKs)));
  GlobalData_t INa = ((((((p->GNa*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(v-(ENa)));
  GlobalData_t INaL = (((((p->GNaL*sv->ml)*sv->ml)*sv->ml)*sv->hl)*(v-(ENa)));
  GlobalData_t INab = (((p->PNab*F)*((sv->Na_i*(exp(vfrt)))-(nao)))*((v==0.) ? 1. : (vfrt/(expm1(vfrt)))));
  GlobalData_t Jrelol = (greljsrol*(sv->cajsr-(sv->cass)));
  GlobalData_t Knai = (Knai0*(exp(((delta*vfrt)/3.0))));
  GlobalData_t Knao = (Knao0*(exp((((1.0-(delta))*vfrt)/3.0))));
  GlobalData_t PhiCaK = ((F*(((0.75*sv->kss)*(exp(vfrt)))-((0.75*ko))))*((v==0.) ? 1. : (vfrt/(expm1(vfrt)))));
  GlobalData_t PhiCaL = (((2.0*F)*((sv->cass*(exp((2.0*vfrt))))-((0.341*cao))))*((v==0.) ? 1. : ((2.*vfrt)/(expm1((2.0*vfrt))))));
  GlobalData_t PhiCaNa = ((F*(((0.75*sv->nass)*(exp(vfrt)))-((0.75*nao))))*((v==0.) ? 1. : (vfrt/(expm1(vfrt)))));
  GlobalData_t b3 = (((k3m*P)*H)/(1.0+(MgATP/Kmgatp)));
  GlobalData_t h5 = ((sv->Na_i*sv->Na_i)/((h4*kna1)*kna2));
  GlobalData_t h51 = ((sv->nass*sv->nass)/((h41*kna1)*kna2));
  GlobalData_t h6 = (1.0/h4);
  GlobalData_t h61 = (1.0/h41);
  GlobalData_t hca = (exp((qca*vfrt)));
  GlobalData_t hna = (exp((qna*vfrt)));
  GlobalData_t ICaK = ((((PCaK*PhiCaK)*sv->d)*f)*sv->fca);
  GlobalData_t ICaL = ((((sv->PCa*PhiCaL)*sv->d)*f)*sv->fca);
  GlobalData_t ICaNa = ((((PCaNa*PhiCaNa)*sv->d)*f)*sv->fca);
  GlobalData_t Jrel = (sv->Jrel1+Jrelol);
  GlobalData_t a1 = ((k1p*(cube((sv->Na_i/Knai))))/(((cube((1.0+(sv->Na_i/Knai))))+(square((1.0+(sv->ki/Kki)))))-(1.0)));
  GlobalData_t a3 = ((k3p2*(square((ko/Kko))))/(((cube((1.0+(nao/Knao))))+(square((1.0+(ko/Kko)))))-(1.0)));
  GlobalData_t b2 = ((k2m*(cube((nao/Knao))))/(((cube((1.0+(nao/Knao))))+(square((1.0+(ko/Kko)))))-(1.0)));
  GlobalData_t b4 = ((k4m*(square((sv->ki/Kki))))/(((cube((1.0+(sv->Na_i/Knai))))+(square((1.0+(sv->ki/Kki)))))-(1.0)));
  GlobalData_t fJupp = (1.0/(1.0+(KmCaMK/CaMKa)));
  GlobalData_t h1 = (1.+((sv->Na_i/kna3)*(1.+hna)));
  GlobalData_t h111 = (1.+((sv->nass/kna3)*(1.+hna)));
  GlobalData_t h7 = (1.0+((nao/kna3)*(1.0+(1.0/hna))));
  GlobalData_t h71 = (1.0+((nao/kna3)*(1.0+(1.0/hna))));
  GlobalData_t k6 = ((h6*sv->cai)*kcaon);
  GlobalData_t k61 = ((h61*sv->cass)*kcaon);
  GlobalData_t Jup = ((((1.0-(fJupp))*Jupnp)+(fJupp*Jupp))-(Jleak));
  GlobalData_t Jup2 = ((((1.0-(fJupp))*Jupnp2)+(fJupp*Jupp2))-(Jleak));
  GlobalData_t h2 = ((sv->Na_i*hna)/(kna3*h1));
  GlobalData_t h21 = ((sv->nass*hna)/(kna3*h111));
  GlobalData_t h3 = (1.0/h1);
  GlobalData_t h31 = (1.0/h111);
  GlobalData_t h8 = (nao/((kna3*hna)*h7));
  GlobalData_t h81 = (nao/((kna3*hna)*h71));
  GlobalData_t h9 = (1.0/h7);
  GlobalData_t h91 = (1.0/h71);
  GlobalData_t kito2 = (1.-((1./(1.+(square((Jrel/0.4)))))));
  GlobalData_t x12 = (((((a4*a1)*a2)+((b2*b4)*b3))+((a2*b4)*b3))+((b3*a1)*a2));
  GlobalData_t x22 = (((((b2*b1)*b4)+((a1*a2)*a3))+((a3*b1)*b4))+((a2*a3)*b4));
  GlobalData_t x32 = (((((a2*a3)*a4)+((b3*b2)*b1))+((b2*b1)*a4))+((a3*a4)*b1));
  GlobalData_t x42 = (((((b4*b3)*b2)+((a3*a4)*a1))+((b2*a4)*a1))+((b3*b2)*a1));
  GlobalData_t E12 = (x12/(((x12+x22)+x32)+x42));
  GlobalData_t E22 = (x22/(((x12+x22)+x32)+x42));
  GlobalData_t E32 = (x32/(((x12+x22)+x32)+x42));
  GlobalData_t E42 = (x42/(((x12+x22)+x32)+x42));
  GlobalData_t ITo = ((((p->Gto*sv->aa)*rito2)*kito2)*(v-(ECl)));
  GlobalData_t k3p = (h9*wca);
  GlobalData_t k3p1 = (h91*wca);
  GlobalData_t k3pp = (h8*wnaca);
  GlobalData_t k3pp1 = (h81*wnaca);
  GlobalData_t k4p = ((h3*wca)/hca);
  GlobalData_t k4p1 = ((h31*wca)/hca);
  GlobalData_t k4pp = (h2*wnaca);
  GlobalData_t k4pp1 = (h21*wnaca);
  GlobalData_t k7 = ((h5*h2)*wna);
  GlobalData_t k71 = ((h51*h21)*wna);
  GlobalData_t k8 = ((h8*h11)*wna);
  GlobalData_t k81 = ((h81*h1111)*wna);
  GlobalData_t JnakK = (2.0*((E42*b1)-((E32*a1))));
  GlobalData_t JnakNa = (3.0*((E12*a3)-((E22*b3))));
  GlobalData_t k3 = (k3p+k3pp);
  GlobalData_t k31 = (k3p1+k3pp1);
  GlobalData_t k4 = (k4p+k4pp);
  GlobalData_t k41 = (k4p1+k4pp1);
  GlobalData_t INaK = (Pnak*((zna*JnakNa)+(zk*JnakK)));
  GlobalData_t x1 = (((k2*k4)*(k7+k6))+((k5*k7)*(k2+k3)));
  GlobalData_t x11 = (((k21*k41)*(k71+k61))+((k51*k71)*(k21+k31)));
  GlobalData_t x2 = (((k1*k7)*(k4+k5))+((k4*k6)*(k1+k8)));
  GlobalData_t x21 = (((k11*k71)*(k41+k51))+((k41*k61)*(k11+k81)));
  GlobalData_t x3 = (((k1*k3)*(k7+k6))+((k8*k6)*(k2+k3)));
  GlobalData_t x31 = (((k11*k31)*(k71+k61))+((k81*k61)*(k21+k31)));
  GlobalData_t x4 = (((k2*k8)*(k4+k5))+((k3*k5)*(k1+k8)));
  GlobalData_t x41 = (((k21*k81)*(k41+k51))+((k31*k51)*(k11+k81)));
  GlobalData_t E1 = (x1/(((x1+x2)+x3)+x4));
  GlobalData_t E11 = (x11/(((x11+x21)+x31)+x41));
  GlobalData_t E2 = (x2/(((x1+x2)+x3)+x4));
  GlobalData_t E21 = (x21/(((x11+x21)+x31)+x41));
  GlobalData_t E3 = (x3/(((x1+x2)+x3)+x4));
  GlobalData_t E31 = (x31/(((x11+x21)+x31)+x41));
  GlobalData_t E4 = (x4/(((x1+x2)+x3)+x4));
  GlobalData_t E41 = (x41/(((x11+x21)+x31)+x41));
  GlobalData_t JncxCa = ((E2*k2)-((E1*k1)));
  GlobalData_t JncxCa1 = ((E21*k21)-((E11*k11)));
  GlobalData_t JncxNa = (((3.0*((E4*k7)-((E1*k8))))+(E3*k4pp))-((E2*k3pp)));
  GlobalData_t JncxNa1 = (((3.0*((E41*k71)-((E11*k81))))+(E31*k4pp1))-((E21*k3pp1)));
  GlobalData_t INaCa_i = (((0.8*p->Gncx)*allo)*((zna*JncxNa)+(zca*JncxCa)));
  GlobalData_t INaCa_ss = (((0.2*p->Gncx)*allo1)*((zna*JncxNa1)+(zca*JncxCa1)));
  GlobalData_t INaCa = (INaCa_i+INaCa_ss);
  //Output the desired variables
  fprintf(file, "%4.12f\t", CaMKa);
  fprintf(file, "%4.12f\t", ICaK);
  fprintf(file, "%4.12f\t", ICaL);
  fprintf(file, "%4.12f\t", ICaNa);
  fprintf(file, "%4.12f\t", ICab);
  fprintf(file, "%4.12f\t", IK1);
  fprintf(file, "%4.12f\t", IKb);
  fprintf(file, "%4.12f\t", IKr);
  fprintf(file, "%4.12f\t", IKs);
  fprintf(file, "%4.12f\t", INa);
  fprintf(file, "%4.12f\t", INaCa);
  fprintf(file, "%4.12f\t", INaCa_i);
  fprintf(file, "%4.12f\t", INaCa_ss);
  fprintf(file, "%4.12f\t", INaK);
  fprintf(file, "%4.12f\t", INaL);
  fprintf(file, "%4.12f\t", INab);
  fprintf(file, "%4.12f\t", ITo);
  fprintf(file, "%4.12f\t", IpCa);
  fprintf(file, "%4.12f\t", Jrel);
  fprintf(file, "%4.12f\t", sv->Jrel2);
  fprintf(file, "%4.12f\t", Jup);
  fprintf(file, "%4.12f\t", Jup2);
  fprintf(file, "%4.12f\t", sv->Na_i);
  fprintf(file, "%4.12f\t", sv->cacsr);
  fprintf(file, "%4.12f\t", sv->cai);
  fprintf(file, "%4.12f\t", sv->cai2);
  fprintf(file, "%4.12f\t", cai_avg);
  fprintf(file, "%4.12f\t", sv->cajsr);
  fprintf(file, "%4.12f\t", sv->cansr);
  fprintf(file, "%4.12f\t", sv->cass);
  fprintf(file, "%4.12f\t", sv->ki);
  fprintf(file, "%4.12f\t", sv->kss);
  fprintf(file, "%4.12f\t", sv->nass);
  fprintf(file, "%4.12f\t", v);
  //Change the units of external variables as appropriate.
  
  

}
IonIfBase* GaurIonType::make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const {
        // Place the allocated IonIf in managed memory if a GPU target exists for this model
        // otherwise, place it in main RAM
    IonIfDerived* ptr;
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_ROCM, 1, true);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CUDA, 1, true);
    }
    else {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CPU, 1, true);
    }
    // Using placement new to place the object in the correct memory
    return new(ptr) IonIfDerived(*this, this->select_target(target),
    num_node, plugins);
}

void GaurIonType::destroy_ion_if(IonIfBase *imp) const {
    // Call destructor and deallocate manually because the object might
    // be located on GPU (delete won't work in this case)
    imp->~IonIfBase();
    IonIfDerived* ptr = static_cast<IonIfDerived *>(imp);
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_ROCM, ptr);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CUDA, ptr);
    }
    else {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CPU, ptr);
    }
}

}  // namespace limpet
        