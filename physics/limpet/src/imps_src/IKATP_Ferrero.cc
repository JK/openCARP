// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Ferrero (Jr), J.M., Saiz, J., Ferrero-Corral, J.M., Thakor, N.V.
*  Year: 1996
*  Title: Simulation of Action Potentials from Metabolically Impaired Cardiac Myocytes. Role of ATP-Sensitive Potassium Current
*  Journal: Circulation Research, 79: 208-221
*  DOI: 10.1161/01.RES.79.2.208
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "IKATP_Ferrero.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

IKATP_FerreroIonType::IKATP_FerreroIonType(bool plugin) : IonType(std::move(std::string("IKATP_Ferrero")), plugin) {}

size_t IKATP_FerreroIonType::params_size() const {
  return sizeof(struct IKATP_Ferrero_Params);
}

size_t IKATP_FerreroIonType::dlo_vector_size() const {

  return 1;
}

uint32_t IKATP_FerreroIonType::reqdat() const {
  return IKATP_Ferrero_REQDAT;
}

uint32_t IKATP_FerreroIonType::moddat() const {
  return IKATP_Ferrero_MODDAT;
}

void IKATP_FerreroIonType::destroy(IonIfBase& imp_base) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  imp.destroy_luts();
  // rarely need to do anything else
}

Target IKATP_FerreroIonType::select_target(Target target) const {
  switch (target) {
    case Target::AUTO:
#   ifdef IKATP_FERRERO_MLIR_CUDA_GENERATED
      return Target::MLIR_CUDA;
#   elif defined(IKATP_FERRERO_MLIR_ROCM_GENERATED)
      return Target::MLIR_ROCM;
#   elif defined(IKATP_FERRERO_MLIR_CPU_GENERATED)
      return Target::MLIR_CPU;
#   elif defined(IKATP_FERRERO_CPU_GENERATED)
      return Target::CPU;
#   else
      return Target::UNKNOWN;
#   endif
#   ifdef IKATP_FERRERO_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      return Target::MLIR_CUDA;
#   endif
#   ifdef IKATP_FERRERO_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      return Target::MLIR_ROCM;
#   endif
#   ifdef IKATP_FERRERO_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      return Target::MLIR_CPU;
#   endif
#   ifdef IKATP_FERRERO_CPU_GENERATED
    case Target::CPU:
      return Target::CPU;
#   endif
    default:
      return Target::UNKNOWN;
  }
}

void IKATP_FerreroIonType::compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  switch(target) {
    case Target::AUTO:
#   ifdef IKATP_FERRERO_MLIR_CUDA_GENERATED
      compute_IKATP_Ferrero_mlir_gpu_cuda(start, end, imp, data);
#   elif defined(IKATP_FERRERO_MLIR_ROCM_GENERATED)
      compute_IKATP_Ferrero_mlir_gpu_rocm(start, end, imp, data);
#   elif defined(IKATP_FERRERO_MLIR_CPU_GENERATED)
      compute_IKATP_Ferrero_mlir_cpu(start, end, imp, data);
#   elif defined(IKATP_FERRERO_CPU_GENERATED)
      compute_IKATP_Ferrero_cpu(start, end, imp, data);
#   else
#     error "Could not generate method IKATP_FerreroIonType::compute."
#   endif
      break;
#   ifdef IKATP_FERRERO_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      compute_IKATP_Ferrero_mlir_gpu_cuda(start, end, imp, data);
      break;
#   endif
#   ifdef IKATP_FERRERO_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      compute_IKATP_Ferrero_mlir_gpu_rocm(start, end, imp, data);
      break;
#   endif
#   ifdef IKATP_FERRERO_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      compute_IKATP_Ferrero_mlir_cpu(start, end, imp, data);
      break;
#   endif
#   ifdef IKATP_FERRERO_CPU_GENERATED
    case Target::CPU:
      compute_IKATP_Ferrero_cpu(start, end, imp, data);
      break;
#   endif
    default:
      throw std::runtime_error(std::string("Could not compute with the given target ") + get_string_from_target(target) + ".");
      break;
  }
}

// Define all constants
#define BETA (GlobalData_t)((2.0/0.0007))
#define F (GlobalData_t)(96485.)
#define Ke_init (GlobalData_t)(5.4)
#define Ki_init (GlobalData_t)(138.4)
#define Mgi (GlobalData_t)(3.1)
#define Nai_init (GlobalData_t)(10.0)
#define Q10 (GlobalData_t)(1.3)
#define T0 (GlobalData_t)(36.)
#define alpha (GlobalData_t)(1.0)
#define beta (GlobalData_t)(1.0)
#define delta_Mg (GlobalData_t)(0.32)
#define delta_Na (GlobalData_t)(0.35)
#define po (GlobalData_t)(0.91)
#define sigma (GlobalData_t)(3.8)
#define RTF (GlobalData_t)(((8.3143*(273.+T0))/96.4867))
#define T (GlobalData_t)(T0)
#define fT (GlobalData_t)((pow(Q10,((T-(T0))/10.))))



void IKATP_FerreroIonType::initialize_params(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  cell_geom* region = &imp.cgeom();
  IKATP_Ferrero_Params *p = imp.params();

  // Compute the regional constants
  {
    p->ADPi_init = 15.0;
    p->ATPi_init = 6.8;
    p->GAMMA0 = 0.8;
    p->gamma = 35.375;
  }
  // Compute the regional initialization
  {
  }

}


// Define the parameters for the lookup tables
enum Tables {
  ADP_TAB,
  Ke_TAB,

  N_TABS
};

// Define the indices into the lookup tables.

    enum Rosenbrock {
    

      N_ROSEN
    };
enum ADP_TableIndex {
  NROWS_ADP
};

enum Ke_TableIndex {
  gamma_0_idx,
  NROWS_Ke
};



void IKATP_FerreroIonType::construct_tables(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom* region = &imp.cgeom();
  IKATP_Ferrero_Params *p = imp.params();

  imp.tables().resize(N_TABS);

  // Define the constants that depend on the parameters.
  
  // Create the Ke lookup table
  LUT* Ke_tab = &imp.tables()[Ke_TAB];
  LUT_alloc(Ke_tab, NROWS_Ke, 0., 16., 0.001, "IKATP_Ferrero Ke", imp.get_target());
  for (int __i=Ke_tab->mn_ind; __i<=Ke_tab->mx_ind; __i++) {
    double Ke = Ke_tab->res*__i;
    LUT_data_t* Ke_row = Ke_tab->tab[__i];
    Ke_row[gamma_0_idx] = (p->gamma*(pow((Ke/5.4),0.24)));
  }
  check_LUT(Ke_tab);
  

}



void IKATP_FerreroIonType::initialize_sv(IonIfBase& imp_base, GlobalData_t **impdata ) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom *region = &imp.cgeom();
  IKATP_Ferrero_Params *p = imp.params();

  IKATP_Ferrero_state *sv_base = (IKATP_Ferrero_state *)imp.sv_tab().data();
  GlobalData_t t = 0;

  IonIfDerived* IF = &imp;
  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *Ke_ext = impdata[Ke];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.
  
  int __ADPi_sizeof;
  int __ADPi_offset;
  SVgetfcn __ADPi_SVgetfcn = imp.parent()->get_type().get_sv_offset( "ADPi", &__ADPi_offset, &__ADPi_sizeof );
  SVputfcn __ADPi_SVputfcn = __ADPi_SVgetfcn ? getPutSV(__ADPi_SVgetfcn) : NULL;
  
  int __ATPi_sizeof;
  int __ATPi_offset;
  SVgetfcn __ATPi_SVgetfcn = imp.parent()->get_type().get_sv_offset( "ATPi", &__ATPi_offset, &__ATPi_sizeof );
  SVputfcn __ATPi_SVputfcn = __ATPi_SVgetfcn ? getPutSV(__ATPi_SVgetfcn) : NULL;
  
  int __Ki_sizeof;
  int __Ki_offset;
  SVgetfcn __Ki_SVgetfcn = imp.parent()->get_type().get_sv_offset( "Ki", &__Ki_offset, &__Ki_sizeof );
  SVputfcn __Ki_SVputfcn = __Ki_SVgetfcn ? getPutSV(__Ki_SVgetfcn) : NULL;
  
  int __Nai_sizeof;
  int __Nai_offset;
  SVgetfcn __Nai_SVgetfcn = imp.parent()->get_type().get_sv_offset( "Nai", &__Nai_offset, &__Nai_sizeof );
  SVputfcn __Nai_SVputfcn = __Nai_SVgetfcn ? getPutSV(__Nai_SVgetfcn) : NULL;

  //set the initial values
  for(int __i=0; __i < imp.get_num_node(); __i+=1 ){
    IKATP_Ferrero_state *sv = sv_base+__i / 1;
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t Ke = Ke_ext[__i];
    GlobalData_t V = V_ext[__i];
    GlobalData_t ADPi = __ADPi_SVgetfcn ? __ADPi_SVgetfcn(*imp.parent(), __i, __ADPi_offset) :sv->__ADPi_local;
    GlobalData_t ATPi = __ATPi_SVgetfcn ? __ATPi_SVgetfcn(*imp.parent(), __i, __ATPi_offset) :sv->__ATPi_local;
    GlobalData_t Ki = __Ki_SVgetfcn ? __Ki_SVgetfcn(*imp.parent(), __i, __Ki_offset) :sv->__Ki_local;
    GlobalData_t Nai = __Nai_SVgetfcn ? __Nai_SVgetfcn(*imp.parent(), __i, __Nai_offset) :sv->__Nai_local;
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    ADPi = p->ADPi_init;
    ATPi = p->ATPi_init;
    Ke = Ke_init;
    Ki = Ki_init;
    Nai = Nai_init;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    Ke_ext[__i] = Ke;
    V_ext[__i] = V;
    if( __ADPi_SVputfcn ) 
    	__ADPi_SVputfcn(*imp.parent(), __i, __ADPi_offset, ADPi);
    else
    	sv->__ADPi_local=ADPi;
    if( __ATPi_SVputfcn ) 
    	__ATPi_SVputfcn(*imp.parent(), __i, __ATPi_offset, ATPi);
    else
    	sv->__ATPi_local=ATPi;
    if( __Ki_SVputfcn ) 
    	__Ki_SVputfcn(*imp.parent(), __i, __Ki_offset, Ki);
    else
    	sv->__Ki_local=Ki;
    if( __Nai_SVputfcn ) 
    	__Nai_SVputfcn(*imp.parent(), __i, __Nai_offset, Nai);
    else
    	sv->__Nai_local=Nai;
  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
#ifdef IKATP_FERRERO_CPU_GENERATED
extern "C" {
void compute_IKATP_Ferrero_cpu(int start, int end, IonIfBase& imp_base, GlobalData_t **impdata )
{
  IKATP_FerreroIonType::IonIfDerived& imp = static_cast<IKATP_FerreroIonType::IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt()*1e0;
  cell_geom *region = &imp.cgeom();
  IKATP_Ferrero_Params *p  = imp.params();
  IKATP_Ferrero_state *sv_base = (IKATP_Ferrero_state *)imp.sv_tab().data();

  GlobalData_t t = imp.get_tstp().cnt*dt;

  IKATP_FerreroIonType::IonIfDerived* IF = &imp;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *Ke_ext = impdata[Ke];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.
  
  int __ADPi_sizeof;
  int __ADPi_offset;
  SVgetfcn __ADPi_SVgetfcn = imp.parent()->get_type().get_sv_offset( "ADPi", &__ADPi_offset, &__ADPi_sizeof );
  SVputfcn __ADPi_SVputfcn = __ADPi_SVgetfcn ? getPutSV(__ADPi_SVgetfcn) : NULL;
  
  // Calculates addresses for the MLIR code generator
  int __ADPi_getfcn_exists = __ADPi_SVgetfcn ? 1 : 0;
  int __ADPi_putfcn_exists = __ADPi_SVputfcn ? 1 : 0;
  
          char* __parent_table_address = (char*)imp.parent()->get_sv_address();
          int __parent_table_size = imp.parent()->get_sv_size() / 1;
      
  
  int __ATPi_sizeof;
  int __ATPi_offset;
  SVgetfcn __ATPi_SVgetfcn = imp.parent()->get_type().get_sv_offset( "ATPi", &__ATPi_offset, &__ATPi_sizeof );
  SVputfcn __ATPi_SVputfcn = __ATPi_SVgetfcn ? getPutSV(__ATPi_SVgetfcn) : NULL;
  
  // Calculates addresses for the MLIR code generator
  int __ATPi_getfcn_exists = __ATPi_SVgetfcn ? 1 : 0;
  int __ATPi_putfcn_exists = __ATPi_SVputfcn ? 1 : 0;
  
  int __Ki_sizeof;
  int __Ki_offset;
  SVgetfcn __Ki_SVgetfcn = imp.parent()->get_type().get_sv_offset( "Ki", &__Ki_offset, &__Ki_sizeof );
  SVputfcn __Ki_SVputfcn = __Ki_SVgetfcn ? getPutSV(__Ki_SVgetfcn) : NULL;
  
  // Calculates addresses for the MLIR code generator
  int __Ki_getfcn_exists = __Ki_SVgetfcn ? 1 : 0;
  int __Ki_putfcn_exists = __Ki_SVputfcn ? 1 : 0;
  
  int __Nai_sizeof;
  int __Nai_offset;
  SVgetfcn __Nai_SVgetfcn = imp.parent()->get_type().get_sv_offset( "Nai", &__Nai_offset, &__Nai_sizeof );
  SVputfcn __Nai_SVputfcn = __Nai_SVgetfcn ? getPutSV(__Nai_SVgetfcn) : NULL;
  
  // Calculates addresses for the MLIR code generator
  int __Nai_getfcn_exists = __Nai_SVgetfcn ? 1 : 0;
  int __Nai_putfcn_exists = __Nai_SVputfcn ? 1 : 0;

#pragma omp parallel for schedule(static)
  for (int __i=(start / 1) * 1; __i<end; __i+=1) {
    IKATP_Ferrero_state *sv = sv_base+__i / 1;
                    
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t Ke = Ke_ext[__i];
    GlobalData_t V = V_ext[__i];
    GlobalData_t ADPi = __ADPi_SVgetfcn ? __ADPi_SVgetfcn(*imp.parent(), __i, __ADPi_offset) :sv->__ADPi_local;
    GlobalData_t ATPi = __ATPi_SVgetfcn ? __ATPi_SVgetfcn(*imp.parent(), __i, __ATPi_offset) :sv->__ATPi_local;
    GlobalData_t Ki = __Ki_SVgetfcn ? __Ki_SVgetfcn(*imp.parent(), __i, __Ki_offset) :sv->__Ki_local;
    GlobalData_t Nai = __Nai_SVgetfcn ? __Nai_SVgetfcn(*imp.parent(), __i, __Nai_offset) :sv->__Nai_local;
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    LUT_data_t Ke_row[NROWS_Ke];
    LUT_interpRow(&IF->tables()[Ke_TAB], Ke, __i, Ke_row);
    
    
    //Compute storevars and external modvars
    GlobalData_t Ek = (RTF*(log((Ke/Ki))));
    GlobalData_t H = (1.3+((0.74*beta)*(exp((-0.09*ADPi)))));
    GlobalData_t KhMg = ((0.65/(sqrt((Ke+5.))))*(exp((((-2.*delta_Mg)/RTF)*V))));
    GlobalData_t KhNa = (25.9*(exp((((-delta_Na)/RTF)*V))));
    GlobalData_t Km = (alpha*(35.8+(17.9*(pow(ADPi,0.256)))));
    GlobalData_t fATP = (1./(1.+(pow((ATPi/(Km*0.001)),H))));
    GlobalData_t fM = (1./(1.+(Mgi/KhMg)));
    GlobalData_t fN = (1./(1.+((Nai/KhNa)*(Nai/KhNa))));
    GlobalData_t IKATP = (((((((sigma*po)*Ke_row[gamma_0_idx])*fM)*fN)*fT)*fATP)*(V-(Ek)));
    Iion = (Iion+IKATP);
    Ke = (Ke+(((((IKATP*BETA)/F)*1.e-9)*p->GAMMA0)/(1.-(p->GAMMA0))));
    Ki = (Ki-((((IKATP*BETA)/F)*1.e-9)));
    
    
    //Complete Forward Euler Update
    
    
    //Complete Rush Larsen Update
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    Iion = Iion;
    Ke = Ke;
    Ki = Ki;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    Ke_ext[__i] = Ke;
    V_ext[__i] = V;
    if( __ADPi_SVputfcn ) 
    	__ADPi_SVputfcn(*imp.parent(), __i, __ADPi_offset, ADPi);
    else
    	sv->__ADPi_local=ADPi;
    if( __ATPi_SVputfcn ) 
    	__ATPi_SVputfcn(*imp.parent(), __i, __ATPi_offset, ATPi);
    else
    	sv->__ATPi_local=ATPi;
    if( __Ki_SVputfcn ) 
    	__Ki_SVputfcn(*imp.parent(), __i, __Ki_offset, Ki);
    else
    	sv->__Ki_local=Ki;
    if( __Nai_SVputfcn ) 
    	__Nai_SVputfcn(*imp.parent(), __i, __Nai_offset, Nai);
    else
    	sv->__Nai_local=Nai;

  }

            }
}
#endif // IKATP_FERRERO_CPU_GENERATED

bool IKATP_FerreroIonType::has_trace() const {
    return true;
}

void IKATP_FerreroIonType::trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** impdata) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("IKATP_Ferrero_trace_header.txt","wt");
    fprintf(theader->fd,
        "fATP\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom *region = &imp.cgeom();
  IKATP_Ferrero_Params *p  = imp.params();

  IKATP_Ferrero_state *sv_base = (IKATP_Ferrero_state *)imp.sv_tab().data();

  IKATP_Ferrero_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = imp.get_tstp().cnt * dt;
  IonIfBase* IF = &imp;
  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *Ke_ext = impdata[Ke];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.
  
  int __ADPi_sizeof;
  int __ADPi_offset;
  SVgetfcn __ADPi_SVgetfcn = imp.parent()->get_type().get_sv_offset( "ADPi", &__ADPi_offset, &__ADPi_sizeof );
  SVputfcn __ADPi_SVputfcn = __ADPi_SVgetfcn ? getPutSV(__ADPi_SVgetfcn) : NULL;
  
  int __ATPi_sizeof;
  int __ATPi_offset;
  SVgetfcn __ATPi_SVgetfcn = imp.parent()->get_type().get_sv_offset( "ATPi", &__ATPi_offset, &__ATPi_sizeof );
  SVputfcn __ATPi_SVputfcn = __ATPi_SVgetfcn ? getPutSV(__ATPi_SVgetfcn) : NULL;
  
  int __Ki_sizeof;
  int __Ki_offset;
  SVgetfcn __Ki_SVgetfcn = imp.parent()->get_type().get_sv_offset( "Ki", &__Ki_offset, &__Ki_sizeof );
  SVputfcn __Ki_SVputfcn = __Ki_SVgetfcn ? getPutSV(__Ki_SVgetfcn) : NULL;
  
  int __Nai_sizeof;
  int __Nai_offset;
  SVgetfcn __Nai_SVgetfcn = imp.parent()->get_type().get_sv_offset( "Nai", &__Nai_offset, &__Nai_sizeof );
  SVputfcn __Nai_SVputfcn = __Nai_SVgetfcn ? getPutSV(__Nai_SVgetfcn) : NULL;
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t Ke = Ke_ext[__i];
  GlobalData_t V = V_ext[__i];
  GlobalData_t ADPi = __ADPi_SVgetfcn ? __ADPi_SVgetfcn(*imp.parent(), __i, __ADPi_offset) :sv->__ADPi_local;
  GlobalData_t ATPi = __ATPi_SVgetfcn ? __ATPi_SVgetfcn(*imp.parent(), __i, __ATPi_offset) :sv->__ATPi_local;
  GlobalData_t Ki = __Ki_SVgetfcn ? __Ki_SVgetfcn(*imp.parent(), __i, __Ki_offset) :sv->__Ki_local;
  GlobalData_t Nai = __Nai_SVgetfcn ? __Nai_SVgetfcn(*imp.parent(), __i, __Nai_offset) :sv->__Nai_local;
  //Change the units of external variables as appropriate.
  
  
  GlobalData_t H = (1.3+((0.74*beta)*(exp((-0.09*ADPi)))));
  GlobalData_t Km = (alpha*(35.8+(17.9*(pow(ADPi,0.256)))));
  GlobalData_t fATP = (1./(1.+(pow((ATPi/(Km*0.001)),H))));
  //Output the desired variables
  fprintf(file, "%4.12f\t", fATP);
  //Change the units of external variables as appropriate.
  
  

}
IonIfBase* IKATP_FerreroIonType::make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const {
        // Place the allocated IonIf in managed memory if a GPU target exists for this model
        // otherwise, place it in main RAM
    IonIfDerived* ptr;
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_ROCM, 1, true);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CUDA, 1, true);
    }
    else {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CPU, 1, true);
    }
    // Using placement new to place the object in the correct memory
    return new(ptr) IonIfDerived(*this, this->select_target(target),
    num_node, plugins);
}

void IKATP_FerreroIonType::destroy_ion_if(IonIfBase *imp) const {
    // Call destructor and deallocate manually because the object might
    // be located on GPU (delete won't work in this case)
    imp->~IonIfBase();
    IonIfDerived* ptr = static_cast<IonIfDerived *>(imp);
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_ROCM, ptr);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CUDA, ptr);
    }
    else {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CPU, ptr);
    }
}

}  // namespace limpet
        