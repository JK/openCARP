// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Ashihara T, Trayanova NA
*  Year: 2004
*  Title: Asymmetry in membrane responses to electric shocks: insights from bidomain simulations
*  Journal: Biophys J., 87(4):2271-82
*  DOI: 10.1529/biophysj.104.043091
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __DEFIB_ASHIHARATRAYANOVA_H__
#define __DEFIB_ASHIHARATRAYANOVA_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"

#if !(defined(DEFIB_ASHIHARATRAYANOVA_CPU_GENERATED)    || defined(DEFIB_ASHIHARATRAYANOVA_MLIR_CPU_GENERATED)    || defined(DEFIB_ASHIHARATRAYANOVA_MLIR_ROCM_GENERATED)    || defined(DEFIB_ASHIHARATRAYANOVA_MLIR_CUDA_GENERATED))
#ifdef MLIR_CPU_GENERATED
#define DEFIB_ASHIHARATRAYANOVA_MLIR_CPU_GENERATED
#endif

#ifdef MLIR_ROCM_GENERATED
#define DEFIB_ASHIHARATRAYANOVA_MLIR_ROCM_GENERATED
#endif

#ifdef MLIR_CUDA_GENERATED
#define DEFIB_ASHIHARATRAYANOVA_MLIR_CUDA_GENERATED
#endif
#endif

#ifdef CPU_GENERATED
#define DEFIB_ASHIHARATRAYANOVA_CPU_GENERATED
#endif

namespace limpet {

#define Defib_AshiharaTrayanova_REQDAT Iion_DATA_FLAG|Vm_DATA_FLAG
#define Defib_AshiharaTrayanova_MODDAT Iion_DATA_FLAG

struct Defib_AshiharaTrayanova_Params {
    GlobalData_t VtakeOff;
    GlobalData_t slopeFac;

};

struct Defib_AshiharaTrayanova_state {
    GlobalData_t Ki;
    GlobalData_t __sl_i2c_local;

};

class Defib_AshiharaTrayanovaIonType : public IonType {
public:
    using IonIfDerived = IonIf<Defib_AshiharaTrayanovaIonType>;
    using params_type = Defib_AshiharaTrayanova_Params;
    using state_type = Defib_AshiharaTrayanova_state;

    Defib_AshiharaTrayanovaIonType(bool plugin);

    size_t params_size() const override;

    size_t dlo_vector_size() const override;

    uint32_t reqdat() const override;

    uint32_t moddat() const override;

    void initialize_params(IonIfBase& imp_base) const override;

    void construct_tables(IonIfBase& imp_base) const override;

    void destroy(IonIfBase& imp_base) const override;

    void initialize_sv(IonIfBase& imp_base, GlobalData_t** data) const override;

    Target select_target(Target target) const override;

    void compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const override;

    bool has_trace() const override;

    void trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const override;

    void tune(IonIfBase& imp_base, const char* im_par) const override;

    int read_svs(IonIfBase& imp_base, FILE* file) const override;

    int write_svs(IonIfBase& imp_base, FILE* file, int node) const override;

    SVgetfcn get_sv_offset(const char* svname, int* off, int* sz) const override;

    int get_sv_list(char*** list) const override;

    int get_sv_type(const char* svname, int* type, char** type_name) const override;

    void print_params() const override;

    void print_metadata() const override;

    IonIfBase* make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const override;

    void destroy_ion_if(IonIfBase *imp) const override;
};

// This needs to be extern C in order to be linked correctly with the MLIR code
extern "C" {

//void compute_Defib_AshiharaTrayanova(int, int, IonIfBase&, GlobalData_t**);
#ifdef DEFIB_ASHIHARATRAYANOVA_CPU_GENERATED
void compute_Defib_AshiharaTrayanova_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef DEFIB_ASHIHARATRAYANOVA_MLIR_CPU_GENERATED
void compute_Defib_AshiharaTrayanova_mlir_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef DEFIB_ASHIHARATRAYANOVA_MLIR_ROCM_GENERATED
void compute_Defib_AshiharaTrayanova_mlir_gpu_rocm(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef DEFIB_ASHIHARATRAYANOVA_MLIR_CUDA_GENERATED
void compute_Defib_AshiharaTrayanova_mlir_gpu_cuda(int, int, IonIfBase&, GlobalData_t**);
#endif

}
}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
