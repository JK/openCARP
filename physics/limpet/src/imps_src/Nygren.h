// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: A. Nygren , C. Fiset , L. Firek, J. W. Clark , D. S. Lindblad , R. B. Clark , and W. R. Giles
*  Year: 1998
*  Title: Mathematical model of an adult human atrial cell: the role of K+ currents in repolarization
*  Journal: Circulation Research,9-23,82(1),63-81
*  DOI: 10.1161/01.res.82.1.63
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __NYGREN_H__
#define __NYGREN_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"

#if !(defined(NYGREN_CPU_GENERATED)    || defined(NYGREN_MLIR_CPU_GENERATED)    || defined(NYGREN_MLIR_ROCM_GENERATED)    || defined(NYGREN_MLIR_CUDA_GENERATED))
#ifdef MLIR_CPU_GENERATED
#define NYGREN_MLIR_CPU_GENERATED
#endif

#ifdef MLIR_ROCM_GENERATED
#define NYGREN_MLIR_ROCM_GENERATED
#endif

#ifdef MLIR_CUDA_GENERATED
#define NYGREN_MLIR_CUDA_GENERATED
#endif
#endif

#ifdef CPU_GENERATED
#define NYGREN_CPU_GENERATED
#endif

namespace limpet {

#define Nygren_REQDAT Vm_DATA_FLAG
#define Nygren_MODDAT Iion_DATA_FLAG

struct Nygren_Params {
    GlobalData_t GCaB;
    GlobalData_t GCaL;
    GlobalData_t GK1;
    GlobalData_t GKS;
    GlobalData_t GKr;
    GlobalData_t GNaB;
    GlobalData_t Gsus;
    GlobalData_t Gt;
    GlobalData_t KNaCa;
    GlobalData_t maxINaK;

};

struct Nygren_state {
    GlobalData_t Ca_d;
    GlobalData_t Ca_rel;
    GlobalData_t Ca_up;
    GlobalData_t Cai;
    GlobalData_t F1;
    GlobalData_t F2;
    Gatetype O_C;
    Gatetype O_Calse;
    Gatetype O_TC;
    GlobalData_t O_TMgC;
    GlobalData_t O_TMgMg;
    Gatetype d_L;
    Gatetype f_L1;
    Gatetype f_L2;
    Gatetype h1;
    Gatetype h2;
    Gatetype m;
    Gatetype n;
    Gatetype p_a;
    Gatetype r;
    Gatetype r_sus;
    Gatetype s;
    Gatetype s_sus;

};

class NygrenIonType : public IonType {
public:
    using IonIfDerived = IonIf<NygrenIonType>;
    using params_type = Nygren_Params;
    using state_type = Nygren_state;

    NygrenIonType(bool plugin);

    size_t params_size() const override;

    size_t dlo_vector_size() const override;

    uint32_t reqdat() const override;

    uint32_t moddat() const override;

    void initialize_params(IonIfBase& imp_base) const override;

    void construct_tables(IonIfBase& imp_base) const override;

    void destroy(IonIfBase& imp_base) const override;

    void initialize_sv(IonIfBase& imp_base, GlobalData_t** data) const override;

    Target select_target(Target target) const override;

    void compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const override;

    bool has_trace() const override;

    void trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const override;

    void tune(IonIfBase& imp_base, const char* im_par) const override;

    int read_svs(IonIfBase& imp_base, FILE* file) const override;

    int write_svs(IonIfBase& imp_base, FILE* file, int node) const override;

    SVgetfcn get_sv_offset(const char* svname, int* off, int* sz) const override;

    int get_sv_list(char*** list) const override;

    int get_sv_type(const char* svname, int* type, char** type_name) const override;

    void print_params() const override;

    void print_metadata() const override;

    IonIfBase* make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const override;

    void destroy_ion_if(IonIfBase *imp) const override;
};

// This needs to be extern C in order to be linked correctly with the MLIR code
extern "C" {

//void compute_Nygren(int, int, IonIfBase&, GlobalData_t**);
#ifdef NYGREN_CPU_GENERATED
void compute_Nygren_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef NYGREN_MLIR_CPU_GENERATED
void compute_Nygren_mlir_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef NYGREN_MLIR_ROCM_GENERATED
void compute_Nygren_mlir_gpu_rocm(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef NYGREN_MLIR_CUDA_GENERATED
void compute_Nygren_mlir_gpu_cuda(int, int, IonIfBase&, GlobalData_t**);
#endif

}
}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
