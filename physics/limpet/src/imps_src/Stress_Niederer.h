// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Steven Niederer, Peter Hunter, Nicholas Smith
*  Year: 2006
*  Title: A Quantitative Analysis of Cardiac Myocyte Relaxation: A Simulation Study
*  Journal: Biophysical Journal 2006;90:1697-1722
*  DOI: 10.1529/biophysj.105.069534
*  Comment: Rat ventricular active stress model (plugin)
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __STRESS_NIEDERER_H__
#define __STRESS_NIEDERER_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"

#if !(defined(STRESS_NIEDERER_CPU_GENERATED)    || defined(STRESS_NIEDERER_MLIR_CPU_GENERATED)    || defined(STRESS_NIEDERER_MLIR_ROCM_GENERATED)    || defined(STRESS_NIEDERER_MLIR_CUDA_GENERATED))
#ifdef MLIR_CPU_GENERATED
#define STRESS_NIEDERER_MLIR_CPU_GENERATED
#endif

#ifdef MLIR_ROCM_GENERATED
#define STRESS_NIEDERER_MLIR_ROCM_GENERATED
#endif

#ifdef MLIR_CUDA_GENERATED
#define STRESS_NIEDERER_MLIR_CUDA_GENERATED
#endif
#endif

#ifdef CPU_GENERATED
#define STRESS_NIEDERER_CPU_GENERATED
#endif

namespace limpet {

#define Stress_Niederer_REQDAT Tension_DATA_FLAG|delLambda_DATA_FLAG|Lambda_DATA_FLAG
#define Stress_Niederer_MODDAT Tension_DATA_FLAG

struct Stress_Niederer_Params {
    GlobalData_t A_1;
    GlobalData_t A_2;
    GlobalData_t A_3;
    GlobalData_t Ca_50ref;
    GlobalData_t Ca_TRPN_Max;
    GlobalData_t K_z;
    GlobalData_t T_ref;
    GlobalData_t a;
    GlobalData_t alpha_0;
    GlobalData_t alpha_1;
    GlobalData_t alpha_2;
    GlobalData_t alpha_3;
    GlobalData_t alpha_r1;
    GlobalData_t alpha_r2;
    GlobalData_t beta_0;
    GlobalData_t beta_1;
    GlobalData_t gamma_trpn;
    GlobalData_t k_Ref_off;
    GlobalData_t k_on;
    GlobalData_t n_Hill;
    GlobalData_t n_Rel;
    GlobalData_t z_p;

};

struct Stress_Niederer_state {
    GlobalData_t Q_1;
    GlobalData_t Q_2;
    GlobalData_t Q_3;
    GlobalData_t TRPN;
    GlobalData_t z;
    GlobalData_t __Cai_local;

};

class Stress_NiedererIonType : public IonType {
public:
    using IonIfDerived = IonIf<Stress_NiedererIonType>;
    using params_type = Stress_Niederer_Params;
    using state_type = Stress_Niederer_state;

    Stress_NiedererIonType(bool plugin);

    size_t params_size() const override;

    size_t dlo_vector_size() const override;

    uint32_t reqdat() const override;

    uint32_t moddat() const override;

    void initialize_params(IonIfBase& imp_base) const override;

    void construct_tables(IonIfBase& imp_base) const override;

    void destroy(IonIfBase& imp_base) const override;

    void initialize_sv(IonIfBase& imp_base, GlobalData_t** data) const override;

    Target select_target(Target target) const override;

    void compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const override;

    bool has_trace() const override;

    void trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const override;

    void tune(IonIfBase& imp_base, const char* im_par) const override;

    int read_svs(IonIfBase& imp_base, FILE* file) const override;

    int write_svs(IonIfBase& imp_base, FILE* file, int node) const override;

    SVgetfcn get_sv_offset(const char* svname, int* off, int* sz) const override;

    int get_sv_list(char*** list) const override;

    int get_sv_type(const char* svname, int* type, char** type_name) const override;

    void print_params() const override;

    void print_metadata() const override;

    IonIfBase* make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const override;

    void destroy_ion_if(IonIfBase *imp) const override;
};

// This needs to be extern C in order to be linked correctly with the MLIR code
extern "C" {

//void compute_Stress_Niederer(int, int, IonIfBase&, GlobalData_t**);
#ifdef STRESS_NIEDERER_CPU_GENERATED
void compute_Stress_Niederer_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef STRESS_NIEDERER_MLIR_CPU_GENERATED
void compute_Stress_Niederer_mlir_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef STRESS_NIEDERER_MLIR_ROCM_GENERATED
void compute_Stress_Niederer_mlir_gpu_rocm(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef STRESS_NIEDERER_MLIR_CUDA_GENERATED
void compute_Stress_Niederer_mlir_gpu_cuda(int, int, IonIfBase&, GlobalData_t**);
#endif

}
}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
