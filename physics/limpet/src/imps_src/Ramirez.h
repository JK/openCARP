// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Rafael J. Ramirez, Stanley Nattel, and Marc Courtemanche
*  Year: 2000
*  Title: Mathematical analysis of canine atrial action potentials: rate, regional factors, and electrical remodeling
*  Journal: American Journal of Physiology-Heart and Circulatory Physiology, 279(4), 1767-1785
*  DOI: 10.1152/ajpheart.2000.279.4.H1767
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __RAMIREZ_H__
#define __RAMIREZ_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"

#if !(defined(RAMIREZ_CPU_GENERATED)    || defined(RAMIREZ_MLIR_CPU_GENERATED)    || defined(RAMIREZ_MLIR_ROCM_GENERATED)    || defined(RAMIREZ_MLIR_CUDA_GENERATED))
#ifdef MLIR_CPU_GENERATED
#define RAMIREZ_MLIR_CPU_GENERATED
#endif

#ifdef MLIR_ROCM_GENERATED
#define RAMIREZ_MLIR_ROCM_GENERATED
#endif

#ifdef MLIR_CUDA_GENERATED
#define RAMIREZ_MLIR_CUDA_GENERATED
#endif
#endif

#ifdef CPU_GENERATED
#define RAMIREZ_CPU_GENERATED
#endif

namespace limpet {

#define Ramirez_REQDAT Vm_DATA_FLAG
#define Ramirez_MODDAT Iion_DATA_FLAG

struct Ramirez_Params {
    GlobalData_t Cao;
    GlobalData_t Clo;
    GlobalData_t GCaL;
    GlobalData_t GClCa;
    GlobalData_t GK1;
    GlobalData_t GKr;
    GlobalData_t GKs;
    GlobalData_t GNa;
    GlobalData_t GbCa;
    GlobalData_t GbNa;
    GlobalData_t Gto;
    GlobalData_t Ko;
    GlobalData_t Nao;
    GlobalData_t maxCa_up;
    GlobalData_t maxINaCa;
    GlobalData_t maxINaK;
    GlobalData_t maxIpCa;
    GlobalData_t maxIup;
    GlobalData_t tau_tr;
    GlobalData_t tau_u;

};

struct Ramirez_state {
    GlobalData_t CaCmdn;
    GlobalData_t CaCsqn;
    GlobalData_t CaRel;
    GlobalData_t CaTrpn;
    GlobalData_t CaUp;
    GlobalData_t Cai;
    GlobalData_t Cli;
    GlobalData_t Ki;
    GlobalData_t Nai;
    Gatetype Oa;
    Gatetype Oi;
    Gatetype QCa;
    Gatetype Ua;
    Gatetype Ui;
    Gatetype d;
    Gatetype f;
    Gatetype fCa;
    Gatetype h;
    Gatetype j;
    Gatetype m;
    Gatetype u;
    Gatetype v;
    Gatetype w;
    Gatetype xr;
    Gatetype xs;

};

class RamirezIonType : public IonType {
public:
    using IonIfDerived = IonIf<RamirezIonType>;
    using params_type = Ramirez_Params;
    using state_type = Ramirez_state;

    RamirezIonType(bool plugin);

    size_t params_size() const override;

    size_t dlo_vector_size() const override;

    uint32_t reqdat() const override;

    uint32_t moddat() const override;

    void initialize_params(IonIfBase& imp_base) const override;

    void construct_tables(IonIfBase& imp_base) const override;

    void destroy(IonIfBase& imp_base) const override;

    void initialize_sv(IonIfBase& imp_base, GlobalData_t** data) const override;

    Target select_target(Target target) const override;

    void compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const override;

    bool has_trace() const override;

    void trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** data) const override;

    void tune(IonIfBase& imp_base, const char* im_par) const override;

    int read_svs(IonIfBase& imp_base, FILE* file) const override;

    int write_svs(IonIfBase& imp_base, FILE* file, int node) const override;

    SVgetfcn get_sv_offset(const char* svname, int* off, int* sz) const override;

    int get_sv_list(char*** list) const override;

    int get_sv_type(const char* svname, int* type, char** type_name) const override;

    void print_params() const override;

    void print_metadata() const override;

    IonIfBase* make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const override;

    void destroy_ion_if(IonIfBase *imp) const override;
};

// This needs to be extern C in order to be linked correctly with the MLIR code
extern "C" {

//void compute_Ramirez(int, int, IonIfBase&, GlobalData_t**);
#ifdef RAMIREZ_CPU_GENERATED
void compute_Ramirez_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef RAMIREZ_MLIR_CPU_GENERATED
void compute_Ramirez_mlir_cpu(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef RAMIREZ_MLIR_ROCM_GENERATED
void compute_Ramirez_mlir_gpu_rocm(int, int, IonIfBase&, GlobalData_t**);
#endif
#ifdef RAMIREZ_MLIR_CUDA_GENERATED
void compute_Ramirez_mlir_gpu_cuda(int, int, IonIfBase&, GlobalData_t**);
#endif

}
}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
