// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Joost Lumens, Tammo Delhaas, Borut Kirn, Theo Arts
*  Year: 2009
*  Title: Three-wall segment (TriSeg) model describing mechanics and hemodynamics of ventricular interaction
*  Journal: Ann Biomed Eng. 2009;37:2234-55
*  DOI: 10.1007/s10439-009-9774-2
*  Comment: Plugin
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "Stress_Lumens.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

Stress_LumensIonType::Stress_LumensIonType(bool plugin) : IonType(std::move(std::string("Stress_Lumens")), plugin) {}

size_t Stress_LumensIonType::params_size() const {
  return sizeof(struct Stress_Lumens_Params);
}

size_t Stress_LumensIonType::dlo_vector_size() const {

  return 1;
}

uint32_t Stress_LumensIonType::reqdat() const {
  return Stress_Lumens_REQDAT;
}

uint32_t Stress_LumensIonType::moddat() const {
  return Stress_Lumens_MODDAT;
}

void Stress_LumensIonType::destroy(IonIfBase& imp_base) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  imp.destroy_luts();
  // rarely need to do anything else
}

Target Stress_LumensIonType::select_target(Target target) const {
  switch (target) {
    case Target::AUTO:
#   ifdef STRESS_LUMENS_MLIR_CUDA_GENERATED
      return Target::MLIR_CUDA;
#   elif defined(STRESS_LUMENS_MLIR_ROCM_GENERATED)
      return Target::MLIR_ROCM;
#   elif defined(STRESS_LUMENS_MLIR_CPU_GENERATED)
      return Target::MLIR_CPU;
#   elif defined(STRESS_LUMENS_CPU_GENERATED)
      return Target::CPU;
#   else
      return Target::UNKNOWN;
#   endif
#   ifdef STRESS_LUMENS_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      return Target::MLIR_CUDA;
#   endif
#   ifdef STRESS_LUMENS_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      return Target::MLIR_ROCM;
#   endif
#   ifdef STRESS_LUMENS_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      return Target::MLIR_CPU;
#   endif
#   ifdef STRESS_LUMENS_CPU_GENERATED
    case Target::CPU:
      return Target::CPU;
#   endif
    default:
      return Target::UNKNOWN;
  }
}

void Stress_LumensIonType::compute(Target target, int start, int end, IonIfBase& imp_base, GlobalData_t** data) const {
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  switch(target) {
    case Target::AUTO:
#   ifdef STRESS_LUMENS_MLIR_CUDA_GENERATED
      compute_Stress_Lumens_mlir_gpu_cuda(start, end, imp, data);
#   elif defined(STRESS_LUMENS_MLIR_ROCM_GENERATED)
      compute_Stress_Lumens_mlir_gpu_rocm(start, end, imp, data);
#   elif defined(STRESS_LUMENS_MLIR_CPU_GENERATED)
      compute_Stress_Lumens_mlir_cpu(start, end, imp, data);
#   elif defined(STRESS_LUMENS_CPU_GENERATED)
      compute_Stress_Lumens_cpu(start, end, imp, data);
#   else
#     error "Could not generate method Stress_LumensIonType::compute."
#   endif
      break;
#   ifdef STRESS_LUMENS_MLIR_CUDA_GENERATED
    case Target::MLIR_CUDA:
      compute_Stress_Lumens_mlir_gpu_cuda(start, end, imp, data);
      break;
#   endif
#   ifdef STRESS_LUMENS_MLIR_ROCM_GENERATED
    case Target::MLIR_ROCM:
      compute_Stress_Lumens_mlir_gpu_rocm(start, end, imp, data);
      break;
#   endif
#   ifdef STRESS_LUMENS_MLIR_CPU_GENERATED
    case Target::MLIR_CPU:
      compute_Stress_Lumens_mlir_cpu(start, end, imp, data);
      break;
#   endif
#   ifdef STRESS_LUMENS_CPU_GENERATED
    case Target::CPU:
      compute_Stress_Lumens_cpu(start, end, imp, data);
      break;
#   endif
    default:
      throw std::runtime_error(std::string("Could not compute with the given target ") + get_string_from_target(target) + ".");
      break;
  }
}

// Define all constants
#define C_init (GlobalData_t)(0.0)
#define Crest (GlobalData_t)(0.02)
#define Lsc0 (GlobalData_t)(1.51)
#define Lseiso (GlobalData_t)(0.04)
#define Lsref (GlobalData_t)(2.0)
#define Sact (GlobalData_t)(120.)
#define Spas (GlobalData_t)(7.)
#define V_init (GlobalData_t)(-84.)
#define delta_sl_init (GlobalData_t)(0.)
#define length_init (GlobalData_t)(1.)
#define tact_init (GlobalData_t)(-1.)
#define tauD (GlobalData_t)(32.)
#define tauR (GlobalData_t)(48.)
#define tauSC (GlobalData_t)(425.)
#define tcur_init (GlobalData_t)(0.)
#define vmax (GlobalData_t)(7.)
#define Lsc_init (GlobalData_t)(Lsc0)
#define Vmp_init (GlobalData_t)(V_init)



void Stress_LumensIonType::initialize_params(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  cell_geom* region = &imp.cgeom();
  Stress_Lumens_Params *p = imp.params();

  // Compute the regional constants
  {
    p->VmThresh = -50.;
  }
  // Compute the regional initialization
  {
  }

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.

    enum Rosenbrock {
    

      N_ROSEN
    };


void Stress_LumensIonType::construct_tables(IonIfBase& imp_base) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom* region = &imp.cgeom();
  Stress_Lumens_Params *p = imp.params();

  imp.tables().resize(N_TABS);

  // Define the constants that depend on the parameters.

}



void Stress_LumensIonType::initialize_sv(IonIfBase& imp_base, GlobalData_t **impdata ) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom *region = &imp.cgeom();
  Stress_Lumens_Params *p = imp.params();

  Stress_Lumens_state *sv_base = (Stress_Lumens_state *)imp.sv_tab().data();
  GlobalData_t t = 0;

  IonIfDerived* IF = &imp;
  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Tension_ext = impdata[Tension];
  GlobalData_t *V_ext = impdata[Vm];
  GlobalData_t *delta_sl_ext = impdata[delLambda];
  GlobalData_t *length_ext = impdata[Lambda];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i < imp.get_num_node(); __i+=1 ){
    Stress_Lumens_state *sv = sv_base+__i / 1;
    //Initialize the external vars to their current values
    GlobalData_t Tension = Tension_ext[__i];
    GlobalData_t V = V_ext[__i];
    GlobalData_t delta_sl = delta_sl_ext[__i];
    GlobalData_t length = length_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    sv->C = C_init;
    sv->Lsc = Lsc_init;
    V = V_init;
    sv->Vmp = Vmp_init;
    delta_sl = delta_sl_init;
    length = length_init;
    sv->tact = tact_init;
    sv->tcur = tcur_init;
    double epsf = length;
    double Ls = (Lsref*epsf);
    Tension = ((((Sact*sv->C)*(sv->Lsc-(Lsc0)))*(Ls-(sv->Lsc)))/Lseiso);
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Tension_ext[__i] = Tension;
    V_ext[__i] = V;
    delta_sl_ext[__i] = delta_sl;
    length_ext[__i] = length;
  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
#ifdef STRESS_LUMENS_CPU_GENERATED
extern "C" {
void compute_Stress_Lumens_cpu(int start, int end, IonIfBase& imp_base, GlobalData_t **impdata )
{
  Stress_LumensIonType::IonIfDerived& imp = static_cast<Stress_LumensIonType::IonIfDerived&>(imp_base);
  GlobalData_t dt = imp.get_dt()*1e0;
  cell_geom *region = &imp.cgeom();
  Stress_Lumens_Params *p  = imp.params();
  Stress_Lumens_state *sv_base = (Stress_Lumens_state *)imp.sv_tab().data();

  GlobalData_t t = imp.get_tstp().cnt*dt;

  Stress_LumensIonType::IonIfDerived* IF = &imp;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Tension_ext = impdata[Tension];
  GlobalData_t *V_ext = impdata[Vm];
  GlobalData_t *delta_sl_ext = impdata[delLambda];
  GlobalData_t *length_ext = impdata[Lambda];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=(start / 1) * 1; __i<end; __i+=1) {
    Stress_Lumens_state *sv = sv_base+__i / 1;
                    
    //Initialize the external vars to their current values
    GlobalData_t Tension = Tension_ext[__i];
    GlobalData_t V = V_ext[__i];
    GlobalData_t delta_sl = delta_sl_ext[__i];
    GlobalData_t length = length_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    GlobalData_t Vmp_new = V;
    delta_sl = (sv->Lsc/Lsref);
    GlobalData_t epsf = length;
    GlobalData_t tact_new = (((sv->Vmp<=p->VmThresh)&&(V>p->VmThresh)) ? sv->tcur : sv->tact);
    GlobalData_t tcur_new = (sv->tcur+dt);
    GlobalData_t Ls = (Lsref*epsf);
    Tension = ((((Sact*sv->C)*(sv->Lsc-(Lsc0)))*(Ls-(sv->Lsc)))/Lseiso);
    
    
    //Complete Forward Euler Update
    GlobalData_t Clen = (tanh(((4.*(sv->Lsc-(Lsc0)))*(sv->Lsc-(Lsc0)))));
    GlobalData_t TLsc = (tauSC*(0.29+(0.3*sv->Lsc)));
    GlobalData_t _tR = (t/tauR);
    GlobalData_t diff_Lsc = ((((Ls-(sv->Lsc))/Lseiso)-(1.))*vmax);
    GlobalData_t dtAct = ((sv->tact<0.) ? 0. : (t-(sv->tact)));
    GlobalData_t maxF = ((_tR>0.) ? _tR : 0.);
    GlobalData_t x = ((maxF<8.) ? maxF : 8.);
    GlobalData_t Frise = ((((((0.02*x)*x)*x)*(8.-(x)))*(8.-(x)))*(exp((-x))));
    GlobalData_t diff_C = ((((1./tauR)*Clen)*Frise)+(((1./tauD)*(Crest-(sv->C)))/(1.+(exp(((TLsc-(dtAct))/tauD))))));
    GlobalData_t C_new = sv->C+diff_C*dt;
    GlobalData_t Lsc_new = sv->Lsc+diff_Lsc*dt;
    
    
    //Complete Rush Larsen Update
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    if (C_new < 0) { IIF_warn(__i, "C < 0, clamping to 0"); sv->C = 0; }
    else {sv->C = C_new;}
    if (Lsc_new < 0) { IIF_warn(__i, "Lsc < 0, clamping to 0"); sv->Lsc = 0; }
    else {sv->Lsc = Lsc_new;}
    Tension = Tension;
    sv->Vmp = Vmp_new;
    delta_sl = delta_sl;
    sv->tact = tact_new;
    sv->tcur = tcur_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Tension_ext[__i] = Tension;
    V_ext[__i] = V;
    delta_sl_ext[__i] = delta_sl;
    length_ext[__i] = length;

  }

            }
}
#endif // STRESS_LUMENS_CPU_GENERATED

bool Stress_LumensIonType::has_trace() const {
    return true;
}

void Stress_LumensIonType::trace(IonIfBase& imp_base, int node, FILE* file, GlobalData_t** impdata) const
{
  IonIfDerived& imp = static_cast<IonIfDerived&>(imp_base);
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("Stress_Lumens_trace_header.txt","wt");
    fprintf(theader->fd,
        "Tension\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = imp.get_dt() * 1e0;
  cell_geom *region = &imp.cgeom();
  Stress_Lumens_Params *p  = imp.params();

  Stress_Lumens_state *sv_base = (Stress_Lumens_state *)imp.sv_tab().data();

  Stress_Lumens_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = imp.get_tstp().cnt * dt;
  IonIfBase* IF = &imp;
  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Tension_ext = impdata[Tension];
  GlobalData_t *V_ext = impdata[Vm];
  GlobalData_t *delta_sl_ext = impdata[delLambda];
  GlobalData_t *length_ext = impdata[Lambda];
  //Prepare all the private functions.
  //Initialize the external vars to their current values
  GlobalData_t Tension = Tension_ext[__i];
  GlobalData_t V = V_ext[__i];
  GlobalData_t delta_sl = delta_sl_ext[__i];
  GlobalData_t length = length_ext[__i];
  //Change the units of external variables as appropriate.
  
  
  GlobalData_t epsf = length;
  GlobalData_t Ls = (Lsref*epsf);
  Tension = ((((Sact*sv->C)*(sv->Lsc-(Lsc0)))*(Ls-(sv->Lsc)))/Lseiso);
  //Output the desired variables
  fprintf(file, "%4.12f\t", Tension);
  //Change the units of external variables as appropriate.
  
  

}
IonIfBase* Stress_LumensIonType::make_ion_if(Target target, int num_node, const std::vector<std::reference_wrapper<IonType>>& plugins) const {
        // Place the allocated IonIf in managed memory if a GPU target exists for this model
        // otherwise, place it in main RAM
    IonIfDerived* ptr;
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_ROCM, 1, true);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CUDA, 1, true);
    }
    else {
        ptr = allocate_on_target<IonIfDerived>(Target::MLIR_CPU, 1, true);
    }
    // Using placement new to place the object in the correct memory
    return new(ptr) IonIfDerived(*this, this->select_target(target),
    num_node, plugins);
}

void Stress_LumensIonType::destroy_ion_if(IonIfBase *imp) const {
    // Call destructor and deallocate manually because the object might
    // be located on GPU (delete won't work in this case)
    imp->~IonIfBase();
    IonIfDerived* ptr = static_cast<IonIfDerived *>(imp);
    if (this->select_target(Target::MLIR_ROCM) == Target::MLIR_ROCM) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_ROCM, ptr);
    }
    else if (this->select_target(Target::MLIR_CUDA) == Target::MLIR_CUDA) {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CUDA, ptr);
    }
    else {
        deallocate_on_target<IonIfDerived>(Target::MLIR_CPU, ptr);
    }
}

}  // namespace limpet
        