// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#ifndef RESTITUTE_H
#define RESTITUTE_H

#include "MULTI_ION_IF.h"
#include "ap_analyzer.h"

namespace limpet {

typedef enum _r_protocol {S1S2,DYNAMIC,S1S2_fast} r_prtcl;
struct TrgList {
  int     n;                  //!< number of pulses required for protocol
  double *lst;                //!< store instants of pulse delivery
  bool   *pmat;               //!< store flag to indicate prematurity
};

struct restitute_S1S2 {
  float   bcl;                //!< basic cycle length
  float   S2_start;           //!< bcl of first premature beat
  float   S2_end;             //!< bcl of last premature beat
  int     beats_per_S2;       //!< number of beats before S2
  float   S2_dec;             //!< decrement for S2 beats
};

struct restitute_dynamic {
  float   bcl_start;          //!< initial basic cycle length
  float   bcl_end;            //!< final basic cycle length
  int     beats_per_bcl;      //!< number of beats for a particular bcl
  float   bcl_dec;            //!< decrement in bcl
};

struct restitution {
  TrgList trigs;              //!< trigger list for defining stim sequence
  TrgList saveState;          //!< instants at wich we save state vectors
  double  dur;                //!< total duration of protocol
  r_prtcl prtcl;              //!< protocol type
  int     numppBeats;         //!< number of prepaced beats before protocol
  union {
    restitute_S1S2    S1S2;
    restitute_dynamic dyn;
  } rtype;
};

void restitution_trigger_list(char *restitution_file,restitution *r,char *protocol,int*,double**);
void restitution_save_sv(MULTI_IF *miif, int R1, restitution *r, action_potential *AP);

}  // namespace limpet

#endif
