// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#ifndef LIMPET_DATA_H
#define LIMPET_DATA_H

#include "target.h"
#ifdef HAS_ROCM_MODEL
#include <hip/hip_runtime.h>
#endif
#ifdef HAS_CUDA_MODEL
#include <cuda_runtime.h>
#endif
#include <iostream>

namespace limpet {
template<typename T>
class LimpetData {
  public:
    LimpetData(Target target, std::size_t n) : _target(target), _size(n) {
      this->_data = allocate_on_target<T>(this->_target, this->_size);
      this->_count = new std::size_t(0);
    };

    LimpetData() : _target(Target::UNKNOWN), _size(0), _data(nullptr) {}

    ~LimpetData() {
      if (*this->_count == 0) {
        deallocate_on_target<T>(this->_target, this->_data);
      }
      else {
        (*this->_count)--;
      }
    }

#if defined HAS_ROCM_MODEL || HAS_CUDA_MODEL
    __device__ __host__
#endif
    T *data() const {
      return this->_data;
    }

    LimpetData<T>& operator=(const LimpetData<T> &other) {
      if (this != &other) {
      this->_data = other._data;
      this->_size = other._size;
      this->_target = other._target;
      this->_count = other._count;
      (*this->_count)++;
      }
      return *this;
    }

  T operator*() {
    return *this->_data;
  }
  T* operator&() {
    return this->_data;
  }

  private:
    Target _target;
    std::size_t _size;
    T *_data = nullptr;
    std::size_t *_count = nullptr;
    bool _copied;
};
}

#endif // LIMPET_DATA_H
