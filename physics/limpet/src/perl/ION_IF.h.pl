#!/usr/bin/perl
# ----------------------------------------------------------------------------
# openCARP is an open cardiac electrophysiology simulator.
#
# Copyright (C) 2020 openCARP project
#
# This program is licensed under the openCARP Academic Public License (APL)
# v1.0: You can use and redistribute it and/or modify it in non-commercial
# academic environments under the terms of APL as published by the openCARP
# project v1.0, or (at your option) any later version. Commercial use requires
# a commercial license (info@opencarp.org).
#
# This program is distributed without any warranty; see the openCARP APL for
# more details.
#
# You should have received a copy of the openCARP APL along with this program
# and can find it online: http://www.opencarp.org/license
# ----------------------------------------------------------------------------

#
# This PERL script creates a header file by parsing IMPs.h
#
#   To add a new ionic model or plugin (an IMP), add the name of the file
#   to either the IONIC_MODELS or PLUGINS list in IMPs.h.
#   Also, update the Makefile.
#   If a new data type is required, add it to IMPDATATYPES.
#
#   Note, the IMP name must contain at least one uppercase character
#   When adding an IMP called IMP, the following functions must be declared:
#   initialize_params_IMP() // modifiable IMP parameters
#   initialize_sv_IMP()     // allocate and initialize the SV table entries
#   construct_tables_IMP()  // allocate memory for and fill lookup table
#   compute_IMP()           // compute desired quantities
#   destroy_IMP()           // free tables used by IMP
#   and
#   #define IMP_REQDAT mask // in the IMP.h, required data
#   #define IMP_MODDAT mask // in the IMP.h, modified data
#   and
#   define a structure IMP_Params which contains IMP specific
#   parameters in IMP.h
#
#   The parameter \\b is a special parameter signifying a list of flags
#   which define model specific behaviour. This parameter is meant to be
#   used to signal that a specific set of parameter values should be used,
#   adjusted for a particular behaviour.
#
#   Ionic models and plugins use data not provided by the ionic models.
#   You must define a mask specifying what data must be passed in.
#   The mask is OR'ed together from the IMP data type flags
#   These data MUST be passed in the IMPDataStruct vector. The length
#   of these data vectors must be equal to the total #nodes even if
#   not all nodes use all data types. Data
#   passed like this typically signify interaction between IMPS and other
#   procedures, beyond the scope of an IMP, in the program.
#
#   Plug-ins may access parent data and modify it. Two helper functions
#   return pointers to functions that perform these two functions:
#   get_sv_offset()
#   getPutSV()
#
#

require "common.pl";

#get the list of IMPs
(undef,undef,undef) = readIMPs();

####################
# Start writing ION_IF.h here
####################
open $cfile, ">", "ION_IF_datatypes.h";
select $cfile;
print "// Generated in ION_IF.h.pl\n\n/* define model IDs for different ionic models */\ntypedef enum {";
foreach $idt (@I_DATA_T)
{
	print "$idt,";
}
print "bogusIDT} IMPDataType;\n";
print "#define NUM_IMP_DATA_TYPES ".($#I_DATA_T+1)."\n";
printf "/* define IMP data type flags */\n";
$val = 1;
foreach $idt (@I_DATA_T)
{
  print "#define $idt"."_DATA_FLAG $val\n";
  $val *=2;
}

print "\n//! data types needed by IMPs\n";
print "static const int imp_data_flag[] = {\n";
foreach $idt (@I_DATA_T)
{
  print "  $idt"."_DATA_FLAG,\n";
}
print " 0\n};\n";

print "//!strings for each enum type\n";
print "static const char *imp_data_names[] = { ";
foreach $idt (@I_DATA_T)
{
	print "\"$idt\", ";
}
print "\"BOGUS\" };\n";


open $cfile, ">", "ION_IF_sv.h";
select $cfile;
print <<EOF
// Generated in ION_IF.h.pl

/** get the value of a state variable
 *
 * \\param sv     pointer to ionic model
 * \\param num    index of state variable
 * \\param offset offset of the state variable into the structure
 *
 * \\return the value of the SV converted to a GlobalData_t
 */
EOF
;
foreach $svtype ( keys %scanchar ) {
  print "GlobalData_t get",$svtype,"SV( IonIfBase& sv, int num, int offset );\n";
}

foreach $svtype ( keys %scanchar ) {
  print "void put",$svtype,"SV( IonIfBase& sv, int num, int offset, GlobalData_t v );\n";
}
