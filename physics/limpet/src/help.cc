// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/*
 * This file is used to print more detailed information about imp like
 * available ionic models
 * available plugins
 *
 * and model specific information like
 *
 * modifiable parameters of a model
 * state variables of a model
 *
 * Further, output strings are created which can be directly used in bidomain par files.
 */
#include "MULTI_ION_IF.h"

#include <stdlib.h>
#include <stdio.h>
#include "help_cmdline.h"
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>
#include <limits.h>

void  help_ims_avail( int );
void  help_plugs_avail(int );
void  print_IMP_parameters( void );
void  help_modifiable_params(ION_TYPE, int );
void    help_svs(ION_TYPE, int );

#undef __FUNCT__
#define __FUNCT__ "main"
int main( int argc, char *argv[] )
{
  struct gengetopt_args_info  params;
  int               carp;
  ION_TYPE IM_IC;

  PetscInitialize( &argc, &argv, (char *)0, "" );
  PetscOptionsInsertString("-options_left no");

  if ( cmdline_parser(argc, argv, &params) != 0 )
    exit(1);

  carp = params.carp_flag;

  if ( params.imps_flag )
    help_ims_avail(carp);

  if ( params.plugs_flag )
    help_plugs_avail(carp);

  if ( params.all_par_flag )
    print_IMP_parameters();

  if ( params.imp_par_given )  {
    if ( get_ION_TYPE( params.imp_par_arg ) == NULL ) {
      fprintf( stderr, "Illegal IMP specified: %s\n", params.imp_par_arg );
      exit(1);
    } else
      IM_IC = get_ION_TYPE( params.imp_par_arg );

    help_modifiable_params(IM_IC,carp);
  }

  if ( params.imp_svs_given )  {
    if ( get_ION_TYPE( params.imp_svs_arg ) == NULL ) {
      fprintf( stderr, "Illegal IMP specified: %s\n", params.imp_svs_arg );
      exit(1);
    } else
      IM_IC = get_ION_TYPE( params.imp_svs_arg );

    help_svs(IM_IC,carp);
  }

  CHKERRQ(PetscFinalize());

  return(0);
}

