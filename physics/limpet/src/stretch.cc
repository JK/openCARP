// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/** \file
 *  Provide routines for performing single cell mechanics tests
 *
 */
#include <stdlib.h>
#include "stretch.h"

namespace limpet {

using ::opencarp::log_msg;
using ::opencarp::timer_manager;

void apply_stretch_pulse(MULTI_IF *miif, pulseStretch*s, timer_manager *tm);

/**
 *
 *
 */
void
initializePulseStretch(float strain, float onset, float duration, float rise, 
                                                        float fall, stretch *s)
{
  s->pulse.prtcl    = STRAIN_PULSE;
  s->pulse.sr       = strain + 1.;
  s->pulse.onset    = onset;
  s->pulse.duration = duration;
  s->pulse.rise     = rise;
  s->pulse.fall     = fall;
}


/**
 *
 *
 */
void
apply_stretch(MULTI_IF *miif, stretch *s, timer_manager *tm)
{
  if(s->prtcl==STRAIN_PULSE)  {
    apply_stretch_pulse(miif, &s->pulse, tm);
    return;
  }
  if(s->prtcl==ISOMETRIC)  {
    log_msg(NULL, 2, 0, "Isometric stretch protocol not implemented yet.");
    return;
  }
}


/**
 *
 *
 */
void
apply_stretch_pulse(MULTI_IF *miif, pulseStretch*s, timer_manager *tm)
{
  double t  = tm->time;
  double dt = tm->time_step;

  if(s->sr>1.0)  {
    if((t>s->onset) && ((t-s->onset)<s->rise)) {
      // rising phase of stretch pulse
      double strain_rate = (s->sr-1.)/s->rise;
      double inc_strain  = strain_rate*dt;

      miif->gdata[delLambda]->set(strain_rate);
      *(miif->gdata[Lambda]) += inc_strain;
    }
    else if ((t>=(s->onset+s->rise)) && t<(s->onset+s->duration-s->fall)) {
      // plateau phase of stretch pulse
      miif->gdata[delLambda]->set(0.0);
      miif->gdata[Lambda]->set(s->sr);
    }
    else if ((t>=(s->onset+s->duration-s->fall)) && (t<(s->onset+s->duration)))  {
      // falling phase of stretch pulse
      double strain_rate =  (s->sr-1.)/s->fall;
      double dec_strain  = -strain_rate*dt;
      miif->gdata[delLambda]->set(-strain_rate);
      miif->gdata[Lambda]->set(dec_strain);
    }
    else  {
      // pulse is off, stretch and stretch ratio must be 0 now
      miif->gdata[delLambda]->set(0.0);
      miif->gdata[Lambda]->set(1.);
    }
  }
}

}  // namespace limpet
