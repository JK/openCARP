// Load and Store functions for different vector types
// NOTE: they need to be in a separate file from where they are used,
// since its caller uses different function signatures with llvm.ptr and i32
// instead of memref and index types

func.func @load_gather_vector_2xf32(%base : memref<2xf32>, %indices: vector<2xi32>, %mask: vector<2xi1>, %pass_thru: vector<2xf32>) -> vector<2xf32> {
    %c0 = arith.constant 0 : index
    %ld = vector.gather %base[%c0][%indices], %mask, %pass_thru
        : memref<2xf32>, vector<2xi32>, vector<2xi1>, vector<2xf32> into vector<2xf32>
    return %ld : vector<2xf32>
}

func.func @load_gather_vector_4xf32(%base : memref<4xf32>, %indices: vector<4xi32>, %mask: vector<4xi1>, %pass_thru: vector<4xf32>) -> vector<4xf32> {
    %c0 = arith.constant 0 : index
    %ld = vector.gather %base[%c0][%indices], %mask, %pass_thru
        : memref<4xf32>, vector<4xi32>, vector<4xi1>, vector<4xf32> into vector<4xf32>
    return %ld : vector<4xf32>
}

func.func @load_gather_vector_8xf32(%base : memref<8xf32>, %indices: vector<8xi32>, %mask: vector<8xi1>, %pass_thru: vector<8xf32>) -> vector<8xf32> {
    %c0 = arith.constant 0 : index
    %ld = vector.gather %base[%c0][%indices], %mask, %pass_thru
        : memref<8xf32>, vector<8xi32>, vector<8xi1>, vector<8xf32> into vector<8xf32>
    return %ld : vector<8xf32>
}

func.func @load_gather_vector_16xf32(%base : memref<16xf32>, %indices: vector<16xi32>, %mask: vector<16xi1>, %pass_thru: vector<16xf32>) -> vector<16xf32> {
    %c0 = arith.constant 0 : index
    %ld = vector.gather %base[%c0][%indices], %mask, %pass_thru
        : memref<16xf32>, vector<16xi32>, vector<16xi1>, vector<16xf32> into vector<16xf32>
    return %ld : vector<16xf32>
}

func.func @load_gather_vector_32xf32(%base : memref<32xf32>, %indices: vector<32xi32>, %mask: vector<32xi1>, %pass_thru: vector<32xf32>) -> vector<32xf32> {
    %c0 = arith.constant 0 : index
    %ld = vector.gather %base[%c0][%indices], %mask, %pass_thru
        : memref<32xf32>, vector<32xi32>, vector<32xi1>, vector<32xf32> into vector<32xf32>
    return %ld : vector<32xf32>
}

func.func @load_gather_vector_64xf32(%base : memref<64xf32>, %indices: vector<64xi32>, %mask: vector<64xi1>, %pass_thru: vector<64xf32>) -> vector<64xf32> {
    %c0 = arith.constant 0 : index
    %ld = vector.gather %base[%c0][%indices], %mask, %pass_thru
        : memref<64xf32>, vector<64xi32>, vector<64xi1>, vector<64xf32> into vector<64xf32>
    return %ld : vector<64xf32>
}

func.func @store_scatter_vector_2xf32(%base : memref<2xf32>, %indices: vector<2xi32>, %mask: vector<2xi1>, %stored_value: vector<2xf32>) {
    %c0 = arith.constant 0 : index
    vector.scatter %base[%c0][%indices], %mask, %stored_value
        : memref<2xf32>, vector<2xi32>, vector<2xi1>, vector<2xf32>
    return
}

func.func @store_scatter_vector_4xf32(%base : memref<4xf32>, %indices: vector<4xi32>, %mask: vector<4xi1>, %stored_value: vector<4xf32>) {
    %c0 = arith.constant 0 : index
    vector.scatter %base[%c0][%indices], %mask, %stored_value
        : memref<4xf32>, vector<4xi32>, vector<4xi1>, vector<4xf32>
    return
}

func.func @store_scatter_vector_8xf32(%base : memref<8xf32>, %indices: vector<8xi32>, %mask: vector<8xi1>, %stored_value: vector<8xf32>) {
    %c0 = arith.constant 0 : index
    vector.scatter %base[%c0][%indices], %mask, %stored_value
        : memref<8xf32>, vector<8xi32>, vector<8xi1>, vector<8xf32>
    return
}

func.func @store_scatter_vector_16xf32(%base : memref<16xf32>, %indices: vector<16xi32>, %mask: vector<16xi1>, %stored_value: vector<16xf32>) {
    %c0 = arith.constant 0 : index
    vector.scatter %base[%c0][%indices], %mask, %stored_value
        : memref<16xf32>, vector<16xi32>, vector<16xi1>, vector<16xf32>
    return
}

func.func @store_scatter_vector_32xf32(%base : memref<32xf32>, %indices: vector<32xi32>, %mask: vector<32xi1>, %stored_value: vector<32xf32>) {
    %c0 = arith.constant 0 : index
    vector.scatter %base[%c0][%indices], %mask, %stored_value
        : memref<32xf32>, vector<32xi32>, vector<32xi1>, vector<32xf32>
    return
}

func.func @store_scatter_vector_64xf32(%base : memref<64xf32>, %indices: vector<64xi32>, %mask: vector<64xi1>, %stored_value: vector<64xf32>) {
    %c0 = arith.constant 0 : index
    vector.scatter %base[%c0][%indices], %mask, %stored_value
        : memref<64xf32>, vector<64xi32>, vector<64xi1>, vector<64xf32>
    return
}

func.func @load_gather_vector_2xf64(%base : memref<2xf64>, %indices: vector<2xi32>, %mask: vector<2xi1>, %pass_thru: vector<2xf64>) -> vector<2xf64> {
    %c0 = arith.constant 0 : index
    %ld = vector.gather %base[%c0][%indices], %mask, %pass_thru
        : memref<2xf64>, vector<2xi32>, vector<2xi1>, vector<2xf64> into vector<2xf64>
    return %ld : vector<2xf64>
}

func.func @load_gather_vector_4xf64(%base : memref<4xf64>, %indices: vector<4xi32>, %mask: vector<4xi1>, %pass_thru: vector<4xf64>) -> vector<4xf64> {
    %c0 = arith.constant 0 : index
    %ld = vector.gather %base[%c0][%indices], %mask, %pass_thru
        : memref<4xf64>, vector<4xi32>, vector<4xi1>, vector<4xf64> into vector<4xf64>
    return %ld : vector<4xf64>
}

func.func @load_gather_vector_8xf64(%base : memref<8xf64>, %indices: vector<8xi32>, %mask: vector<8xi1>, %pass_thru: vector<8xf64>) -> vector<8xf64> {
    %c0 = arith.constant 0 : index
    %ld = vector.gather %base[%c0][%indices], %mask, %pass_thru
        : memref<8xf64>, vector<8xi32>, vector<8xi1>, vector<8xf64> into vector<8xf64>
    return %ld : vector<8xf64>
}

func.func @load_gather_vector_16xf64(%base : memref<16xf64>, %indices: vector<16xi32>, %mask: vector<16xi1>, %pass_thru: vector<16xf64>) -> vector<16xf64> {
    %c0 = arith.constant 0 : index
    %ld = vector.gather %base[%c0][%indices], %mask, %pass_thru
        : memref<16xf64>, vector<16xi32>, vector<16xi1>, vector<16xf64> into vector<16xf64>
    return %ld : vector<16xf64>
}

func.func @load_gather_vector_32xf64(%base : memref<32xf64>, %indices: vector<32xi32>, %mask: vector<32xi1>, %pass_thru: vector<32xf64>) -> vector<32xf64> {
    %c0 = arith.constant 0 : index
    %ld = vector.gather %base[%c0][%indices], %mask, %pass_thru
        : memref<32xf64>, vector<32xi32>, vector<32xi1>, vector<32xf64> into vector<32xf64>
    return %ld : vector<32xf64>
}

func.func @load_gather_vector_64xf64(%base : memref<64xf64>, %indices: vector<64xi32>, %mask: vector<64xi1>, %pass_thru: vector<64xf64>) -> vector<64xf64> {
    %c0 = arith.constant 0 : index
    %ld = vector.gather %base[%c0][%indices], %mask, %pass_thru
        : memref<64xf64>, vector<64xi32>, vector<64xi1>, vector<64xf64> into vector<64xf64>
    return %ld : vector<64xf64>
}

func.func @store_scatter_vector_2xf64(%base : memref<2xf64>, %indices: vector<2xi32>, %mask: vector<2xi1>, %stored_value: vector<2xf64>) {
    %c0 = arith.constant 0 : index
    vector.scatter %base[%c0][%indices], %mask, %stored_value
        : memref<2xf64>, vector<2xi32>, vector<2xi1>, vector<2xf64>
    return
}

func.func @store_scatter_vector_4xf64(%base : memref<4xf64>, %indices: vector<4xi32>, %mask: vector<4xi1>, %stored_value: vector<4xf64>) {
    %c0 = arith.constant 0 : index
    vector.scatter %base[%c0][%indices], %mask, %stored_value
        : memref<4xf64>, vector<4xi32>, vector<4xi1>, vector<4xf64>
    return
}

func.func @store_scatter_vector_8xf64(%base : memref<8xf64>, %indices: vector<8xi32>, %mask: vector<8xi1>, %stored_value: vector<8xf64>) {
    %c0 = arith.constant 0 : index
    vector.scatter %base[%c0][%indices], %mask, %stored_value
        : memref<8xf64>, vector<8xi32>, vector<8xi1>, vector<8xf64>
    return
}

func.func @store_scatter_vector_16xf64(%base : memref<16xf64>, %indices: vector<16xi32>, %mask: vector<16xi1>, %stored_value: vector<16xf64>) {
    %c0 = arith.constant 0 : index
    vector.scatter %base[%c0][%indices], %mask, %stored_value
        : memref<16xf64>, vector<16xi32>, vector<16xi1>, vector<16xf64>
    return
}

func.func @store_scatter_vector_32xf64(%base : memref<32xf64>, %indices: vector<32xi32>, %mask: vector<32xi1>, %stored_value: vector<32xf64>) {
    %c0 = arith.constant 0 : index
    vector.scatter %base[%c0][%indices], %mask, %stored_value
        : memref<32xf64>, vector<32xi32>, vector<32xi1>, vector<32xf64>
    return
}

func.func @store_scatter_vector_64xf64(%base : memref<64xf64>, %indices: vector<64xi32>, %mask: vector<64xi1>, %stored_value: vector<64xf64>) {
    %c0 = arith.constant 0 : index
    vector.scatter %base[%c0][%indices], %mask, %stored_value
        : memref<64xf64>, vector<64xi32>, vector<64xi1>, vector<64xf64>
    return
}
