// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2022 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#include "Passes/Passes.h"
#include "mlir/Dialect/SCF/IR/SCF.h"
#include "mlir/Dialect/Affine/IR/AffineOps.h"
#include "mlir/Transforms/GreedyPatternRewriteDriver.h"
#include "llvm/Support/Debug.h"

#include <optional>

#define DEBUG_TYPE "convert-forloop-to-affinefor"

using namespace mlir;

namespace {
struct ConvertForLoopToAffineFor : ConvertForLoopToAffineForBase<ConvertForLoopToAffineFor> {
  void runOnOperation() override;
};
} // namespace

struct ForToAffineForPass : OpRewritePattern<scf::ForOp> {

  using OpRewritePattern<scf::ForOp>::OpRewritePattern;

  LogicalResult matchAndRewrite(scf::ForOp loopOp,
                                PatternRewriter &rewriter) const final {
    auto affineForOp
    = rewriter.create<affine::AffineForOp>(loopOp.getLoc(), loopOp.getLowerBound(),
                                   rewriter.getDimIdentityMap(),
                                   loopOp.getUpperBound(), rewriter.getDimIdentityMap(),
                                   1, /*iterArgs*/std::nullopt, /*bodyBuilderFn=*/nullptr);
    // Copy the body of the for op, delete scf.yield and create affine.yield
    rewriter.eraseBlock(affineForOp.getBody());
    rewriter.inlineRegionBefore(loopOp.getRegion(), affineForOp.getRegion(),
                                std::prev(affineForOp.getRegion().end()));
    rewriter.setInsertionPoint(&affineForOp.getBody()->back());
    rewriter.create<affine::AffineYieldOp>(affineForOp.getBody()->back().getLoc());
    rewriter.eraseOp(&affineForOp.getBody()->back());
    loopOp.replaceAllUsesWith(affineForOp);
    return success();
  }
};

void ConvertForLoopToAffineFor::runOnOperation() {
  RewritePatternSet patterns(&getContext());
  patterns.insert<ForToAffineForPass>(&getContext());
  (void)applyPatternsAndFoldGreedily(getOperation(), std::move(patterns));
}

std::unique_ptr<mlir::Pass> mlir::createConvertForLoopToAffineForPass() {
  return std::make_unique<ConvertForLoopToAffineFor>();
}
