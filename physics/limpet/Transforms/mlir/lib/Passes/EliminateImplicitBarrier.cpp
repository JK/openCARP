// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2022 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#include "Passes/Passes.h"
#include "mlir/Dialect/OpenMP/OpenMPDialect.h"
#include "mlir/Transforms/GreedyPatternRewriteDriver.h"
#include "llvm/Support/Debug.h"

#define DEBUG_TYPE "convert-forloop-to-wsloop"

using namespace mlir;

namespace {
struct EliminateImplicitBarrier
  : EliminateImplicitBarrierBase<EliminateImplicitBarrier> {
  void runOnOperation() override;
};
} // namespace

struct EliminateImplicitBarrierPass : OpRewritePattern<omp::WsLoopOp> {

  using OpRewritePattern<omp::WsLoopOp>::OpRewritePattern;

  LogicalResult matchAndRewrite(omp::WsLoopOp loopOp,
                                PatternRewriter &rewriter) const final {
    if (loopOp.getNowait())
      return failure();

    auto noWaitAttr = UnitAttr::get(rewriter.getContext());
    auto staticSched
    = omp::ClauseScheduleKindAttr::get(rewriter.getContext(),
                                       omp::ClauseScheduleKind::Static);
    loopOp.setNowaitAttr(noWaitAttr);
    loopOp.setScheduleValAttr(staticSched);
    return success();
  }
};

void EliminateImplicitBarrier::runOnOperation() {
  RewritePatternSet patterns(&getContext());
  patterns.insert<EliminateImplicitBarrierPass>(&getContext());
  (void)applyPatternsAndFoldGreedily(getOperation(), std::move(patterns));
}

std::unique_ptr<mlir::Pass> mlir::createEliminateImplicitBarrierPass() {
  return std::make_unique<EliminateImplicitBarrier>();
}
