// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2022 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#include "Passes/Passes.h"
#include "mlir/Conversion/FuncToLLVM/ConvertFuncToLLVM.h"
#include "mlir/Conversion/LLVMCommon/ConversionTarget.h"
#include "mlir/Conversion/LLVMCommon/LoweringOptions.h"
#include "mlir/Conversion/LLVMCommon/Pattern.h"
#include "mlir/Conversion/LLVMCommon/TypeConverter.h"
#include "mlir/Conversion/MemRefToLLVM/MemRefToLLVM.h"
#include "mlir/Conversion/VectorToLLVM/ConvertVectorToLLVM.h"
#include "mlir/Dialect/Func/IR/FuncOps.h"
#include "mlir/Dialect/GPU/IR/GPUDialect.h"
#include "mlir/Dialect/LLVMIR/LLVMDialect.h"
#include "mlir/Dialect/MemRef/IR/MemRef.h"
#include "mlir/Transforms/GreedyPatternRewriteDriver.h"
#include "llvm/Support/Debug.h"

#define DEBUG_TYPE "addrspace-alloca-lowering"

using namespace mlir;

namespace {

struct AddrSpaceAllocaOpLowering
      : public AddrSpaceAllocaOpLoweringBase<AddrSpaceAllocaOpLowering> {
  void runOnOperation() override;
};
} // namespace

struct AddrSpaceAllocaOpLoweringPass
    : public ConvertOpToLLVMPattern<memref::AllocaOp> {
  using ConvertOpToLLVMPattern<memref::AllocaOp>::ConvertOpToLLVMPattern;

LogicalResult
  matchAndRewrite(memref::AllocaOp op, memref::AllocaOp::Adaptor adaptor,
                  ConversionPatternRewriter &rewriter) const override {

    MemRefType memRefType = op->getResult(0).getType().cast<MemRefType>();
    if (!isConvertibleAndHasIdentityMaps(memRefType))
      return rewriter.notifyMatchFailure(op, "incompatible memref type");
    auto loc = op.getLoc();

    // Get actual size and strides from the allocaOp
    llvm::SmallVector<Value, 4> sizes;
    llvm::SmallVector<Value, 4> strides;
    Value sizeInBytes;
    this->getMemRefDescriptorSizes(loc, memRefType, op.getDynamicSizes(),
                                   rewriter, sizes, strides, sizeInBytes);

    // Get the correct address space, MemRefType and its LLVM pointer type
    auto addrSpaceAttr =
        IntegerAttr::get(IntegerType::get(op.getContext(), 64),
                         static_cast<int>(gpu::GPUDialect::getPrivateAddressSpace()));
    auto addrSpaceMemRefType =
        MemRefType::get(memRefType.getShape(), memRefType.getElementType(),
                        memRefType.getLayout(), addrSpaceAttr);
    auto newElementPtrType = this->getElementPtrType(addrSpaceMemRefType);

    // Create a new alloca with proper address space, and cast it
    auto elementPtrType = this->getElementPtrType(op.getType());
    auto allocatedElementPtr = rewriter.create<LLVM::AllocaOp>(
        loc, newElementPtrType, elementPtrType, sizeInBytes, op.getAlignment().value_or(0));
    auto castedPtr = rewriter.create<LLVM::AddrSpaceCastOp>(
        loc, elementPtrType, allocatedElementPtr);

    // Create the MemRef descriptor.
    Value memRefDescriptor = this->createMemRefDescriptor(
        loc, memRefType, castedPtr, castedPtr, sizes, strides, rewriter);

    // Potential users of this alloca still expect MemRef::AllocaOp,
    // convert it back to it. This cast will be optimized out.
    auto castToMemRef = rewriter.create<UnrealizedConversionCastOp>(
        loc, memRefType, memRefDescriptor);

    op.replaceAllUsesWith(castToMemRef->getResult(0));
    return success();
  }
};

void AddrSpaceAllocaOpLowering::runOnOperation() {
  LowerToLLVMOptions options(&getContext());
  LLVMTypeConverter converter(&getContext(), options);
  RewritePatternSet patterns(&getContext());
  patterns.insert<AddrSpaceAllocaOpLoweringPass>(converter);
  (void)applyPatternsAndFoldGreedily(getOperation(), std::move(patterns));
}

std::unique_ptr<mlir::Pass> mlir::createAddrSpaceAllocaOpLoweringPass() {
  return std::make_unique<AddrSpaceAllocaOpLowering>();
}

