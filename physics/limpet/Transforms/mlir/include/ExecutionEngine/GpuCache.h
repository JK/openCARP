#ifndef EXECUTIONENGINE_GPUCACHE_H
#define EXECUTIONENGINE_GPUCACHE_H

#include <unordered_map>

// A cache for storing hip modules and streams.
//
// This class is inspired by TensorFlow's gpu wrappers (see
// <https://cs.opensource.google/tensorflow/tensorflow/+/master:tensorflow/compiler/mlir/tools/kernel_gen/tf_gpu_runtime_wrappers.cc>)
// This class expects a struct with specific fields as a template parameter.
// The struct acts as an interface and needs to define the following fields:
// * error_t - a type for errors
// * module_t - a type for modules
// * stream_t - a type for streams
// * success - a value of type error_t, to return when an operation is successful
// * moduleLoad - a static function with the signature error_t moduleLoad(module_t *, void *)
// * moduleUnload - a static function with the signature error_t moduleUnload(module_t)
// * streamCreate - a static function with the signature error_t streamCreate(stream_t *)
// * streamDestroy - a static function with the signature error_t streamDestroy(stream_t)
template<typename GpuApi>
class GpuCache {
  // Generic types for GPU objects
  using error_t = typename GpuApi::error_t;
  using module_t = typename GpuApi::module_t;
  using stream_t = typename GpuApi::stream_t;
  public:
    GpuCache() {
      // Initialize the module cache
      moduleCache = new std::unordered_map<void *, module_t>();
    }

    ~GpuCache() {
        // Unload every module that was cached
        for (auto module : *moduleCache) {
          GpuApi::moduleUnload(module.second);
        }
        // Destroy the cached stream
        if (streamCache != nullptr) {
          GpuApi::streamDestroy(streamCache);
        }
        delete moduleCache;
    }

    // Look for module in cache, or load it and store it in the cache
    error_t getOrLoadModule(module_t *mod, void *data) {
      if (moduleCache->count(data) > 0) {
         *mod = (*moduleCache)[data];
         return GpuApi::success;
      }
      else {
        error_t error = GpuApi::moduleLoad(mod, data);
        (*moduleCache)[data] = *mod;
        return error;
      }
    }

    // Return cached stream, or create one and cache it
    error_t getOrCreateStream(stream_t *stream) {
        if (streamCache != nullptr) {
          *stream = streamCache;
          return GpuApi::success;
        }
        else {
          error_t error = GpuApi::streamCreate(&streamCache);
          *stream = streamCache;
          return error;
        }
    }

  private:
    // Hashmap containing modules (the key is the data pointer for the module)
    std::unordered_map<void *, module_t>* moduleCache = nullptr;
    // Cached stream
    stream_t streamCache = nullptr;
};

#endif //EXECUTIONENGINE_GPUCACHE_H
