// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2022 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#ifndef MLIR_OPENCARP_TRANSFORM_PASSES
#define MLIR_OPENCARP_TRANSFORM_PASSES

#include "mlir/Conversion/LLVMCommon/TypeConverter.h"
#include "mlir/Dialect/Func/IR/FuncOps.h"
#include "mlir/IR/BuiltinTypes.h"
#include "mlir/IR/Dialect.h"
#include "mlir/IR/OpDefinition.h"
#include "mlir/Interfaces/SideEffectInterfaces.h"
#include "mlir/Pass/Pass.h"

namespace mlir {
class Pass;

//===----------------------------------------------------------------------===//
// Passes
//===----------------------------------------------------------------------===//
std::unique_ptr<mlir::Pass> createConvertFmaToFMulFAddPass();

std::unique_ptr<mlir::Pass> createConvertMaxToNvMaxPass();

std::unique_ptr<mlir::Pass> createConvertMaxToRocdlMaxPass();

std::unique_ptr<mlir::Pass> createConvertForLoopToParallelPass();

std::unique_ptr<mlir::Pass> createEliminateImplicitBarrierPass();

std::unique_ptr<mlir::Pass> createRoofLineCounterPass();

std::unique_ptr<mlir::Pass> createConvertForLoopToAffineForPass();

/// AMD Alloca Address Space Fix
std::unique_ptr<mlir::Pass> createAddrSpaceAllocaOpLoweringPass();

/// Register pass to serialize GPU kernel functions to a CUBIN binary
/// annotation.
void registeropenCARPGpuSerializeToCubinPass();

/// Register pass to serialize GPU kernel functions to a HSACO binary
/// annotation.
void registeropenCARPGpuSerializeToHsacoPass();

//===----------------------------------------------------------------------===//
// Registration
//===----------------------------------------------------------------------===//

namespace affine {
class AffineDialect;
}

namespace arith {
class ArithDialect;
} // end namespace arith

namespace gpu {
class GPUDialect;
class GPUModuleOp;
} // namespace gpu

namespace math {
class MathDialect;
} // namespace math

namespace NVVM {
class NVVMDialect;
} // namespace NVVM

namespace LLVM {
class LLVMDialect;
} // end namespace LLVM

namespace omp {
class OpenMPDialect;
} // end namespace omp

namespace scf {
class SCFDialect;
} // end namespace scf

namespace vector {
class VectorDialect;
} // namespace vector


#define GEN_PASS_CLASSES
#include "Passes/Passes.h.inc"

#define GEN_PASS_REGISTRATION
#include "Passes/Passes.h.inc"
} // namespace mlir

#endif
