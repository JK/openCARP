

#include <stdio.h>
#include <iostream>
#include <random>
#include <string>
#include <mpi.h>
#include <ginkgo/ginkgo.hpp>

#include "../fem/slimfem/src/SF_base.h"
#include "electrics.cc"

//relative path to electric.cc we may have to change the place of the tests in
//order to avoid using relatives pathes
#include "../../physics/electric.cc"
#include <math.h>
#include <tuple>
//#include "petsc_utils.h"
//using namespace opencarp;

class TestGinkgo {

  private:

    int n_l = 4;
    int n_g = 4;


  protected:

  public:

    void fill_matrices_same_random(SF::abstract_matrix<int, double> & mat1,SF::abstract_matrix<int, double> & mat2)
    {
      mat1.init(n_g, n_g, n_l, n_l, 0, n_l);
      mat2.init(n_g, n_g, n_l, n_l, 0, n_l);


      mat_rows = SF::vector<int>(n_g);
      mat_cols = SF::vector<int>(n_g);
      mat_vals = SF::vector<double>(n_g);

      for (size_t r = 0; r < n_g; r++) {
        for (size_t c = 0; c < n_g; c++) {
          mat_rows[c] = r;
          mat_cols[c] = c;
          mat_vals[c] = rand();
        }
        //if set value doesnt work on ginkgo mat use set_values
        mat1.set_value(mat_rows, mat_cols, mat_vals, false);
        mat2.set_value(mat_rows, mat_cols, mat_vals, false);
      }
      //std::tuple<auto, auto> {mat1,mat2}
    }

    std::tuple<auto, auto> generate_random_petsc_ginkgo_matrices()
    {
      mat_g = new SF::ginkgo_matrix<int, double>();
      mat_p = new SF::petsc_matrix<int, double>();

      vec_p = new SF::petsc_vector<int, double>();
      vec_g = new SF::ginkgo_matrix<int, double>();

      fill_matrices_same_random(mat_g,mat_p)
      fill_matrices_same_random(vec_g,vec_p)

      return  {{mat_g, mat_p},{vec_g, vec_p}};
    }

    // virtual void test_solver_elliptic_electrics()
    // {
    //   std::tuple<auto,auto> T = generate_random_petsc_ginkgo_matrices();
    //   auto A_p = T[0][1];
    //   auto A_g = T[0][0];
    //   auto b_p = T[1][1];
    //   auto b_g = T[1][0];
    //
    //   intervalle = 0.0000000001
    //
    //   ginkgo_solution = elliptic_solver::solve("METTRE ARGUMENTS / creer solve du solver")
    //   petsc_solution = elliptic_solver::solve("arguments missing")
    //
    //   delta_sq_sqrt = pow(pow(ginkgo_solution-petsc_solution,2),0.5)
    //
    //   assertTrue( delta_sq_sqrt <= pow(pow(intervalle,2),0.5), "solver elliptic elec OK");
    // }

    void assertTrue(bool cond_bool, std::string str)
    {
      if (cond_bool)
      {
          std::cout << "\033[32m[SUCCESS]\033[0m  " << str << std::endl;
      }
      else
      {
          std::cout << "\033[31m[FAILURE]\033[0m  " << str << std::endl;
      }
    }
    }

int main(int argc, char** argv)
{
  // init MPI
  MPI_Init(&argc, &argv);
  MPI_Comm comm = SF_COMM;
  int rank, size;
  MPI_Comm_size(comm, &size); MPI_Comm_rank(comm, &rank);

  // init PETSc
  PetscInitialize( &argc, &argv, (char *)0, "" );

  // start testing
  auto test = TestGinkgo();
  //test.test_solver_elliptic_electrics()

  // end MPI
  MPI_Finalize();
}
