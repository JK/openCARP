message("Processing physics")

add_library(physics STATIC
    electrics.cc
    electrics.h
    electric_integrators.cc
    ionics.cc
    ionics.h
    stimulate.cc
    stimulate.h
    mechanics.cc
    mechanics.h
    mechanic_integrators.cc
    mechanic_integrators.h
)

add_library(physics::physics ALIAS physics)

target_include_directories(physics
    PUBLIC
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
        $<INSTALL_INTERFACE:>
    PRIVATE
        ../simulator # TODO: workaround to include sim_utils.h
)

target_link_libraries(physics
    PUBLIC numerics::numerics
    PUBLIC fem::fem
    PUBLIC limpet::limpet
    PUBLIC matlib::matlib
)
