// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file constitutive_model_library.h
* @brief Basic constitutive model.
* @author Jonathan Krauß
* @version 
* @date 2024-03-06
*/

#include "constitutive_model.h"
#include "constitutive_model_library.h"
#include "mechanic_integrators.h"
#include <math.h>

namespace opencarp {

void Neo_Hooke::calc_cauchy_stress(double * iF, double * F, double & detF, double * sigma)
{
  double b[9];
  calc_left_cauchy_green(F, b);
  /// sigma in Voigt Notation {sigma11, sigma22, sigma33, sigma12, sigma13, sigma23}
  /// sigma = my/detF (b - 1) + lambda/detF ln(detF) 1
  sigma[0] = my/detF * (b[0] - 1) + lambda/detF * log(detF);
  sigma[1] = my/detF * (b[4] - 1) + lambda/detF * log(detF);
  sigma[2] = my/detF * (b[8] - 1) + lambda/detF * log(detF);
  
  sigma[3] = my/detF * b[3];
  sigma[4] = my/detF * b[6];
  sigma[5] = my/detF * b[5];
}

void Neo_Hooke::calc_PK1_stress(double * iF, double * F, double & detF, double * S_PK1)
{
    // calculating S_PK1 using the deformation gradient and its inverse transpose (transpose is done implicitely in the equation using the indices
    // equation: S_PK1 = my (F - F^(-T)) + lambda * ln(detF) * F^(-T)
    // S_PK1 is not symmetric so it needs 9 entries (as opposed to S_PK2 and sigma
    // Index numbering like for the deformation gradient
    
    //first column
    S_PK1[0] = my * (F[0] - iF[0]) + lambda * log(detF) * iF[0];
    S_PK1[1] = my * (F[1] - iF[3]) + lambda * log(detF) * iF[3];
    S_PK1[2] = my * (F[2] - iF[6]) + lambda * log(detF) * iF[6];
    
    // second column
    S_PK1[3] = my * (F[3] - iF[1]) + lambda * log(detF) * iF[1];
    S_PK1[4] = my * (F[4] - iF[4]) + lambda * log(detF) * iF[4];
    S_PK1[5] = my * (F[5] - iF[7]) + lambda * log(detF) * iF[7];
    
    // third column
    S_PK1[6] = my * (F[6] - iF[2]) + lambda * log(detF) * iF[2];
    S_PK1[7] = my * (F[7] - iF[5]) + lambda * log(detF) * iF[5];
    S_PK1[8] = my * (F[8] - iF[8]) + lambda * log(detF) * iF[8];
}

void Neo_Hooke::calc_PK2_stress(double * iF, double * F, double & detF, double * S_PK2)
{
  // calculating S_PK2 with right cauchy green tensor C
  
  double iC[9];
  double detC;
  calc_right_cauchy_green(F, iC);
  SF::invert_3x3(iC, detC);
  
  S_PK2[0] = my * (1 - iC[0]) + lambda * log(detF) * iC[0];
  S_PK2[1] = my * (1 - iC[4]) + lambda * log(detF) * iC[4];
  S_PK2[2] = my * (1 - iC[8]) + lambda * log(detF) * iC[8];
  
  S_PK2[3] = my * (-iC[1]) + lambda * log(detF) * iC[1];
  S_PK2[4] = my * (-iC[2]) + lambda * log(detF) * iC[2];
  S_PK2[5] = my * (-iC[5]) + lambda * log(detF) * iC[5];
}

void Neo_Hooke::calc_energy(double * F, double & detF, double & mech_energy)
{
  double C[9];
  calc_right_cauchy_green(F, C);
  double trace_C = C[0] + C[4] + C[8];
  mech_energy = my/2 * (trace_C - 3) - my * log(detF) + lambda/2 * log(detF) * log(detF);
}

void Neo_Hooke::initialize()
{
  
}

void Neo_Hooke::destroy()
{
  
}

void Guccione::calc_cauchy_stress(double * iF, double * F, double & detF, double * sigma)
{
  log_msg(0, 0, 0, "Calc Cauchy stress not implemented for Guccione yet.");
}

void Guccione::calc_energy(double * F, double & detF, double & mech_energy)
{
  // strain energy function: W = W_iso + W_vol; W_iso = C_Guccione/2 * (e^Q - 1); W_vol = k/2 * (J - 1)^2
  double E[9];
  calc_green_strain(F, E);
  double Q = b_f * E[0] * E[0] + b_t * (E[4] * E[4] + E[8] * E[8] + E[7] * E[7] + E[5] * E[5]) + b_fs * (E[3] * E[3] + E[1] * E[1] + E[6] * E[6] + E[2] * E[2]); // exponent
  mech_energy = C_Guccione / 2.0 * (exp(Q) - 1.0); /// iso
  mech_energy += k_vol / 2.0 * (detF - 1.0) * (detF - 1.0); /// vol
}

void Guccione::calc_PK2_stress(double * iF, double * F, double & detF, double * S_PK2)
{
//  double C[9];
  double iC[9];
  double detC;
  double E[9];
  calc_right_cauchy_green(F, iC); //here it's not the inverse yet
  SF::invert_3x3(iC, detC);
  calc_green_strain(F, E);
  double Q = b_f * E[0] * E[0] + b_t * (E[4] * E[4] + E[8] * E[8] + E[7] * E[7] + E[5] * E[5]) + b_fs * (E[3] * E[3] + E[1] * E[1] + E[6] * E[6] + E[2] * E[2]); // exponent
  double expQ = exp(Q);
  /// iso
  S_PK2[0] = C_Guccione * b_f * E[0] * expQ;
  S_PK2[1] = C_Guccione * b_fs * E[1] * expQ;
  S_PK2[2] = C_Guccione * b_fs * E[2] * expQ;
  S_PK2[3] = C_Guccione * b_fs * E[3] * expQ;
  S_PK2[4] = C_Guccione * b_t * E[4] * expQ;
  S_PK2[5] = C_Guccione * b_t * E[5] * expQ;
  S_PK2[6] = C_Guccione * b_fs * E[6] * expQ;
  S_PK2[7] = C_Guccione * b_t * E[7] * expQ;
  S_PK2[8] = C_Guccione * b_t * E[8] * expQ;
  
  /// vol
  S_PK2[0] += k_vol * (detF - 1.0) * detF * iC[0];
  S_PK2[1] += k_vol * (detF - 1.0) * detF * iC[1];
  S_PK2[2] += k_vol * (detF - 1.0) * detF * iC[2];
  S_PK2[3] += k_vol * (detF - 1.0) * detF * iC[3];
  S_PK2[4] += k_vol * (detF - 1.0) * detF * iC[4];
  S_PK2[5] += k_vol * (detF - 1.0) * detF * iC[5];
  S_PK2[6] += k_vol * (detF - 1.0) * detF * iC[6];
  S_PK2[7] += k_vol * (detF - 1.0) * detF * iC[7];
  S_PK2[8] += k_vol * (detF - 1.0) * detF * iC[8];
}

void Guccione::calc_PK1_stress(double * iF, double * F, double & detF, double * S_PK1)
{
  double E[9];
  calc_green_strain(F, E);
  double Q = b_f * E[0] * E[0] + b_t * (E[4] * E[4] + E[8] * E[8] + E[7] * E[7] + E[5] * E[5]) + b_fs * (E[3] * E[3] + E[1] * E[1] + E[6] * E[6] + E[2] * E[2]); // exponent
  double expQ = exp(Q);
  /// iso
  S_PK1[0] = C_Guccione * expQ * (b_f * F[0] * E[0] + b_fs * F[3] * E[1] + b_fs * F[6] * E[2]);
  S_PK1[1] = C_Guccione * expQ * (b_f * F[1] * E[0] + b_fs * F[4] * E[1] + b_fs * F[7] * E[2]);
  S_PK1[2] = C_Guccione * expQ * (b_f * F[2] * E[0] + b_fs * F[5] * E[1] + b_fs * F[8] * E[2]);
  S_PK1[3] = C_Guccione * expQ * (b_fs * F[0] * E[3] + b_t * F[3] * E[4] + b_t * F[6] * E[5]);
  S_PK1[4] = C_Guccione * expQ * (b_fs * F[1] * E[3] + b_t * F[4] * E[4] + b_t * F[7] * E[5]);
  S_PK1[5] = C_Guccione * expQ * (b_fs * F[2] * E[3] + b_t * F[5] * E[4] + b_t * F[8] * E[5]);
  S_PK1[6] = C_Guccione * expQ * (b_fs * F[0] * E[6] + b_t * F[3] * E[7] + b_t * F[6] * E[8]);
  S_PK1[7] = C_Guccione * expQ * (b_fs * F[1] * E[6] + b_t * F[4] * E[7] + b_t * F[7] * E[8]);
  S_PK1[8] = C_Guccione * expQ * (b_fs * F[2] * E[6] + b_t * F[5] * E[7] + b_t * F[8] * E[8]);
  
  
  /// vol
  double kJ = k_vol * detF * (detF - 1.0);
  S_PK1[0] += kJ * iF[0];
  S_PK1[1] += kJ * iF[3];
  S_PK1[2] += kJ * iF[6];
  S_PK1[3] += kJ * iF[1];
  S_PK1[4] += kJ * iF[4];
  S_PK1[5] += kJ * iF[7];
  S_PK1[6] += kJ * iF[2];
  S_PK1[7] += kJ * iF[5];
  S_PK1[8] += kJ * iF[8];
}

void Guccione::initialize()
{
  
}

void Guccione::destroy()
{
  
}
}  // namespace opencarp


