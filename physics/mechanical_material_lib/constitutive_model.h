// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file constitutive_model.h
* @brief Basic constitutive model.
* @author Jonathan Krauß
* @version 
* @date 2023-03-06
*/

#ifndef CONSTITUTIVE_MODEL_H
#define CONSTITUTIVE_MODEL_H

#include "basics.h"

namespace opencarp {

//// unit conversion
//#define UM2_to_CM2                1.0e-8              //!< convert um^2 to cm^2
//#define UM_to_CM                  1.0e-4              //!< convert um to cm
//#define UM_to_MM                  1.0e-3              //!< convert um to mm
//#define MM_to_UM                  1.0e3               //!< convert mm to um


/**
* @brief The abstract constitutive model interface we can use to trigger all constitutive models
*/
class Basic_constitutive_model {
public:
  /// The name of the constitutive model, each model should have one.
  const char* name = NULL;


  virtual void initialize() = 0;
  virtual void destroy()    = 0;
  /// Calculate 2nd Piola-Kirchhoff-Stress using deformation gradient F, its inverse iF and its determinant detF.
  /// Output is 2nd Piola-Kirchhoff-Stress in Voigt Notation S_PK2 = {S_PK2_11, S_PK2_22, S_PK2_33, S_PK2_12, S_PK2_13, S_PK2_23}
  virtual void calc_PK2_stress(double * iF, double * F, double & detF, double * S_PK2) = 0;
  /// Calculate 1st Piola-Kirchhoff-Stress using deformation gradient F, its inverse iF and its determinand detF
  /// Output is 1st Piola-Kirchhogg Stress with 9 entries (non-symmetric): S_PK1 = {S_PK1_11, S_PK1_21, S_PK1_31, S_PK1_12, S_PK1_22, S_PK1_32, S_PK1_31, S_PK1_32, S_PK1_33}
  virtual void calc_PK1_stress(double * iF, double * F, double & detF, double * S_PK1) = 0;
  /// Calculate Cauchy-Stress using deformation gradient F, its inverse iF and its determinant detF.
  /// Output is Cauchy-Stress in Voigt Notation sigma = {sigma_11, sigma_22, sigma_33, sigma_12, sigma_13, sigma_23}
  virtual void calc_cauchy_stress(double * iF, double * F, double & detF, double * sigma) = 0;
  /// Calculate mechanical energy using deformation gradient F and its determinant detF.
  /// Output is  mechanical energy mech_energy.
  virtual void calc_energy(double * F, double & detF, double & mech_energy) = 0;
  /// Set the parameters of the used constitutive model. Input is an array of the parameters in the correct order needed for the constitutive model.
  virtual void set_parameters(double * parameter_list) = 0;
//
};
}  // namespace opencarp

#endif
