// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file constitutive_model_library.h
* @brief Basic constitutive model.
* @author Jonathan Krauß
* @version 
* @date 2024-03-06
*/

#ifndef CONSTITUTIVE_MODEL_LIBRARY_H
#define CONSTITUTIVE_MODEL_LIBRARY_H

#include "constitutive_model.h"

namespace opencarp {



/**
* @brief Isotropic and compressible Neo-Hooke, simple basic material
 Equations are derived from Bonet et al., 2016, Nonlinear Solid Mechanics for Finite Element Analys
*/
class Neo_Hooke : public Basic_constitutive_model {
public:
  /// Material parameters
  double my = 0.0; ///< shear modulus
  double lambda = 0.0; ///< second Lamé parameter
//  double b[9];
  Neo_Hooke()
  {
      name = "Isotropic and compressible Neo-Hooke";
//      log_msg(0,0,0, "Material model %s intitialized", name);
  }

  void initialize();
  void destroy();
  void calc_PK1_stress(double * iF, double * F, double & detF, double * S_PK1);
  void calc_PK2_stress(double * iF, double * F, double & detF, double * S_PK2);
  void calc_cauchy_stress(double * iF, double * F, double & detF, double * sigma);
  void calc_energy(double * F, double & detF, double & mech_energy);
  inline void set_parameters(double * parameter_list)
  {
    my = parameter_list[0];
    lambda = parameter_list[1];
  }
  
  ~Neo_Hooke();
};

/**
* @brief Transverse isotropic and quasi-incompressible Guccione material
 Equations are derived from Guccione et al. 1995, Journal of Biomechanical Engineering
*/
class Guccione : public Basic_constitutive_model {
public:
  /// Material parameters (W = C/2 (e^Q - 1), Q = bf E11^2 + bt (E22^2 + E33^2 + E23^2 + E32^2) + bfs (E12^2 + E21^2 + E13^2 + E31^2)
  double C_Guccione = 0.0; ///< material constant (in kPa)
  double b_f = 0.0; ///< in fiber direction
  double b_t = 0.0; ///< in transverse direction
  double b_fs = 0.0; ///< fiber-cross-fiber
  double k_vol = 0.0; ///< incompressibility penalty
//  double b[9];
  Guccione()
  {
      name = "Guccione";
//      log_msg(0,0,0, "Material model %s intitialized", name);
  }

  void initialize();
  void destroy();
  void calc_PK1_stress(double * iF, double * F, double & detF, double * S_PK1);
  void calc_PK2_stress(double * iF, double * F, double & detF, double * S_PK2);
  void calc_cauchy_stress(double * iF, double * F, double & detF, double * sigma);
  void calc_energy(double * F, double & detF, double & mech_energy);
  inline void set_parameters(double * parameter_list)
  {
    C_Guccione = parameter_list[0];
    b_f = parameter_list[1];
    b_t = parameter_list[2];
    b_fs = parameter_list[3];
    k_vol = parameter_list[4];
  }
  
  ~Guccione();
};
}  // namespace opencarp

#endif
