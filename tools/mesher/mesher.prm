$gallocation = $static

$structure FiberDef {
	$member rotEndo {
		$type = $float,
		$s_desc = "fibre angle at the endocardium (minimum z value)",
		$default = 0.,
		$units = "degrees"
	}
	$member rotEpi {
		$type = $float,
		$s_desc = "fibre angle at the epicardium (maximum z value)",
		$default = 0.,
		$units = "degrees"
	}
	$member tm_fiber_profile {
		$type   = $string,
		$s_desc = "file holding fiber rotation as a function of relative transmural position",
		$default = ""
	}
	$member imbrication {
		$type = $float,
		$s_desc = "imbrication angle, typically 0 or smaller than 5 degrees",
		$default = 0.,
		$units = "degrees"
	}
	$member sheetEndo {
		$type = $float,
		$s_desc = "sheet angle at the endocardium (minimum z value)",
		$default = 0.,
		$units = "degrees"
	}
	$member sheetEpi {
		$type = $float,
		$s_desc = "sheet angle at the epicardium (maximum z value)",
		$default = 0.,
		$units = "degrees"
	}
	$member tm_sheet_profile {
		$type   = $string,
		$s_desc = "file holding sheet angle as a function of relative transmural position",
		$default = ""
	}
}

$structure RegionDef {
	$member p0 {
		$type = $float[3],
		$s_desc = "initial point",
		$units = "cm"
	}
	$member p1 {
		$type = $float[3],
		$s_desc = "second point",
		$units = "cm"
	}
    $member cyllen {
        $type   = $float,
        $s_desc = "length of cylinder (0=infinity)",
        $default = 0.,
        $min = 0.
    }
	$member rad {
		$type = $float,
		$s_desc = "radius",
		$min = 0,
		$units = "cm"
	}
	$member type {
		$type = $int,
		$s_desc = "how to define region",
		$menu = {
			0 = "block",
			1 = "sphere",
			2 = "cylinder"
		},
		$default = 0
	}
	$member bath {
		$type = $int,
        $menu = {
           0 = "no bath",
           1 = "isotropic bath",
           2 = "anisotropic bath"
        },
		$s_desc ="set for bath",
        $default = 0
	}
    $member tag {
        $type = $int,
        $s_desc = "tag ID",
        $default = -1
    }
}

$variable resolution {
	$type = $float[3],
	$s_desc = "resolution in each direction",
	$min = 0,
	$default = { 100., 100., 100 },
	$units = "micrometers"
}

$variable size {
	$type = $float[3],
	$s_desc = "size in each direction",
	$min = 0,
	$default = { 1., 1., 1. },
	$units = "cm"
}

$variable center {
	$type = $float[3],
	$s_desc = "offset of tissue center from origin of cartesian reference, by default tissue origin is (x=0,y=0,z=0)",
	$default = { 0., 0., 0. },
	$units = "cm"
}

$variable bath {
	$type = $float[3],
	$s_desc = "bath in each direction, -z=bath on both sides",
	$default = { 0., 0., 0. },
	$units = "cm"
}

$variable mesh {
	$type = $string,
	$s_desc = "basename of output mesh",
	$default = "mesh"
}

$variable fibers {
        $type = FiberDef,
        $s_desc = "Define tissue orthotropy"
}

$variable numRegions {
	$type = $int,
	$default = 0,
	$min = 0,
	$s_desc = "number of extra regions to add"
}

$variable regdef {
	$type = RegionDef[numRegions],
	$s_desc = "Region definitions"
}

$variable  first_reg {
  $type = $short,
  $s_desc = "stop when first region matches?",
  $default = 1
}

$variable perturb {
	$type = $float,
	$s_desc = "perturb interior points to make irregular mesh",
	$default = 0
}

$variable Elem3D {
	$type = $int,
	$default = 0,
	$min  = 0,
    $max  = 1,
	$s_desc = "Element type to generate: 0->Tets, 1->Hex"
}


$variable tri2D {
	$type = $flag,
	$s_desc = "Generate tris instead of quads in 2D"
}

$variable anisoBath {
	$type = $int,
	$default = 0,
	$min  = 0,
    $max  = 1,
	$s_desc = "make bath anisotropic: 0->isotropic bath, 1->anisotropic bath"
}

$variable periodic {
    $type = $int,
    $s_desc = "simulate periodic b.c.s by connecting edges with linear elements",
    $default = 0,
    $menu = {
        0 = "None",
        1 = "X",
        2 = "Y",
        3 = "XY"
    }
}

$variable periodic_tag  {
    $type = $int,
    $s_desc = "periodic connections region tag",
    $default = 1234,
    $min = 0
}
