# The Open Cardiac Electrophysiology Simulator

## Features
The current capabilities of openCARP are:
* CARP mesh format support.
* Parmetis and KDtree partitioning.
* The former `LIMPET/bench` functionality is offered by `physics/limpet/bench`.
* Full Monodomain and Bidomain support.
* Full PETSc solvers support.
* Linear solvers output solving statistics into their respective log files.
* Stimulation protocols can be specifed and viewed in protocol trace files.
* Mesh and surface outputting.
* Checkpointing.
* Adding region tags via tagreg parameters.
* Experiments 0 to 4.
* LAT detection.
* Prepacing.
* Global IMP state variable output.
* Pseudo-Bidomain.
* Phie recovery.
* Laplace solve.
* Electrode balancing.

Things that dont work yet but will be included:
* Swapping out fibers with `-orthoname`.

## Differences between openCARP and CARP/CARPentry

See [docs/CARP_CARPENTRY_USER.md](https://git.opencarp.org/openCARP/openCARP/blob/master/docs/CARP_CARPENTRY_USER.md)

## Installation

See [docs/INSTALL.md](https://git.opencarp.org/openCARP/openCARP/blob/master/docs/INSTALL.md)

## Building from Source

See [docs/BUILD.md](https://git.opencarp.org/openCARP/openCARP/blob/master/docs/BUILD.md)

## Testing

To be added

## Contributing
You are here to help? Awesome, feel welcome and read [CONTRIBUTING.md](https://opencarp.org/community/contribute) to learn how to ask questions and how to work on something and our process for submitting merge requests to the openCARP project.
All members of our community are expected to follow our [Code of Conduct](https://opencarp.org/community/code-of-conduct). Please make sure you are welcoming and friendly in all of our spaces.

## Authors
See our [list of contributors](https://opencarp.org/community/contributors).

## License
This project is licensed under the Academic Public License - see the [LICENSE.md](https://opencarp.org/download/license) file for details.

## Acknowledgments
openCARP builds on
* [CMake](https://cmake.org)
* [Docker](https://www.docker.com)
* [gengetopt](https://www.gnu.org/software/gengetopt/gengetopt.html)
* [meshtool](https://bitbucket.org/aneic/meshtool/src/master/)
* [PETSc](https://www.mcs.anl.gov/petsc/)
* [VTK](https://vtk.org)

