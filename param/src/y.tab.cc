/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Copy the first part of user declarations.  */

/*---------------------------------------------------------------------------*\

    param.y:	Gramatical structure of param.

    Author:	Andre Bleau, eng.

    Laboratoire de Modelisation Biomedicale
    Institut de Genie Biomedical
    Ecole Polytechnique / Faculte de Medecine
    Universite de Montreal

    Revision:	November 12, 1998

\*---------------------------------------------------------------------------*/

#include <stdio.h>
#include "param.h"

int yyerror(const char* s);
int yylex();

#ifdef NOGETTXT
#define gettxt(a, b) b
#endif

extern Boolean    parser_error;
extern Parameters parameters;

#ifndef YY_NULLPTR
#if defined __cplusplus && 201103L <= __cplusplus
#define YY_NULLPTR nullptr
#else
#define YY_NULLPTR 0
#endif
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
#undef YYERROR_VERBOSE
#define YYERROR_VERBOSE 1
#else
#define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "y.tab.h".  */
#ifndef YY_YY_Y_TAB_H_INCLUDED
#define YY_YY_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
#define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
#define YYTOKENTYPE
enum yytokentype {
  MEMBER          = 258,
  PARAMGRP        = 259,
  PARAMSGRP       = 260,
  STRUCTURE       = 261,
  VARIABLE        = 262,
  LOCATION        = 263,
  ALLOCATION      = 264,
  AUXILIARY       = 265,
  DEFAULT         = 266,
  DIR             = 267,
  DISPLAY         = 268,
  EXT             = 269,
  HIDDEN          = 270,
  KEY             = 271,
  KIND            = 272,
  LENGTH          = 273,
  MANDATORY       = 274,
  MAX             = 275,
  MENU            = 276,
  MIN             = 277,
  OUTPUT          = 278,
  S_DESC          = 279,
  TYPE            = 280,
  UNITS           = 281,
  VALIDF          = 282,
  VECTOR          = 283,
  VIEW            = 284,
  VOLATIL         = 285,
  END_FUNCTION    = 286,
  GALLOCATION     = 287,
  GS_DESC         = 288,
  GVALIDF         = 289,
  ICON            = 290,
  TRAIL_ARGS      = 291,
  ACEGR_CURV      = 292,
  ACEGR_HIST      = 293,
  ALLOCATION_TYPE = 294,
  C_EXP           = 295,
  E_TYPE          = 296,
  FATHER          = 297,
  FUNCTIOND       = 298,
  INDEX           = 299,
  VIEW_TYPE       = 300,
  LEFTPAREN       = 301,
  RIGHTPAREN      = 302,
  LEFTBRACKET     = 303,
  RIGHTBRACKET    = 304,
  LEFTBRACE       = 305,
  RIGHTBRACE      = 306,
  EQUAL           = 307,
  COMA            = 308,
  POINT           = 309,
  ARROW           = 310,
  DEF_POUND       = 311,
  DEC_POUND       = 312,
  IDENTIFIER      = 313,
  BNUM            = 314,
  INUM            = 315,
  FNUM            = 316,
  CNUM            = 317,
  PRMID           = 318,
  STR             = 319,
  UNKNOWN_KEYWORD = 320,
  GL_DESC         = 321,
  L_DESC          = 322,
  INC_CODE        = 323,
  TEXT            = 324,
  TEXTBOX         = 325
};
#endif

/* Value type.  */
#if !defined YYSTYPE && !defined YYSTYPE_IS_DECLARED

union YYSTYPE {
  int                 ival;
  char*               str;
  struct Arg*         arg;
  struct Description* description;
  struct Descriptor*  descriptor;
  struct Function*    function;
  struct Member*      member;
  struct Paramgrp*    paramgrp;
  struct Structure*   structure;
  struct Value*       value;
  struct Variable*    variable;
  Text                text;
};

typedef union YYSTYPE YYSTYPE;
#define YYSTYPE_IS_TRIVIAL  1
#define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE yylval;

int yyparse(void);

#endif /* !YY_YY_Y_TAB_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#ifdef short
#undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char      yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char        yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int          yytype_int16;
#endif

#ifndef YYSIZE_T
#ifdef __SIZE_TYPE__
#define YYSIZE_T __SIZE_TYPE__
#elif defined size_t
#define YYSIZE_T size_t
#elif !defined YYSIZE_T
#include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#define YYSIZE_T size_t
#else
#define YYSIZE_T unsigned int
#endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T)-1)

#ifndef YY_
#if defined YYENABLE_NLS && YYENABLE_NLS
#if ENABLE_NLS
#include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#define YY_(Msgid) dgettext("bison-runtime", Msgid)
#endif
#endif
#ifndef YY_
#define YY_(Msgid) Msgid
#endif
#endif

#ifndef YY_ATTRIBUTE
#if (defined __GNUC__ && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__))) || \
    defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#define YY_ATTRIBUTE(Spec) __attribute__(Spec)
#else
#define YY_ATTRIBUTE(Spec) /* empty */
#endif
#endif

#ifndef YY_ATTRIBUTE_PURE
#define YY_ATTRIBUTE_PURE YY_ATTRIBUTE((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
#define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE((__unused__))
#endif

#if !defined _Noreturn && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
#if defined _MSC_VER && 1200 <= _MSC_VER
#define _Noreturn __declspec(noreturn)
#else
#define _Noreturn YY_ATTRIBUTE((__noreturn__))
#endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if !defined lint || defined __GNUC__
#define YYUSE(E) ((void)(E))
#else
#define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                                            \
  _Pragma("GCC diagnostic push") _Pragma("GCC diagnostic ignored \"-Wuninitialized\"") \
      _Pragma("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
#define YY_IGNORE_MAYBE_UNINITIALIZED_END _Pragma("GCC diagnostic pop")
#else
#define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
#define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
#define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
#define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if !defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

#ifdef YYSTACK_USE_ALLOCA
#if YYSTACK_USE_ALLOCA
#ifdef __GNUC__
#define YYSTACK_ALLOC __builtin_alloca
#elif defined __BUILTIN_VA_ARG_INCR
#include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#elif defined _AIX
#define YYSTACK_ALLOC __alloca
#elif defined _MSC_VER
#include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#define alloca _alloca
#else
#define YYSTACK_ALLOC alloca
#if !defined _ALLOCA_H && !defined EXIT_SUCCESS
#include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
/* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#ifndef EXIT_SUCCESS
#define EXIT_SUCCESS 0
#endif
#endif
#endif
#endif
#endif

#ifdef YYSTACK_ALLOC
/* Pacify GCC's 'empty if-body' warning.  */
#define YYSTACK_FREE(Ptr) \
  do { /* empty */        \
    ;                     \
  } while (0)
#ifndef YYSTACK_ALLOC_MAXIMUM
/* The OS might guarantee only one guard page at the bottom of the stack,
   and a page size can be as small as 4096 bytes.  So we cannot safely
   invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
   to allow for a few compiler-allocated temporary stack slots.  */
#define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#endif
#else
#define YYSTACK_ALLOC YYMALLOC
#define YYSTACK_FREE  YYFREE
#ifndef YYSTACK_ALLOC_MAXIMUM
#define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#endif
#if (defined __cplusplus && !defined EXIT_SUCCESS && \
     !((defined YYMALLOC || defined malloc) && (defined YYFREE || defined free)))
#include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#ifndef EXIT_SUCCESS
#define EXIT_SUCCESS 0
#endif
#endif
#ifndef YYMALLOC
#define YYMALLOC malloc
#if !defined malloc && !defined EXIT_SUCCESS
void* malloc(YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#endif
#endif
#ifndef YYFREE
#define YYFREE free
#if !defined free && !defined EXIT_SUCCESS
void  free(void*);      /* INFRINGES ON USER NAME SPACE */
#endif
#endif
#endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */

#if (!defined yyoverflow && \
     (!defined __cplusplus || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc {
  yytype_int16 yyss_alloc;
  YYSTYPE      yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
#define YYSTACK_GAP_MAXIMUM (sizeof(union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
#define YYSTACK_BYTES(N) ((N) * (sizeof(yytype_int16) + sizeof(YYSTYPE)) + YYSTACK_GAP_MAXIMUM)

#define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
#define YYSTACK_RELOCATE(Stack_alloc, Stack)                         \
  do {                                                               \
    YYSIZE_T yynewbytes;                                             \
    YYCOPY(&yyptr->Stack_alloc, Stack, yysize);                      \
    Stack      = &yyptr->Stack_alloc;                                \
    yynewbytes = yystacksize * sizeof(*Stack) + YYSTACK_GAP_MAXIMUM; \
    yyptr += yynewbytes / sizeof(*yyptr);                            \
  } while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
#ifndef YYCOPY
#if defined __GNUC__ && 1 < __GNUC__
#define YYCOPY(Dst, Src, Count) __builtin_memcpy(Dst, Src, (Count) * sizeof(*(Src)))
#else
#define YYCOPY(Dst, Src, Count)                                  \
  do {                                                           \
    YYSIZE_T yyi;                                                \
    for (yyi = 0; yyi < (Count); yyi++) (Dst)[yyi] = (Src)[yyi]; \
  } while (0)
#endif
#endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL 64
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST 487

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS 71
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS 117
/* YYNRULES -- Number of rules.  */
#define YYNRULES 249
/* YYNSTATES -- Number of states.  */
#define YYNSTATES 375

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK 2
#define YYMAXUTOK  325

#define YYTRANSLATE(YYX) ((unsigned int)(YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] = {
    0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 2, 3, 4, 5, 6, 7, 8,
    9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32,
    33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56,
    57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70};

#if YYDEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] = {
    0, 88, 88, 100, 103, 108, 109, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122,
    125, 127, 131, 135, 139, 143, 148, 152, 156, 160, 164, 168, 172, 176, 178, 182, 186, 190,
    194, 199, 203, 206, 211, 213, 215, 217, 219, 221, 223, 225, 229, 231, 233, 235, 237, 239,
    241, 243, 245, 249, 253, 257, 261, 266, 270, 274, 276, 280, 282, 287, 291, 295, 299, 301,
    303, 307, 309, 311, 313, 315, 319, 320, 321, 322, 323, 324, 325, 326, 327, 328, 329, 330,
    331, 332, 333, 334, 335, 336, 337, 338, 339, 340, 343, 347, 351, 355, 360, 362, 366, 370,
    371, 373, 375, 377, 379, 381, 383, 385, 387, 391, 393, 397, 401, 403, 407, 409, 413, 417,
    419, 423, 427, 431, 432, 434, 436, 438, 442, 444, 447, 452, 456, 460, 464, 468, 472, 477,
    481, 485, 489, 494, 498, 502, 503, 505, 507, 509, 511, 513, 517, 521, 525, 527, 531, 533,
    535, 537, 540, 541, 543, 545, 547, 551, 555, 559, 561, 563, 567, 569, 572, 575, 578, 581,
    586, 590, 594, 596, 600, 603, 607, 611, 615, 619, 621, 623, 625, 627, 631, 632, 634, 636,
    638, 642, 646, 650, 654, 658, 663, 668, 669, 672, 673, 675, 677, 679, 681, 683, 685, 687,
    689, 693, 695, 699, 703, 708, 712, 716, 720, 722, 726, 728, 729, 731, 733, 734, 736, 737,
    739, 740, 743, 747, 751, 753, 757, 762, 766, 770, 775, 779, 783, 787, 791, 794};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char* const yytname[] = {"$end",
                                      "error",
                                      "$undefined",
                                      "MEMBER",
                                      "PARAMGRP",
                                      "PARAMSGRP",
                                      "STRUCTURE",
                                      "VARIABLE",
                                      "LOCATION",
                                      "ALLOCATION",
                                      "AUXILIARY",
                                      "DEFAULT",
                                      "DIR",
                                      "DISPLAY",
                                      "EXT",
                                      "HIDDEN",
                                      "KEY",
                                      "KIND",
                                      "LENGTH",
                                      "MANDATORY",
                                      "MAX",
                                      "MENU",
                                      "MIN",
                                      "OUTPUT",
                                      "S_DESC",
                                      "TYPE",
                                      "UNITS",
                                      "VALIDF",
                                      "VECTOR",
                                      "VIEW",
                                      "VOLATIL",
                                      "END_FUNCTION",
                                      "GALLOCATION",
                                      "GS_DESC",
                                      "GVALIDF",
                                      "ICON",
                                      "TRAIL_ARGS",
                                      "ACEGR_CURV",
                                      "ACEGR_HIST",
                                      "ALLOCATION_TYPE",
                                      "C_EXP",
                                      "E_TYPE",
                                      "FATHER",
                                      "FUNCTIOND",
                                      "INDEX",
                                      "VIEW_TYPE",
                                      "LEFTPAREN",
                                      "RIGHTPAREN",
                                      "LEFTBRACKET",
                                      "RIGHTBRACKET",
                                      "LEFTBRACE",
                                      "RIGHTBRACE",
                                      "EQUAL",
                                      "COMA",
                                      "POINT",
                                      "ARROW",
                                      "DEF_POUND",
                                      "DEC_POUND",
                                      "IDENTIFIER",
                                      "BNUM",
                                      "INUM",
                                      "FNUM",
                                      "CNUM",
                                      "PRMID",
                                      "STR",
                                      "UNKNOWN_KEYWORD",
                                      "GL_DESC",
                                      "L_DESC",
                                      "INC_CODE",
                                      "TEXT",
                                      "TEXTBOX",
                                      "$accept",
                                      "parameters",
                                      "def_section",
                                      "definition_list",
                                      "definition",
                                      "def_line_spec",
                                      "def_pound_word",
                                      "end_function",
                                      "end_func_word",
                                      "func_declare",
                                      "functiond_word",
                                      "funcd_id_list",
                                      "gallocation",
                                      "gallocation_word",
                                      "gl_desc",
                                      "gs_desc",
                                      "gs_desc_word",
                                      "string",
                                      "gvalidf",
                                      "gvalidf_word",
                                      "function",
                                      "func_id",
                                      "arg_list",
                                      "argument",
                                      "lvalue",
                                      "icon",
                                      "icon_word",
                                      "inc_code",
                                      "structure",
                                      "struct_word",
                                      "struct_id",
                                      "member_list",
                                      "member",
                                      "member_word",
                                      "mem_id",
                                      "description",
                                      "desc_list",
                                      "one_descriptor",
                                      "descriptor",
                                      "allocation",
                                      "allocation_word",
                                      "auxiliary",
                                      "hidden",
                                      "default",
                                      "default_word",
                                      "value",
                                      "c_exp",
                                      "c_exp_word",
                                      "dependent_list",
                                      "dependent",
                                      "vector",
                                      "value_list",
                                      "dir",
                                      "dir_word",
                                      "str_value",
                                      "display",
                                      "display_word",
                                      "ext",
                                      "ext_word",
                                      "key",
                                      "key_word",
                                      "kind",
                                      "l_desc",
                                      "length",
                                      "length_word",
                                      "mandatory",
                                      "max",
                                      "max_word",
                                      "comp_value",
                                      "menu",
                                      "menu_word",
                                      "item_list",
                                      "item",
                                      "bool_value",
                                      "value_vector",
                                      "vector_word",
                                      "vector_desc",
                                      "indexable",
                                      "min",
                                      "min_word",
                                      "output",
                                      "output_word",
                                      "s_desc",
                                      "s_desc_word",
                                      "type",
                                      "type_word",
                                      "type_value",
                                      "nelem",
                                      "units",
                                      "units_word",
                                      "validf",
                                      "validf_word",
                                      "volatil",
                                      "trail_args",
                                      "declaration_list",
                                      "declaration",
                                      "dec_line_spec",
                                      "dec_pound_word",
                                      "paramgrp",
                                      "paramgrp_word",
                                      "pgrp_id",
                                      "paramgrp_desc",
                                      "pgrp_desc_list",
                                      "pgrp_desc",
                                      "paramsgrp",
                                      "paramsgrp_word",
                                      "id_list",
                                      "variable",
                                      "variable_word",
                                      "var_id_list",
                                      "location",
                                      "location_word",
                                      "loc_id_list",
                                      "lvalue_list",
                                      "textbox",
                                      "view",
                                      "view_word",
                                      YY_NULLPTR};
#endif

#ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] = {
    0, 256, 257, 258, 259, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272,
    273, 274, 275, 276, 277, 278, 279, 280, 281, 282, 283, 284, 285, 286, 287, 288, 289, 290,
    291, 292, 293, 294, 295, 296, 297, 298, 299, 300, 301, 302, 303, 304, 305, 306, 307, 308,
    309, 310, 311, 312, 313, 314, 315, 316, 317, 318, 319, 320, 321, 322, 323, 324, 325};
#endif

#define YYPACT_NINF -274

#define yypact_value_is_default(Yystate) (!!((Yystate) == (-274)))

#define YYTABLE_NINF -197

#define yytable_value_is_error(Yytable_value) 0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int16 yypact[] = {
    265, -274, -274, -274, -274, -274, -274, -274, -274, -274, -274, -274, -274, -274, -274, -274,
    -274, -274, -274, -274, -274, -274, 36, 310, 22, -274, -274, -35, -274, -6, -274, 1,
    -274, 18, -274, -274, 28, -274, 40, -274, 43, -274, -274, 44, -274, -274, -274, 76,
    -274, 79, -274, 310, -274, -274, 93, -274, 115, -274, 1, -274, 25, -274, -274, 126,
    -274, 310, -274, 105, 132, -274, 168, 172, 205, 184, 132, 192, -274, 211, 184, 132,
    -274, 198, -274, 213, 172, 232, 233, 112, 138, 232, -274, 222, 235, -274, -274, 244,
    261, 245, -274, -274, 241, -274, -274, 30, 241, -274, 247, 336, -274, 190, -274, 252,
    37, 253, 254, 57, 257, 258, -274, -274, -274, 337, -274, 268, 271, -274, -274, -274,
    -274, 20, -274, 262, -274, -274, -274, -274, -274, -274, -274, 321, -274, -274, 1, -274,
    -274, -274, -274, 256, -274, -274, -274, -274, -274, -274, -274, -274, -274, -274, -274, -274,
    -274, -274, -274, -274, 420, 69, -274, 141, -274, 272, -274, -274, -274, 275, -274, 278,
    -274, 286, -274, 288, -274, 290, -274, -274, -274, 294, -274, -274, 295, -274, 297, -274,
    300, -274, 301, -274, -274, -274, 302, -274, -274, 420, -274, -274, 306, 307, -274, -274,
    308, 309, -274, -274, -274, -274, 112, -274, -274, -274, -274, 241, -13, -274, 138, -274,
    2, -274, -274, -274, 232, -274, -274, 172, -274, -274, -274, 398, -274, -274, 320, 191,
    49, 26, 49, 303, 304, 54, 312, 54, 416, 49, -274, -274, -274, -274, -274, -274,
    97, 75, 318, -274, -274, -274, -274, -274, -274, 416, 117, -274, -274, -274, -274, 241,
    -274, 138, -274, -274, 319, -274, 117, 241, -274, 138, -274, -274, -274, -274, -274, -274,
    -274, -274, -274, 117, -274, -274, -274, 138, -274, -274, 90, -274, -274, -274, -274, 182,
    311, 182, -274, 111, 322, -274, 316, 181, -274, -274, 323, 210, -274, -274, 138, -274,
    325, 331, 327, -274, 416, 185, 49, -274, 90, 329, -274, -274, -274, -274, -274, 63,
    333, -274, 317, 334, 112, 138, 217, -274, 87, 77, -274, -274, 63, 175, -274, -274,
    138, -274, 339, 338, 140, 173, 341, -274, -274, 313, 342, 344, 346, 347, 77, 223,
    226, 230, 240, 251, 373, 77, -274};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] = {
    0, 221, 61, 241, 244, 103, 186, 202, 249, 22, 27, 30, 34, 58, 204, 24, 20, 219, 28,
    143, 59, 247, 0, 0, 4, 5, 7, 0, 8, 0, 9, 0, 10, 0, 11, 12, 0, 13,
    0, 14, 0, 15, 16, 0, 216, 208, 211, 0, 213, 0, 17, 3, 205, 207, 0, 210, 0,
    214, 0, 209, 0, 212, 215, 0, 1, 2, 6, 0, 0, 238, 0, 25, 0, 0, 0, 0,
    62, 0, 0, 0, 206, 0, 222, 0, 242, 0, 0, 0, 246, 0, 245, 0, 19, 37, 21,
    0, 0, 0, 26, 31, 29, 33, 57, 0, 185, 201, 218, 0, 220, 0, 240, 0, 0, 0,
    0, 0, 0, 0, 243, 248, 18, 0, 188, 0, 0, 239, 32, 67, 65, 0, 63, 0, 217,
    237, 235, 227, 230, 232, 226, 0, 224, 229, 0, 233, 228, 231, 234, 0, 101, 102, 106, 128,
    137, 139, 141, 142, 145, 146, 148, 157, 181, 184, 200, 203, 0, 0, 70, 77, 78, 0, 79,
    84, 80, 0, 81, 0, 82, 0, 83, 0, 85, 0, 86, 87, 88, 0, 89, 90, 0, 91,
    0, 92, 0, 93, 183, 94, 95, 96, 0, 97, 99, 0, 98, 53, 0, 0, 52, 55, 0,
    0, 54, 56, 47, 36, 41, 43, 44, 45, 46, 42, 0, 38, 40, 23, 0, 60, 64, 68,
    0, 223, 225, 236, 72, 73, 69, 0, 75, 76, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 74, 50, 48, 51, 49, 35, 0, 189, 192, 187, 66, 71, 100, 119, 113, 0,
    114, 109, 110, 111, 112, 108, 116, 115, 104, 107, 0, 105, 131, 130, 133, 132, 129, 127, 135,
    136, 134, 138, 140, 144, 152, 153, 150, 151, 155, 154, 149, 147, 0, 180, 182, 199, 39, 0,
    0, 0, 125, 0, 0, 170, 162, 0, 158, 163, 0, 37, 195, 198, 197, 194, 0, 0, 0,
    124, 0, 0, 0, 156, 0, 0, 190, 191, 193, 126, 117, 0, 161, 159, 0, 0, 122, 123,
    0, 120, 0, 0, 169, 118, 0, 37, 165, 168, 167, 164, 0, 0, 174, 0, 173, 121, 160,
    0, 0, 0, 0, 0, 0, 53, 52, 55, 54, 56, 172, 0, 171};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] = {
    -274, -274, -274, -274, 363, -99, -274, -274, -274, -274, -274, -274, -274, -274, -274,
    -274, -274, -72, -274, -274, -66, -274, -274, 195, -60, -274, -274, -274, -274, -274,
    -274, -274, 324, -274, -274, -86, -274, 224, -126, -274, -274, -274, -102, -274, -274,
    -213, -218, -274, -274, 100, -274, -274, -274, -274, -210, -274, -274, -274, -274, -274,
    -274, -274, -98, -274, -274, -274, -274, -274, 214, -274, -274, -274, 124, -274, -274,
    -274, -274, -273, -274, -274, -274, -274, -95, -274, 361, -274, -274, 157, -274, -274,
    -92, -274, -274, -274, 440, 73, -88, -274, -274, -274, -274, -274, -274, 328, -274,
    -274, -38, -65, -274, -274, -62, -274, -274, -274, -57, -91, -274};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] = {
    -1, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 70, 32, 33, 34, 35, 36,
    271, 37, 38, 272, 95, 220, 221, 273, 39, 40, 41, 42, 43, 77, 129, 130, 131,
    228, 110, 165, 166, 167, 168, 169, 170, 44, 172, 173, 310, 275, 276, 342, 343, 277,
    307, 174, 175, 283, 176, 177, 178, 179, 180, 181, 182, 45, 184, 185, 186, 187, 188,
    297, 189, 190, 311, 312, 354, 313, 314, 339, 358, 191, 192, 193, 194, 46, 47, 196,
    124, 259, 320, 197, 198, 48, 49, 200, 50, 51, 52, 53, 54, 55, 56, 83, 108,
    139, 140, 141, 142, 71, 57, 58, 85, 59, 60, 89, 90, 61, 62, 63};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] = {
    88, 100, 94, 118, 128, 134, 104, 171, 101, 135, 164, 183, 136, 105, 195, 137, 146,
    199, 202, 138, 84, 201, 282, 127, 282, 67, 274, 296, 2, 296, 128, 282, 287, 127,
    255, 300, 64, 134, 233, 301, 256, 135, 143, 257, 136, 144, 68, 137, 146, 219, 145,
    138, 306, 9, 10, 11, 12, 13, 14, 69, 258, 222, 171, 284, 285, 15, 183, 86,
    236, 195, 72, 225, 199, 202, 143, 250, 16, 144, 16, 237, 73, 204, 145, 87, 93,
    319, 16, 319, 18, 263, 20, 86, 74, 372, 263, 75, 86, 205, 290, 171, 374, 208,
    76, 183, 231, 86, 195, 278, 282, 199, 202, 333, 291, 99, 292, 293, 336, 209, 309,
    355, 234, 340, 235, 303, 80, 304, 353, 263, 78, 86, 263, 79, 86, 171, 264, 356,
    164, 183, 80, 86, 195, 212, 260, 199, 202, 349, 350, 201, 266, 267, 268, 269, 270,
    81, 99, 214, 215, 216, 217, 218, 112, 99, 323, -37, 324, 112, 113, 114, 279, 92,
    279, 113, 114, 82, 280, 286, 280, 279, 91, 294, 281, 294, 281, 280, 219, 295, 115,
    295, 112, 281, 93, 147, 116, 117, 362, 363, 222, 16, 17, 148, 149, 150, 151, 152,
    153, 5, 154, 155, 156, 157, 158, 159, 160, 161, 6, 122, 162, 7, 96, 8, 163,
    115, 263, 112, 86, 97, -166, 364, 365, 113, 114, 263, 327, 86, 328, 264, 334, 317,
    335, 317, 315, 265, 316, 318, 98, 318, 16, 17, 99, 266, 267, 268, 269, 270, 279,
    99, 102, 19, 112, -196, 280, 103, 106, 107, 113, 114, 281, 119, 347, 1, 348, 2,
    3, 4, -176, 341, -176, -175, 351, -175, 5, -178, 109, -178, 352, 357, 122, 111, 341,
    6, 121, -177, 7, -177, 8, 120, 9, 10, 11, 12, 13, 14, -179, 125, -179, 126,
    357, 132, 15, 232, 203, 206, 207, 357, 1, 210, 211, 3, 4, 223, 227, 16, 17,
    224, 238, 5, 133, 239, 3, 4, 240, 18, 19, 20, 6, 21, 5, 7, 241, 8,
    242, 133, 243, 3, 4, 6, 244, 245, 7, 246, 8, 5, 247, 248, 249, 251, 252,
    253, 254, 262, 6, 288, 298, 7, 289, 8, 305, 17, 326, 308, 345, 367, 229, 329,
    330, 321, 332, 19, 17, 86, 21, 212, 331, 344, 213, 346, 325, 66, 19, 338, 360,
    21, 361, 17, 366, 214, 215, 216, 217, 218, 368, 99, 369, 19, 370, 371, 21, 148,
    149, 150, 151, 152, 153, 5, 154, 155, 156, 157, 158, 159, 160, 161, 6, 122, 162,
    7, 373, 8, 163, 148, 149, 150, 151, 152, 153, 5, 154, 155, 156, 157, 158, 159,
    160, 161, 6, 122, 162, 7, 359, 8, 163, 302, 337, 226, 16, 17, 263, 123, 86,
    261, 264, 299, 322, 65, 0, 19, 0, 230, 0, 0, 0, 0, 0, 0, 266, 267,
    268, 269, 270, 0, 99, 0, 0, 0, 0, 0, 0, 19};

static const yytype_int16 yycheck[] = {
    60, 73, 68, 89, 103, 107, 78, 109, 74, 107, 109, 109, 107, 79, 109, 107, 107, 109, 109,
    107, 58, 109, 240, 3, 242, 60, 239, 245, 6, 247, 129, 249, 242, 3, 47, 248, 0, 139,
    164, 249, 53, 139, 107, 41, 139, 107, 52, 139, 139, 121, 107, 139, 265, 31, 32, 33, 34,
    35, 36, 58, 58, 121, 164, 37, 38, 43, 164, 42, 167, 164, 52, 51, 164, 164, 139, 201,
    56, 139, 56, 167, 52, 44, 139, 58, 58, 303, 56, 305, 66, 40, 68, 42, 52, 366, 40,
    52, 42, 60, 44, 201, 373, 44, 58, 201, 142, 42, 201, 58, 326, 201, 201, 324, 58, 64,
    60, 61, 326, 60, 28, 42, 51, 58, 53, 48, 51, 50, 344, 40, 52, 42, 40, 52, 42,
    235, 44, 58, 235, 235, 65, 42, 235, 44, 228, 235, 235, 58, 59, 235, 58, 59, 60, 61,
    62, 60, 64, 58, 59, 60, 61, 62, 48, 64, 51, 46, 53, 48, 54, 55, 240, 64, 242,
    54, 55, 58, 240, 241, 242, 249, 52, 245, 240, 247, 242, 249, 256, 245, 48, 247, 48, 249,
    58, 1, 54, 55, 54, 55, 256, 56, 57, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
    19, 20, 21, 22, 23, 24, 25, 26, 27, 50, 29, 30, 48, 40, 48, 42, 53, 51, 54,
    55, 54, 55, 40, 51, 42, 53, 44, 51, 303, 53, 305, 58, 50, 60, 303, 39, 305, 56,
    57, 64, 58, 59, 60, 61, 62, 326, 64, 64, 67, 48, 49, 326, 50, 64, 50, 54, 55,
    326, 45, 51, 4, 53, 6, 7, 8, 51, 335, 53, 51, 344, 53, 15, 51, 50, 53, 344,
    345, 25, 54, 348, 24, 46, 51, 27, 53, 29, 60, 31, 32, 33, 34, 35, 36, 51, 58,
    53, 64, 366, 60, 43, 53, 58, 58, 58, 373, 4, 58, 58, 7, 8, 51, 58, 56, 57,
    52, 52, 15, 5, 52, 7, 8, 52, 66, 67, 68, 24, 70, 15, 27, 52, 29, 52, 5,
    52, 7, 8, 24, 52, 52, 27, 52, 29, 15, 52, 52, 52, 49, 49, 49, 49, 39, 24,
    58, 50, 27, 60, 29, 48, 57, 52, 50, 53, 58, 51, 50, 49, 64, 49, 67, 57, 42,
    70, 44, 51, 50, 47, 51, 64, 24, 67, 60, 51, 70, 54, 57, 53, 58, 59, 60, 61,
    62, 58, 64, 58, 67, 58, 58, 70, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
    20, 21, 22, 23, 24, 25, 26, 27, 53, 29, 30, 9, 10, 11, 12, 13, 14, 15, 16,
    17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 348, 29, 30, 256, 328, 129, 56, 57,
    40, 96, 42, 235, 44, 247, 305, 23, -1, 67, -1, 139, -1, -1, -1, -1, -1, -1, 58,
    59, 60, 61, 62, -1, 64, -1, -1, -1, -1, -1, -1, 67};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] = {
    0, 4, 6, 7, 8, 15, 24, 27, 29, 31, 32, 33, 34, 35, 36, 43, 56, 57, 66,
    67, 68, 70, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 83, 84, 85, 86, 87, 89,
    90, 96, 97, 98, 99, 100, 113, 133, 153, 154, 161, 162, 164, 165, 166, 167, 168, 169, 170,
    178, 179, 181, 182, 185, 186, 187, 0, 165, 75, 60, 52, 58, 82, 177, 52, 52, 52, 52,
    58, 101, 52, 52, 166, 60, 58, 171, 177, 180, 42, 58, 95, 183, 184, 52, 64, 58, 91,
    92, 50, 53, 39, 64, 88, 91, 64, 50, 88, 91, 64, 50, 172, 50, 106, 54, 48, 54,
    55, 48, 54, 55, 106, 45, 60, 46, 25, 155, 156, 58, 64, 3, 76, 102, 103, 104, 60,
    5, 113, 133, 153, 161, 167, 173, 174, 175, 176, 178, 181, 185, 186, 1, 9, 10, 11, 12,
    13, 14, 16, 17, 18, 19, 20, 21, 22, 23, 26, 30, 76, 107, 108, 109, 110, 111, 112,
    113, 114, 115, 123, 124, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 140,
    141, 149, 150, 151, 152, 153, 155, 159, 160, 161, 163, 167, 186, 58, 44, 60, 58, 58, 44,
    60, 58, 58, 44, 47, 58, 59, 60, 61, 62, 88, 93, 94, 95, 51, 52, 51, 103, 58,
    105, 51, 174, 177, 53, 109, 51, 53, 76, 167, 52, 52, 52, 52, 52, 52, 52, 52, 52,
    52, 52, 52, 109, 49, 49, 49, 49, 47, 53, 41, 58, 157, 106, 108, 39, 40, 44, 50,
    58, 59, 60, 61, 62, 88, 91, 95, 116, 117, 118, 121, 58, 88, 91, 95, 117, 125, 37,
    38, 91, 125, 58, 60, 44, 58, 60, 61, 91, 95, 117, 139, 50, 139, 116, 125, 94, 48,
    50, 48, 116, 122, 50, 28, 116, 142, 143, 145, 146, 58, 60, 91, 95, 117, 158, 64, 158,
    51, 53, 64, 52, 51, 53, 50, 49, 51, 49, 116, 51, 53, 125, 143, 60, 147, 58, 95,
    119, 120, 50, 53, 51, 51, 53, 58, 59, 91, 95, 117, 144, 42, 58, 95, 148, 120, 51,
    54, 54, 55, 54, 55, 53, 58, 58, 58, 58, 58, 148, 53, 148};

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] = {
    0, 71, 72, 72, 73, 74, 74, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75,
    76, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 88, 89, 90, 91,
    91, 92, 93, 93, 94, 94, 94, 94, 94, 94, 94, 94, 95, 95, 95, 95, 95, 95,
    95, 95, 95, 96, 97, 98, 99, 100, 101, 102, 102, 103, 103, 104, 105, 106, 107, 107,
    107, 108, 108, 108, 108, 108, 109, 109, 109, 109, 109, 109, 109, 109, 109, 109, 109, 109,
    109, 109, 109, 109, 109, 109, 109, 109, 109, 109, 110, 111, 112, 113, 114, 114, 115, 116,
    116, 116, 116, 116, 116, 116, 116, 116, 116, 117, 117, 118, 119, 119, 120, 120, 121, 122,
    122, 123, 124, 125, 125, 125, 125, 125, 126, 126, 126, 127, 128, 129, 130, 131, 132, 133,
    134, 135, 136, 137, 138, 139, 139, 139, 139, 139, 139, 139, 140, 141, 142, 142, 143, 143,
    143, 143, 144, 144, 144, 144, 144, 145, 146, 147, 147, 147, 148, 148, 148, 148, 148, 148,
    149, 150, 151, 151, 152, 153, 154, 155, 156, 157, 157, 157, 157, 157, 158, 158, 158, 158,
    158, 159, 160, 161, 162, 163, 164, 165, 165, 166, 166, 166, 166, 166, 166, 166, 166, 166,
    166, 167, 167, 168, 169, 170, 171, 172, 173, 173, 174, 174, 174, 174, 174, 174, 174, 174,
    174, 174, 175, 176, 177, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187};

/* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] = {
    0, 2, 2, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4, 3, 1, 3, 1, 5, 1, 1, 3, 1, 1, 3, 1, 1,
    2, 3, 1, 4, 3, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 4, 4, 4, 4, 3, 3, 3, 3, 3, 3, 1, 1, 5, 1, 1, 1,
    2, 1, 3, 1, 1, 3, 1, 3, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 3, 1, 1, 1, 3, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4, 6, 1, 1, 3, 1, 1, 3, 1, 3, 3,
    1, 1, 1, 1, 1, 1, 3, 3, 3, 1, 3, 1, 3, 1, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 5, 1, 1, 3,
    6, 3, 1, 1, 1, 1, 1, 1, 1, 4, 1, 7, 5, 3, 1, 3, 3, 3, 3, 3, 3, 1, 3, 1, 1, 3, 1, 3, 1, 1, 4, 4,
    1, 4, 1, 1, 1, 1, 1, 3, 1, 3, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4, 3, 1, 3, 1, 1, 3,
    1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 3, 3, 1, 1, 3, 1, 1, 1, 1, 3, 1};

#define yyerrok   (yyerrstatus = 0)
#define yyclearin (yychar = YYEMPTY)
#define YYEMPTY   (-2)
#define YYEOF     0

#define YYACCEPT goto yyacceptlab
#define YYABORT  goto yyabortlab
#define YYERROR  goto yyerrorlab

#define YYRECOVERING() (!!yyerrstatus)

#define YYBACKUP(Token, Value)                      \
  do                                                \
    if (yychar == YYEMPTY) {                        \
      yychar = (Token);                             \
      yylval = (Value);                             \
      YYPOPSTACK(yylen);                            \
      yystate = *yyssp;                             \
      goto yybackup;                                \
    } else {                                        \
      yyerror(YY_("syntax error: cannot back up")); \
      YYERROR;                                      \
    }                                               \
  while (0)

/* Error token number */
#define YYTERROR  1
#define YYERRCODE 256

/* Enable debugging if requested.  */
#if YYDEBUG

#ifndef YYFPRINTF
#include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#define YYFPRINTF fprintf
#endif

#define YYDPRINTF(Args)          \
  do {                           \
    if (yydebug) YYFPRINTF Args; \
  } while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
#define YY_LOCATION_PRINT(File, Loc) ((void)0)
#endif

#define YY_SYMBOL_PRINT(Title, Type, Value, Location) \
  do {                                                \
    if (yydebug) {                                    \
      YYFPRINTF(stderr, "%s ", Title);                \
      yy_symbol_print(stderr, Type, Value);           \
      YYFPRINTF(stderr, "\n");                        \
    }                                                 \
  } while (0)

/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void yy_symbol_value_print(FILE* yyoutput, int yytype, YYSTYPE const* const yyvaluep)
{
  FILE* yyo = yyoutput;
  YYUSE(yyo);
  if (!yyvaluep) return;
#ifdef YYPRINT
  if (yytype < YYNTOKENS) YYPRINT(yyoutput, yytoknum[yytype], *yyvaluep);
#endif
  YYUSE(yytype);
}

/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void yy_symbol_print(FILE* yyoutput, int yytype, YYSTYPE const* const yyvaluep)
{
  YYFPRINTF(yyoutput, "%s %s (", yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print(yyoutput, yytype, yyvaluep);
  YYFPRINTF(yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void yy_stack_print(yytype_int16* yybottom, yytype_int16* yytop)
{
  YYFPRINTF(stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++) {
    int yybot = *yybottom;
    YYFPRINTF(stderr, " %d", yybot);
  }
  YYFPRINTF(stderr, "\n");
}

#define YY_STACK_PRINT(Bottom, Top)               \
  do {                                            \
    if (yydebug) yy_stack_print((Bottom), (Top)); \
  } while (0)

/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void yy_reduce_print(yytype_int16* yyssp, YYSTYPE* yyvsp, int yyrule)
{
  unsigned long int yylno  = yyrline[yyrule];
  int               yynrhs = yyr2[yyrule];
  int               yyi;
  YYFPRINTF(stderr, "Reducing stack by rule %d (line %lu):\n", yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++) {
    YYFPRINTF(stderr, "   $%d = ", yyi + 1);
    yy_symbol_print(stderr, yystos[yyssp[yyi + 1 - yynrhs]], &(yyvsp[(yyi + 1) - (yynrhs)]));
    YYFPRINTF(stderr, "\n");
  }
}

#define YY_REDUCE_PRINT(Rule)                         \
  do {                                                \
    if (yydebug) yy_reduce_print(yyssp, yyvsp, Rule); \
  } while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
#define YYDPRINTF(Args)
#define YY_SYMBOL_PRINT(Title, Type, Value, Location)
#define YY_STACK_PRINT(Bottom, Top)
#define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */

/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
#define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
#define YYMAXDEPTH 10000
#endif

#if YYERROR_VERBOSE

#ifndef yystrlen
#if defined __GLIBC__ && defined _STRING_H
#define yystrlen strlen
#else
/* Return the length of YYSTR.  */
static YYSIZE_T yystrlen(const char* yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++) continue;
  return yylen;
}
#endif
#endif

#ifndef yystpcpy
#if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#define yystpcpy stpcpy
#else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char* yystpcpy(char* yydest, const char* yysrc)
{
  char*       yyd = yydest;
  const char* yys = yysrc;

  while ((*yyd++ = *yys++) != '\0') continue;

  return yyd - 1;
}
#endif
#endif

#ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T yytnamerr(char* yyres, const char* yystr)
{
  if (*yystr == '"') {
    YYSIZE_T    yyn = 0;
    char const* yyp = yystr;

    for (;;) switch (*++yyp) {
        case '\'':
        case ',': goto do_not_strip_quotes;

        case '\\':
          if (*++yyp != '\\') goto do_not_strip_quotes;
          /* Fall through.  */
        default:
          if (yyres) yyres[yyn] = *yyp;
          yyn++;
          break;

        case '"':
          if (yyres) yyres[yyn] = '\0';
          return yyn;
      }
  do_not_strip_quotes:;
  }

  if (!yyres) return yystrlen(yystr);

  return yystpcpy(yyres, yystr) - yyres;
}
#endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int yysyntax_error(YYSIZE_T* yymsg_alloc, char** yymsg, yytype_int16* yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr(YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize  = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char* yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const* yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY) {
    int yyn          = yypact[*yyssp];
    yyarg[yycount++] = yytname[yytoken];
    if (!yypact_value_is_default(yyn)) {
      /* Start YYX at -YYN if negative to avoid negative indexes in
         YYCHECK.  In other words, skip the first -YYN actions for
         this state because they are default actions.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;
      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend     = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yyx;

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
        if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR &&
            !yytable_value_is_error(yytable[yyx + yyn])) {
          if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM) {
            yycount = 1;
            yysize  = yysize0;
            break;
          }
          yyarg[yycount++] = yytname[yyx];
          {
            YYSIZE_T yysize1 = yysize + yytnamerr(YY_NULLPTR, yytname[yyx]);
            if (!(yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)) return 2;
            yysize = yysize1;
          }
        }
    }
  }

  switch (yycount) {
#define YYCASE_(N, S) \
  case N: yyformat = S; break
    YYCASE_(0, YY_("syntax error"));
    YYCASE_(1, YY_("syntax error, unexpected %s"));
    YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
    YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
    YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
    YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
#undef YYCASE_
  }

  {
    YYSIZE_T yysize1 = yysize + yystrlen(yyformat);
    if (!(yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)) return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize) {
    *yymsg_alloc = 2 * yysize;
    if (!(yysize <= *yymsg_alloc && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
      *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
    return 1;
  }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char* yyp = *yymsg;
    int   yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount) {
        yyp += yytnamerr(yyp, yyarg[yyi++]);
        yyformat += 2;
      } else {
        yyp++;
        yyformat++;
      }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void yydestruct(const char* yymsg, int yytype, YYSTYPE* yyvaluep)
{
  YYUSE(yyvaluep);
  if (!yymsg) yymsg = "Deleting";
  YY_SYMBOL_PRINT(yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE(yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}

/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;

/*----------.
| yyparse.  |
`----------*/

int yyparse(void)
{
  int yystate;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;

  /* The stacks and their tools:
     'yyss': related to states.
     'yyvs': related to semantic values.

     Refer to the stacks through separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  yytype_int16  yyssa[YYINITDEPTH];
  yytype_int16* yyss;
  yytype_int16* yyssp;

  /* The semantic value stack.  */
  YYSTYPE  yyvsa[YYINITDEPTH];
  YYSTYPE* yyvs;
  YYSTYPE* yyvsp;

  YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char     yymsgbuf[128];
  char*    yymsg       = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N) (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize  = YYINITDEPTH;

  YYDPRINTF((stderr, "Starting parse\n"));

  yystate     = 0;
  yyerrstatus = 0;
  yynerrs     = 0;
  yychar      = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

  /*------------------------------------------------------------.
  | yynewstate -- Push a new state, which is found in yystate.  |
  `------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp) {
    /* Get the current used size of the three stacks, in elements.  */
    YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
    {
      /* Give user a chance to reallocate the stack.  Use copies of
         these so that the &'s don't force the real ones into
         memory.  */
      YYSTYPE*      yyvs1 = yyvs;
      yytype_int16* yyss1 = yyss;

      /* Each stack pointer address is followed by the size of the
         data in use in that stack, in bytes.  This used to be a
         conditional around just the two extra args, but that might
         be undefined if yyoverflow is a macro.  */
      yyoverflow(YY_("memory exhausted"), &yyss1, yysize * sizeof(*yyssp), &yyvs1,
                 yysize * sizeof(*yyvsp), &yystacksize);

      yyss = yyss1;
      yyvs = yyvs1;
    }
#else /* no yyoverflow */
#ifndef YYSTACK_RELOCATE
    goto yyexhaustedlab;
#else
    /* Extend the stack our own way.  */
    if (YYMAXDEPTH <= yystacksize) goto yyexhaustedlab;
    yystacksize *= 2;
    if (YYMAXDEPTH < yystacksize) yystacksize = YYMAXDEPTH;

    {
      yytype_int16*  yyss1 = yyss;
      union yyalloc* yyptr = (union yyalloc*)YYSTACK_ALLOC(YYSTACK_BYTES(yystacksize));
      if (!yyptr) goto yyexhaustedlab;
      YYSTACK_RELOCATE(yyss_alloc, yyss);
      YYSTACK_RELOCATE(yyvs_alloc, yyvs);
#undef YYSTACK_RELOCATE
      if (yyss1 != yyssa) YYSTACK_FREE(yyss1);
    }
#endif
#endif /* no yyoverflow */

    yyssp = yyss + yysize - 1;
    yyvsp = yyvs + yysize - 1;

    YYDPRINTF((stderr, "Stack size increased to %lu\n", (unsigned long int)yystacksize));

    if (yyss + yystacksize - 1 <= yyssp) YYABORT;
  }

  YYDPRINTF((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL) YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default(yyn)) goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY) {
    YYDPRINTF((stderr, "Reading a token: "));
    yychar = yylex();
  }

  if (yychar <= YYEOF) {
    yychar = yytoken = YYEOF;
    YYDPRINTF((stderr, "Now at end of input.\n"));
  } else {
    yytoken = YYTRANSLATE(yychar);
    YY_SYMBOL_PRINT("Next token is", yytoken, &yylval, &yylloc);
  }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken) goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0) {
    if (yytable_value_is_error(yyn)) goto yyerrlab;
    yyn = -yyn;
    goto yyreduce;
  }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus) yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;

/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0) goto yyerrlab;
  goto yyreduce;

/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1 - yylen];

  YY_REDUCE_PRINT(yyn);
  switch (yyn) {
    case 2:

    {
#if (SYSTEME != LINUX)

      /*
           This action is introduced only to add
           a reference to otherwise unreferenced
           labels in y.tab.c
      */
      if (never()) YYBACKUP(-1, yyval)
#endif
    }

    break;

    case 4:

    {
      Fset_section(DECLARATIONS);
      Fpgrp_id(newstr("Main"));
    }

    break;

    case 18:

    {
      Fline_spec(newstr((yyvsp[-2].str)), newstr((yyvsp[-1].str)));
    }

    break;

    case 19:

    {
      Fline_spec(newstr((yyvsp[-1].str)), newstr((yyvsp[0].str)));
    }

    break;

    case 20:

    {
      Fset_word(DEF_POUND);
    }

    break;

    case 21:

    {
      Fend_function(&parameters, (yyvsp[0].function));
    }

    break;

    case 22:

    {
      Fset_word(END_FUNCTION);
    }

    break;

    case 23:

    {
      Ffunc_declare(&parameters, (yyvsp[-3].text), (yyvsp[-1].descriptor));
      Fset_state(IN_UNKNOWN);
    }

    break;

    case 24:

    {
      Fset_word(FUNCTIOND);
    }

    break;

    case 25:

    {
      Ffuncd_id_list((yyvsp[0].text));
    }

    break;

    case 26:

    {
      Fgallocation(&parameters, (yyvsp[0].ival));
    }

    break;

    case 27:

    {
      Fset_word(GALLOCATION);
    }

    break;

    case 28:

    {
      Fgl_desc(&parameters, (yyvsp[0].text));
    }

    break;

    case 29:

    {
      Fgs_desc(&parameters, (yyvsp[0].str));
    }

    break;

    case 30:

    {
      Fset_word(GS_DESC);
    }

    break;

    case 31:

    {
      (yyval.str) = Fstring(NULL, newstr((yyvsp[0].str)));
    }

    break;

    case 32:

    {
      (yyval.str) = Fstring((yyvsp[-1].str), newstr((yyvsp[0].str)));
    }

    break;

    case 33:

    {
      Fgvalidf(&parameters, (yyvsp[0].function));
    }

    break;

    case 34:

    {
      Fset_word(GVALIDF);
    }

    break;

    case 35:

    {
      Fsub_state(IN_ARGUMENT);
      Fsub_state(IN_FUNCTION);
      (yyval.function) = Ffunction((yyvsp[-3].str), (yyvsp[-1].arg));
    }

    break;

    case 36:

    {
      Fsub_state(IN_FUNCTION);
      (yyval.function) = Ffunction((yyvsp[-2].str), NULL);
    }

    break;

    case 37:

    {
      Ffunc_id(newstr((yyvsp[0].str)));
    }

    break;

    case 38:

    {
      Farg_id((yyvsp[0].arg));
      (yyval.arg) = Farg_list(NULL, (yyvsp[0].arg));
    }

    break;

    case 39:

    {
      Farg_id((yyvsp[0].arg));
      (yyval.arg) = Farg_list((yyvsp[-2].arg), (yyvsp[0].arg));
    }

    break;

    case 40:

    {
      (yyval.arg) = Farg(LVALUE, (yyvsp[0].str));
    }

    break;

    case 41:

    {
      (yyval.arg) = Farg(IDENTIFIER, newstr((yyvsp[0].str)));
    }

    break;

    case 42:

    {
      (yyval.arg) = Farg(STR, (yyvsp[0].str));
    }

    break;

    case 43:

    {
      (yyval.arg) = Farg(BNUM, newstr((yyvsp[0].str)));
    }

    break;

    case 44:

    {
      (yyval.arg) = Farg(INUM, newstr((yyvsp[0].str)));
    }

    break;

    case 45:

    {
      (yyval.arg) = Farg(FNUM, newstr((yyvsp[0].str)));
    }

    break;

    case 46:

    {
      (yyval.arg) = Farg(CNUM, newstr((yyvsp[0].str)));
    }

    break;

    case 47:

    {
      (yyval.arg) = Farg(INDEX, newstr("$Index"));
    }

    break;

    case 48:

    {
      (yyval.str) = Farray(newstr((yyvsp[-3].str)), newstr((yyvsp[-1].str)));
    }

    break;

    case 49:

    {
      (yyval.str) = Farray((yyvsp[-3].str), newstr((yyvsp[-1].str)));
    }

    break;

    case 50:

    {
      (yyval.str) = Farray(newstr((yyvsp[-3].str)), newstr("$Index"));
    }

    break;

    case 51:

    {
      (yyval.str) = Farray((yyvsp[-3].str), newstr("$Index"));
    }

    break;

    case 52:

    {
      (yyval.str) = Fpoint(newstr((yyvsp[-2].str)), newstr((yyvsp[0].str)));
    }

    break;

    case 53:

    {
      (yyval.str) = Fpoint(newstr("$Father"), newstr((yyvsp[0].str)));
    }

    break;

    case 54:

    {
      (yyval.str) = Fpoint((yyvsp[-2].str), newstr((yyvsp[0].str)));
    }

    break;

    case 55:

    {
      (yyval.str) = Farrow(newstr((yyvsp[-2].str)), newstr((yyvsp[0].str)));
    }

    break;

    case 56:

    {
      (yyval.str) = Farrow((yyvsp[-2].str), newstr((yyvsp[0].str)));
    }

    break;

    case 57:

    {
      Ficon(&parameters, newstr((yyvsp[0].str)));
    }

    break;

    case 58:

    {
      Fset_word(ICON);
    }

    break;

    case 59:

    {
      Finc_code(&parameters, (yyvsp[0].text));
    }

    break;

    case 60:

    {
      Fstructure(&parameters, (yyvsp[-3].str), (yyvsp[-1].member));
      Fset_state(IN_UNKNOWN);
    }

    break;

    case 61:

    {
      Fset_word(STRUCTURE);
    }

    break;

    case 62:

    {
      Fstruct_id(newstr((yyvsp[0].str)));
    }

    break;

    case 63:

    {
      (yyval.member) = Fmember_list(NULL, (yyvsp[0].member));
    }

    break;

    case 64:

    {
      (yyval.member) = Fmember_list((yyvsp[-1].member), (yyvsp[0].member));
    }

    break;

    case 65:

    {
      (yyval.member) = Fline_member();
    }

    break;

    case 66:

    {
      (yyval.member) = Fmember((yyvsp[-1].str), (yyvsp[0].description));
      Fset_state(IN_STRUCTURE);
    }

    break;

    case 67:

    {
      Fset_word(MEMBER);
    }

    break;

    case 68:

    {
      Fmem_id(newstr((yyvsp[0].str)));
    }

    break;

    case 69:

    {
      (yyval.description) = Fdescription((yyvsp[-1].descriptor));
    }

    break;

    case 70:

    {
      (yyval.descriptor) = Fdesc_list(NULL, (yyvsp[0].descriptor));
    }

    break;

    case 71:

    {
      (yyval.descriptor) = Fdesc_list((yyvsp[-2].descriptor), (yyvsp[0].descriptor));
    }

    break;

    case 72:

    {
      parser_error = TRUE;
    }

    break;

    case 73:

    {
      Fsub_state(IN_DESCRIPTOR);
      (yyval.descriptor) = (yyvsp[0].descriptor);
    }

    break;

    case 74:

    {
      Fsub_state(IN_DESCRIPTOR);
      (yyval.descriptor) = (yyvsp[0].descriptor);
    }

    break;

    case 75:

    {
      Fsub_state(IN_DESCRIPTOR);
      (yyval.descriptor) = (yyvsp[-1].descriptor);
    }

    break;

    case 76:

    {
      Fsub_state(IN_DESCRIPTOR);
      (yyval.descriptor) = (yyvsp[-1].descriptor);
    }

    break;

    case 77:

    {
      Fsub_state(IN_DESCRIPTOR);
    }

    break;

    case 100:

    {
      (yyval.descriptor) = Fallocation((yyvsp[0].ival));
    }

    break;

    case 101:

    {
      Fdesc_id(ALLOCATION);
    }

    break;

    case 102:

    {
      Fdesc_id(AUXILIARY);
      (yyval.descriptor) = Fauxiliary();
    }

    break;

    case 103:

    {
      Fdesc_id(HIDDEN);
      (yyval.descriptor) = Fhidden();
    }

    break;

    case 104:

    {
      (yyval.descriptor) = Fdefault_value((yyvsp[0].value));
    }

    break;

    case 105:

    {
      (yyval.descriptor) = Fdefault_value((yyvsp[0].value));
    }

    break;

    case 106:

    {
      Fdesc_id(DEFAULT);
    }

    break;

    case 108:

    {
      (yyval.value) = Fvalue(STR, (yyvsp[0].str));
    }

    break;

    case 109:

    {
      (yyval.value) = Fvalue(BNUM, newstr((yyvsp[0].str)));
    }

    break;

    case 110:

    {
      (yyval.value) = Fvalue(INUM, newstr((yyvsp[0].str)));
    }

    break;

    case 111:

    {
      (yyval.value) = Fvalue(FNUM, newstr((yyvsp[0].str)));
    }

    break;

    case 112:

    {
      (yyval.value) = Fvalue(CNUM, newstr((yyvsp[0].str)));
    }

    break;

    case 113:

    {
      (yyval.value) = Fvalue(INDEX, newstr("$Index"));
    }

    break;

    case 114:

    {
      (yyval.value) = Fvalue(IDENTIFIER, newstr((yyvsp[0].str)));
    }

    break;

    case 115:

    {
      (yyval.value) = Fvalue(LVALUE, (yyvsp[0].str));
    }

    break;

    case 116:

    {
      (yyval.value) = Fvalue_func((yyvsp[0].function));
    }

    break;

    case 117:

    {
      (yyval.value) = Fc_exp(newstr((yyvsp[-1].str)), NULL);
    }

    break;

    case 118:

    {
      (yyval.value) = Fc_exp(newstr((yyvsp[-3].str)), (yyvsp[-1].value));
    }

    break;

    case 119:

    {
      Fset_word(C_EXP);
    }

    break;

    case 120:

    {
      (yyval.value) = Fvector(NULL, (yyvsp[0].value));
    }

    break;

    case 121:

    {
      (yyval.value) = Fvector((yyvsp[-2].value), (yyvsp[0].value));
    }

    break;

    case 122:

    {
      (yyval.value) = Fvalue(IDENTIFIER, newstr((yyvsp[0].str)));
    }

    break;

    case 123:

    {
      (yyval.value) = Fvalue(LVALUE, (yyvsp[0].str));
    }

    break;

    case 124:

    {
      (yyval.value) = (yyvsp[-1].value);
    }

    break;

    case 125:

    {
      (yyval.value) = Fvector(NULL, (yyvsp[0].value));
    }

    break;

    case 126:

    {
      (yyval.value) = Fvector((yyvsp[-2].value), (yyvsp[0].value));
    }

    break;

    case 127:

    {
      (yyval.descriptor) = Fdir_value((yyvsp[0].value));
    }

    break;

    case 128:

    {
      Fdesc_id(DIR);
    }

    break;

    case 130:

    {
      (yyval.value) = Fvalue(STR, (yyvsp[0].str));
    }

    break;

    case 131:

    {
      (yyval.value) = Fvalue(IDENTIFIER, newstr((yyvsp[0].str)));
    }

    break;

    case 132:

    {
      (yyval.value) = Fvalue(LVALUE, (yyvsp[0].str));
    }

    break;

    case 133:

    {
      (yyval.value) = Fvalue_func((yyvsp[0].function));
    }

    break;

    case 134:

    {
      (yyval.descriptor) = Fdisplay((yyvsp[0].function));
    }

    break;

    case 135:

    {
      (yyval.descriptor) = Fdisplay(Ffunction(newstr("$ACEGR_CURV"), NULL));
    }

    break;

    case 136:

    {
      (yyval.descriptor) = Fdisplay(Ffunction(newstr("$ACEGR_HIST"), NULL));
    }

    break;

    case 137:

    {
      Fdesc_id(DISPLAY);
    }

    break;

    case 138:

    {
      (yyval.descriptor) = Fext_value((yyvsp[0].value));
    }

    break;

    case 139:

    {
      Fdesc_id(EXT);
    }

    break;

    case 140:

    {
      (yyval.descriptor) = Fkey(newstr((yyvsp[0].str)));
    }

    break;

    case 141:

    {
      Fdesc_id(KEY);
    }

    break;

    case 142:

    {
      Fdesc_id(KIND);
      (yyval.descriptor) = Fkind((yyvsp[0].ival));
    }

    break;

    case 143:

    {
      (yyval.descriptor) = Fl_desc((yyvsp[0].text));
    }

    break;

    case 144:

    {
      (yyval.descriptor) = Flength(newstr((yyvsp[0].str)));
    }

    break;

    case 145:

    {
      Fdesc_id(LENGTH);
    }

    break;

    case 146:

    {
      Fdesc_id(MANDATORY);
      (yyval.descriptor) = Fmandatory();
    }

    break;

    case 147:

    {
      (yyval.descriptor) = Fmax_value((yyvsp[0].value));
    }

    break;

    case 148:

    {
      Fdesc_id(MAX);
    }

    break;

    case 150:

    {
      (yyval.value) = Fvalue(INUM, newstr((yyvsp[0].str)));
    }

    break;

    case 151:

    {
      (yyval.value) = Fvalue(FNUM, newstr((yyvsp[0].str)));
    }

    break;

    case 152:

    {
      (yyval.value) = Fvalue(INDEX, newstr("$Index"));
    }

    break;

    case 153:

    {
      (yyval.value) = Fvalue(IDENTIFIER, newstr((yyvsp[0].str)));
    }

    break;

    case 154:

    {
      (yyval.value) = Fvalue(LVALUE, (yyvsp[0].str));
    }

    break;

    case 155:

    {
      (yyval.value) = Fvalue_func((yyvsp[0].function));
    }

    break;

    case 156:

    {
      (yyval.descriptor) = Fmenu((yyvsp[-1].value));
    }

    break;

    case 157:

    {
      Fdesc_id(MENU);
    }

    break;

    case 158:

    {
      (yyval.value) = Fvector(NULL, (yyvsp[0].value));
    }

    break;

    case 159:

    {
      (yyval.value) = Fvector((yyvsp[-2].value), (yyvsp[0].value));
    }

    break;

    case 160:

    {
      (yyval.value) = Fitem((yyvsp[-5].value), (yyvsp[-3].value), (yyvsp[-1].value));
    }

    break;

    case 161:

    {
      (yyval.value) = Fitem((yyvsp[-2].value), (yyvsp[0].value), NULL);
    }

    break;

    case 162:

    {
      (yyval.value) = Fitem((yyvsp[0].value), NULL, NULL);
    }

    break;

    case 165:

    {
      (yyval.value) = Fvalue(BNUM, newstr((yyvsp[0].str)));
    }

    break;

    case 166:

    {
      (yyval.value) = Fvalue(IDENTIFIER, newstr((yyvsp[0].str)));
    }

    break;

    case 167:

    {
      (yyval.value) = Fvalue(LVALUE, (yyvsp[0].str));
    }

    break;

    case 168:

    {
      (yyval.value) = Fvalue_func((yyvsp[0].function));
    }

    break;

    case 169:

    {
      (yyval.value) = (yyvsp[-1].value);
    }

    break;

    case 170:

    {
      Fset_word(VECTOR);
    }

    break;

    case 171:

    {
      (yyval.value) =
          Fvalue_vector((yyvsp[-6].str), (yyvsp[-4].value), (yyvsp[-2].value), (yyvsp[0].value));
    }

    break;

    case 172:

    {
      (yyval.value) = Fvalue_vector((yyvsp[-4].str), (yyvsp[-2].value), (yyvsp[0].value), NULL);
    }

    break;

    case 173:

    {
      (yyval.value) = Fvalue_vector((yyvsp[-2].str), (yyvsp[0].value), NULL, NULL);
    }

    break;

    case 174:

    {
      (yyval.value) = Fvalue(IDENTIFIER, newstr((yyvsp[0].str)));
    }

    break;

    case 175:

    {
      (yyval.value) = Fvalue(LVALUE, Fpoint(newstr((yyvsp[-2].str)), newstr((yyvsp[0].str))));
    }

    break;

    case 176:

    {
      (yyval.value) = Fvalue(LVALUE, Fpoint(newstr("$Father"), newstr((yyvsp[0].str))));
    }

    break;

    case 177:

    {
      (yyval.value) = Fvalue(LVALUE, Fpoint((yyvsp[-2].str), newstr((yyvsp[0].str))));
    }

    break;

    case 178:

    {
      (yyval.value) = Fvalue(LVALUE, Farrow(newstr((yyvsp[-2].str)), newstr((yyvsp[0].str))));
    }

    break;

    case 179:

    {
      (yyval.value) = Fvalue(LVALUE, Farrow((yyvsp[-2].str), newstr((yyvsp[0].str))));
    }

    break;

    case 180:

    {
      (yyval.descriptor) = Fmin_value((yyvsp[0].value));
    }

    break;

    case 181:

    {
      Fdesc_id(MIN);
    }

    break;

    case 182:

    {
      (yyval.descriptor) = Foutput((yyvsp[0].value));
    }

    break;

    case 183:

    {
      (yyval.descriptor) = Foutput(NULL);
    }

    break;

    case 184:

    {
      Fdesc_id(OUTPUT);
    }

    break;

    case 185:

    {
      (yyval.descriptor) = Fs_desc((yyvsp[0].str));
    }

    break;

    case 186:

    {
      Fdesc_id(S_DESC);
    }

    break;

    case 187:

    {
      (yyval.descriptor) = Ftype((yyvsp[0].value));
    }

    break;

    case 188:

    {
      Fdesc_id(TYPE);
    }

    break;

    case 189:

    {
      (yyval.value) = Ftype_value((yyvsp[0].ival));
    }

    break;

    case 190:

    {
      (yyval.value) = Ftype_vector((yyvsp[-3].ival), (yyvsp[-1].value));
    }

    break;

    case 191:

    {
      (yyval.value) = Fany((yyvsp[-3].ival), newstr((yyvsp[-1].str)));
    }

    break;

    case 192:

    {
      (yyval.value) = Fstruct_value(newstr((yyvsp[0].str)));
    }

    break;

    case 193:

    {
      (yyval.value) = Fstruct_vector(newstr((yyvsp[-3].str)), (yyvsp[-1].value));
    }

    break;

    case 195:

    {
      (yyval.value) = Fvalue(INUM, newstr((yyvsp[0].str)));
    }

    break;

    case 196:

    {
      (yyval.value) = Fvalue(IDENTIFIER, newstr((yyvsp[0].str)));
    }

    break;

    case 197:

    {
      (yyval.value) = Fvalue(LVALUE, (yyvsp[0].str));
    }

    break;

    case 198:

    {
      (yyval.value) = Fvalue_func((yyvsp[0].function));
    }

    break;

    case 199:

    {
      (yyval.descriptor) = Funits_value((yyvsp[0].value));
    }

    break;

    case 200:

    {
      Fdesc_id(UNITS);
    }

    break;

    case 201:

    {
      (yyval.descriptor) = Fvalidf((yyvsp[0].function));
    }

    break;

    case 202:

    {
      Fdesc_id(VALIDF);
    }

    break;

    case 203:

    {
      Fdesc_id(VOLATIL);
      (yyval.descriptor) = Fvolatil();
    }

    break;

    case 204:

    {
      Fdesc_id(TRAIL_ARGS);
      Ftrail_args(&parameters);
    }

    break;

    case 208:

    {
      Fml_desc(&parameters, (yyvsp[0].descriptor));
    }

    break;

    case 209:

    {
      Fglocation(&parameters, (yyvsp[0].variable));
    }

    break;

    case 210:

    {
      Fgparamsgrp(&parameters, (yyvsp[0].paramgrp));
    }

    break;

    case 211:

    {
      Fms_desc(&parameters, (yyvsp[0].descriptor));
    }

    break;

    case 212:

    {
      Fgtextbox(&parameters, (yyvsp[0].variable));
    }

    break;

    case 213:

    {
      Fmvalidf(&parameters, (yyvsp[0].descriptor));
    }

    break;

    case 214:

    {
      Fgvariable(&parameters, (yyvsp[0].variable));
    }

    break;

    case 215:

    {
      Fmview(&parameters, (yyvsp[0].descriptor));
    }

    break;

    case 216:

    {
      Fmhide(&parameters, (yyvsp[0].descriptor));
    }

    break;

    case 217:

    {
      Fline_spec(newstr((yyvsp[-2].str)), newstr((yyvsp[-1].str)));
    }

    break;

    case 218:

    {
      Fline_spec(newstr((yyvsp[-1].str)), newstr((yyvsp[0].str)));
    }

    break;

    case 219:

    {
      Fset_word(DEC_POUND);
    }

    break;

    case 220:

    {
      (yyval.paramgrp) = Fparamgrp(&parameters, (yyvsp[-1].str), (yyvsp[0].descriptor));
      Fpgrp_id(newstr("Main"));
    }

    break;

    case 221:

    {
      Fset_word(PARAMGRP);
    }

    break;

    case 222:

    {
      (yyval.str) = Fpgrp_id(newstr((yyvsp[0].str)));
    }

    break;

    case 223:

    {
      (yyval.descriptor) = (yyvsp[-1].descriptor);
    }

    break;

    case 224:

    {
      (yyval.descriptor) = Fdesc_list(NULL, (yyvsp[0].descriptor));
    }

    break;

    case 225:

    {
      (yyval.descriptor) = Fdesc_list((yyvsp[-1].descriptor), (yyvsp[0].descriptor));
    }

    break;

    case 226:

    {
      (yyval.descriptor) = Fline_desc();
    }

    break;

    case 228:

    {
      (yyval.descriptor) = Flocation_list((yyvsp[0].variable));
    }

    break;

    case 229:

    {
      (yyval.descriptor) = Fparamsgrp((yyvsp[0].text));
    }

    break;

    case 231:

    {
      (yyval.descriptor) = Ftextbox_list((yyvsp[0].variable));
    }

    break;

    case 233:

    {
      (yyval.descriptor) = Fvariable_list((yyvsp[0].variable));
    }

    break;

    case 236:

    {
      (yyval.text) = (yyvsp[0].text);
    }

    break;

    case 237:

    {
      Fset_word(PARAMSGRP);
    }

    break;

    case 238:

    {
      (yyval.text) = Ftext(NULL, newstr((yyvsp[0].str)));
    }

    break;

    case 239:

    {
      (yyval.text) = Ftext((yyvsp[-2].text), newstr((yyvsp[0].str)));
    }

    break;

    case 240:

    {
      (yyval.variable) = Fvariable((yyvsp[-1].text), (yyvsp[0].description));
      Fset_state(IN_PARAMGRP);
    }

    break;

    case 241:

    {
      Fset_word(VARIABLE);
    }

    break;

    case 242:

    {
      Fvar_id_list((yyvsp[0].text));
    }

    break;

    case 243:

    {
      (yyval.variable) = Flocation((yyvsp[-1].text), (yyvsp[0].description));
      Fset_state(IN_PARAMGRP);
    }

    break;

    case 244:

    {
      Fset_word(LOCATION);
    }

    break;

    case 245:

    {
      Floc_id_list((yyvsp[0].text));
    }

    break;

    case 246:

    {
      (yyval.text) = Ftext(NULL, (yyvsp[0].str));
    }

    break;

    case 247:

    {
      (yyval.variable) = Ftextbox((yyvsp[0].text));
    }

    break;

    case 248:

    {
      (yyval.descriptor) = Fview((yyvsp[0].ival));
    }

    break;

    case 249:

    {
      Fset_word(VIEW);
    }

    break;

    default: break;
  }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK(yylen);
  yylen = 0;
  YY_STACK_PRINT(yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;

/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE(yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus) {
    ++yynerrs;
#if !YYERROR_VERBOSE
    yyerror(YY_("syntax error"));
#else
#define YYSYNTAX_ERROR yysyntax_error(&yymsg_alloc, &yymsg, yyssp, yytoken)
    {
      char const* yymsgp = YY_("syntax error");
      int         yysyntax_error_status;
      yysyntax_error_status = YYSYNTAX_ERROR;
      if (yysyntax_error_status == 0)
        yymsgp = yymsg;
      else if (yysyntax_error_status == 1) {
        if (yymsg != yymsgbuf) YYSTACK_FREE(yymsg);
        yymsg = (char*)YYSTACK_ALLOC(yymsg_alloc);
        if (!yymsg) {
          yymsg                 = yymsgbuf;
          yymsg_alloc           = sizeof yymsgbuf;
          yysyntax_error_status = 2;
        } else {
          yysyntax_error_status = YYSYNTAX_ERROR;
          yymsgp                = yymsg;
        }
      }
      yyerror(yymsgp);
      if (yysyntax_error_status == 2) goto yyexhaustedlab;
    }
#undef YYSYNTAX_ERROR
#endif
  }

  if (yyerrstatus == 3) {
    /* If just tried and failed to reuse lookahead token after an
       error, discard it.  */

    if (yychar <= YYEOF) {
      /* Return failure if at end of input.  */
      if (yychar == YYEOF) YYABORT;
    } else {
      yydestruct("Error: discarding", yytoken, &yylval);
      yychar = YYEMPTY;
    }
  }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;

/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0) goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK(yylen);
  yylen = 0;
  YY_STACK_PRINT(yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;

/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3; /* Each real token shifted decrements this.  */

  for (;;) {
    yyn = yypact[yystate];
    if (!yypact_value_is_default(yyn)) {
      yyn += YYTERROR;
      if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR) {
        yyn = yytable[yyn];
        if (0 < yyn) break;
      }
    }

    /* Pop the current state because it cannot handle the error token.  */
    if (yyssp == yyss) YYABORT;

    yydestruct("Error: popping", yystos[yystate], yyvsp);
    YYPOPSTACK(1);
    yystate = *yyssp;
    YY_STACK_PRINT(yyss, yyssp);
  }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Shift the error token.  */
  YY_SYMBOL_PRINT("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;

/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror(YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY) {
    /* Make sure we have latest lookahead translation.  See comments at
       user semantic actions for why this is necessary.  */
    yytoken = YYTRANSLATE(yychar);
    yydestruct("Cleanup: discarding lookahead", yytoken, &yylval);
  }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK(yylen);
  YY_STACK_PRINT(yyss, yyssp);
  while (yyssp != yyss) {
    yydestruct("Cleanup: popping", yystos[*yyssp], yyvsp);
    YYPOPSTACK(1);
  }
#ifndef yyoverflow
  if (yyss != yyssa) YYSTACK_FREE(yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf) YYSTACK_FREE(yymsg);
#endif
  return yyresult;
}
