/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_Y_TAB_H_INCLUDED
#define YY_YY_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
#define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
#define YYTOKENTYPE
enum yytokentype {
  MEMBER          = 258,
  PARAMGRP        = 259,
  PARAMSGRP       = 260,
  STRUCTURE       = 261,
  VARIABLE        = 262,
  LOCATION        = 263,
  ALLOCATION      = 264,
  AUXILIARY       = 265,
  DEFAULT         = 266,
  DIR             = 267,
  DISPLAY         = 268,
  EXT             = 269,
  HIDDEN          = 270,
  KEY             = 271,
  KIND            = 272,
  LENGTH          = 273,
  MANDATORY       = 274,
  MAX             = 275,
  MENU            = 276,
  MIN             = 277,
  OUTPUT          = 278,
  S_DESC          = 279,
  TYPE            = 280,
  UNITS           = 281,
  VALIDF          = 282,
  VECTOR          = 283,
  VIEW            = 284,
  VOLATIL         = 285,
  END_FUNCTION    = 286,
  GALLOCATION     = 287,
  GS_DESC         = 288,
  GVALIDF         = 289,
  ICON            = 290,
  TRAIL_ARGS      = 291,
  ACEGR_CURV      = 292,
  ACEGR_HIST      = 293,
  ALLOCATION_TYPE = 294,
  C_EXP           = 295,
  E_TYPE          = 296,
  FATHER          = 297,
  FUNCTIOND       = 298,
  INDEX           = 299,
  VIEW_TYPE       = 300,
  LEFTPAREN       = 301,
  RIGHTPAREN      = 302,
  LEFTBRACKET     = 303,
  RIGHTBRACKET    = 304,
  LEFTBRACE       = 305,
  RIGHTBRACE      = 306,
  EQUAL           = 307,
  COMA            = 308,
  POINT           = 309,
  ARROW           = 310,
  DEF_POUND       = 311,
  DEC_POUND       = 312,
  IDENTIFIER      = 313,
  BNUM            = 314,
  INUM            = 315,
  FNUM            = 316,
  CNUM            = 317,
  PRMID           = 318,
  STR             = 319,
  UNKNOWN_KEYWORD = 320,
  GL_DESC         = 321,
  L_DESC          = 322,
  INC_CODE        = 323,
  TEXT            = 324,
  TEXTBOX         = 325
};
#endif

/* Value type.  */
#if !defined YYSTYPE && !defined YYSTYPE_IS_DECLARED

union YYSTYPE {
  int                 ival;
  char*               str;
  struct Arg*         arg;
  struct Description* description;
  struct Descriptor*  descriptor;
  struct Function*    function;
  struct Member*      member;
  struct Paramgrp*    paramgrp;
  struct Structure*   structure;
  struct Value*       value;
  struct Variable*    variable;
  Text                text;
};

typedef union YYSTYPE YYSTYPE;
#define YYSTYPE_IS_TRIVIAL  1
#define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE yylval;

int yyparse(void);

#endif /* !YY_YY_Y_TAB_H_INCLUDED  */
