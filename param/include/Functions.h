// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#ifndef __MALLOC_H__
#ifndef __STDLIB_H__
#ifndef __malloc_h
#ifndef __stdlib_h
#ifndef _INC_MALLOC
#endif
#endif
#endif
#endif
#endif

#ifdef __STDC__
//char	*PrMnewstr(char*);
char* PrMrequote(char*);
Text  PrMaddline(Text, char*);
Text  PrMgeterrortext(void);
Text  PrMnewtext(void);
void  PrMadderror(char*);
void  PrMclearerrors(void);
void  PrMprinterrors(void);
void  PrMquote(char*);
void  PrMunquote(char*);
//void	PrMusage(char*, char*);
//int	param();
#else
//char	*PrMnewstr(/* char* */);
char* PrMrequote(/* char* */);
Text  PrMaddline(/* Text, char* */);
Text  PrMgeterrortext(/* void */);
Text  PrMnewtext(/* void */);
void  PrMadderror(/* char* */);
void  PrMclearerrors(/* void */);
void  PrMprinterrors(/* void */);
void  PrMquote(/* char* */);
void  PrMunquote(/* char* */);
//void	PrMusage(/* char*, char* */);
//int	param();
#endif

#ifdef MOTIF
int PrMmotif();
#endif
