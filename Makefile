include ./switches.def
include ./make.def

ifdef SILENT_MAKE
		MAKE_FLG += -s
endif

default: openCARP

param/param:
	@(cd $(PRM_DIR); $(MAKE) param)

update:
	@printf '%s\n' "--- updating openCARP -------------------------------------"
	@(git pull)

status:
	@printf '%s\n' "--- status openCARP ---------------------------------------"
	@(git status)

my_switches.def : switches.def
ifneq (,$(wildcard my_switches.def))
	./merge_switches.py $@ $< --output $@ --backup
else
	@sed -n "/^#.*=/p" $< > $@
endif
	@echo -e  "\nswitches.def modified --- Edit $@ file and rerun $(MAKE)\n"
	@false

setup: my_switches.def
	$(MAKE) update
	$(MAKE) all

openCARP: my_switches.def param/param
	@(cd $(SIM_DIR);    $(MAKE) build/openCARP_p.o)
	@(cd $(NUM_DIR);    $(MAKE) $(MAKE_FLG))
	@(cd $(FEM_DIR);    $(MAKE) $(MAKE_FLG))
	@(cd $(LIMPET_DIR); $(MAKE) $(MAKE_FLG))
	@(cd $(PHYS_DIR);   $(MAKE) $(MAKE_FLG))
	@(cd $(SIM_DIR);    $(MAKE) $(MAKE_FLG))
	$(MAKE) exe_dir

carptools: my_switches.def
	@(cd $(TOOLS_DIR);  $(MAKE))
	$(MAKE) exe_dir

all: openCARP carptools

doxydoc:
	doxygen doxygen_doc/openCARP.doxygen

check:
	@(cd $(NUM_DIR);    $(MAKE) $(MAKE_FLG) check)
	@(cd $(NUM_P_DIR);  $(MAKE) $(MAKE_FLG) check)
	@(cd $(NUM_G_DIR);  $(MAKE) $(MAKE_FLG) check)
	@(cd $(FEM_DIR);    $(MAKE) $(MAKE_FLG) check)
	@(cd $(PHYS_DIR);   $(MAKE) $(MAKE_FLG) check)
	@(cd $(SIM_DIR);    $(MAKE) $(MAKE_FLG) check)

format:
	@(cd $(PRM_DIR);    $(MAKE) $(MAKE_FLG) format)
	@(cd $(NUM_DIR);    $(MAKE) $(MAKE_FLG) format)
	@(cd $(NUM_P_DIR);  $(MAKE) $(MAKE_FLG) format)
	@(cd $(NUM_G_DIR);  $(MAKE) $(MAKE_FLG) format)
	@(cd $(FEM_DIR);    $(MAKE) $(MAKE_FLG) format)
	@(cd $(SLIMFEM_DIR);$(MAKE) $(MAKE_FLG) format)
	@(cd $(PHYS_DIR);   $(MAKE) $(MAKE_FLG) format)
	@(cd $(SIM_DIR);    $(MAKE) $(MAKE_FLG) format)
	@(cd $(LIMPET_DIR); $(MAKE) $(MAKE_FLG) format)
	@(cd $(TOOLS_DIR);  $(MAKE) $(MAKE_FLG) format)

clean: my_switches.def
	@(cd $(SIM_DIR);    $(MAKE) clean)
	@(cd $(NUM_DIR);    $(MAKE) clean)
	@(cd $(SLIMFEM_DIR);$(MAKE) clean)
	@(cd $(FEM_DIR);    $(MAKE) clean)
	@(cd $(PHYS_DIR);   $(MAKE) clean)
	@(cd $(LIMPET_DIR); $(MAKE) clean)
	@(cd $(TOOLS_DIR);  $(MAKE) clean)
	rm -rf bin

distclean: my_switches.def
	@(cd $(SIM_DIR);    $(MAKE) clean)
	@(cd $(NUM_DIR);    $(MAKE) clean)
	@(cd $(FEM_DIR);    $(MAKE) clean)
	@(cd $(PHYS_DIR);   $(MAKE) clean)
	@(cd $(LIMPET_DIR); $(MAKE) clean)
	@(cd $(TOOLS_DIR);  $(MAKE) clean)
	@(cd param;         $(MAKE) clean)
	rm -rf bin

nightly-build: update clean param all exe_dir

tags:
	@if [ -e TAGS ]; then rm TAGS; fi
	@if [ -e tags ]; then ctags -R --fields=+l; fi
	find . -name '*.[ch]' -o -name '*.cc'  -o -name '*.[ch]pp' | xargs $(TAGEXE)

exe_dir:
	@(echo "Generating soft-links in ./bin")
	@(mkdir -p bin)
	@(rm -f bin/*)
	@(ln -s ../$(SIM_DIR)/openCARP$(FLV) bin/.)
	@(ln -s ../$(LIMPET_DIR)/bench$(FLV) bin/.)
	@(ln -s ../$(LIMPET_DIR)/src/make_dynamic_model.sh bin/.)
	@(ln -s ../$(LIMPET_DIR)/src/python/make_dynamic_model.py bin/.)
	@(ln -s ../$(LIMPET_DIR)/src/python/limpet_fe.py bin/.)
	@(ln -s ../$(TOOLS_DIR)/mesher/mesher bin/.)
	@(ln -s ../$(TOOLS_DIR)/igbutils/igbhead bin/.)
	@(ln -s ../$(TOOLS_DIR)/igbutils/igbextract bin/.)
	@(ln -s ../$(TOOLS_DIR)/igbutils/igbops bin/.)
	@(ln -s ../$(TOOLS_DIR)/igbutils/igbapd bin/.)
